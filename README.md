# Obligatorio PAP

Tomcat Para windows: [link](http://espejito.fder.edu.uy/apache/tomcat/tomcat-8/v8.5.45/bin/apache-tomcat-8.5.45-windows-x64.zip)

Tomcat Para linux: [link](http://espejito.fder.edu.uy/apache/tomcat/tomcat-8/v8.5.45/bin/apache-tomcat-8.5.45.tar.gz)

Configurar tomat:

![](https://i.imgur.com/C2hz8hi.png)
![](https://i.imgur.com/IjIzSlT.png)
![](https://i.imgur.com/xIBuREl.png)
![](https://i.imgur.com/FExeBH0.png)

La base de datos ahora se debe levantar en modo servidor para que mas de una apliacion la pueda usar a la vez:

```java -cp h2-*.jar org.h2.tools.Server -tcp -tcpAllowOthers -ifNotExists```