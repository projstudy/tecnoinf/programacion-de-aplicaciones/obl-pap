import DTO.DtoListas;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ILista;
import org.uytube.controladores.IUsuario;
import org.uytube.controladores.IVideo;
import org.uytube.datatypes.DtCanal;
import org.uytube.datatypes.DtLista;
import org.uytube.datatypes.DtVideo;

@WebServlet(name = "Buscar", value = "/Buscar")
public class Buscar extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // Mostramos la inicial
    IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
    ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);
    IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
    try {
      String texto = request.getParameter("nombre");
      boolean sortAlfa = Boolean.parseBoolean(request.getParameter("sortAlfa"));
      boolean sortFecha = Boolean.parseBoolean(request.getParameter("sortFecha"));
      int pagina = Integer.parseInt(request.getParameter("pagina"));
      int offset = Integer.parseInt(request.getParameter("offset"));
      if (pagina < 0 || offset < 0) {
        throw new NumberFormatException();
      }
      List<DtVideo> videos = iVideo.buscarVideos(texto, sortFecha, sortAlfa, pagina, offset);
      List<DtLista> listas = iLista.buscarListas(texto, sortFecha, sortAlfa, pagina, offset);
      List<DtCanal> canales = iUsuario.buscarCanales(texto, sortFecha, sortAlfa, pagina, offset);
      DtoListas l = new DtoListas(listas, videos, canales);
      Gson gson = new Gson();
      String data = gson.toJson(l);
      response.setContentType("application/json;charset=UTF-8");
      response.getOutputStream().print(data);
    } catch (NumberFormatException e) {
      response.setStatus(400);
      response.getOutputStream().print("pagina o offset incorrecto");
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {}
}
