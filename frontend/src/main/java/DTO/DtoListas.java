package DTO;

import java.util.List;
import org.uytube.datatypes.DtCanal;
import org.uytube.datatypes.DtLista;
import org.uytube.datatypes.DtVideo;

public class DtoListas {
  final List<DtLista> listas;
  final List<DtVideo> videos;
  final List<DtCanal> canales;

  public DtoListas(List<DtLista> listas, List<DtVideo> videos, List<DtCanal> canales) {
    this.listas = listas;
    this.videos = videos;
    this.canales = canales;
  }

  public List<DtLista> getListas() {
    return listas;
  }

  public List<DtVideo> getVideos() {
    return videos;
  }
}
