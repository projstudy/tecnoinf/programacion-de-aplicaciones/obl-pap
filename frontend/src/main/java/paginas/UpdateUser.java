package paginas;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.uytube.datatypes.DtUsuarioExt;

@WebServlet(name = "UpdateUser", value = "/UpdateUser")
public class UpdateUser extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String id = request.getParameter("id");
    RequestDispatcher rd;
    if (id == null) {
      rd = request.getRequestDispatcher("/Dash");
      rd.forward(request, response);
    } else {
      Object session = request.getSession().getAttribute("DtUsuario");
      if (session == null) {
        rd = request.getRequestDispatcher("/Dash");
      } else {
        DtUsuarioExt dt = (DtUsuarioExt) session;
        if (!dt.getUsuario().getNickname().equalsIgnoreCase(id)) {
          rd = request.getRequestDispatcher("/Dash");
        } else {
          rd = request.getRequestDispatcher("/modificarUsuario.jsp?id=" + id);
        }
      }
    }
    rd.forward(request, response);
  }
}
