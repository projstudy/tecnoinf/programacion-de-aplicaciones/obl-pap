package paginas;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.IUsuario;
import org.uytube.datatypes.DtUsuarioExt;
import org.uytube.exception.YonaException;

@WebServlet(name = "Login", value = "/Login")
public class Login extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    if (request.getSession().getAttribute("DtUsuario") != null) {
      // Lo devolvemos al dash, ya esta logeado
      response.sendRedirect(request.getContextPath() + "/Dash");
    } else {
      // Mostramos la vista del login.
      request.getRequestDispatcher("/login.jsp").forward(request, response);
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String user = request.getParameter("nickname");
    String pass = request.getParameter("password");

    if (user == null || pass == null) {
      response.setStatus(401);
      response.getWriter().write("Unauthorized");
    } else {
      IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
      try {
        // Si el login es exitoso, se recibe un Dt con datos del componente periferico.
        DtUsuarioExt dtUsuario = iUsuario.login(user, pass);
        HttpSession session = request.getSession();
        session.setAttribute("DtUsuario", dtUsuario);
        response.sendRedirect(request.getContextPath() + "/Dash");
      } catch (YonaException e) {
        request.setAttribute("message", e.getMessage());
        request.getRequestDispatcher("/login.jsp").forward(request, response);
      }
    }
  }
}
