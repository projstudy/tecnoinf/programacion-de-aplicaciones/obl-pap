package paginas;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Watch", value = "/Watch")
public class Watch extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String idVideo = request.getParameter("idLista");
    String idLista = request.getParameter("idVideo");
    if (idVideo == null && idLista == null) {
      RequestDispatcher rd = request.getRequestDispatcher("/Dash");
      rd.forward(request, response);
    } else {
      RequestDispatcher rd =
          request.getRequestDispatcher("/video.jsp?idVideo=" + idVideo + "&idLista=" + idLista);
      rd.forward(request, response);
    }
  }
}
