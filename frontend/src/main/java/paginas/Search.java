package paginas;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Search", value = "/Search")
public class Search extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // Mostramos la inicial
    String id = request.getParameter("query");
    if (id == null) {
      RequestDispatcher rd = request.getRequestDispatcher("/Dash");
      rd.forward(request, response);
    } else {
      RequestDispatcher rd = request.getRequestDispatcher("/search.jsp?query=" + id);
      rd.forward(request, response);
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {}
}
