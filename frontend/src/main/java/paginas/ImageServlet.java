package paginas;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ImageServlet", value = "/ImageServlet/*")
public class ImageServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // Mostramos la inicial
    String imageName = request.getPathInfo().substring(1);
    ServletContext sc = getServletContext();
    String filename = imageName;
    // Get the MIME type of the image
    String mimeType = sc.getMimeType(filename);
    if (mimeType == null) {
      sc.log("Could not get MIME type of " + filename);
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
      return;
    }

    // Set content type
    response.setContentType(mimeType);

    // Set content size
    String home = System.getProperty("user.home");
    File file = new File(home + "/uytube/imagenes/" + filename);
    response.setContentLength((int) file.length());

    // Open the file and output streams
    FileInputStream in = new FileInputStream(file);
    OutputStream out = response.getOutputStream();

    // Copy the contents of the file to the output stream
    byte[] buf = new byte[1024];
    int count = 0;
    while ((count = in.read(buf)) >= 0) {
      out.write(buf, 0, count);
    }
    in.close();
    out.close();
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {}
}
