import com.google.gson.Gson;
import java.io.IOException;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ICategoria;
import org.uytube.datatypes.*;
import org.uytube.exception.YonaException;

@WebServlet(name = "Categoria", value = "/Categoria")
public class Categoria extends HttpServlet {

  private static final long serialVersionUID = 2L;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    ICategoria iCategoria = Fabrica.getControllerOf(Fabrica.Controller.CATEOGRIA);
    response.setContentType("application/json;charset=UTF-8");
    if (request.getParameter("nombre") != null) {
      try {
        String nombre = request.getParameter("nombre");
        DtListasVideos resultado = iCategoria.consultaCategoriaPublica(nombre);
        Gson gson = new Gson();
        String data = gson.toJson(resultado);
        response.setStatus(200);
        response.getOutputStream().print(data);
      } catch (YonaException e) {
        response.setStatus(400);
        response.getOutputStream().print(e.getMessage());
      }
    } else {
      List<String> resultado = iCategoria.listarCategorias();
      Gson gson = new Gson();
      String data = gson.toJson(resultado);
      response.setStatus(200);
      response.getOutputStream().print(data);
    }
  }

  @Override
  protected void doPut(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {}

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {}
}
