import com.google.gson.Gson;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ILista;
import org.uytube.controladores.IUsuario;
import org.uytube.controladores.IVideo;
import org.uytube.datatypes.DtCanal;
import org.uytube.datatypes.DtLista;
import org.uytube.datatypes.DtModUsuario;
import org.uytube.datatypes.DtUsuario;
import org.uytube.datatypes.DtUsuarioExt;
import org.uytube.datatypes.DtVideo;
import org.uytube.exception.YonaException;

@WebServlet(name = "Canal", value = "/Canales/*")
public class Canal extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  protected void doPut(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String path;
    try {
      path = request.getPathInfo().substring(1);
    } catch (NullPointerException e) {
      path = "";
    }
    switch (path) {
      case "Password":
        {
          changePassword(request, response);
        }
        break;
      default:
        {
          response.setStatus(404);
        }
    }
  }

  private void updateUser(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    String nombre = request.getParameter("nombre");
    String apellido = request.getParameter("apellido");
    String foto = request.getParameter("foto"); // como base64
    String fecha = request.getParameter("fechaNacimiento");
    boolean privado = Boolean.parseBoolean(request.getParameter("privado"));
    DtUsuarioExt dtLogin = (DtUsuarioExt) request.getSession().getAttribute("DtUsuario");
    if (nombre == null || apellido == null || foto == null || fecha == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros");
    } else {
      if (!foto.isEmpty()) {
        // foto = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPAAAADwCAYAAAA+VemSAAAgAEl...=='
        // hay que quitar la primera parte, hasta la "coma"
        String base64Image = foto.split(",")[1];
        byte[] decodedImg =
            Base64.getDecoder().decode(base64Image.getBytes(StandardCharsets.UTF_8));
        foto = UUID.randomUUID().toString() + ".png";
        Path directory = Paths.get(System.getProperty("user.home") + "/uytube/imagenes/");
        if (Files.notExists(directory)) {
          Files.createDirectories(directory);
        }
        Path file = Paths.get(System.getProperty("user.home") + "/uytube/imagenes/" + foto);
        Files.write(file, decodedImg);
      } else {
        foto = dtLogin.getUsuario().getFoto();
      }
      try {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        Date fechaNacimiento = df.parse(fecha);
        DtModUsuario dtMod = new DtModUsuario(nombre, apellido, foto, fechaNacimiento, privado);
        IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
        iUsuario.modificarUsuario(dtLogin.getUsuario().getNickname(), dtMod);
      } catch (ParseException e) {
        response.setStatus(400);
        response.getOutputStream().print("Fecha invalida");
      } catch (YonaException e) {
        response.setStatus(400);
        response.getOutputStream().print(e.getMessage());
      }
      response.setStatus(200);
    }
  }

  private void changePassword(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Object session = request.getSession().getAttribute("DtUsuario");
    if (session == null) {
      response.setStatus(401);
      response.getOutputStream().print("Sesion no iniciada");
    } else {
      String password = request.getParameter("password");
      if (password == null) {
        response.setStatus(400);
        response.getOutputStream().print("Faltan parametros");
      } else {
        try {
          DtUsuarioExt dt = (DtUsuarioExt) session;
          String nick = dt.getUsuario().getNickname();
          IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
          iUsuario.actualizarPasswd(nick, password);
          response.getOutputStream().print("ok");
        } catch (YonaException e) {
          response.setStatus(400);
          response.getOutputStream().print(e.getMessage());
        }
      }
    }
  }

  @Override
  protected void doDelete(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path;
    try {
      path = request.getPathInfo().substring(1);
    } catch (NullPointerException e) {
      path = "";
    }
    switch (path) {
      case "Seguir":
        {
          noSeguirUsuario(request, response);
        }
        break;
      default:
        {
          response.setStatus(404);
        }
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path;
    try {
      path = request.getPathInfo().substring(1);
    } catch (NullPointerException e) {
      path = "";
    }
    switch (path) {
      case "Update":
        {
          updateUser(request, response);
        }
        break;
      case "Seguir":
        {
          seguirUsuario(request, response);
        }
        break;
      case "":
        {
          altaUsuario(request, response);
        }
        break;
      default:
        {
          response.setStatus(404);
        }
    }
  }

  private void seguirUsuario(HttpServletRequest request, HttpServletResponse response)
      throws IOException {

    Object session = request.getSession().getAttribute("DtUsuario");
    if (session == null) {
      response.setStatus(401);
      response.getOutputStream().print("Sesion no iniciada");
    } else {

      IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
      DtUsuarioExt dt = (DtUsuarioExt) session;

      String nick = dt.getUsuario().getNickname();
      String seguido = request.getParameter("canal");
      if (seguido == null) {
        response.setStatus(400);
        response.getOutputStream().print("faltan parametros");
      } else {

        try {

          iUsuario.seguirUsuario(nick, seguido);
          response.getOutputStream().print("ok");
        } catch (YonaException e) {
          response.setStatus(400);
          response.getOutputStream().print(e.getMessage());
        }
      }
    }
  }

  private void noSeguirUsuario(HttpServletRequest request, HttpServletResponse response)
      throws IOException {

    Object session = request.getSession().getAttribute("DtUsuario");
    if (session == null) {
      response.setStatus(401);
      response.getOutputStream().print("Sesion no iniciada");
    } else {
      IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
      DtUsuarioExt dt = (DtUsuarioExt) session;
      String nick = dt.getUsuario().getNickname();
      String seguido = request.getParameter("canal");
      if (seguido == null) {
        response.setStatus(400);
        response.getOutputStream().print("faltan parametros");
      } else {
        try {
          iUsuario.dejarSeguirUsuario(nick, seguido);
          response.getOutputStream().print("ok");
        } catch (YonaException e) {
          response.setStatus(400);
          response.getOutputStream().print(e.getMessage());
        }
      }
    }
  }

  private void altaUsuario(HttpServletRequest request, HttpServletResponse response)
      throws IOException {

    String user = request.getParameter("nickname");
    String pass = request.getParameter("password");
    String mail = request.getParameter("mail");
    String nombre = request.getParameter("name");
    String apellido = request.getParameter("lastname");
    String fnac = request.getParameter("bDate");
    String foto = request.getParameter("foto");
    String canal = request.getParameter("canal[nombre]");
    boolean privado = Boolean.parseBoolean(request.getParameter("canal[privado]"));
    String desc = request.getParameter("canal[descripcion]");
    String categoria = request.getParameter("canal[categoria]");

    if (user == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros: nickname");
    } else if (pass == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros: password");
    } else if (nombre == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros: name");
    } else if (apellido == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros: lastname");
    } else if (mail == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros: mail");
    } else if (fnac == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros: bDate");
    } else if (desc == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros: canal.descripcion");
    } else if (canal == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros: canal.nombre");
    } else {
      String uniqueID = UUID.randomUUID().toString();
      if (foto != null && !foto.isEmpty()) {
        // foto = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPAAAADwCAYAAAA+VemSAAAgAEl...=='
        // hay que quitar la primera parte, hasta la "coma"
        String base64Image = foto.split(",")[1];
        byte[] decodedImg =
            Base64.getDecoder().decode(base64Image.getBytes(StandardCharsets.UTF_8));
        Path directory = Paths.get(System.getProperty("user.home") + "/uytube/imagenes/");
        if (Files.notExists(directory)) {
          Files.createDirectories(directory);
        }
        Path file =
            Paths.get(System.getProperty("user.home") + "/uytube/imagenes/" + uniqueID + ".png");
        Files.write(file, decodedImg);
      }
      SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
      df.setLenient(false);
      try {
        Date fechaNacimiento = df.parse(fnac);
        IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
        if (categoria.equals("")) {
          categoria = null;
        }
        DtUsuario dtUsuario =
            new DtUsuario(user, pass, nombre, apellido, mail, uniqueID + ".png", fechaNacimiento);
        DtCanal dtCanal = new DtCanal(canal, desc, privado, foto, categoria);
        iUsuario.altausuario(dtUsuario, dtCanal);
        response.setStatus(200);
      } catch (YonaException e) {
        response.setStatus(400);
        response.getOutputStream().print(e.getMessage());
      } catch (ParseException e) {
        response.setStatus(400);
        response.getOutputStream().print("Error al convertir la fecha.");
      }
    }
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path;
    try {
      path = request.getPathInfo().substring(1);
    } catch (NullPointerException e) {
      path = "";
    }
    switch (path) {
      case "Listas":
        {
          getListas(request, response);
        }
        break;
      case "Videos":
        {
          getVideos(request, response);
        }
        break;
      default:
        {
          consultaUsuario(request, response);
        }
    }
  }

  private void getListas(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);
    String nickname = request.getParameter("nickname");
    if (nickname == null) {
      response.setStatus(400);
      response.getOutputStream().print("faltan parametros");
    } else {
      try {
        DtUsuarioExt dtLogin = (DtUsuarioExt) request.getSession().getAttribute("DtUsuario");
        ArrayList<DtLista> dt = new ArrayList<>();
        if (dtLogin != null && dtLogin.getUsuario().getNickname().equalsIgnoreCase(nickname)) {
          dt = iLista.listasDeUsuario(nickname);
        } else {
          dt = iLista.listasParticularesUsuarioPublico(nickname);
          dt.removeIf(DtLista::isPrivado);
        }
        response.setContentType("application/json;charset=UTF-8");
        response.getOutputStream().print(new Gson().toJson(dt));
      } catch (YonaException e) {
        response.setStatus(400);
        response.getOutputStream().print(e.getMessage());
      }
    }
  }

  private void getVideos(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
    String nickname = request.getParameter("nickname");
    if (nickname == null) {
      response.setStatus(400);
      response.getOutputStream().print("faltan parametros");
    } else {
      try {
        DtUsuarioExt dtLogin = (DtUsuarioExt) request.getSession().getAttribute("DtUsuario");
        ArrayList<DtVideo> resultado;
        if (dtLogin != null && dtLogin.getUsuario().getNickname().equalsIgnoreCase(nickname)) {
          resultado = iVideo.listarVideosPublicosPrivados(nickname);
        } else {
          resultado = iVideo.listarVideosPublicosDeCanal(nickname);
        }
        response.setContentType("application/json;charset=UTF-8");
        response.getOutputStream().print(new Gson().toJson(resultado));
      } catch (YonaException e) {
        response.setStatus(400);
        response.getOutputStream().print(e.getMessage());
      }
    }
  }

  private void consultaUsuario(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
    String nickname = request.getParameter("nickname");
    if (nickname == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros.");
    } else {
      try {
        DtUsuarioExt dt = iUsuario.consultaDeUsuario(nickname);
        response.setContentType("application/json;charset=UTF-8");
        response.getOutputStream().print(new Gson().toJson(dt));
      } catch (YonaException e) {
        response.setStatus(400);
        response.getOutputStream().print(e.getMessage());
      }
    }
  }
}
