import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ILista;
import org.uytube.datatypes.DtLista;
import org.uytube.datatypes.DtUsuarioExt;
import org.uytube.exception.YonaException;

@WebServlet(name = "Lista", value = "/Listas/*")
public class Lista extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  protected void doDelete(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path;
    try {
      path = request.getPathInfo().substring(1);
    } catch (NullPointerException e) {
      path = "";
    }
    switch (path) {
      case "Videos":
        {
          quitarVideo(request, response);
        }
        break;
      default:
        {
          response.getOutputStream().print("Para hacer");
        }
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path;
    try {
      path = request.getPathInfo().substring(1);
    } catch (NullPointerException e) {
      path = "";
    }
    switch (path) {
      case "Videos":
        {
          agregarVideo(request, response);
        }
        break;
      case "":
        {
          crearLista(request, response);
        }
        break;
      default:
        {
          response.setStatus(404);
        }
    }
  }

  private void crearLista(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    Object session = request.getSession().getAttribute("DtUsuario");
    if (session == null) {
      response.setStatus(401);
      response.getOutputStream().print("Sesion no iniciada");
    } else {
      DtUsuarioExt dt = (DtUsuarioExt) session;
      String nick = dt.getUsuario().getNickname();
      ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);

      String nombre = request.getParameter("nombre");
      String categoria = request.getParameter("categoria");
      String privado = request.getParameter("privado");

      if (nombre == null || privado == null) {
        response.setStatus(400);
        response.getOutputStream().print("pagina o offset incorrecto");
      } else {
        try {

          boolean esPrivado = Boolean.parseBoolean(privado);
          DtLista dtLista = new DtLista(nombre, esPrivado, categoria);

          iLista.altaListaParticular(nick, dtLista);
          response.setStatus(200);
          response.setContentType("application/json;charset=UTF-8");
          response.getOutputStream().print("ok");
        } catch (YonaException e) {
          response.setStatus(400);
          response.getOutputStream().print(e.getMessage());
        }
      }
    }
  }

  private void quitarVideo(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);
    String idVideo = request.getParameter("idVideo");
    String idLista = request.getParameter("idLista");

    if (idLista == null || idVideo == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros");
    } else {
      try {
        int idVideoParseado = Integer.parseInt(idVideo);
        int idListaParseado = Integer.parseInt(idLista);
        iLista.quitarVideoLista(idVideoParseado, idListaParseado);
        response.setStatus(200);
        response.setContentType("application/json;charset=UTF-8");
        response.getOutputStream().print("ok");
      } catch (NumberFormatException e) {
        response.setStatus(400);
        response.getOutputStream().print("Valores incorrectos");
      } catch (YonaException e) {
        response.setStatus(400);
        response.getOutputStream().print(e.getMessage());
      }
    }
  }

  private void agregarVideo(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);
    String idVideo = request.getParameter("idVideo");
    String idLista = request.getParameter("idLista");

    if (idLista == null || idVideo == null) {
      response.setStatus(400);
      response.getOutputStream().print("Faltan parametros");
    } else {
      try {
        int idVideoParseado = Integer.parseInt(idVideo);
        int idListaParseado = Integer.parseInt(idLista);
        iLista.agregarVideoLista(idVideoParseado, idListaParseado);
        response.setStatus(200);
        response.getOutputStream().print("ok");
      } catch (NumberFormatException e) {
        response.setStatus(400);
        response.getOutputStream().print("Valores incorrectos");
      } catch (YonaException e) {
        response.setStatus(400);
        response.getOutputStream().print(e.getMessage());
      }
    }
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    ILista iListas = Fabrica.getControllerOf(Fabrica.Controller.LISTA);
    String id = request.getParameter("id");

    if (id == null) {
      response.setStatus(400);
      response.getOutputStream().print("Falta parametro id de Lista");
    } else {
      try {

        int idLista = Integer.parseInt(id);
        DtLista lista = iListas.consultaLista(idLista);
        Gson gson = new Gson();
        String data = gson.toJson(lista);
        response.setContentType("application/json;charset=UTF-8");
        response.getOutputStream().print(data);
        response.setStatus(200);
      } catch (NumberFormatException e) {
        response.setStatus(400);
        response.getOutputStream().print("formato de idLista incorrecto");
      } catch (YonaException e) {
        response.setStatus(400);
        response.getOutputStream().print(e.getMessage());
      }
    }
  }

  @Override
  protected void doPut(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Object session = request.getSession().getAttribute("DtUsuario");
    if (session == null) {
      response.setStatus(401);
      response.getOutputStream().print("Sesion no iniciada");
    } else {

      ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);

      String categoria = request.getParameter("categoria");
      String privacidad = request.getParameter("privado");
      String idLista = request.getParameter("id");

      if (idLista == null || categoria == null || privacidad == null) {
        response.setStatus(400);
        response.getOutputStream().print("Faltan parametros");
      } else {
        try {
          // modificarLista(boolean privacidad, String categoria, int idLista)
          boolean priv = Boolean.parseBoolean(privacidad);
          int idL = Integer.parseInt(idLista);
          iLista.modificarLista(priv, categoria, idL);
          response.setStatus(200);
        } catch (YonaException e) {
          response.setStatus(400);
          response.getOutputStream().print(e.getMessage());
        }
      }
    }
  }
}
