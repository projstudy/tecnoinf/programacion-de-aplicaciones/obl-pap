import com.google.gson.Gson;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.IVideo;
import org.uytube.datatypes.DtComentario;
import org.uytube.datatypes.DtUsuarioExt;
import org.uytube.datatypes.DtValoracion;
import org.uytube.datatypes.DtVideo;
import org.uytube.datatypes.DtVideoExt;
import org.uytube.exception.YonaException;

@WebServlet(name = "Video", value = "/Videos/*")
public class Video extends HttpServlet {

  private static final long serialVersionUID = 1L;

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path;
    try {
      path = request.getPathInfo().substring(1);
    } catch (NullPointerException e) {
      path = "";
    }
    switch (path) {
      case "Responder":
        {
          responderComentario(request, response);
        }
        break;
      case "Comentar":
        {
          comentarVideo(request, response);
        }
        break;
      case "Valorar":
        {
          valorar(request, response);
        }
        break;
      default:
        {
          crearVideo(request, response);
        }
    }
  }

  private void valorar(HttpServletRequest request, HttpServletResponse response)
      throws IOException {

    Object session = request.getSession().getAttribute("DtUsuario");
    if (session == null) {
      response.setStatus(401);
      response.getOutputStream().print("Sesion no iniciada");
    } else {

      DtUsuarioExt dt = (DtUsuarioExt) session;
      String nick = dt.getUsuario().getNickname();

      IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
      String id = request.getParameter("id");
      String gusta = request.getParameter("leGusta");

      if (id == null || gusta == null) {
        response.setStatus(400);
        response.getOutputStream().print("Faltan parametros");
      } else {
        try {
          int idParsed = Integer.parseInt(id);
          boolean leGusta = Boolean.parseBoolean(gusta);

          DtValoracion v = new DtValoracion(idParsed, leGusta, nick);

          iVideo.valorarVideo(idParsed, v);
          response.setStatus(200);
          response.getOutputStream().print("");
        } catch (NumberFormatException e) {
          response.setStatus(400);
          response.getOutputStream().print("formato de id incorrecto");
        } catch (YonaException e) {
          response.setStatus(400);
          response.getOutputStream().print(e.getMessage());
        }
      }
    }
  }

  private void comentarVideo(HttpServletRequest request, HttpServletResponse response)
      throws IOException {

    Object session = request.getSession().getAttribute("DtUsuario");
    if (session == null) {
      response.setStatus(401);
      response.getOutputStream().print("Sesion no iniciada");
    } else {

      DtUsuarioExt dt = (DtUsuarioExt) session;
      String nick = dt.getUsuario().getNickname();

      IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
      String texto = request.getParameter("texto");
      String id = request.getParameter("id");

      if (texto == null || id == null) {
        response.setStatus(400);
        response.getOutputStream().print("Faltan parametros");
      } else {
        try {
          int idParsed = Integer.parseInt(id);
          Date fecha = new Date();

          DtComentario dtComentario = new DtComentario(texto, nick, new ArrayList<>(), fecha);
          // int idResultado = iVideo.responderComentario(idParsed, dtComentario);
          int idResultado = iVideo.comentarVideo(idParsed, dtComentario);
          response.setStatus(200);
          response.setContentType("application/json;charset=UTF-8");
          response.getOutputStream().print(idResultado);

        } catch (NumberFormatException e) {
          response.setStatus(400);
          response.getOutputStream().print("formato de id incorrecto");
        } catch (YonaException e) {
          response.setStatus(400);
          response.getOutputStream().print(e.getMessage());
        }
      }
    }

    // response.getOutputStream().print("Para hacer");
  }

  private void crearVideo(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    Object session = request.getSession().getAttribute("DtUsuario");
    if (session == null) {
      response.setStatus(401);
      response.getOutputStream().print("Sesion no iniciada");
    } else {
      DtUsuarioExt dt = (DtUsuarioExt) session;
      String nick = dt.getUsuario().getNickname();
      // Mostramos la inicial
      IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
      // IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
      String nombre = request.getParameter("nombre");
      String url = request.getParameter("url");
      String descripcion = request.getParameter("descripcion");
      String fecha = request.getParameter("fechaPublicacion");
      String duracion = request.getParameter("duracion");

      boolean privado = Boolean.parseBoolean(request.getParameter("privado"));
      String categoria = request.getParameter("categoria");

      if (nombre == null
          || url == null
          || descripcion == null
          || fecha == null
          || duracion == null) {
        response.setStatus(400);
        response.getOutputStream().print("pagina o offset incorrecto");
      } else {
        try {
          SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
          df.setLenient(false);

          Date fechaPublicacion = df.parse(fecha);
          int duracionPar = Integer.parseInt(duracion);

          DtVideo dtVideo =
              new DtVideo(
                  nombre, url, descripcion, fechaPublicacion, categoria, duracionPar, privado);

          iVideo.crearVideo(dtVideo, nick);

          response.setContentType("application/json;charset=UTF-8");
          response.getOutputStream().print("ok");

        } catch (NumberFormatException e) {
          response.setStatus(400);
          response.getOutputStream().print("formato de duracion incorrecto");

        } catch (ParseException e) {
          response.setStatus(400);
          response.getOutputStream().print("la fecha es incorrecta");
        } catch (YonaException e) {
          response.setStatus(400);
          response.getOutputStream().print(e.getMessage());
        }
      }
    }
  }

  private void responderComentario(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    Object session = request.getSession().getAttribute("DtUsuario");
    if (session == null) {
      response.setStatus(401);
      response.getOutputStream().print("Sesion no iniciada");
    } else {

      DtUsuarioExt dt = (DtUsuarioExt) session;
      String nick = dt.getUsuario().getNickname();

      IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
      String texto = request.getParameter("texto");
      String id = request.getParameter("id");

      if (texto == null || id == null) {
        response.setStatus(400);
        response.getOutputStream().print("Faltan parametros");
      } else {
        try {
          int idParsed = Integer.parseInt(id);
          DtComentario dtComentario = new DtComentario(texto, nick, new ArrayList<>(), new Date());
          int idResultado = iVideo.responderComentario(idParsed, dtComentario);
          response.setContentType("application/json;charset=UTF-8");
          response.getOutputStream().print(idResultado);
        } catch (NumberFormatException e) {
          response.setStatus(400);
          response.getOutputStream().print("formato de id incorrecto");
        } catch (YonaException e) {
          response.setStatus(400);
          response.getOutputStream().print(e.getMessage());
        }
      }
    }
  }

  @Override
  protected void doPut(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Object session = request.getSession().getAttribute("DtUsuario");
    if (session == null) {
      response.setStatus(401);
      response.getOutputStream().print("Sesion no iniciada");
    } else {
      DtUsuarioExt dt = (DtUsuarioExt) session;
      String nick = dt.getUsuario().getNickname();

      IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);

      String nombre = request.getParameter("nombre");
      String url = request.getParameter("url");
      String descripcion = request.getParameter("descripcion");
      String fecha = request.getParameter("fechaPublicacion");
      String duracion = request.getParameter("duracion");
      String privado = request.getParameter("privado");
      String idVideo = request.getParameter("id");
      String categoria = request.getParameter("categoria");

      if (nombre == null
          || idVideo == null
          || url == null
          || descripcion == null
          || fecha == null
          || duracion == null
          || privado == null) {
        response.setStatus(400);
        response.getOutputStream().print("Faltan parametros");
      } else {
        try {
          SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
          df.setLenient(false);
          Date fechaPublicacion = df.parse(fecha);
          boolean privadoPar = Boolean.parseBoolean(privado);
          int duracionPar = Integer.parseInt(duracion);
          int idPar = Integer.parseInt(idVideo);

          DtVideo dtVideo = new DtVideo(nombre, url, descripcion, fechaPublicacion, categoria, duracionPar, privadoPar);
          iVideo.modificarVideo(idPar, dtVideo);

          response.setStatus(200);

        } catch (NumberFormatException e) {
          response.setStatus(400);
          response.getOutputStream().print("formato de duracion incorrecto");
        } catch (ParseException e) {
          response.setStatus(400);
          response.getOutputStream().print("la fecha es incorrecta");
        } catch (YonaException e) {
          response.setStatus(400);
          response.getOutputStream().print(e.getMessage());
        }
      }
    }
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
    String id = request.getParameter("id");

    if (id == null) {
      response.setStatus(400);
      response.getOutputStream().print("Falta parametro id de Video");
    } else {
      try {

        int idVideo = Integer.parseInt(id);
        DtVideoExt VideoMostrar = iVideo.verDatosVideo(idVideo);
        Gson gson = new Gson();
        String data = gson.toJson(VideoMostrar);
        response.setContentType("application/json;charset=UTF-8");
        response.getOutputStream().print(data);
        response.setStatus(200);
      } catch (NumberFormatException e) {
        response.setStatus(400);
        response.getOutputStream().print("formato de idVideo incorrecto");
      } catch (YonaException e) {
        response.setStatus(400);
        response.getOutputStream().print(e.getMessage());
      }
    }
  }
}
