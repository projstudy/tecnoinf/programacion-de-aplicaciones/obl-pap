<%@ taglib tagdir="/WEB-INF/tags" prefix="tf" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tf:toolbar>
  <div class="container-fluid pl-5">
    <div class="row pt-4 justify-content-center">
      <h2 style="font-family: Nunito,sans-serif; color: #23272b;">
        <i class="fas fa-edit"></i>
        MODIFICAR USUARIO</h2>
    </div>
    <div class="row pt-4" style="padding-left: 5.5em;">
      <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active tab-title " id="nav-canal-tab" data-toggle="tab" href="#nav-canal"
             role="tab"
             aria-controls="nav-home" aria-selected="true" style="color: #b5a3a3a6;"><i
              class="fas fa-user-circle pr-1"></i> CANAL</a>
          <a class="nav-item nav-link tab-title " id="nav-videos-tab" data-toggle="tab" href="#nav-videos"
             role="tab"
             aria-controls="nav-home" aria-selected="true" style="color: #b5a3a3a6;"><i
              class="fas fa-play-circle pr-2"></i>VIDEOS</a>
          <a class="nav-item nav-link tab-title" id="nav-listas-tab" data-toggle="tab" href="#nav-listas" role="tab"
             aria-controls="nav-profile" aria-selected="false" style="color: #b5a3a3a6;"><i
              class="fas fa-bars pr-2"></i>LISTAS</a>
        </div>
      </nav>
    </div>
    <div class="row" style="padding-left: 5.5em; padding-right: 5.5em;">
      <div class="tab-content" id="nav-tabContent" style="width: 100%;">
        <!-- Canal tab -->
        <div class="tab-pane fade show active" id="nav-canal" role="tabpanel" aria-labelledby="nav-canal-tab">
          <div class="card py-5 px-5">
            <div class="pl-5">
              <div class="image-upload" style="display: inline-block;">
                <label for="file-input">
                  <img id="file-img" :src="usuario.foto" alt="image not found"
                       style="width: 180px; height: 180px; border-radius: 100%; cursor: pointer;"/>
                </label>
                <input id="file-input" type="file"/>
              </div>
              <div class="pt-4" style="display: inline-block; position: absolute;">
                <span class="pl-3"
                      style="font-family: Nunito,sans-serif; font-size: 36px;">{{ usuario.nickname }}</span>
                <p class="pl-3" style="font-family: Nunito,sans-serif; font-size: 16px; color: #646464;">{{
                  usuario.email }}</p>
                <a data-toggle="modal" data-target="#changePassword" class="pl-3"
                   style="color: #007bff; cursor: pointer;">Cambiar Contrasena</a>
              </div>
            </div>

            <div class="row pt-5">
              <div class="col text-center">
                <h2 style="font-family: Nunito, sans-serif;">Datos de Usuario</h2>
                <div
                    class="input-group"
                    style="padding-right: 20%; padding-left: 20%; padding-top: 45px;"
                >
                  <input
                      type="text"
                      class="form-control"
                      placeholder="Nombre"
                      aria-label="Nombre"
                      name="nombre"
                      v-model="usuario.nombre"
                      v-bind:class="{'form-control':true, 'is-invalid' : !usuario.nombre && nombreUsuarioSelected}"
                      v-on:blur="nombreUsuarioSelected = true">
                </div>
                <div
                    class="input-group pt-4"
                    style="padding-right: 20%; padding-left: 20%;"
                >
                  <input
                      type="text"
                      class="form-control"
                      placeholder="Apellido"
                      aria-label="Apellido"
                      name="apellido"
                      v-model="usuario.apellido"
                      v-bind:class="{'form-control':true, 'is-invalid' : !usuario.apellido && apellidoUsuarioSelected}"
                      v-on:blur="apellidoUsuarioSelected = true">
                </div>
                <div
                    class="input-group pt-4"
                    style="padding-right: 20%; padding-left: 20%;"
                >
                  <input type="date" class="form-control" min="1900-01-01" max="2020-01-01"
                         v-model="usuario.fechaNacimiento"
                         v-bind:class="{'form-control':true, 'is-invalid' : !validDate(usuario.fechaNacimiento) && fechaNacimientoSelected}"
                         v-on:blur="fechaNacimientoSelected = true">
                </div>
              </div>
              <div class="col text-center">
                <h2 style="font-family: Nunito,sans-serif;">Datos de Canal</h2>
                <div class="input-group pt-4" style="padding-right: 20%; padding-left: 25%;">
                  <input
                      readonly
                      type="text"
                      class="form-control"
                      placeholder="Nombre"
                      aria-label="Nombre"
                      name="nombre"
                      v-model="canal.nombre">
                </div>
                <div class="input-group pt-4" style="padding-left: 25%; padding-right: 20%;">
                  <select class="form-control" disabled>
                    <option disabled value="">Elegir categoria</option>
                    <option v-for="categoria in categorias">{{ categoria }}</option>
                  </select>
                  <div class="pt-2 custom-control custom-switch" style="padding-left: 4.25rem !important">
                    <input type="checkbox" class="custom-control-input" id="customSwitch1" v-model="canal.privado">
                    <label class="custom-control-label" for="customSwitch1">Privado</label>
                  </div>
                </div>
                <div class="input-group pt-4" style="padding-left: 25%; padding-right: 20%;">
                  <textarea readonly class="form-control" id="descripcion" placeholder="Descripcion" rows="2"
                            v-model="canal.descripcion"></textarea>
                </div>
              </div>
              <div class="col-12 text-center pt-3">
                <div class="pt-5 pl-5" style="margin: auto;">
                  <button @click="updateUser" style="width: 250px;" class="btn btn-success">Guardar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Videos tab -->
        <div class="tab-pane fade" id="nav-videos" role="tabpanel" aria-labelledby="nav-videos-tab">
          <div class="card py-5 px-5">
            <div class="table-responsive">
              <table class="table table-hover table-bordered">
                <thead>
                <th>Nombre</th>
                <th>Privacidad</th>
                <th>FechaPublicacion</th>
                <th>Duracion</th>
                </thead>
                <tbody>
                <tr v-for="(video,index) in videos" data-toggle="modal" data-target="#updateVideo"
                    @click="openUpdateVideo(index)">
                  <td>{{ video.nombre }}</td>
                  <td><i :class="{ 'fas fa-lock-open' : !video.privado, 'fas fa-lock' : video.privado }"
                         class="pl-4"></i></td>
                  <td>{{ video.fechaPublicacion }}</td>
                  <td>{{ video.duracion }}</td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- Listas tab -->
        <div class="tab-pane fade" id="nav-listas" role="tabpanel" aria-labelledby="nav-listas-tab">
          <div class="card py-5 px-5">
            <div class="table-responsive">
              <table class="table table-hover table-bordered">
                <thead>
                <th>Nombre</th>
                <th>Tipo</th>
                <th>Privacidad</th>
                <th>Categoria</th>
                </thead>
                <tbody>
                <tr v-for="(lista, index) in listas" data-toggle="modal" data-target="#updateLista"
                    @click="openUpdateLista(index)">
                  <td>{{ lista.nombre }}</td>
                  <td>{{ lista.tipo }}</td>
                  <td><i :class="{ 'fas fa-lock-open' : !lista.privado, 'fas fa-lock' : lista.privado }"
                         class="pl-4"></i></td>
                  <td>{{ lista.categoria }}</td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- This is the password modal -->
    <div class="modal fade" id="changePassword" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">CAMBIAR CONTRASENA</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="px-5 py-3">
              <input
                  type="password"
                  class="form-control"
                  placeholder="Nueva clave"
                  aria-label="Nueva clave"
                  name="newPassword"
                  v-bind:class="{'form-control':true, 'is-invalid' : !checkEmpty(newPassword) && newPasswordSelected}"
                  v-model="newPassword" v-on:blur="newPasswordSelected = true"
              />
              <div class="invalid-feedback" style="text-align: start;">Contrasena invalida!</div>
            </div>
            <div class="px-5 py-3">
              <input
                  type="password"
                  class="form-control"
                  placeholder="Confirmar clave"
                  aria-label="Confirmar clave"
                  name="newPassword2"
                  v-bind:class="{'form-control':true, 'is-invalid' : (!checkEmpty(newPassword2)  || newPassword != newPassword2) && passwordSelected}"
                  v-model="newPassword2" v-on:blur="passwordSelected=true">
              <div class="invalid-feedback" style="text-align: start;">Contrasenas no coinciden!</div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancelar</button>
            <button @click="changePassword" type="button" class="btn btn-outline-success">Confirmar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- This is the update video modal -->
    <div class="modal fade" id="updateVideo" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitleVideo" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitleVideo">MODIFICAR VIDEO</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="px-5 pt-3">
              <input
                  type="text"
                  class="form-control"
                  placeholder="Nombre"
                  aria-label="Nombre"
                  name="nombre"
                  v-model="video.nombre"
                  v-bind:class="{'form-control':true, 'is-invalid' : !video.nombre && nombreSelected}"
                  v-on:blur="nombreSelected = true">
            </div>
            <div class="px-5 pt-3">
              <input
                  type="text"
                  class="form-control"
                  placeholder="URL"
                  aria-label="URL"
                  name="url"
                  v-model="video.url"
                  v-bind:class="{'form-control':true, 'is-invalid' : !video.url && urlSelected}"
                  v-on:blur="urlSelected = true">
            </div>
            <div class="input-group pt-3 px-5">
              <input type="text" class="form-control"
                     placeholder="00:00:00"
                     v-model="video.duracion"
                     v-bind:class="{'form-control':true, 'is-invalid' : !checkDuration(video.duracion) && duracionSelected}"
                     v-on:blur="duracionSelected = true">
              <input type="date" class="form-control" min="1900-01-01" max="2020-01-01"
                     v-model="video.fechaPublicacion"
                     v-bind:class="{'form-control':true, 'is-invalid' : !validDate(video.fechaPublicacion) && fechaPublicacionSelected}"
                     v-on:blur="fechaPublicacionSelected = true">
            </div>
            <div class="input-group pt-3 px-5">
              <select class="form-control" v-model="video.categoria">
                <option disabled value="">Elegir categoria</option>
                <option v-for="categoria in categorias">{{ categoria }}</option>
              </select>
              <div class="pt-2 custom-control custom-switch" style="padding-left: 4.25rem !important">
                <input type="checkbox" class="custom-control-input" id="customSwitch17" v-model="video.privado">
                <label class="custom-control-label" for="customSwitch17">Privado</label>
              </div>
            </div>
            <div class="input-group px-5 pt-3">
              <textarea class="form-control" placeholder="Descripcion" rows="2"
                        v-model="video.descripcion"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancelar</button>
            <button @click="updateVideo" type="button" class="btn btn-outline-success">Confirmar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- This is the update lista modal -->
    <div class="modal fade" id="updateLista" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitleLista" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitleLista">MODIFICAR LISTA</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="input-group pt-4 px-5">
              <select class="form-control" v-model="lista.categoria">
                <option disabled value="">Elegir categoria</option>
                <option v-for="categoria in categorias">{{ categoria }}</option>
              </select>
              <div class="pt-2 custom-control custom-switch" style="padding-left: 4.25rem !important">
                <input type="checkbox" class="custom-control-input" id="customSwitch7" v-model="lista.privado">
                <label class="custom-control-label" for="customSwitch7">Privado</label>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancelar</button>
            <button @click="updateLista" type="button" class="btn btn-outline-success">Confirmar</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</tf:toolbar>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script>

  Vue.use(Toasted,
    {
      action: {
        text: 'X',
        onClick: (e, toastObject) => {
          toastObject.goAway(0);
        }
      },
      position: 'top-right',
      duration: 4000
    }
  );

  //extiende el componente de la toolbar
  const Model = BaseModel.extend({
    data() {
      return {
        passwordSelected: false,
        videos: [],
        listas: [],
        canal: {},
        usuario: {
          nombre: "", fechaNacimiento: "", apellido: ""
        },
        video: {
          url: "", nombre: "", descripcion: "", duracion: "", fechaPublicacion: "", privado: "", cateogoria: ""
        },
        lista: {},
        nombreUsuarioSelected: false,
        apellidoUsuarioSelected: false,
        fechaNacimientoSelected: false,
        //
        urlSelected: false,
        nombreSelected: false,
        descripcionSelected: false,
        duracionSelected: false,
        fechaPublicacionSelected: false,
      }
    },
    methods: {
      async updateLista() {
        try {
          let response = axios.put('Listas?' + Qs.stringify(this.lista));
          console.log(response.data);
          this.updateListaTable(this.lista.id);
          $('#updateLista').modal('toggle');
          this.$toasted.success('Se modifico la lista');
        } catch (ex) {
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      },
      async updateVideo() {
        try {
          let data;
          if (this.video.categoria) {
            data = Object.assign({}, this.video);
          } else {
            data = Object.assign({}, {
              url: this.video.url,
              nombre: this.video.nombre,
              descripcion: this.video.descripcion,
              duracion: this.video.duracion,
              fechaPublicacion: this.video.fechaPublicacion,
              privado: this.video.privado
            });
          }
          data.duracion = this.toSeg(data.duracion);
          let x = data.fechaPublicacion.split("-");
          data.fechaPublicacion = x[2] + "/" + x[1] + "/" + x[0];
          let response = axios.put('Videos?' + Qs.stringify(data));
          this.updateVideoTable(this.video.id);
          $('#updateVideo').modal('toggle');
          this.$toasted.success('Se modifico el video');
        } catch (ex) {
          if (ex.response.status === 400) {
            this.$toasted.error(error.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      },
      openUpdateVideo(index) {
        this.video = Object.assign({}, this.videos[index]);
      },
      updateListaTable(id) {
        let lista = this.listas.find(l => {
          return l.id == id;
        });
        lista = Object.assign(lista, this.lista);
      },
      updateVideoTable(id) {
        let video = this.videos.find(v => {
          return v.id == id;
        });
        video = Object.assign(video, this.video);
      },
      openUpdateLista(index) {
        this.lista = Object.assign({}, this.listas[index]);
      },
      async updateUser() {
        if (this.usuario.nombre && this.usuario.apellido && this.validDate(this.usuario.fechaNacimiento)) {
          try {
            let data = Object.assign({}, this.usuario);
            if (data.foto64) {
              data.foto = data.foto64;
            } else {
              data.foto = "";
            }
            data.privado = this.canal.privado;
            const fechaParseada = data.fechaNacimiento.split("-");
            data.fechaNacimiento = fechaParseada[2] + "/" + fechaParseada[1] + "/" + fechaParseada[0];
            await axios.post('Canales/Update', Qs.stringify(data));
            this.$toasted.success("Se actualizaron los datos");
          } catch (ex) {
            console.log(ex);
            if (ex.response.status === 400) {
              this.$toasted.error(ex.response.data);
            } else {
              this.$toasted.error("Upps, algo salio mal :c");
            }
          }
        }
      },
      async changePassword() {
        if (this.checkEmpty(this.newPassword) && this.checkEmpty(this.newPassword2)
          && this.newPassword === this.newPassword2) {
          try {
            await axios.put('Canales/Password?password=' + this.newPassword);
            this.newPassword2 = "";
            this.newPassword = "";
            this.newPasswordSelected = false;
            this.$toasted.success("Se cambio la contraseña");
            $('#changePassword').modal('toggle');
          } catch (ex) {
            console.log(ex);
            if (ex.response.status === 400) {
              this.$toasted.error(ex.response.data);
            } else {
              this.$toasted.error("Upps, algo salio mal :c");
            }
          }
        }
      },
    },
    async created() {
      let queryParams = getParams(window.location.href);
      await this.getCanal(queryParams.id);
      this.getCategorias();
      this.usuario.fechaNacimiento = moment(this.usuario.fechaNacimiento, 'MMMMDDY').format("YYYY-MM-DD");
    }
  });
  const x = new Model({el: '#toolbar'})

  const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  async function readURL(input) {
    if (input.files && input.files[0]) {
      x.usuario.foto64 = await toBase64(input.files[0]);
      const reader = new FileReader();

      reader.onload = function (e) {
        $('#file-img').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#file-input").change(function () {
    readURL(this);
  });

</script>

<style scoped>
  .image-upload > input {
    display: none;
  }

  a.active {
    color: #1b1e21 !important;
  }

  .tab-title {
    font-family: Nunito, sans-serif;
    font-size: 18px;
  }

  .table-hover tbody tr:hover {
    cursor: pointer;
  }

</style>
