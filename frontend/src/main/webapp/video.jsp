<%@ taglib tagdir="/WEB-INF/tags" prefix="tf" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tf:toolbar>
  <div class="container-fluid">
    <!-- This is the madafcking iframe -->
    <div class="row" style="padding: 20px 0px 0px 80px;">
      <div class="col" v-if="this.video.nombre">
        <iframe style="width: calc(100vw - 650px); height: 70vh; max-width: 1280px;" :src="embeddedUrl" frameborder="0"
                allowfullscreen></iframe>

        <h2 style="font-family: Nunito,sans-serif;">{{ video.nombre }}
          <span
              class="badge badge-pill badge-danger pr-3"
              style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
              class="fas fa-tag px-1"></i>{{ video.categoria }}</span>
        </h2>
        <span
            style="color: #606060; font-weight: 400; font-size: 16px; font-family: Nunito, sans-serif;">{{ video.fechaPublicacion }}</span>
        </span>
        <div style="float: right;">
          <i class="far fa-thumbs-up" style="color: #606060; cursor: pointer;" @click="like(true)"></i>
          <span class="pl-2 pr-4" style="font-family: Nunito,sans-serif; color: #606060; ">{{ likes }}</span>
          <i class="far fa-thumbs-down" style="transform: rotateY(-180deg); color: #606060; cursor: pointer;"
             @click="like(false)"></i>
          <span class="pl-2 pr-4"
                style="font-family: Nunito,sans-serif; color: #606060;">{{ dislikes }}</span>
          <c:if test="${sessionScope.DtUsuario != null}">
            <a data-toggle="modal" data-target="#addVideoToList" @click="loadListas" style="cursor: pointer;">
              <i class="fas fa-bars" style="color: #606060;"></i>
              <span class="pl-2" style="font-family: Nunito,sans-serif; color: #606060;">AGREGAR</span>
            </a>
          </c:if>
        </div>
        <!-- Description of the video :) -->
        <hr style="border: 0.5px solid #b5a3a3a6;">
        <c:if test="${sessionScope.DtUsuario != null}">
          <div v-if="showSuscribe">
            <div v-if="isSuscribed">
              <button class="btn btn-outline-danger" @click="unfollow(canalName)"
                      style="position: absolute; right: 10px; z-index: 10;">DEJAR DE
                SEGUIR
              </button>
            </div>
            <div v-else>
              <button @click="follow(canalName)" class="btn btn-outline-danger"
                      style="position: absolute; right: 10px; z-index: 10;">SEGUIR
              </button>
            </div>
          </div>
        </c:if>
        <div style="position: relative;">
          <a :href='"Channel?id="+canalName' style="outline: none; text-decoration: none; color: #000;">
            <img alt="Canal not found" :src="usuario.foto" style="width: 70px; height: 70px; border-radius: 100%;">
            <span style="font-family: Nunito,sans-serif; font-size: 16px; font-weight: 700;">{{ canalName }}</span>
          </a>
          <span class="badge badge-pill badge-dark"><i class="fas fa-check"></i></span>
          <p style="font-family: Nunito, sans-serif; color: #606060; position: absolute; top: 50px; left: 85px;">
            {{seguidores.length}} seguidores</p>
        </div>
        <p class="pt-2" style="padding-left: 85px; font-family: Nunito,sans-serif; color: #606060; font-size: 14px;">{{
          video.descripcion }}</p>
        <hr style="border: 0.5px solid #b5a3a3a6;">
        <!-- Comments goes fucking here -->
        <div>
          <h2 style="font-family: Nunito,sans-serif;">Comentarios</h2>
          <c:if test="${sessionScope.DtUsuario != null}">
            <div class="form-group">
              <textarea v-model="comentario" class="form-control" rows="2"
                        placeholder="Agregar un comentario..."></textarea>
              <button @click.prevent="comentar(comentario)" class="btn btn-outline-secondary mt-2 pt-2"
                      style="float: right;">COMENTAR
              </button>
            </div>
          </c:if>
          <ul class="pt-5 pl-0" style="list-style-type: none;">
            <tree-item
                class="item"
                v-for="comment in treeData"
                :key="comment.name"
                :item="comment"
                @responder="responder"
            ></tree-item>
          </ul>
        </div>
      </div>
      <!-- This is the section of videos -->
      <div class="col pl-4" style="min-width: 400px;">
        <div class="row" v-if="lista.nombre">
          <div class="card px-4 pt-2 pb-3" style="width: 400px;">
            <div style="border-bottom: 1px solid #606060;">
              <span
                  style="font-family: Nunito, sans-serif; font-weight: bold; font-size: 20px;">{{ lista.nombre }}</span>
            </div>
            <div v-if="lista.videos.length === 0">
              <h2 class="pt-2">No hay videos</h2>
            </div>
            <div class="col pt-3">
              <div class="row pt-3" v-for="(video, index) in lista.videos">
                <div style="display: inline-block; vertical-align: top;">
                  <a :href="'${pageContext.servletContext.contextPath}/Watch?idVideo='+video.id+'&idLista='+lista.id"
                     style="outline: none; text-decoration: none; color: black">
                    <img :src="video.img" alt="Video not found" style="width: 130px; height: 70px;">
                  </a>
                </div>
                <div class="pl-2" style="display: inline-block; vertical-align: top; width: 200px; position: relative;">
                  <a :href="'${pageContext.servletContext.contextPath}/Watch?idVideo='+video.id+'&idLista='+lista.id"
                     style="outline: none; text-decoration: none; color: black">
                    <p class="mb-0" style="font-family: 'Nunito', sans-serif; font-size: 14px;">{{ video.nombre }}</p>
                  </a>
                  <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+lista.canal"
                     style="outline: none; text-decoration: none; color: black">
                    <span style="font-family: 'Nunito', sans-serif; font-weight: 400; font-size: 12px; color: #606060;"> {{
                      lista.canal }} </span>
                  </a>
                  <span v-if="lista.canal == '${sessionScope.DtUsuario.usuario.nickname}'"
                        @click="removeVideoFromList(video.id, index)"
                        style="position: absolute; top: 15px; right: 0; z-index: 12; cursor: pointer;"><i
                      class="fas fa-trash"></i></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row pt-3 pr-5" v-for="elemento in elementos" v-if="elemento.fechaPublicacion">
          <a :href="'${pageContext.servletContext.contextPath}/Watch?idVideo='+elemento.id"
             style="outline: none; text-decoration: none; color: black">
            <img :src="elemento.img" alt="Video not found" style="width: 200px; height: 120px;">
          </a>
          <div class="pl-2">
            <a :href="'${pageContext.servletContext.contextPath}/Watch?idVideo='+elemento.id"
               style="outline: none; text-decoration: none; color: black">
              <span style="font-family: 'Nunito', sans-serif; font-size: 22px;">{{ elemento.nombre }}</span>
            </a>
            <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+elemento.canal"
               style="outline: none; text-decoration: none; color: black">
              <p style="font-family: 'Nunito', sans-serif; font-weight: 400; font-size: 16px; color: #606060;"> {{
                elemento.canal }} </p>
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- This is the add video to list modal -->
    <div class="modal fade" id="addVideoToList" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Agregar video a lista</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" v-for="(lista, index) in listas">
            <div class="custom-control custom-checkbox">
              <input @change="updateSelected(lista.id, index)" type="checkbox" class="custom-control-input"
                     :id="lista.nombre">
              <label class="custom-control-label" :for="lista.nombre">{{ lista.nombre }}</label>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancelar</button>
            <button @click.prevent="addVideoToList" type="button" class="btn btn-outline-success">Agregar</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</tf:toolbar>

<script type="text/x-template" id="item-template">
  <li>
    <div style="font-family: Nunito, sans-serif;">
      <img :src="item.foto" style="width: 50px; height: 50px; border-radius: 100%;">
      <span class="pl-2 bold" style="font-family: Nunito,sans-serif; font-size: 18px;"> {{ item.nickname }} </span>
      <span style="font-family: Nunito,sans-serif; font-size: 12px; color: #606060;">{{ item.fechaPublicacion }}</span>
    </div>
    <span style="padding-left: 65px;">{{ item.texto }}</span>
    <c:if test="${sessionScope.DtUsuario != null}">
      <p style="padding-left: 65px;" class="add mb-0" @click="replyTo" v-if="!reply">RESPONDER</p>
    </c:if>
    <div class="form-group pb-4" v-if="reply" style="padding-left: 60px;">
      <textarea class="form-control" rows="2" placeholder="Responder un comentario..." v-model="answer"></textarea>
      <button class="btn btn-outline-secondary mt-2 pt-2 ml-2" @click="add(item)"
              style="float: right;">
        RESPONDER
      </button>
      <button class="btn btn-outline-secondary mt-2 pt-2" @click="replyTo" style="float: right;">CANCELAR</button>
    </div>
    <p v-if="isFolder" @click="toggle" class="bold"
       style="padding-left: 65px; font-family: Nunito, sans-serif; font-size: 14px;">{{
      isOpen
      ? 'Ocultar respuestas ^' : 'Ver respuestas v' }}</p>
    <ul v-show="isOpen" v-if="isFolder" class="children">
      <tree-item
          class="item"
          v-for="(answer, index) in item.respuestas"
          :key="index"
          :item="answer"
          @responder="$emit('responder', $event)"
      ></tree-item>
    </ul>
  </li>
</script>

<script>

  Vue.use(Toasted,
    {
      action: {
        text: 'X',
        onClick: (e, toastObject) => {
          toastObject.goAway(0);
        }
      },
      position: 'top-right',
      duration: 4000
    }
  );

  Vue.component('tree-item', {
    template: '#item-template',
    props: {
      item: Object
    },
    data: function () {
      return {
        isOpen: false,
        reply: false,
        answer: '',
      }
    },
    computed: {
      isFolder: function () {
        return this.item.respuestas && this.item.respuestas.length;
      },
    },
    methods: {
      toggle: function () {
        this.isOpen = !this.isOpen
      },
      replyTo: function () {
        this.reply = !this.reply;
      },
      add: function (item) {
        if (this.answer) {
          let respuesta = this.answer;
          this.answer = "";
          let foto = "${sessionScope.DtUsuario.usuario.foto}";
          if (foto) {
            foto = "ImageServlet/" + foto;
          } else {
            foto = "assets/avatar1.jpg";
          }
          this.$emit('responder', {
            id: item.id, texto: respuesta, done: (id) => {
              this.item.respuestas.push({
                id: id,
                foto: foto,
                fechaPublicacion: new Date().toLocaleDateString('en-Us', {
                  month: 'short',
                  day: 'numeric',
                  year: 'numeric',
                  hour: 'numeric',
                  minute: 'numeric',
                  second: 'numeric'
                }),
                nickname: "${sessionScope.DtUsuario.usuario.nickname}",
                texto: respuesta,
                respuestas: []
              });
              this.reply = !this.reply;
            }
          });
        } else {
          this.$toasted.error('El mensaje de respuesta no puede ser vacio!')
        }
      }
    }
  });

  //extiende el componente de la toolbar
  const Model = BaseModel.extend({
    data() {
      return {
        embeddedUrl: "",
        video: {},
        valoraciones: [],
        listas: [],
        treeData: [],
        canalName: '',
        likes: 0,
        lista: {},
        dislikes: 0,
        answerId: 0,
        comentario: '',
        selectedList: [],
        showSuscribe: false,
        seguidores: [],
        seguidos: []
      }
    },
    methods: {
      async like(leGusta) {
        try {
          await axios.post('Videos/Valorar', Qs.stringify({
            leGusta: leGusta,
            id: this.video.id
          }));
          let valoracion = this.valoraciones.find(e => {
            return e.nickname === "${sessionScope.DtUsuario.usuario.nickname}";
          });
          if (valoracion) {
            if (valoracion.leGusta !== leGusta) {
              if (leGusta) {
                this.likes++;
                this.dislikes--;
              } else {
                this.dislikes++;
                this.likes--;
              }
              valoracion.leGusta = leGusta;
              this.$toasted.success("Video valorado!");
            }
          } else {
            this.valoraciones.push({
              leGusta: leGusta,
              nickname: "${sessionScope.DtUsuario.usuario.nickname}"
            });
            leGusta ? this.likes++ : this.dislikes++;
            this.$toasted.success("Video valorado!");
          }
        } catch (ex) {
          if (ex.response.status === 401) {
            this.$toasted.error("Debes iniciar sesision para valorar un video.");
          } else if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }

      },
      async fixImgUrl(comentario) {
        if (comentario.foto) {
          comentario.foto = "ImageServlet/" + comentario.foto;
        } else {
          comentario.foto = "assets/avatar1.jpg";
        }
        comentario.respuestas.forEach(respuesta => this.fixImgUrl(respuesta));
      },
      async getVideo(id) {
        try {
          let response = await axios.get('Videos?id=' + id);
          this.video = response.data.video;
          this.valoraciones = response.data.valoraciones;
          this.canalName = response.data.canal;
          response.data.comentarios.forEach(comentario => this.fixImgUrl(comentario));
          this.treeData = response.data.comentarios.reverse();
          this.likes = response.data.valoraciones.reduce((acc, valoracion) => {
            return valoracion.leGusta ? acc + 1 : acc
          }, 0);
          this.dislikes = response.data.valoraciones.length - this.likes;
          const n = this.video.url.lastIndexOf("=");
          this.embeddedUrl = "https://www.youtube.com/embed/" + this.video.url.substring(n + 1)
        } catch (ex) {
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      },
      async getLista(id) {
        try {
          let response = await axios.get('Listas?id=' + id);
          response.data.videos.forEach(v => {
            const n = v.url.lastIndexOf("=");
            v.img =
              "https://img.youtube.com/vi/" + v.url.substring(n + 1) + "/0.jpg";
          });
          this.lista = response.data;
          console.log(this.lista);
        } catch (ex) {
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      },
      async responder(comentario) {
        try {
          let response = await axios.post('Videos/Responder', Qs.stringify(comentario));
          comentario.done(response.data);
          this.$toasted.success('Se respondio correctamente');
        } catch (ex) {
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      },
      async comentar(texto) {
        try {
          let response = await axios.post('Videos/Comentar', Qs.stringify({id: this.video.id, texto: texto}));
          let foto = "${sessionScope.DtUsuario.usuario.foto}";
          if (foto) {
            foto = "ImageServlet/" + foto;
          } else {
            foto = "assets/avatar1.jpg";
          }
          this.treeData.unshift({
            id: response.data,
            foto: foto,
            fechaPublicacion: new Date().toLocaleDateString('en-Us', {
              month: 'short',
              day: 'numeric',
              year: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
              second: 'numeric'
            }),
            nickname: "${sessionScope.DtUsuario.usuario.nickname}",
            texto: texto,
            respuestas: []
          });
          this.$toasted.success('Se agrego el comentario correctamente');
          this.comentario = '';
        } catch (ex) {
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      },
      async loadListas() {
        const nickname = "${sessionScope.DtUsuario.usuario.nickname}";
        if (nickname) {
          this.listas = await getListas(nickname)
        }
      },
      async removeVideoFromList(id, index) {
        try {
          let response = axios.delete('Listas/Videos?idVideo=' + id + '&idLista=' + this.lista.id);
          console.log(this.lista.videos);
          console.log(index);
          this.lista.videos.splice(index, 1);
          this.$toasted.success('Videos eliminado de la lista!');
        } catch (ex) {
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      },
      async addVideoToList() {
        try {
          const peticiones = [];
          this.selectedList.forEach((lista) => {
            peticiones.push(axios.post('Listas/Videos', Qs.stringify({idVideo: this.video.id, idLista: lista})));
          });
          await Promise.all(peticiones);
          $('#addVideoToList').modal('toggle');
          this.$toasted.success('Videos agregados!');
        } catch (ex) {
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      },
      updateSelected(idLista, index) {
        if (!this.selectedList[index]) {
          this.selectedList[index] = idLista;
        } else {
          this.selectedList.splice(index, 1);
        }
      }
    },
    async created() {
      let queryParams = getParams(window.location.href);
      if (queryParams.idVideo && !queryParams.idLista) {
        await this.getVideo(queryParams.idVideo);
        await this.getCanal(this.canalName);
        console.log(this.canal);
        if (this.canal.foto) {
          this.canal.foto = "ImageServlet/" + this.canal.foto;
        } else {
          this.canal.foto = "assets/avatar1.jpg";
        }
        this.searchVideoListasCanales(0, 5, "");
        this.getCategorias();
        const canal = "${sessionScope.DtUsuario.usuario.nickname}";
        if (canal && canal != this.canal.nombre) {
          let response = await axios.get("Canales?nickname=" + canal);
          this.suscribedTo = response.data.suscripciones;
          this.isSuscribed = this.suscribedTo.some(e => e.nickname === this.canalName);
          this.showSuscribe = true;
        }
      } else {
        await this.getLista(queryParams.idLista);
        if (queryParams.idVideo) {
          await this.getVideo(queryParams.idVideo);
        } else {
          await this.getVideo(this.lista.videos[0].id);
        }
        await this.getCanal(this.canalName);
        if (this.canal.foto) {
          this.canal.foto = "ImageServlet/" + this.canal.foto;
        } else {
          this.canal.foto = "assets/avatar1.jpg";
        }
        this.getCategorias();
        const canal = "${sessionScope.DtUsuario.usuario.nickname}";
        if (canal && canal != this.canal.nombre) {
          let response = await axios.get("Canales?nickname=" + canal);
          this.suscribedTo = response.data.suscripciones;
          this.isSuscribed = this.suscribedTo.some(e => e.nickname === this.canalName);
          this.showSuscribe = true;
        }
      }
    }
  });
  new Model({el: '#toolbar'})
</script>


<style scoped>
  .toasted-container .toasted {
    top: 0px !important;
    right: -100px !important;
  }

  .toasted.toasted-primary .action {
    color: #0200019e;
  }

  #addVideoToList .modal-content {
    width: 400px !important;
  }

  #addVideoToList .modal-dialog {
    max-width: 400px !important;
  }

  .modal-title {
    font-family: Nunito, sans-serif !important;
    font-size: 22px !important;
  }

  .item {
    cursor: pointer;
  }

  .bold {
    font-weight: bold;
  }

  .children {
    padding-left: 2em;
    line-height: 1.5em;
    list-style-type: none;
  }

  .add {
    font-family: Nunito, sans-serif;
    font-size: 14px;
    color: #606060;
    cursor: pointer;
  }
</style>
