/**
 * Get the URL parameters
 * source: https://css-tricks.com/snippets/javascript/get-url-variables/
 * @param  {String} url The URL
 * @return {Object}     The URL parameters
 */
const getParams = function (url) {
  const params = {};
  const parser = document.createElement('a');
  parser.href = url;
  const query = parser.search.substring(1);
  const vars = query.split('&');
  for (let i = 0; i < vars.length; i++) {
    const pair = vars[i].split('=');
    params[pair[0]] = decodeURIComponent(pair[1]);
  }
  return params;
};

const BaseModel = Vue.extend({
  data() {
    return {
      menuToggler: false,
      profileToggler: false,
      canal: "",
      usuario: "",
      suscriptores: [],
      ordenarPor: "",
      texto: "",
      nombre: "",
      busy: false,
      pagina: 0,
      elementos: [],
      categorias: [],
      nombreLista: '',
      privacidadLista: false,
      categoriaLista: '',
      enableEdit: false,
      isSuscribed: false,
      suscribedTo: [],
      newVideo: {
        nombre: "", url: "", descripcion: "", fechaPublicacion: "", duracion: "00:00:01", categoria: "", privado: true,
        dateSelected: false,
        nameSelected: false,
        urlSelected: false,
        timeSelected: false
      },
      newPassword: '',
      newPasswordSelected: false,
      newPassword2: '',
      valid: false,
    };
  },
  methods: {
    async searchVideoListasCanales(page, offset, criteria) {
      let sortAlfa = false;
      let sortFecha = false;
      switch (this.ordenarPor) {
        case "fecha": {
          sortFecha = true;
          break;
        }
        case "alfa": {
          sortAlfa = true;
          break;
        }
      }
      let query = "Buscar?pagina=" + page + "&offset=" + offset + "&sortAlfa=" + sortAlfa + "&sortFecha=" + sortFecha + "&nombre=" + criteria;
      try {
        const response = await axios.get(query);
        response.data.canales.forEach(canal => {
          if (canal.foto) {
            canal.foto = "ImageServlet/" + canal.foto;
          } else {
            canal.foto = "assets/avatar1.jpg";
          }
        });
        response.data.videos.forEach(v => {
          const n = v.url.lastIndexOf("=");
          v.img =
            "https://img.youtube.com/vi/" + v.url.substring(n + 1) + "/0.jpg";
        });
        this.elementos.push(...response.data.canales);
        this.elementos.push(...response.data.listas);
        this.elementos.push(...response.data.videos);
        this.busy =
          response.data.videos.length < 10 &&
          response.data.listas.length < 10 &&
          response.data.canales.length < 10;
        this.pagina++;
      } catch (ex) {
        if (ex.response.status === 400) {
          this.$toasted.error(ex.response.data);
        } else {
          this.$toasted.error("Upps, algo salio mal :c");
        }
      }
    },
    async getCategorias() {
      try {
        let response = await axios.get('Categoria');
        this.categorias = response.data;
      } catch (ex) {
        if (ex.response.status === 400) {
          this.$toasted.error(ex.response.data);
        } else {
          this.$toasted.error("Upps, algo salio mal :c");
        }
      }
    },
    async crearLista() {
      if (this.nombreLista) {
        try {
          const data = {
            nombre: this.nombreLista,
            privado: this.privacidadLista
          };
          if (this.categoriaLista) {
            data.categoria = this.categoriaLista
          }
          let response = await axios.post('Listas', Qs.stringify(data));
          this.$toasted.success('Se creo la lista!');
          $('#addLista').modal('toggle');
        } catch (ex) {
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else if (ex.response.status === 401) {
            this.$toasted.error("Sesion no iniciada");
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      } else {
        this.$toasted.error("Debe ingresar un nombre a la lista");
      }
    },
    async follow(canal) {
      try {
        await axios.post("Canales/Seguir", Qs.stringify({canal: canal}));
        this.suscribedTo.push(canal);
        this.isSuscribed = true;
        this.$toasted.success('Ahora es seguidor de ' + canal);
      } catch (ex) {
        console.log(ex)
        if (ex.response.status === 400) {
          this.$toasted.error(ex.response.data);
        } else {
          this.$toasted.error("Upps, algo salio mal :c");
        }
      }
    },
    async unfollow(canal) {
      try {
        await axios.delete("Canales/Seguir?canal=" + canal);
        this.suscribedTo = this.suscribedTo.filter(e => e !== canal);
        this.isSuscribed = false;
        this.$toasted.success('Ahora ya no sigues a ' + canal);
      } catch (ex) {
        console.log(ex)
        if (ex.response.status === 400) {
          this.$toasted.error(ex.response.data);
        } else {
          this.$toasted.error("Upps, algo salio mal :c");
        }
      }
    },
    async getCanal(nickname) {
      try {
        let response = await axios.get("Canales?nickname=" + nickname);
        this.canal = response.data.canal;
        this.usuario = response.data.usuario;
        response.data.suscriptores.forEach(s => {
          if (s.foto) {
            s.foto = "ImageServlet/" + this.canal.foto;
          } else {
            s.foto = "assets/avatar1.jpg";
          }
        });
        response.data.suscripciones.forEach(s => {
          if (s.foto) {
            s.foto = "ImageServlet/" + this.canal.foto;
          } else {
            s.foto = "assets/avatar1.jpg";
          }
        });

        this.seguidores = response.data.suscriptores;
        this.seguidos = response.data.suscripciones;
        if (this.usuario.foto) {
          this.usuario.foto = "ImageServlet/" + this.usuario.foto;
        } else {
          this.usuario.foto = "assets/avatar1.jpg";
        }
        const resultado = await Promise.all([getVideos(nickname), getListas(nickname)]);
        this.listas = resultado[1];
        this.videos = resultado[0];
      } catch (ex) {
        console.log(ex)
        if (ex.response.status === 400) {
          this.$toasted.error(ex.response.data);
        } else {
          this.$toasted.error("Upps, algo salio mal :c");
        }
      }
    },
    async crearVideo() {
      this.validate();
      if (this.valid) {
        try {
          let data = Object.assign({}, this.newVideo);
          data.duracion = this.toSeg(data.duracion);
          let x = data.fechaPublicacion.split("-");
          data.fechaPublicacion = x[2] + "/" + x[1] + "/" + x[0];
          await axios.post('Videos', Qs.stringify(data));
          this.newVideo = {
            nombre: "",
            url: "",
            descripcion: "",
            fechaPublicacion: "",
            duracion: "00:00:01",
            categoria: "",
            privado: true,
            dateSelected: false,
            nameSelected: false,
            urlSelected: false,
            timeSelected: false
          }
          this.$toasted.success('Se creo el video!');
          $('#addVideo').modal('toggle');
        } catch (ex) {
          console.log(ex);
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      }
    },
    scroll() {
      this.searchVideoListasCanales(this.pagina, 10, this.nombre);
    },
    reOrder() {
      this.nombre = this.texto;
      this.pagina = 0;
      this.elementos = [];
      this.searchVideoListasCanales(this.pagina, 10, this.nombre);
    },
    doSearch() {
      let url = window.location.href.slice(0, window.location.href.lastIndexOf("/"));
      if (!window.location.href.includes("Search")) {
        window.location.replace(url + "/Search?query=" + this.texto);
        return;
      } else {
        //actualizo la url para mas placer
        history.pushState({page: 1}, "UyTube", "?query=" + this.texto);
      }
      if (this.texto) {
        this.nombre = this.texto;
        this.pagina = 0;
        this.elementos = [];
        this.searchVideoListasCanales(this.pagina, 10, this.nombre);
      }
    },
    checkDuration: function (text) {
      return !!/^\d\d:\d\d:\d\d$/.test(text);
    },
    checkEmpty: function (text) {
      return !!text;
    },
    validDate: function (text) {
      let parsedDate = text.split("-");
      return parsedDate[0] > 1900;
    },
    toSeg: (time) => {
      const hourMinSec = time.split(":");
      const hour = parseInt(hourMinSec[0]);
      const mins = parseInt(hourMinSec[1]);
      const seg = parseInt(hourMinSec[2]);
      const hoursInSecs = hour * 60 * 60;
      const minInSecs = mins * 60;
      return hoursInSecs + minInSecs + seg;
    },
    validate: function () {
      this.newVideo.timeSelected = true;
      this.newVideo.dateSelected = true;
      this.newVideo.nameSelected = true;
      this.newVideo.urlSelected = true;
      if (this.checkEmpty(this.newVideo.nombre) && this.checkEmpty(this.newVideo.url) && this.validDate(this.newVideo.fechaPublicacion) && this.checkDuration(this.newVideo.duracion)) {
        this.valid = true;
      }
    }
  }
});
