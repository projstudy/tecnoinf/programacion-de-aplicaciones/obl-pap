async function getListas(nickname) {
  try {
    let response = await axios.get("Canales/Listas?nickname=" + nickname);
    return response.data;
  } catch (ex) {
    if (ex.response.status === 400) {
      this.$toasted.error(ex.response.data);
    } else {
      this.$toasted.error("Upps, algo salio mal :c");
    }
  }
};

