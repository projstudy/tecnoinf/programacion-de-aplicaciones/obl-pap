async function getVideos(nickname) {
  try {
    let response = await axios.get("Canales/Videos?nickname=" + nickname);
    response.data.forEach(v => {
      const n = v.url.lastIndexOf("=");
      v.img =
        "https://img.youtube.com/vi/" + v.url.substring(n + 1) + "/0.jpg";

      v.fechaPublicacion = moment(v.fechaPublicacion, 'MMMMDDY').format("YYYY-MM-DD");
      console.log(v.duracion, 'DURACION DE VIDEO');
      const totalSecs = v.duracion;
      console.log(totalSecs, 'SEGUNDOS DE VIDEO');
      const hours = totalSecs / 3600;
      console.log(hours, 'HORAS DE VIDEO');
      const minutes = (totalSecs % 3600) / 60;
      console.log(hours, 'MINUTOS DE VIDEO');
      const seconds = totalSecs % 60;
      console.log(hours, 'SEGUNDOS DE VIDEO');
      const parsedH = Number.parseInt(hours).toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping: false});
      const parsedM = Number.parseInt(minutes).toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping: false});
      const parsedS = Number.parseInt(seconds).toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping: false});
      v.duracion = parsedH + ":" + parsedM + ":" + parsedS;
    });
    return response.data;
  } catch (ex) {
    console.log(ex);
    if (ex.response.status === 400) {
      this.$toasted.error(ex.response.data);
    } else {
      this.$toasted.error("Upps, algo salio mal :c");
    }
  }
}

