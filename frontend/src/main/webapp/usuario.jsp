<%@ taglib tagdir="/WEB-INF/tags" prefix="tf" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tf:toolbar>
  <div class="container-fluid">
    <div class="row pt-5" style="padding-left: 5em;">
      <div>
        <img :src="usuario.foto" alt="image dont found" style="width: 160px; height: 160px; border-radius: 100%;">
        <div style="display: inline-block; vertical-align: top;" class="pt-4">
          <p class="pl-3 mb-0" style="font-family: Nunito,sans-serif; font-size: 36px;">{{ usuario.nickname }}
            <a :href="'${pageContext.servletContext.contextPath}/UpdateUser?id='+usuario.nickname">
              <i v-if="enableEdit" class="fas fa-edit pt-3"
                 style="float: right; font-size: 24px; color: #404040; cursor: pointer;"></i>
            </a>
          </p>
          <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+canal.nombre"
             style="outline: none; text-decoration: none; color: black">
            <span style="font-family: 'Nunito', sans-serif; font-weight: 800; font-size: 16px;" class="pl-3"> {{ canal.nombre }}
              <span class="badge badge-pill badge-info"><i class="fas fa-check"></i></span>
            </span>
          </a>
          <span class="pl-4"
                style="font-family: Nunito,sans-serif; font-size: 16px; color: #606060;">{{ seguidores.length }} SEGUIDORES</span>
          <p class="pl-3 pt-2" v-if="!canal.privado" style="font-size: 20px; font-family: Nunito, sans-serif;"><i
              class="fas fa-eye pr-2"></i>Publico</p>
          <p class="pl-3 pt-2" v-if="canal.privado" style="font-size: 20px; font-family: Nunito, sans-serif;"><i
              class="fas fa-eye-slash pr-2"></i>Privado</p>
          <span v-if="canal.categoria"
                class="badge badge-pill badge-danger pr-3"
                style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
              class="fas fa-tag px-1"></i>{{canal.categoria}}</span>
        </div>
        <div v-if="showSuscribe">
          <div v-if="isSuscribed" class="pt-2">
            <button class="btn btn-outline-danger" @click="unfollow(canal.nombre)" style="padding-left: -5px;">DEJAR DE
              SEGUIR
            </button>
          </div>
          <div v-else class="pt-2" style="padding-left: 35px;">
            <button @click="follow(canal.nombre)" class="btn btn-outline-danger">SEGUIR</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row pt-5" style="padding-left: 5.5em;">
    <nav>
      <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active tab-title " id="nav-videos-tab" data-toggle="tab" href="#nav-videos"
           role="tab"
           aria-controls="nav-home" aria-selected="true" style="color: #b5a3a3a6;"><i
            class="fas fa-play-circle pr-2"></i>VIDEOS</a>
        <a class="nav-item nav-link tab-title" id="nav-listas-tab" data-toggle="tab" href="#nav-listas" role="tab"
           aria-controls="nav-profile" aria-selected="false" style="color: #b5a3a3a6;"><i
            class="fas fa-bars pr-2"></i>LISTAS</a>
        <a class="nav-item nav-link tab-title" id="nav-seguidores-tab" data-toggle="tab" href="#nav-seguidores"
           role="tab"
           aria-controls="nav-contact" aria-selected="false" style="color: #b5a3a3a6;"><i
            class="fas fa-user-friends pr-2"></i>SEGUIDORES</a>
        <a class="nav-item nav-link tab-title" id="nav-seguidos-tab" data-toggle="tab" href="#nav-seguidos" role="tab"
           aria-controls="nav-contact" aria-selected="false" style="color: #b5a3a3a6;"><i
            class="fas fa-user pr-2"></i>SEGUIDOS</a>
      </div>
    </nav>
  </div>
  <div class="row" style="padding-left: 5.5em; padding-right: 5.5em;">
    <div class="tab-content" id="nav-tabContent" style="width: 100%;">
      <!-- Videos tab -->
      <div class="tab-pane fade show active" id="nav-videos" role="tabpanel" aria-labelledby="nav-videos-tab">
        <div class="card pb-5">
          <div class="row pt-5" v-for="video in videos" v-if="video.fechaPublicacion"
               style="padding-left: 4em;">
            <!-- Videos -->
            <a :href="'${pageContext.servletContext.contextPath}/Watch?idVideo='+video.id"
               style="outline: none; text-decoration: none; color: black">
              <img :src="video.img" style="width: 350px; height: 250px;">
              <div class="pl-4 pt-2" style="display: inline-block;">
                <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ video.nombre }}</span>
                <p class="d-none d-md-block" style="font-family: 'Nunito', sans-serif; font-size: 14px;">{{
                  video.descripcion
                  }}</p>
                <a v-if="video.categoria" :href='"Category?id="+video.categoria' class="pl-2">
                  <span
                      class="badge badge-pill badge-danger pr-3"
                      style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
                      class="fas fa-tag px-1"></i>{{ video.categoria }}</span>
                  </span>
                </a>
              </div>
            </a>
          </div>
        </div>
      </div>

      <!-- Listas tab -->
      <div class="tab-pane fade" id="nav-listas" role="tabpanel" aria-labelledby="nav-listas-tab">
        <div class="card pb-5">
          <div class="row pt-5" v-for="lista in listas" v-if="lista.tipo" style="padding-left: 4em;">
            <a :href="'${pageContext.servletContext.contextPath}/Watch?idLista='+lista.id"
               style="outline: none; text-decoration: none; color: black">
              <img src="assets/lista.png" style="width: 250px; height: 150px;">
              <div class="pl-3 pt-2" style="display: inline-block;">
                <div>
                  <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ lista.nombre }} </span>
                </div>
                <a v-if="lista.categoria" :href='"Category?id="+lista.categoria'>
                    <span
                        class="badge badge-pill badge-danger pr-3"
                        style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
                        class="fas fa-tag px-1"></i>{{ lista.categoria }}</span>
                  </span>
                </a>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-seguidores" role="tabpanel" aria-labelledby="nav-seguidores-tab">
        <div class="card pb-5">
          <div class="row pt-5" v-for="seguidor in seguidores" style="padding-left: 6em;">
            <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+seguidor.nickname"
               style="outline: none; text-decoration: none; color: black">
              <div class="card" style="width: 150px; height: 150px; border-radius: 100%;">
                <img :src="seguidor.foto" style="width: 150px; height: 150px; border-radius: 100%;">
              </div>
              <div class="pl-4">
                  <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ seguidor.nickname }}
                    <span
                        class="badge badge-pill badge-info"
                        style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
                        class="fas fa-check"></i></span>
                  </span>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-seguidos" role="tabpanel" aria-labelledby="nav-seguidos-tab">
        <div class="card pb-5">
          <div class="row pt-5" v-for="s in seguidos" style="padding-left: 6em;">
            <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+s.nickname"
               style="outline: none; text-decoration: none; color: black">
              <div class="card" style="width: 150px; height: 150px; border-radius: 100%;">
                <img :src="s.foto" style="width: 150px; height: 150px; border-radius: 100%;">
              </div>
              <div class="pl-4">
                  <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ s.nickname }}
                    <span
                        class="badge badge-pill badge-info"
                        style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
                        class="fas fa-check"></i></span>
                  </span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- This is the add lista modal -->
  <div class="modal fade" id="addLista" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">CREAR LISTA</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="px-5 py-3">
            <input
                type="text"
                class="form-control"
                placeholder="Nombre"
                aria-label="Nombre"
                name="nombre"
                v-model="nombreLista"
            />
          </div>
          <div class="px-5 py-3 row">
            <div class="col-8">
              <select class="form-control form-control" v-model="categoriaLista">
                <option disabled value="">Eligir categoria</option>
                <option v-for="categoria in categorias">{{ categoria }}</option>
              </select>
            </div>
            <div class="col-4 pt-2">
              <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="switch" v-model="privacidadLista">
                <label class="custom-control-label pr-3" for="switch"
                       style="font-family: Nunito,sans-serif; font-size: 16px;">Privada</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancelar</button>
          <button @click.prevent="crearLista" type="button" class="btn btn-outline-success">Confirmar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- This is the add video modal -->
  <div class="modal fade" id="addVideo" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">CREAR VIDEO</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="px-5 py-3">
            <input
                type="text"
                class="form-control"
                placeholder="Nombre"
                aria-label="Nombre"
                name="nombre"
                v-model="newVideo.nombre"
                v-bind:class="{'form-control':true, 'is-invalid' : !checkEmpty(newVideo.nombre) && newVideo.nameSelected}"
                v-on:blur="newVideo.nameSelected = true">
          </div>
          <div class="px-5 py-3">
            <input
                type="text"
                class="form-control"
                placeholder="URL"
                aria-label="URL"
                name="url"
                v-model="newVideo.url"
                v-bind:class="{'form-control':true, 'is-invalid' : !checkEmpty(newVideo.url) && newVideo.urlSelected}"
                v-on:blur="newVideo.urlSelected = true">
          </div>
          <div class="px-5 py-3 row">
            <div class="col-7">
              <select v-model="newVideo.categoria" class="form-control form-control">
                <option disabled value="">Eligir categoria</option>
                <option v-for="categoria in categorias">{{ categoria }}</option>
              </select>
            </div>
            <div class="col-5">
              <input type="text"
                     v-model="newVideo.duracion"
                     v-bind:class="{'form-control':true, 'is-invalid' : !checkDuration(newVideo.duracion) && newVideo.timeSelected}"
                     v-on:blur="newVideo.timeSelected = true">
            </div>
          </div>
          <div class="row px-5 py-3">
            <div class="col-8">
              <input type="date" class="form-control" min="1900-01-01" max="2020-01-01"
                     v-model="newVideo.fechaPublicacion"
                     v-bind:class="{'form-control':true, 'is-invalid' : !validDate(newVideo.fechaPublicacion) && newVideo.dateSelected}"
                     v-on:blur="newVideo.dateSelected = true">
            </div>
            <div class="col-4">
              <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="switch2" v-model="newVideo.privado">
                <label class="custom-control-label pr-3" for="switch2"
                       style="font-family: Nunito,sans-serif; font-size: 16px;">Privado</label>
              </div>
            </div>
          </div>
          <div class="px-5 py-3">
            <textarea rows="3" class="form-control" placeholder="Descripcion" v-model="newVideo.descripcion"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancelar</button>
          <button @click.prevent="crearVideo" type="button" class="btn btn-outline-success">Confirmar</button>
        </div>
      </div>
    </div>
  </div>
  </div>
</tf:toolbar>

<script>
  Vue.use(Toasted,
    {
      action: {
        text: 'X',
        onClick: (e, toastObject) => {
          toastObject.goAway(0);
        }
      },
      position: 'top-right',
      duration: 4000
    }
  );
  //extiende el componente de la toolbar
  const Model = BaseModel.extend({
    data() {
      return {
        listas: [],
        videos: [],
        canal: [],
        seguidores: [],
        seguidos: [],
        suscribedTo: [],
        isSuscribed: false,
        showSuscribe: false,
      }
    },
    methods: {},
    async created() {
      let queryParams = getParams(window.location.href);
      await Promise.all([this.getCanal(queryParams.id), this.getCategorias()]);
      this.enableEdit = "${sessionScope.DtUsuario.usuario.nickname}" == this.usuario.nickname;
      const canal = "${sessionScope.DtUsuario.usuario.nickname}";
      if (canal && canal != this.canal.nombre) {
        let response = await axios.get("Canales?nickname=" + canal);
        this.suscribedTo = response.data.suscripciones;
        this.isSuscribed = this.suscribedTo.some(e => e.nickname === this.usuario.nombre);
        this.showSuscribe = true;
      }
    }
  });
  new Model({el: '#toolbar'})
</script>

<style scoped>
  .custom-switch .custom-control-input:disabled:checked ~ .custom-control-label::before {
    background-color: #35a04e;
    border-color: #35a04e;
  }

  a.active {
    color: #1b1e21 !important;
  }

  .custom-switch {
    padding-top: 10px;
    padding-left: 3.25rem;
  }

  .tab-title {
    font-family: Nunito, sans-serif;
    font-size: 18px;
  }
</style>
