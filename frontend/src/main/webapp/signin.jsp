<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
  <!-- CSS File link -->
  <link rel="stylesheet" type="text/css" href="styles/style.css"/>

  <!-- Vue.js Script cdn -->
  <script src="js/vendor/vue.js"></script>

  <!-- Toast Script cdn -->
  <script src="js/vendor/vue-toasted.min.js"></script>

  <!-- Axios Script cdn -->
  <script src="js/vendor/axios.js"></script>

  <!-- qs Script cdn -->
  <script src="js/vendor/qs.js"></script>

  <!-- Boostrap link-->
  <script src="js/vendor/jquery3.3.1.js"></script>
  <script src="js/vendor/popper.js"></script>
  <script src="js/vendor/bootstrap.js"></script>
  <link rel="stylesheet" type="text/css" href="styles/vendor/bootstrap.min.css"/>
  <!-- Fontawesome link-->
  <script src="js/vendor/fontawesome.js"></script>
  <link href="fontawesome/css/solid.min.css" rel="stylesheet">
  <!-- Nunito font link-->
  <link
      rel="stylesheet"
      type="text/css"
      href="styles/vendor/nunito.css"
  />
  <title>UyTube</title>
</head>
<body>
<div id="app" class="container-fluid h-100">
  <div style="position: absolute; top: 25%; padding-left: 5%;" class="slide-bottom text-center">
    <img src="assets/logo.png" style="max-width: 100%; height: 160px;">
    <span class="pt-5" style="color: #fff; font-size: 4vw; font-family: Nunito,sans-serif; position: absolute;">
      UyTube</span>
  </div>
  <div class="row h-100 justify-content-center align-items-center">
    <div class="card card-style text-center">
      <span class="pb-3" style="font-size: 36px; font-family: 'Nunito', sans-serif;">Registrarse</span>
      <div style="width: 100%;" class="pb-3"><span class="stepper" style="left: 25px;">1</span>
        <span class="stepper" style="right: 25px;">2</span>
        <div class="progress" style="height: 1px; width: 400px; margin-left: 43px;">
          <div class="progress-bar" role="progressbar" :style="{ width : stepper + '%' }" :aria-valuenow="stepper"
               aria-valuemin="0" aria-valuemax="100"></div>
        </div>
      </div>
      <form>
        <!-- This is the first step of the form -->
        <div class="form-row pt-4" v-show="stepper == 0" style="padding: 0 80px 0 80px;">
          <div class="col">
            <input type="text" class="form-control" placeholder="Nickname"
                   id="nickname" aria-label="Nickname"
                   v-bind:class="{'form-control':true, 'is-invalid' : !checkEmpty(user.nickname) && nickSelected}"
                   v-model="user.nickname" v-on:blur="nickSelected = true">
            <div class="invalid-feedback" style="text-align: start;">Nickname invalido!</div>
          </div>
        </div>
        <div class="form-row pt-4" v-show="stepper == 0" style="padding: 0 80px 0 80px;">
          <div class="col">
            <input type="text" class="form-control" placeholder="Nombre"
                   id="nombre" aria-label="Nombre"
                   v-bind:class="{'form-control':true, 'is-invalid' : !checkEmpty(user.name) && nameSelected}"
                   v-model="user.name" v-on:blur="nameSelected = true">
            <div class="invalid-feedback" style="text-align: start;">Nombre invalido!</div>
          </div>
          <div class="col">
            <input type="text" class="form-control" placeholder="Apellido"
                   id="apellido" aria-label="Apellido" v-model="user.lastname"
                   v-bind:class="{'form-control':true, 'is-invalid' : !checkEmpty(user.lastname) && lastnameSelected} "
                   v-on:blur="lastnameSelected = true">
            <div class="invalid-feedback" style="text-align: start;">Apellido invalido!</div>
          </div>
        </div>
        <div class="form-row pt-4" v-show="stepper == 0" style="padding: 0 80px 0 80px;">
          <div class="col">
            <input type="date" class="form-control" v-model="user.date" min="1900-01-01" max="2020-01-01"
                   v-bind:class="{'form-control':true, 'is-invalid' : !validDate(user.date) && dateSelected} "
                   v-on:blur="dateSelected = true">
          </div>
        </div>
        <div class="form-row pt-4" v-show="stepper == 0" style="padding: 0 80px 0 80px;">
          <div class="col">
            <input
                v-model="user.mail"
                placeholder="Correo"
                aria-label="Correo"
                v-bind:class="{'form-control':true, 'is-invalid' : !validEmail(user.mail) && emailSelected}"
                v-on:blur="emailSelected = true">
            <div class="invalid-feedback" style="text-align: start;">Correo invalido!</div>
          </div>
        </div>
        <div class="form-row pt-4" v-show="stepper == 0" style="padding: 0 80px 0 80px;">
          <div class="col">
            <input type="password" class="form-control" placeholder="Contrasena"
                   id="contrasena"
                   aria-label="Contrasena"
                   v-model="user.password"
                   v-bind:class="{'form-control':true, 'is-invalid' : !checkEmpty(user.password) && passwordSelected}"
                   v-on:blur="passwordSelected = true">
            <div class="invalid-feedback" style="text-align: start;">Contrasena invalida!</div>
          </div>
          <div class="col">
            <input type="password" class="form-control" placeholder="Confirmar"
                   id="confirmar"
                   aria-label="Confirmar"
                   v-model="user.confirmation"
                   v-bind:class="{'form-control':true, 'is-invalid' : (!checkEmpty(user.confirmation)  || !checkPassword()) && passwordSelected}"
                   v-on:blur="passwordSelected=true">
            <div class="invalid-feedback" style="text-align: start;">Contrasenas no coinciden!</div>
          </div>
        </div>
        <!-- This is the second Step of the form -->
        <div class="form-row pt-4" v-show="stepper != 0" style="padding: 0 80px 0 80px;">
          <div class="col">
            <div class="image-upload">
              <label for="file-input">
                <img id="file-img" src="assets/add.svg" style="width: 80px; height: 80px; border-radius: 100%;"/>
              </label>
              <input id="file-input" type="file"/>
            </div>
          </div>
          <div class="col pt-4">
            <input type="text" class="form-control" placeholder="Canal"
                   aria-label="Canal"
                   v-model="user.canal.nombre"
                   v-bind:class="{'form-control':true, 'is-invalid' : !checkEmpty(user.canal.nombre) && canalSelected}"
                   v-on:blur="canalSelected=true">
          </div>
        </div>
        <div class="form-row pt-4" v-show="stepper != 0" style="padding: 0 80px 0 80px;">
          <div class="col">
            <select class="form-control" v-model="selected">
              <option disabled value="">Elegir categoria</option>
              <option v-for="categoria in categorias">{{ categoria }}</option>
            </select>
          </div>
          <div class="col pt-2">
            <div class="custom-control custom-switch">
              <input type="checkbox" class="custom-control-input" id="customSwitch1" v-model="user.canal.privado">
              <label class="custom-control-label" for="customSwitch1">Privado</label>
            </div>
          </div>
        </div>
        <div class="form-row pt-4" v-show="stepper != 0" style="padding: 0 80px 0 80px;">
          <div class="col">
            <textarea class="form-control" id="descripcion" placeholder="Descripcion" rows="3"
                      v-model="user.canal.descripcion"></textarea>
            <div class="invalid-feedback" style="text-align: start;">Descripcion invalida!</div>
          </div>
        </div>
        <div style="text-align: end;" class="pt-4">
          <button v-show="stepper == 0" @click.prevent="nextStep" class="btn text-light"
                  style="background-color: #dc3545; border-radius: 8px; font-family: 'Nunito', sans-serif;"> Siguiente
          </button>
          <button v-show="stepper != 0" @click.prevent="stepper = 0" class="btn text-light"
                  style="background-color: #dc3545; border-radius: 8px; font-family: 'Nunito', sans-serif;"> Volver
          </button>
          <button v-show="stepper != 0" type="button" class="btn text-light"
                  style="background-color: #dc3545; border-radius: 8px; font-family: 'Nunito', sans-serif;"
                  @click.prevent="createUser"> Registrarse
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
</body>
</html>

<script>
  Vue.use(Toasted,
    {
      action: {
        text: 'X',
        onClick: (e, toastObject) => {
          toastObject.goAway(0);
        }
      },
      position: 'top-right',
      duration: 4000
    }
  );

  const app = new Vue({
    el: "#app",
    data: {
      nickSelected: false,
      nameSelected: false,
      dateSelected: false,
      lastnameSelected: false,
      emailSelected: false,
      canalSelected: false,
      passwordSelected: false,
      valid: false,
      submitted: false,
      message: "",
      selected: '',
      user: {
        nickname: "",
        name: "",
        lastname: "",
        mail: "",
        password: "",
        confirmation: "",
        foto: '',
        canal: {
          categoria: "",
          nombre: nickname,
          privado: true,
          descripcion: "",
        },
        date: "",
        bDate: ""
      },
      categorias: [],
      stepper: 0
    },
    created() {
      this.getCategorias();
    },
    methods: {
      async createUser(e) {
        if (this.user.canal.descripcion) {
          e.preventDefault();
          try {
            this.user.canal.categoria = this.selected;
            console.log(this.user);
            let response = await
              axios.post('Canales', Qs.stringify(this.user));
            this.$toasted.success('Usuario creado correctamente');
            setTimeout(() => {
              window.location.href = "Login";
            }, 1000)
          } catch (error) {
            if (error.response.status === 400) {
              this.$toasted.error(error.response.data);
            } else {
              this.$toasted.error("Upps, algo salio mal :c");
            }
          }
        } else {
          document.getElementById("descripcion").classList.add('is-invalid');
        }
      }
      ,
      async getCategorias() {
        try {
          let response = await axios.get('Categoria');
          this.categorias = response.data;
        } catch (ex) {
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      }
      ,
      nextStep() {
        this.validate();
        if (this.valid) {
          this.stepper = 100;
          this.user.canal.nombre = this.user.nickname;
          let x = this.user.date.split("-");
          this.user.bDate = x[2] + "/" + x[1] + "/" + x[0];
        }
      }
      ,
      validate: function () {
        this.nameSelected = true;
        this.lastnameSelected = true;
        this.emailSelected = true;
        this.passwordSelected = true;
        this.nickSelected = true;
        this.dateSelected = true;
        if (
          this.validEmail(this.user.mail) &&
          this.checkEmpty(this.user.name) &&
          this.checkEmpty(this.user.lastname) &&
          this.checkEmpty(this.user.password) &&
          this.validDate(this.user.date) &&
          this.checkPassword(this.user.mail)
        ) {
          this.valid = true;
        }
      }
      ,
      checkEmpty: function (text) {
        return !!text;
      }
      ,
      checkPassword: function () {
        return this.user.password === this.user.confirmation
      }
      ,
      validDate: function () {
        let parsedDate = this.user.date.split("-");
        return parsedDate[0] > 1900;
      }
      ,
      validEmail: function (email) {
        var re = /(.+)@(.+){2,}\.(.+){2,}/;
        return re.test(email.toLowerCase());
      }
    }
  });

  const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  async function readURL(input) {
    if (input.files && input.files[0]) {
      app.user.foto = await toBase64(input.files[0]);
      const reader = new FileReader();

      reader.onload = function (e) {
        $('#file-img').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#file-input").change(function () {
    readURL(this);
  });
</script>

<style scoped>
  body,
  html {
    height: 100%;
    background-image: url("assets/bg.png");
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
    background-attachment: fixed;
  }

  .toasted-container .toasted {
    top: -50px !important;
    right: -100px !important;
  }

  .toasted.toasted-primary .action {
    color: #0200019e;
  }

  .card-style {
    width: 550px;
    border-radius: 8px;
    border-top: 5px solid #dc3545;
    box-shadow: 10px 10px 43px 0px rgba(0, 0, 0, 0.55);
    padding: 25px 30px 25px 30px;
  }

  .stepper {
    position: absolute;
    border-radius: 100%;
    border: 1px solid #007bff;
    top: 75px;
    padding: 5px 12px 5px 12px;
  }

  .image-upload > input {
    display: none;
  }

  .slide-bottom {
    -webkit-animation: slide-bottom 1s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;
    animation: slide-bottom 1s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;
  }

  @-webkit-keyframes slide-bottom {
    0% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
    }
    100% {
      -webkit-transform: translateY(100px);
      transform: translateY(100px);
    }
  }
  @keyframes slide-bottom {
    0% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
    }
    100% {
      -webkit-transform: translateY(100px);
      transform: translateY(100px);
    }
  }
</style>
