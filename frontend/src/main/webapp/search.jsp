<%@ taglib tagdir="/WEB-INF/tags" prefix="tf" %>

<tf:toolbar>
  <div class="container-fluid" v-infinite-scroll="scroll" infinite-scroll-disabled="busy"
       infinite-scroll-distance="100">
    <div class="row"
         style="height: 56px; position: sticky; box-shadow: 0px 1px 1px #2927292e; top: 56px; background: #ffffff; z-index: 7;">
      <div class="col-5 pt-3">
        <span style="font-family: 'Nunito', sans-serif; font-size: 16px;">{{ elementos.length }} Resultados</span>
      </div>
      <div class="col"></div>
      <div class="col pt-2">
        <select @change="reOrder()" v-model="ordenarPor" class="form-control"
                style="border: 1px solid #DFDFDF !important;">
          <option selected disabled value="" hidden>Ordenar Por</option>
          <option value="alfa">Alfabeticamente(a-z)</option>
          <option value="fecha">Fecha Publicacion</option>
        </select>
      </div>
    </div>
    <div class="row" v-for="elemento in elementos" style="padding-top: 50px;">
      <!-- Videos -->
      <div v-if="elemento.fechaPublicacion" class="col pl-5">
        <a :href="'${pageContext.servletContext.contextPath}/Watch?idVideo='+elemento.id"
           style="outline: none; text-decoration: none; color: black">
          <div class="card" style="width: 350px; height: 250px;">
            <img :src="elemento.img" style="width: 350px; height: 250px;">
          </div>
          <div class="video-info">
            <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ elemento.nombre }}</span>
            <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+elemento.canal"
               style="outline: none; text-decoration: none; color: black">
              <p style="font-family: 'Nunito', sans-serif; font-weight: 800; font-size: 16px;"> {{ elemento.canal }}
                <span class="badge badge-pill badge-info"><i class="fas fa-check"></i></span>
              </p>
            </a>
            <p class="d-none d-md-block" style="font-family: 'Nunito', sans-serif; font-size: 14px;">{{
              elemento.descripcion
              }}</p>
            <a v-if="elemento.categoria" :href='"Category?id="+elemento.categoria'>
              <span
                  class="badge badge-pill badge-danger pr-3"
                  style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
                  class="fas fa-tag px-1"></i>{{ elemento.categoria }}</span>
              </span>
            </a>
          </div>
        </a>
      </div>
      <!-- Canales -->
      <div v-else-if="elemento.descripcion" class="col pl-5">
        <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+elemento.nombre"
           style="outline: none; text-decoration: none; color: black">
          <div class="card" style="width: 150px; height: 150px; border-radius: 100%;">
            <img :src="elemento.foto" style="width: 150px; height: 150px; border-radius: 100%;">
          </div>
          <div class="canal-info">
            <div>
                  <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ elemento.nombre }}
                    <span
                        class="badge badge-pill badge-info"
                        style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
                        class="fas fa-check"></i></span>
                  </span>
            </div>
            <p class="d-none d-md-block" style="font-family: 'Nunito', sans-serif; font-size: 14px;">{{
              elemento.descripcion }}</p>
          </div>
        </a>
      </div>
      <!-- Listas -->
      <div v-else class="col pl-5">
        <a :href="'${pageContext.servletContext.contextPath}/Watch?idLista='+elemento.id"
           style="outline: none; text-decoration: none; color: black">
          <div class="card" style="width: 250px; height: 150px;">
            <img src="assets/lista.png" style="width: 250px; height: 150px;">
          </div>
          <div class="lista-info">
            <div>
              <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ elemento.nombre }} </span>
            </div>
            <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+elemento.canal"
               style="outline: none; text-decoration: none; color: black">
              <p style="font-family: 'Nunito', sans-serif; font-weight: 800; font-size: 16px;"> {{ elemento.canal }}
                <span class="badge badge-pill badge-info"><i class="fas fa-check"></i></span>
              </p>
            </a>
            <a v-if="elemento.categoria" :href='"Category?id="+elemento.categoria'>
              <span
                  class="badge badge-pill badge-danger pr-3"
                  style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
                  class="fas fa-tag px-1"></i>{{ elemento.categoria }}</span>
              </span>
            </a>
          </div>
        </a>
      </div>
    </div>

    <footer style="height: 56px; border-top: 1px solid #DFDFDF; margin-top: 80px;">
      <div v-show="!busy" class="spinner-border spinner-loading" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </footer>
  </div>
</tf:toolbar>

<script src="js/vendor/vue-infinite-scroll.js"></script>
<script>
  Vue.use(Toasted,
      {
        action: {
          text: 'X',
          onClick: (e, toastObject) => {
            toastObject.goAway(0);
          }
        },
        position: 'top-right',
        duration: 4000
      }
  );
  //extiende el componente de la toolbar
  const Model = BaseModel.extend({
    data() {
      return {}
    },
    methods: {},
    created() {
      this.getCategorias();
      const queryParams = getParams(window.location.href);
      this.nombre = queryParams.query;
      this.texto = this.nombre;
    }
  });
  new Model({el: '#toolbar'})
</script>

<style scoped>
  .toasted-container .toasted {
    top: 0px !important;
    right: -100px !important;
  }

  .toasted.toasted-primary .action {
    color: #0200019e;
  }

  .spinner-loading {
    position: absolute;
    left: 50%;
    bottom: 15px;
    color: #4236af;
    width: 3em;
    height: 3em;
    border-top: 1px solid #353535;
  }

  .video-info {
    position: absolute;
    top: 0px;
    left: 415px;
    max-width: 450px;
  }

  .lista-info {
    position: absolute;
    top: 0px;
    left: 315px;
    max-width: 450px;
  }

  .canal-info {
    position: absolute;
    top: 20px;
    left: 225px;
    max-width: 450px;
  }

  @media (max-width: 768px) {
    .video-info {
      position: absolute;
      top: 255px;
      left: 20px;
      max-height: 450px;
    }
  }
</style>

