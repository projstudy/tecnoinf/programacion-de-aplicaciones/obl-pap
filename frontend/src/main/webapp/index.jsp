<%@ taglib tagdir="/WEB-INF/tags" prefix="tf" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tf:toolbar>
  <div class="container-fluid">
    <!-- Videos -->
    <div class="row pt-4" style="padding-left: 10%; padding-right: 10%;">
      <div class="col" style="max-width: 300px;" v-for="elemento in elementos" v-if="elemento.fechaPublicacion">
        <a :href="'${pageContext.servletContext.contextPath}/Watch?idVideo='+elemento.id"
           style="outline: none; text-decoration: none; color: black">
          <div class="card" style="width: 250px; height: 170px;">
            <img :src="elemento.img" style="width: 250px; height: 170px;">
          </div>
          <div class="video-info">
            <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ elemento.nombre }}</span>
            <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+elemento.canal"
               style="outline: none; text-decoration: none; color: black">
              <p style="font-family: 'Nunito', sans-serif; font-weight: 800; font-size: 16px;"> {{ elemento.canal }}
                <span class="badge badge-pill badge-info"><i class="fas fa-check"></i></span>
              </p>
            </a>
          </div>
        </a>
      </div>
    </div>
    <div>
      <hr>
    </div>
    <!-- Listas -->
    <div class="row pt-4" style="padding-left: 10%; padding-right: 10%;">
      <div v-for="elemento in elementos" v-if="!elemento.fechaPublicacion && !elemento.descripcion" class="col"
           style="max-width: 300px;">
        <a :href="'${pageContext.servletContext.contextPath}/Watch?idLista='+elemento.id"
           style="outline: none; text-decoration: none; color: black">
          <div class="card" style="width: 250px; height: 150px;">
            <img src="assets/lista.png" style="width: 250px; height: 150px;">
          </div>
          <div class="lista-info">
            <div>
              <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ elemento.nombre }} </span>
            </div>
            <div>
              <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+elemento.canal"
                 style="outline: none; text-decoration: none; color: black">
                <span style="font-family: 'Nunito', sans-serif; font-weight: 800; font-size: 16px;"> {{ elemento.canal }}
                  <span class="badge badge-pill badge-info"><i class="fas fa-check"></i></span>
                </span>
              </a>
              <a v-if="elemento.categoria" :href='"Category?id="+elemento.categoria' class="pl-2">
              <span
                  class="badge badge-pill badge-danger pr-3"
                  style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
                  class="fas fa-tag px-1"></i>{{ elemento.categoria }}</span>
                </span>
              </a>
            </div>
          </div>
        </a>
      </div>
    </div>
    <!-- Canales -->
    <div>
      <hr>
    </div>
    <div class="row pt-4" style="padding-left: 10%; padding-right: 10%;">
      <div v-for="elemento in elementos" v-if="elemento.descripcion && !elemento.fechaPublicacion" class="col"
           style="text-align: center; max-width: 300px;">
        <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+elemento.nombre"
           style="outline: none; text-decoration: none; color: black">
          <img :src="elemento.foto" style="width: 120px; height: 120px; border-radius: 100%;">
          <div class="canal-info">
            <div>
            <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ elemento.nombre }}
              <span
                  class="badge badge-pill badge-info"
                  style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
                  class="fas fa-check"></i></span>
              </span>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>
</tf:toolbar>
<script>
  Vue.use(Toasted,
    {
      action: {
        text: 'X',
        onClick: (e, toastObject) => {
          toastObject.goAway(0);
        }
      },
      position: 'top-right',
      duration: 4000
    }
  );
  //extiende el componente de la toolbar
  const Model = BaseModel.extend({
    data() {
      return {}
    },
    methods: {},
    created() {
      this.searchVideoListasCanales(0, 5, "");
      this.getCategorias();
    }
  });
  new Model({el: '#toolbar'})
</script>

<style scoped>
  .toasted-container .toasted {
    top: 0px !important;
    right: -100px !important;
  }

  .toasted.toasted-primary .action {
    color: #0200019e;
  }
</style>

