<%@ taglib tagdir="/WEB-INF/tags" prefix="tf" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tf:toolbar>
  <div class="container-fluid pl-5">
    <!-- Madafking videos -->
    <div class="row pt-5" v-for="elemento in elementos" v-if="elemento.fechaPublicacion">
      <a :href="'${pageContext.servletContext.contextPath}/Watch?idVideo='+elemento.id"
         style="outline: none; text-decoration: none; color: black">
        <img :src="elemento.img" style="width: 350px; height: 250px;">
      </a>
      <div class="pl-3 pt-2" style="display: inline-block;">
        <a :href="'${pageContext.servletContext.contextPath}/Watch?idVideo='+elemento.id"
           style="outline: none; text-decoration: none; color: black">
          <span style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ elemento.nombre }}</span>
        </a>
        <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+elemento.canal"
           style="outline: none; text-decoration: none; color: black">
          <p style="font-family: 'Nunito', sans-serif; font-weight: 800; font-size: 16px;"> {{ elemento.canal }}
            <span class="badge badge-pill badge-info"><i class="fas fa-check"></i></span>
          </p>
        </a>
        <p class="d-none d-md-block" style="font-family: 'Nunito', sans-serif; font-size: 14px;">{{
          elemento.descripcion
          }}</p>
        <span
            class="badge badge-pill badge-danger"
            style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
            class="fas fa-tag px-1"></i>{{ elemento.categoria }}</span>
        </span>
      </div>
    </div>
    <!-- Madafking listas -->
    <div class="row pt-5" v-for="elemento in elementos" v-if="elemento.tipo">
      <div class="pl-0">
        <a :href="'${pageContext.servletContext.contextPath}/Watch?idLista='+elemento.id"
           style="outline: none; text-decoration: none; color: black">
          <img src="assets/lista.png" style="width: 250px; height: 150px;">
          <div class="pt-2 pl-3" style="display: inline-block; position: absolute;">
            <p class="mb-0" style="font-family: 'Nunito', sans-serif; font-size: 28px;">{{ elemento.nombre }} </p>
            <a :href="'${pageContext.servletContext.contextPath}/Channel?id='+elemento.canal"
               style="outline: none; text-decoration: none; color: black">
              <p class="mt-0" style="font-family: 'Nunito', sans-serif; font-weight: 800; font-size: 16px;"> {{ elemento.canal }}
                <span class="badge badge-pill badge-info"><i class="fas fa-check"></i></span>
              </p>
            </a>
            <span
                class="badge badge-pill badge-danger"
                style="font-size: 12px !important; top: 10px; vertical-align: middle;"><i
                class="fas fa-tag px-1"></i>{{ elemento.categoria }}</span>
            </span>
          </div>
        </a>
      </div>
    </div>
  </div>
</tf:toolbar>

<script>

  Vue.use(Toasted,
    {
      action: {
        text: 'X',
        onClick: (e, toastObject) => {
          toastObject.goAway(0);
        }
      },
      position: 'top-right',
      duration: 4000
    }
  );

  //extiende el componente de la toolbar
  const Model = BaseModel.extend({
    data() {
      return {}
    },
    methods: {
      async getCategoria(nombre) {
        try {
          let response = await axios.get("Categoria?nombre=" + nombre);
          response.data.videos.forEach(v => {
            const n = v.url.lastIndexOf("=");
            v.img =
              "https://img.youtube.com/vi/" + v.url.substring(n + 1) + "/0.jpg";
          });
          this.elementos.push(...response.data.listas);
          this.elementos.push(...response.data.videos);
        } catch (ex) {
          console.log(ex);
          if (ex.response.status === 400) {
            this.$toasted.error(ex.response.data);
          } else {
            this.$toasted.error("Upps, algo salio mal :c");
          }
        }
      },
    },
    created() {
      let queryParams = getParams(window.location.href);
      this.getCategoria(queryParams.id);
      this.getCategorias();
    }
  });
  new Model({el: '#toolbar'})
</script>
