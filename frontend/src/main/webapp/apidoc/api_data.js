define({ "api": [
  {
    "type": "post",
    "url": "/Buscar",
    "title": "Buscar Videos Listas Canales",
    "description": "<p>Retorna una lista de Canales, videos y listas publicos El resultado es paginado</p>",
    "name": "Search",
    "group": "Buscar",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre",
            "description": "<p>texto por el cual se busca</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "sortAlfa",
            "description": "<p>orderna el resultado alfabeticamente</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "sortFecha",
            "description": "<p>orderna el resultado por fecha</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pagina",
            "description": "<p>numero de pagina del resultado</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "description": "<p>items por pagina</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/buscar.js",
    "groupTitle": "Buscar"
  },
  {
    "type": "get",
    "url": "/Canales",
    "title": "Consulta Usuario/Canal",
    "description": "<p>Devuelve todos los datos de consulta canal</p> <p>Si el canal que se esta consultando es el del usuario, se retornan los videos publicos y privados</p> <p>Si se esta consultando a otro usuario, se retorna solo los publicos.</p>",
    "name": "ConsultaCanal",
    "group": "Canal",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>nombre del canal a consultar</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/canal.js",
    "groupTitle": "Canal"
  },
  {
    "type": "post",
    "url": "/Canales/Seguir",
    "title": "Seguir Usuario",
    "description": "<p>Sigue a un usuario</p>",
    "name": "FollowUser",
    "group": "Canal",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "canal",
            "description": "<p>nombre del canal a seguir</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/canal.js",
    "groupTitle": "Canal"
  },
  {
    "type": "get",
    "url": "/Canales/Listas",
    "title": "Listar Listas",
    "description": "<p>Lista las listas de un canal.</p> <p>Si el canal que se esta consultando es el del usuario, se retornan las listas particulares y predeterminadas</p> <p>Si se esta consultando a otro usuario, se retorna solo las particulares.</p>",
    "name": "ListasCanal",
    "group": "Canal",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>nombre del canal a consultar</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/canal.js",
    "groupTitle": "Canal"
  },
  {
    "type": "put",
    "url": "/Canales",
    "title": "Modificar Usuario/Canal",
    "description": "<p>modifica los datos de un usuario/canal</p>",
    "name": "ModUsuario",
    "group": "Canal",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre",
            "description": "<p>nombre del usuairo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "apellido",
            "description": "<p>email del usuario</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fechaNacimiento",
            "description": "<p>fecha de nacimiento [dd/nmm/yyyy]</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "foto",
            "description": "<p>foto en base64</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "privado",
            "description": "<p>privacidad del canal</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/canal.js",
    "groupTitle": "Canal"
  },
  {
    "type": "post",
    "url": "/Canales",
    "title": "Alta Usuario/Canal",
    "description": "<p>Se crea un nuevo usuario y su canal</p>",
    "name": "NuevoUsuario",
    "group": "Canal",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>nickname del usuairo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mail",
            "description": "<p>email del usuario</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>nombre del usuario</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>apellido del usuairo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "bDate",
            "description": "<p>fecha de nacimiento [dd/nmm/yyyy]</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "foto",
            "description": "<p>foto en base64</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "canal",
            "description": "<p>nombre del canal</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "descripcion",
            "description": "<p>descripcion del canal</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "privado",
            "description": "<p>privacidad del canal</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/canal.js",
    "groupTitle": "Canal"
  },
  {
    "type": "delete",
    "url": "/Canales/Seguir",
    "title": "Dejar de Seguir Usuario",
    "description": "<p>Deja de seguir a un usuario</p>",
    "name": "UnfollowUser",
    "group": "Canal",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "canal",
            "description": "<p>nombre del canal a dejar de seguir</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/canal.js",
    "groupTitle": "Canal"
  },
  {
    "type": "put",
    "url": "/Canales/Password",
    "title": "Actualizar Contraseña",
    "description": "<p>actualiza contraseña de un usuario</p>",
    "name": "UpdatePassword",
    "group": "Canal",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>nueva password del usuario</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/canal.js",
    "groupTitle": "Canal"
  },
  {
    "type": "get",
    "url": "/Canales/Videos",
    "title": "Listar Videos",
    "description": "<p>Lista los videos de un canal.</p> <p>Si el canal que se esta consultando es el del usuario, se retornan los videos publicos y privados</p> <p>Si se esta consultando a otro usuario, se retorna solo los publicos.</p>",
    "name": "VideosCanal",
    "group": "Canal",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>nombre del canal a consultar</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/canal.js",
    "groupTitle": "Canal"
  },
  {
    "type": "get",
    "url": "/Categorias",
    "title": "Consulta Categoria",
    "description": "<p>Crea un comentario como respuesta a otro comentario</p>",
    "name": "ConsultaCategoria",
    "group": "Categoria",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre",
            "description": "<p>nombre de la categoria a consultar</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/categoria.js",
    "groupTitle": "Categoria"
  },
  {
    "type": "get",
    "url": "/Categorias",
    "title": "Lista las categorias",
    "description": "<p>Crea un comentario como respuesta a otro comentario</p>",
    "name": "ListarCategoria",
    "group": "Categoria",
    "version": "0.0.0",
    "filename": "myapp/categoria.js",
    "groupTitle": "Categoria"
  },
  {
    "type": "post",
    "url": "/Listas/Video",
    "title": "Agregar Video",
    "description": "<p>agrega un video a una lista</p>",
    "name": "AddVIdeolista",
    "group": "Lista",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idVideo",
            "description": "<p>id del video</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idLista",
            "description": "<p>id de la lista origen</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/lista.js",
    "groupTitle": "Lista"
  },
  {
    "type": "put",
    "url": "/Listas",
    "title": "Modificar Lista",
    "description": "<p>se modifica un lista</p>",
    "name": "MOdLista",
    "group": "Lista",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id de la lista a modificar</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoria",
            "description": "<p>categoria de la lista [puede ser null]</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "privado",
            "description": "<p>privacidad de la lista</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/lista.js",
    "groupTitle": "Lista"
  },
  {
    "type": "post",
    "url": "/Listas",
    "title": "Nueva Lista",
    "description": "<p>crea una nueva lista predeterminada para el usuario logeado</p>",
    "name": "NuevaLista",
    "group": "Lista",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre",
            "description": "<p>nombre de la nueva lista</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoria",
            "description": "<p>categoria de la lista [puede ser null]</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "privado",
            "description": "<p>privacidad de la lista</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/lista.js",
    "groupTitle": "Lista"
  },
  {
    "type": "delete",
    "url": "/Listas/Video",
    "title": "Quitar Video",
    "description": "<p>quita un video de una lista</p>",
    "name": "RemoveVIdeolista",
    "group": "Lista",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idVideo",
            "description": "<p>id del video</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idLista",
            "description": "<p>id de la lista destino</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/lista.js",
    "groupTitle": "Lista"
  },
  {
    "type": "post",
    "url": "/Videos",
    "title": "Crear Video",
    "description": "<p>Crea un video</p>",
    "name": "AltaVideo",
    "group": "Video",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre",
            "description": "<p>nombre del video</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>url de youtube</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "descripcion",
            "description": "<p>descripcion del video</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fechaPublicacion",
            "description": "<p>fecha de publicacion del video</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "duracion",
            "description": "<p>duracion del video en segundos</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "privado",
            "description": "<p>privacidad del video [default false]</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoria",
            "description": "<p>categoria del video [default null]</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/video.js",
    "groupTitle": "Video"
  },
  {
    "type": "get",
    "url": "/Videos",
    "title": "Consulta Video",
    "description": "<p>retorna los datos de consulta de video</p>",
    "name": "ConsultaVideo",
    "group": "Video",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id del video a consltar</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/video.js",
    "groupTitle": "Video"
  },
  {
    "type": "post",
    "url": "/Videos/Comentar",
    "title": "Comentar Video",
    "description": "<p>Crea un comentario para un video</p>",
    "name": "CrearComentario",
    "group": "Video",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "texto",
            "description": "<p>texto del comentario</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/video.js",
    "groupTitle": "Video"
  },
  {
    "type": "post",
    "url": "/Videos/Valorar",
    "title": "Valorar Video",
    "description": "<p>Da &quot;me/no me gusta&quot; a un video</p>",
    "name": "LikeVideo",
    "group": "Video",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "leGusta",
            "description": "<p>representa la valoracion del video</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id del video a valorar</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/video.js",
    "groupTitle": "Video"
  },
  {
    "type": "put",
    "url": "/Videos",
    "title": "Modificar Video",
    "description": "<p>Modifica un video de un usuario</p>",
    "name": "ModVIdeo",
    "group": "Video",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nombre",
            "description": "<p>nombre del video</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>url de youtube</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "descripcion",
            "description": "<p>descripcion del video</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fechaPublicacion",
            "description": "<p>fecha de publicacion del video</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "duracion",
            "description": "<p>duracion del video en segundos</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>duracion del video en segundos</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/video.js",
    "groupTitle": "Video"
  },
  {
    "type": "post",
    "url": "/Videos/Responder",
    "title": "Responder Comentario",
    "description": "<p>Crea un comentario como respuesta a otro comentario</p>",
    "name": "ResponderComentario",
    "group": "Video",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "texto",
            "description": "<p>texto del comentario</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id del comentario al que se responde</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "myapp/video.js",
    "groupTitle": "Video"
  }
] });
