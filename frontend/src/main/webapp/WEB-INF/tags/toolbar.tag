<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
  <!-- Vue.js Script cdn -->
  <script src="js/vendor/vue.js"></script>

  <!-- Toast Script cdn -->
  <script src="js/vendor/vue-toasted.min.js"></script>

  <!-- Axios Script cdn -->
  <script src="js/vendor/axios.js"></script>

  <!-- qs Script cdn -->
  <script src="js/vendor/qs.js"></script>

  <!-- Boostrap link-->
  <script src="js/vendor/jquery3.3.1.js"></script>
  <script src="js/vendor/popper.js"></script>
  <script src="js/vendor/bootstrap.js"></script>
  <link rel="stylesheet" type="text/css" href="styles/vendor/bootstrap.min.css"/>
  <!-- fontawesome link-->
  <script src="js/vendor/fontawesome.js"></script>
  <link href="fontawesome/css/solid.min.css" rel="stylesheet">

  <!-- nunito link-->
  <link rel="stylesheet" type="text/css" href="styles/vendor/nunito.css"/>

  <!-- script para manejar el componente de la toolbar-->
  <script src="js/Listas.js"></script>
  <script src="js/Videos.js"></script>
  <script src="js/Toolbar.js"></script>

  <title>UyTube</title>
</head>
<body>
<div id="toolbar">
  <!--NavBar-->
  <nav class="navbar navbar-expand navbar-dark bg-dark-purple fixed-top pl-1">
    <div class="d-flex flex-fill">
      <button
          type="button"
          class="btn navbar-brand mr-0"
          id="toggle-menu"
          @click="menuToggler = !menuToggler"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <a href="Dash" style="outline: none; text-decoration: none;">
        <img
            src="assets/logo.png"
            alt="logo"
            style="width: 40px; height: 45px;"
        />
      </a>
      <span
          class="text-white pt-3"
          style="font-size: 18px; font-family: Nunito, sans-serif;"
      ><a href="Dash" style="outline: none; text-decoration: none; color: #fff;">UyTube</a></span>
    </div>
    <div class="d-flex flex-fill">
      <form class="form-inline d-none d-md-block flex-nowrap my-2 my-lg-0-">
        <div class="input-group">
          <input
              class="form-control search-input"
              type="search"
              placeholder="Buscar"
              aria-label="Search"
              v-model="texto"
          />
          <span class="input-group-append">
              <button class="btn" style="background: #269F42;" @click.prevent="doSearch">
                <i class="fas fa-search" style="color: #fff;"></i>
              </button>
            </span>
        </div>
      </form>
    </div>
    <div class="d-flex">
      <i
          class="fas fa-search d-block d-md-none pr-3"
          style="color: #fff; padding-top: 12px;"
      ></i>
      <%-- Si esta logeado muestro la foto y nombre --%>
      <c:if test="${sessionScope.DtUsuario != null}">
        <div class="nav-item dropdown" style="cursor: pointer;">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
             aria-haspopup="true" aria-expanded="false" style="text-decoration: none; color: #fff;">
            <span
                style="color: #fff; font-family: 'Nunito', sans-serif; font-size: 18px;">${sessionScope.DtUsuario.usuario.nickname}</span>
            <c:if test="${not empty sessionScope.DtUsuario.usuario.foto}">
              <img src="ImageServlet/${sessionScope.DtUsuario.usuario.foto}" alt="Avatar" class="avatar"/>
            </c:if>
            <c:if test="${empty sessionScope.DtUsuario.usuario.foto}">
              <img src="assets/avatar1.jpg" alt="Avatar" class="avatar"/>
            </c:if>
          </a>
          <div class="dropdown-menu mr-5" aria-labelledby="navbarDropdown">
            <a class="dropdown-item"
               href="${pageContext.servletContext.contextPath}/Channel?id=${sessionScope.DtUsuario.usuario.nickname}">
              <i class="fas fa-portrait pr-2" style="color: #353535;"></i> Tu Canal </a>
            <a class="dropdown-item fix-bg" style="padding-left: 18px;">
              <form method="post" action="Logout" class="btn-logout">
                <button
                    type="submit"
                    style="border: none; background: #fff;"
                >
                  <i class="fas fa-sign-out-alt pr-2"></i>
                  Cerrar Sesion
                </button>
              </form>
            </a>
          </div>
        </div>
      </c:if>
      <c:if test="${sessionScope.DtUsuario == null}">
        <div style="cursor: pointer;" class="pt-2">
          <a href="Login" style="outline: none; text-decoration: none">
            <span style="color: #fff; font-size: 18px; font-family: 'Nunito', sans-serif;">Iniciar Sesion</span>
          </a>
        </div>
      </c:if>
    </div>
  </nav>
  <!--Sidebar Menu-->
  <div id="wrapper" :class="{ 'menu-displayed' : menuToggler }">
    <!--Desktop View-->
    <div class="sidebar-wrapper d-none d-sm-block">
      <ul class="sidebar-nav pt-2">
        <li>
          <a href="Dash"><i class="fas fa-home pr-2"></i>Pagina Principal</a>
        </li>
      </ul>
      <hr>
      <c:if test="${sessionScope.DtUsuario != null}">
      <span class="pl-3" style="font-family: Nunito, sans-serif; font-size: 16px; color: #fff"><i
          class="fas fa-video pr-2"></i>VIDEOS</span>
        <ul class="sidebar-nav pt-2">
          <li>
            <a data-toggle="modal" data-target="#addVideo" class="pl-1"
               style="color: white !important; cursor: pointer;"><i
                class="fas fa-plus pr-2"></i>Subir Video</a>
          </li>
        </ul>
        <hr>
      </c:if>
      <c:if test="${sessionScope.DtUsuario != null}">
      <span class="pl-3" style="font-family: Nunito, sans-serif; font-size: 16px; color: #fff"><i
          class="fas fa-bars pr-2"></i>LISTAS</span>
        <ul class="sidebar-nav pt-2">
          <li>
            <a data-toggle="modal" data-target="#addLista" class="pl-1"
               style="color: white !important; cursor: pointer;"><i
                class="fas fa-plus pr-2"></i>Crear Lista</a>
          </li>
        </ul>
        <hr>
      </c:if>
      <span class="pl-3" style="font-family: Nunito, sans-serif; font-size: 16px; color: #fff"><i
          class="fas fa-tag pr-2"></i>CATEGORIAS</span>
      <ul class="sidebar-nav pt-2">
        <div v-for="(categoria, index) in categorias">
          <a :href='"Category?id="+categoria' style="outline: none; text-decoration: none; color: #fff;">
            <li>
              <span style="padding-left: 30px;">{{ categoria }}</span>
            </li>
          </a>
        </div>
      </ul>
    </div>
    <!--Mobile View-->
    <div class="sidebar-wrapper d-block d-sm-none">
      <ul class="sidebar-nav pt-2">
        <li>
          <a href="Dash"><i class="fas fa-home pr-2"></i>Pagina Principal</a>
        </li>
      </ul>
      <hr>
      <span class="pl-3" style="font-family: Nunito, sans-serif; font-size: 16px; color: #fff"><i
          class="fas fa-bars pr-2"></i>LISTAS</span>
      <ul class="sidebar-nav pt-2">
        <li>
          <a data-toggle="modal" data-target="#addLista" class="pl-1" style="color: white !important; cursor: pointer;"><i
              class="fas fa-plus pr-2"></i>Crear Lista</a>
        </li>
      </ul>
      <hr>
      <span class="pl-3" style="font-family: Nunito, sans-serif; font-size: 16px; color: #fff"><i
          class="fas fa-tag pr-2"></i>CATEGORIAS</span>
      <ul class="sidebar-nav pt-2">
        <li v-for="(categoria, index) in categorias">
          <a :href='"Category?id="+categoria' style="padding-left: 30px;">{{ categoria }}</a>
        </li>
      </ul>
    </div>
    <!-- Page Content -->
    <div class="page-content-wrapper" style="margin-top: 56px;">
      <!-- Profile Menu -->
      <jsp:doBody/>
    </div>
  </div>
  <!-- This is the add lista modal -->
  <div class="modal fade" id="addLista" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">CREAR LISTA</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="px-5 py-3">
            <input
                type="text"
                class="form-control"
                placeholder="Nombre"
                aria-label="Nombre"
                name="nombre"
                v-model="nombreLista"
            />
          </div>
          <div class="px-5 py-3 row">
            <div class="col-8">
              <select class="form-control form-control" v-model="categoriaLista">
                <option disabled value="">Eligir categoria</option>
                <option v-for="categoria in categorias">{{ categoria }}</option>
              </select>
            </div>
            <div class="col-4 pt-2">
              <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="switch" v-model="privacidadLista">
                <label class="custom-control-label pr-3" for="switch"
                       style="font-family: Nunito,sans-serif; font-size: 16px;">Privada</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancelar</button>
          <button @click.prevent="crearLista" type="button" class="btn btn-outline-success">Confirmar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- This is the add video  modal -->
  <div class="modal fade" id="addVideo" tabindex="-1" role="dialog"
       aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">CREAR VIDEO</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="px-5 py-3">
            <input
                type="text"
                class="form-control"
                placeholder="Nombre"
                aria-label="Nombre"
                name="nombre"
                v-model="newVideo.nombre"
                v-bind:class="{'form-control':true, 'is-invalid' : !checkEmpty(newVideo.nombre) && newVideo.nameSelected}"
                v-on:blur="newVideo.nameSelected = true">
          </div>
          <div class="px-5 py-3">
            <input
                type="text"
                class="form-control"
                placeholder="URL"
                aria-label="URL"
                name="url"
                v-model="newVideo.url"
                v-bind:class="{'form-control':true, 'is-invalid' : !checkEmpty(newVideo.url) && newVideo.urlSelected}"
                v-on:blur="newVideo.urlSelected = true">
          </div>
          <div class="px-5 py-3 row">
            <div class="col-7">
              <select v-model="newVideo.categoria" class="form-control form-control">
                <option disabled value="">Eligir categoria</option>
                <option v-for="categoria in categorias">{{ categoria }}</option>
              </select>
            </div>
            <div class="col-5">
              <input type="text"
                     v-model="newVideo.duracion"
                     v-bind:class="{'form-control':true, 'is-invalid' : !checkDuration(newVideo.duracion) && newVideo.timeSelected}"
                     v-on:blur="newVideo.timeSelected = true">
            </div>
          </div>
          <div class="row px-5 py-3">
            <div class="col-8">
              <input type="date" class="form-control" min="1900-01-01" max="2020-01-01"
                     v-model="newVideo.fechaPublicacion"
                     v-bind:class="{'form-control':true, 'is-invalid' : !validDate(newVideo.fechaPublicacion) && newVideo.dateSelected}"
                     v-on:blur="newVideo.dateSelected = true">
            </div>
            <div class="col-4">
              <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" id="switch2" v-model="newVideo.privado">
                <label class="custom-control-label pr-3" for="switch2"
                       style="font-family: Nunito,sans-serif; font-size: 16px;">Privado</label>
              </div>
            </div>
          </div>
          <div class="px-5 py-3">
            <textarea rows="3" class="form-control" placeholder="Descripcion" v-model="newVideo.descripcion"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Cancelar</button>
          <button @click.prevent="crearVideo" type="button" class="btn btn-outline-success">Confirmar</button>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

<script>
  Vue.use(Toasted,
    {
      action: {
        text: 'X',
        onClick: (e, toastObject) => {
          toastObject.goAway(0);
        }
      },
      position: 'top-right',
      duration: 4000
    }
  );
</script>

<style scoped>

  .toasted-container .toasted {
    top: 0px !important;
    right: -100px !important;
  }

  .toasted.toasted-primary .action {
    color: #0200019e;
  }

  .btn-logout button:hover {
    background: #EBEBEB !important;
  }

  .profile-menu {
    position: absolute;
    top: 5px;
    right: 5px;
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
    0 6px 20px 0 rgba(0, 0, 0, 0.19);
    z-index: 10;
  }

  .profile-menu-item {
    padding: 0;
    margin: 0 auto;
    list-style: none;
  }

  .profile-menu-item li {
    padding: 0 15px 0 15px;
    line-height: 40px;
  }

  .profile-menu-item li a {
    width: 100%;
    margin: 0 auto;
    text-decoration: none;
    font-weight: 500;
    font-family: "Nunito", sans-serif;
    font-size: 16px;
  }

  .profile-menu-item li:hover {
    background: #bdbdbd4f;
  }

  .profile-menu-item li:hover {
    background: #bdbdbd4f;
  }

  .search-input {
    width: 350px !important;
    box-shadow: none !important;
  }

  .sidebar-wrapper {
    z-index: 1;
    position: fixed;
    width: 0;
    top: 56px;
    height: 100%;
    overflow-y: hidden;
    background: #423a6f;
    box-shadow: 0 1px 15px 1px #352f5b;
    transition: all 0.05s ease-in;
  }

  @media (max-width: 768px) {
    #wrapper.menu-displayed .sidebar-wrapper {
      width: 100% !important;
    }
  }

  .btn {
    box-shadow: none !important;
  }

  .navbar {
    height: 56px;
    box-shadow: 0 1px 12px 2px #352f5b;
  }

  .page-content-wrapper {
    width: 100%;
    position: absolute;
    padding: 15px;
  }

  .avatar {
    vertical-align: middle;
    width: 45px;
    height: 45px;
    border-radius: 50%;
  }

  .bg-dark-purple {
    background-color: #352f5b;
  }

  #wrapper.menu-displayed .sidebar-wrapper {
    width: 250px;
  }

  #wrapper.menu-displayed .page-content-wrapper {
    padding-left: 265px;
  }

  .sidebar-nav {
    padding: 0;
    margin: 0 auto;
    list-style: none;
  }

  .sidebar-nav li {
    line-height: 40px;
    text-indent: 10px;
  }

  .sidebar-nav li a {
    width: 100%;
    margin: 0 auto;
    text-decoration: none;
    font-weight: 500;
    color: #fff;
    font-family: "Nunito", sans-serif;
    font-size: 16px;
  }

  .sidebar-nav li:hover {
    background: #0000004a;
  }

  .dropdown-menu {
    left: -50px;
  }

  .fix-bg:hover  button {
    background: #F8F9FA !important;
  }

  .fix-bg button:hover {
    background: #F8F9FA !important;
  }

</style>
