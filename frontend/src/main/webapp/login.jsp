<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
  <!-- CSS File link -->
  <link rel="stylesheet" type="text/css" href="styles/style.css"/>

  <!-- Vue.js Script cdn -->
  <script src="js/vendor/vue.js"></script>

  <!-- Toast Script cdn -->
  <script src="js/vendor/vue-toasted.min.js"></script>

  <!-- Axios Script cdn -->
  <script src="js/vendor/axios.js"></script>

  <!-- qs Script cdn -->
  <script src="js/vendor/qs.js"></script>

  <!-- Boostrap link-->

  <script src="js/vendor/jquery3.3.1.js"></script>
  <script src="js/vendor/popper.js"></script>
  <script src="js/vendor/bootstrap.js"></script>
  <link rel="stylesheet" type="text/css" href="styles/vendor/bootstrap.min.css"/>

  <!-- Fontawesome link-->
  <script src="js/vendor/fontawesome.js"></script>
  <link href="fontawesome/css/solid.min.css" rel="stylesheet">

  <!-- Nunito link-->
  <link
      rel="stylesheet"
      type="text/css"
      href="styles/vendor/nunito.css"
  />
  <title>UyTube</title>
</head>
<body>
<div id="app" class="container-fluid h-100">
  <div style="position: absolute; top: 25%; padding-left: 5%;" class="slide-bottom text-center">
    <img src="assets/logo.png" style="max-width: 100%; height: 160px;">
    <span class="pt-5" style="color: #fff; font-size: 4vw; font-family: Nunito,sans-serif; position: absolute;">
      UyTube</span>
  </div>
  <div class="row h-100 justify-content-center align-items-center">
    <div
        class="card card-style text-center"
        style="padding-top: 80px;"
    >
      <form method="post" action="Login">
        <div
            class="input-group pt-4 px-5"
        >
          <i class="fas fa-user pt-3 pr-2" style="color: #353535;"></i>
          <input
              type="text"
              class="form-control"
              placeholder="Username"
              aria-label="Username"
              v-model="user.nickname"
              name="nickname"
          />
        </div>
        <div
            class="input-group py-4 px-5"
        >
          <i class="fas fa-lock pt-3 pr-2" style="color: #353535;"></i>
          <input
              type="password"
              class="form-control"
              placeholder="Password"
              aria-label="Password"
              name="password"
              v-model="user.password"
          />
        </div>
        <div style="cursor: pointer;">
          <a href="SignIn" style="outline: none; text-decoration: none">
            <span style="font-size: 18px; font-family: 'Nunito', sans-serif;">Crear una cuenta</span>
          </a>
        </div>
        <div style="text-align: end;" class="pt-4 pr-2">
          <button
              type="submit"
              class="btn text-light"
              style="background-color: #dc3545; border-radius: 8px; font-family: 'Nunito', sans-serif;"
          >
            Iniciar Sesion
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
</body>
</html>

<script src="js/Toolbar.js"></script>
<script>
  Vue.use(Toasted,
    {
      action: {
        text: 'X',
        onClick: (e, toastObject) => {
          toastObject.goAway(0);
        }
      },
      position: 'top-right',
      duration: 4000
    }
  );
  const app = new Vue({
    el: "#app",
    data: {
      user: {
        nickname: "",
        password: ""
      }
    },
    methods: {},
    created() {
      if ('${message}') {
        this.$toasted.error('${message}');
      }
    }
  });
</script>

<style scoped>
  body,
  html {
    height: 100%;
    background-image: url("assets/bg.png");
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
    background-attachment: fixed;
  }

  .card-style {
    width: 550px;
    border-radius: 8px;
    border-top: 5px solid #dc3545;
    box-shadow: 10px 10px 43px 0px rgba(0, 0, 0, 0.55);
    padding: 30px 30px 30px 30px;
  }

  .toasted-container .toasted {
    top: -50px !important;
    right: -100px !important;
  }

  .toasted.toasted-primary .action {
    color: #0200019e;
  }

  .slide-bottom {
    -webkit-animation: slide-bottom 1s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;
    animation: slide-bottom 1s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;
  }

  @-webkit-keyframes slide-bottom {
    0% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
    }
    100% {
      -webkit-transform: translateY(100px);
      transform: translateY(100px);
    }
  }

  @keyframes slide-bottom {
    0% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
    }
    100% {
      -webkit-transform: translateY(100px);
      transform: translateY(100px);
    }
  }
</style>
