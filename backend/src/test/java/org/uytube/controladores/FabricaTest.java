package org.uytube.controladores;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FabricaTest {

  @Test
  void getControllerOfVideo() {
    assertTrue(Fabrica.getControllerOf(Fabrica.Controller.VIDEO) instanceof IVideo);
  }

  @Test
  void getControllerOfLista() {
    assertTrue(Fabrica.getControllerOf(Fabrica.Controller.LISTA) instanceof ILista);
  }

  @Test
  void getControllerOfUsuario() {
    assertTrue(Fabrica.getControllerOf(Fabrica.Controller.USUARIO) instanceof IUsuario);
  }

  @Test
  void getControllerOfCategoria() {
    assertTrue(Fabrica.getControllerOf(Fabrica.Controller.CATEOGRIA) instanceof ICategoria);
  }
}
