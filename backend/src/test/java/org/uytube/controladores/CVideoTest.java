package org.uytube.controladores;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.uytube.datatypes.DtVideo;
import org.uytube.exception.YonaException;
import org.uytube.manejador.Persistencia;
import org.uytube.modelos.*;

class CVideoTest {
  private final CVideo instance = new CVideo();
  private final EntityManager entityManager = mock(EntityManager.class, RETURNS_DEEP_STUBS);

  @BeforeEach
  void setUp() {
    Persistencia.getInstance().setEntityManager(entityManager);
    when(entityManager.isOpen()).thenReturn(true);
  }

  @Test
  void crearVideo() {
    DtVideo dt = new DtVideo("Video1", "url", "desc", new Date(), 100, true);
    Usuario user1 =
        new Usuario(
            "user1",
            "admin",
            "nopic",
            "mathias",
            "zunino",
            "toto@toto.com",
            new Date(),
            new Canal("canal", "descripcion", false));
    when(entityManager.find(Usuario.class, user1.getNickname())).thenReturn(user1);
    instance.crearVideo(dt, user1.getNickname());
  }

  @Test
  void crearVideoConCategoria() {
    Categoria categoria = new Categoria("categoria");
    DtVideo dt = new DtVideo("Video1", "url", "desc", new Date(), categoria.getNombre(), 100, true);
    Usuario user1 =
        new Usuario(
            "user1",
            "admin",
            "nopic",
            "mathias",
            "zunino",
            "toto@toto.com",
            new Date(),
            new Canal("canal", "descripcion", false));
    when(entityManager.find(Usuario.class, user1.getNickname())).thenReturn(user1);
    instance.crearVideo(dt, user1.getNickname());
  }

  @Test
  void crearVideoUsuarioInvalido() {
    DtVideo dt = new DtVideo("Video1", "url", "desc", new Date(), 100, true);
    assertThrows(YonaException.class, () -> instance.crearVideo(dt, "tito"));
  }

  @Test
  void crearVideoUsuarioFailPersist() {
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    doThrow(RollbackException.class).when(entityTransaction).begin();
    DtVideo dt = new DtVideo("Video1", "url", "desc", new Date(), 100, true);
    Usuario user1 =
        new Usuario(
            "user1",
            "admin",
            "nopic",
            "mathias",
            "zunino",
            "toto@toto.com",
            new Date(),
            new Canal("canal", "descripcion", false));
    when(entityManager.find(Usuario.class, user1.getNickname())).thenReturn(user1);
    assertThrows(YonaException.class, () -> instance.crearVideo(dt, "user1"));
  }

  @Test
  void listarVideosUsuario() {
    Categoria categoria = new Categoria("categoria");
    Usuario user1 =
        new Usuario(
            "user1",
            "admin",
            "nopic",
            "mathias",
            "zunino",
            "toto@toto.com",
            new Date(),
            new Canal("canal", "descripcion", false));
    when(entityManager.find(Usuario.class, user1.getNickname())).thenReturn(user1);

    List<Object[]> result = new ArrayList<>();
    Object[] video = new Object[2];
    video[0] = 1;
    video[1] = "user1";
    result.add(video);
    when(entityManager
            .createNamedQuery("Video.videosUsuario", Object[].class)
            .setParameter("nick", user1.getNickname())
            .getResultList())
        .thenReturn(result);
    assertEquals(instance.listarVideosUsuario(user1.getNickname()).get(1), user1.getNickname());
  }

  @Test
  void modificarVideo() {
    Categoria categoria = new Categoria("categoria");
    DtVideo dt = new DtVideo("video2", "url", "desc", new Date(), "categoria", 100, true);
    Video video = new Video("video", "descripcion", 10, new Date(), "url", false, null);
    video.setId(1);
    when(entityManager.find(Categoria.class, "categoria")).thenReturn(categoria);
    when(entityManager.find(Video.class, 1)).thenReturn(video);
    when(entityManager
            .createNamedQuery("Video.existeVideoEnCanal", Video.class)
            .setParameter("nombreVideo", dt.getNombre())
            .setParameter("idVideo", video.getId())
            .setParameter("canal", video.getCanal())
            .getResultList())
        .thenReturn(new ArrayList<>());
    assertNotEquals(video.getNombre(), dt.getNombre());
    instance.modificarVideo(1, dt);
    assertEquals(video.getNombre(), dt.getNombre());
  }

  @Test
  void modificarVideoDuplicado() {
    Categoria categoria = new Categoria("categoria");
    DtVideo dt = new DtVideo("video2", "url", "desc", new Date(), "categoria", 100, true);
    Video video = new Video("video", "descripcion", 10, new Date(), "url", false, null);
    video.setId(1);
    when(entityManager.find(Categoria.class, "categoria")).thenReturn(categoria);
    when(entityManager.find(Video.class, 1)).thenReturn(video);
    List<Video> videos = new ArrayList<>();
    videos.add(new Video());
    when(entityManager
            .createNamedQuery("Video.existeVideoEnCanal", Video.class)
            .setParameter("nombreVideo", dt.getNombre())
            .setParameter("idVideo", video.getId())
            .setParameter("canal", video.getCanal())
            .getResultList())
        .thenReturn(videos);
    assertThrows(YonaException.class, () -> instance.modificarVideo(1, dt));
  }

  @Test
  void modificarVideoFallaTransaction() {
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    doThrow(RollbackException.class).when(entityTransaction).begin();

    Categoria categoria = new Categoria("categoria");
    DtVideo dt = new DtVideo("video2", "url", "desc", new Date(), "categoria", 100, true);
    Video video = new Video("video", "descripcion", 10, new Date(), "url", false, null);
    video.setId(1);
    when(entityManager.find(Categoria.class, "categoria")).thenReturn(categoria);
    when(entityManager.find(Video.class, 1)).thenReturn(video);
    List<Video> videos = new ArrayList<>();
    when(entityManager
            .createNamedQuery("Video.existeVideoEnCanal", Video.class)
            .setParameter("nombreVideo", dt.getNombre())
            .setParameter("idVideo", video.getId())
            .setParameter("canal", video.getCanal())
            .getResultList())
        .thenReturn(videos);
    assertThrows(YonaException.class, () -> instance.modificarVideo(1, dt));
  }
}
