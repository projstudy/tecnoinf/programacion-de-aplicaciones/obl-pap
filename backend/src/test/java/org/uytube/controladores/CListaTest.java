package org.uytube.controladores;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.uytube.datatypes.DtLista;
import org.uytube.exception.YonaException;
import org.uytube.manejador.Persistencia;
import org.uytube.modelos.*;

/** @author e974616 */
class CListaTest {

  private final CLista instance = new CLista();
  private final EntityManager entityManager = mock(EntityManager.class, RETURNS_DEEP_STUBS);

  CListaTest() {}

  @BeforeEach
  void setUp() {
    Persistencia.getInstance().setEntityManager(entityManager);
    when(entityManager.isOpen()).thenReturn(true);
  }

  /** Test of altaListaDefecto method, of class CLista. */
  @Test
  void testAltaListaDefectoDuplicada() {
    when(entityManager.find(Lista.class, "Deportes")).thenReturn(new Lista());
    Assertions.assertThrows(YonaException.class, () -> instance.altaListaDefecto("Deportes"));
  }

  /** Test of altaListaDefecto method, of class CLista. */
  @Test
  void testAltaListaDefecto() {
    List<Canal> canales = new ArrayList<>();
    canales.add(new Canal());
    when(entityManager.createNamedQuery("Canal.listarCanales", Canal.class).getResultList())
        .thenReturn(canales);
    when(entityManager
            .createNamedQuery("Lista.buscarPorNombre", Lista.class)
            .setParameter("nombreLista", "Deportes")
            .getResultList())
        .thenReturn(new ArrayList<>());
    instance.altaListaDefecto("Deportes");
  }

  /** Test of altaListaDefecto method, of class CLista. fail persist */
  @Test
  void testAltaListaDefectoFailPersist() {
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.createNamedQuery("Canal.listarCanales", Canal.class).getResultList())
        .thenReturn(new ArrayList<>());
    when(entityManager
            .createNamedQuery("Lista.buscarPorNombre", Lista.class)
            .setParameter("nombreLista", "Deportes")
            .getResultList())
        .thenReturn(new ArrayList<>());
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    doThrow(RollbackException.class).when(entityTransaction).begin();
    Assertions.assertThrows(YonaException.class, () -> instance.altaListaDefecto("Deportes"));
  }

  /** Test of altaListaParticular method, of class CLista. */
  @Test
  void testAltaListaParticularCategoriaNoExistente() {
    Usuario u1 = new Usuario();
    u1.setNickname("toto");
    u1.setCanal(new Canal());
    DtLista dtLista = new DtLista("miLista", false, "Deportes");
    when(entityManager.find(Categoria.class, "Deportes")).thenReturn(null);
    Assertions.assertThrows(
        IllegalArgumentException.class, () -> instance.altaListaParticular("toto", dtLista));
  }

  /** Test of altaListaParticular method, of class CLista. */
  @Test
  void testAltaListaParticularUsuarioInvalido() {
    Usuario u1 = new Usuario();
    u1.setNickname("toto");
    u1.setCanal(new Canal());
    DtLista dtLista = new DtLista("miLista", false);
    when(entityManager.find(Usuario.class, "toto")).thenReturn(null);
    Assertions.assertThrows(
        IllegalArgumentException.class, () -> instance.altaListaParticular("toto", dtLista));
  }

  /** Test of altaListaParticular method, of class CLista. */
  @Test
  void testAltaListaParticularUsuarioValido() {
    Usuario u1 = new Usuario();
    u1.setNickname("toto");
    Canal c = new Canal("toto", "toto", true);
    u1.setCanal(c);
    DtLista dtLista = new DtLista("miLista", false);
    when(entityManager.find(Usuario.class, "toto")).thenReturn(u1);
    instance.altaListaParticular("toto", dtLista);
  }

  /** Test of altaListaParticular method, of class CLista. */
  @Test
  void testAltaListaParticularDuplicada() {
    Usuario u1 = new Usuario();
    u1.setNickname("toto");
    Canal c = new Canal("toto", "toto", true);
    c.addLista(new Lista("miLista"));
    u1.setCanal(c);
    DtLista dtLista = new DtLista("miLista", false);
    when(entityManager.find(Usuario.class, "toto")).thenReturn(u1);
    Assertions.assertThrows(
        IllegalArgumentException.class, () -> instance.altaListaParticular("toto", dtLista));
  }

  /** Test of altaListaParticular method, of class CLista. */
  @Test
  void testAltaListaParticularFailPersist() {
    Usuario u1 = new Usuario();
    u1.setNickname("toto");
    Canal c = new Canal("toto", "toto", true);
    u1.setCanal(c);
    DtLista dtLista = new DtLista("miLista", false);
    doThrow(RollbackException.class).when(entityManager).persist(c);
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    doThrow(RollbackException.class).when(entityTransaction).begin();
    when(entityManager.getTransaction().isActive()).thenReturn(true);
    when(entityManager.find(Usuario.class, "toto")).thenReturn(u1);
    Assertions.assertThrows(
        YonaException.class, () -> instance.altaListaParticular("toto", dtLista));
  }

  /** Test of listasParticularesUsuario method, of class CLista. */
  @Test
  void testListasParticularesUsuario() {
    Usuario toto = mock(Usuario.class, RETURNS_DEEP_STUBS);
    Canal c = new Canal("", "", false);
    List<Lista> expected = new ArrayList<>();
    Lista l = new Lista();
    l.setCategoria(new Categoria());
    l.setCanal(c);
    expected.add(l);
    when(entityManager.find(Usuario.class, "toto")).thenReturn(toto);
    when(entityManager
            .createNamedQuery("Canal.getParticulares", Lista.class)
            .setParameter("nombreCanal", toto.getCanal().getNombre())
            .setParameter("clases", Collections.singletonList(Predeterminada.class))
            .getResultList())
        .thenReturn(expected);
    assertFalse(instance.listasParticularesUsuario("toto").isEmpty());
  }

  @Test
  void testListasParticularesUsuarioInvalidUsuario() {
    when(entityManager.find(Usuario.class, "toto")).thenReturn(null);
    Assertions.assertThrows(YonaException.class, () -> instance.listasParticularesUsuario("toto"));
  }

  /** Test of modificarLista method, of class CLista. */
  @Test
  void testModificarLista() {
    Lista lista = new Lista();
    Categoria categoria = new Categoria("cate");
    lista.setCategoria(categoria);
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    when(entityManager.find(Categoria.class, "cate")).thenReturn(categoria);
    instance.modificarLista(true, "cate", 1);
    assertEquals(lista.getCategoria(), categoria);
  }

  /** Test of modificarLista method, of class CLista. */
  @Test
  void testModificarListaFailCommit() {
    Lista lista = new Lista();
    Categoria categoria = new Categoria("cate");
    lista.setCategoria(categoria);
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    when(entityManager.find(Categoria.class, "cate")).thenReturn(categoria);
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    doThrow(RollbackException.class).when(entityTransaction).commit();
    Assertions.assertThrows(YonaException.class, () -> instance.modificarLista(true, "cate", 1));
  }

  /** Test of modificarLista method, of class CLista. */
  @Test
  void testModificarListaInvalidCategoria() {
    Lista lista = new Lista();
    Categoria categoria = new Categoria("cate");
    lista.setCategoria(categoria);
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    when(entityManager.find(Categoria.class, "cate")).thenReturn(null);
    Assertions.assertThrows(YonaException.class, () -> instance.modificarLista(true, "cate", 1));
  }

  /** Test of modificarLista method, of class CLista. */
  @Test
  void testModificarListaInvalidLista() {
    when(entityManager.find(Lista.class, 1)).thenReturn(null);
    Assertions.assertThrows(YonaException.class, () -> instance.modificarLista(true, "cate", 1));
  }

  /** Test of listasDeUsuario method, of class CLista. */
  @Test
  void testListasDeUsuario() {
    Usuario u = new Usuario();
    u.setNickname("toto");
    Canal c = new Canal();
    c.setNombre("toto");
    Lista l = new Lista();
    l.setCategoria(new Categoria());
    l.setId(1);
    l.setCanal(c);
    c.addLista(l);
    u.setCanal(c);
    List<Lista> result = new ArrayList<>();
    result.add(l);
    when(entityManager
            .createNamedQuery("Lista.listasUsuario", Lista.class)
            .setParameter("parmNickName", "toto")
            .getResultList())
        .thenReturn(result);
    ArrayList<DtLista> expected = new ArrayList<>();
    expected.add(Mapper.getDtLista(l));
    assertEquals(expected.size(), instance.listasDeUsuario("toto").size());
  }

  /** Test of listasDeUsuario method, of class CLista. */
  @Test
  void testListasDeUsuarioInexistente() {
    assertTrue(instance.listasDeUsuario("toto").isEmpty());
  }
  //

  /** Test of agregarVideoLista method, of class CLista. */
  @Test
  void testAgregarVideoLista() {
    Video v = mock(Video.class);
    Lista lista = new Lista();
    when(entityManager.find(Video.class, 1)).thenReturn(v);
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    assertTrue(lista.getVideos().isEmpty());
    instance.agregarVideoLista(1, 1);
    assertFalse(lista.getVideos().isEmpty());
  }

  /** Test of agregarVideoLista method, of class CLista. */
  @Test
  void testAgregarVideoListaFailPersist() {
    Video v = mock(Video.class);
    Lista lista = new Lista();
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    when(entityManager.find(Video.class, 1)).thenReturn(v);
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    doThrow(RollbackException.class).when(entityTransaction).commit();
    //    assertThrows(YonaException.class, () -> instance.agregarVideoLista(1, 1));
  }

  /** Test of agregarVideoLista method, of class CLista. */
  @Test
  void testAgregarVideoListaListaNotFound() {
    assertThrows(YonaException.class, () -> instance.agregarVideoLista(1, 1));
  }

  @Test
  void testAgregarVideoListaVideoNotFound() {
    Lista lista = new Lista();
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    assertThrows(YonaException.class, () -> instance.agregarVideoLista(1, 1));
  }

  /** Test of quitarVideoLista method, of class CLista. */
  @Test
  void testQuitarVideoLista() {
    Video v = mock(Video.class);
    Lista lista = new Lista();
    lista.addVideo(v);
    when(entityManager.find(Video.class, 1)).thenReturn(v);
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    assertFalse(lista.getVideos().isEmpty());
    instance.quitarVideoLista(1, 1);
    assertTrue(lista.getVideos().isEmpty());
  }

  /** Test of quitarVideoLista method, of class CLista. */
  @Test
  void testQuitarVideoListaFailPersist() {
    Video v = mock(Video.class);
    Lista lista = new Lista();
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    when(entityManager.find(Video.class, 1)).thenReturn(v);
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    doThrow(RollbackException.class).when(entityTransaction).commit();
    assertThrows(YonaException.class, () -> instance.quitarVideoLista(1, 1));
  }

  /** Test of quitarVideoLista method, of class CLista. */
  @Test
  void testQuitarVideoListaNotFound() {
    assertThrows(YonaException.class, () -> instance.quitarVideoLista(1, 1));
  }

  @Test
  void testQuitarVideoListaVideoNotFound() {
    Lista lista = new Lista();
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    assertThrows(YonaException.class, () -> instance.quitarVideoLista(1, 1));
  }

  /** Test of listarVideos method, of class CLista. */
  @Test
  void testListarVideos() {
    Video v = new Video();
    v.setNombre("video1");
    Lista lista = new Lista();
    lista.setId(1);
    lista.addVideo(v);
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    assertEquals(instance.listarVideos(1).get(0), "video1");
  }

  @Test
  void testListarVideosNotFound() {
    assertThrows(YonaException.class, () -> instance.listarVideos(1));
  }

  /** Test of consultaLista method, of class CLista. */
  @Test
  void testConsultaLista() {
    Video v = new Video();
    v.setNombre("video1");
    Lista lista = new Lista("lista1");
    lista.setCanal(new Canal("", "", false));
    lista.setId(1);
    lista.setCategoria(new Categoria());
    lista.addVideo(v);
    when(entityManager.find(Lista.class, 1)).thenReturn(lista);
    assertEquals("lista1", instance.consultaLista(1).getNombre());
  }

  /** Test of consultaLista method, of class CLista. */
  @Test
  void testConsultaListaNotFound() {
    assertThrows(YonaException.class, () -> instance.consultaLista(1));
  }
}
