package org.uytube.controladores;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.uytube.datatypes.DtCanal;
import org.uytube.datatypes.DtModUsuario;
import org.uytube.datatypes.DtUsuario;
import org.uytube.exception.YonaException;
import org.uytube.manejador.Persistencia;
import org.uytube.modelos.Canal;
import org.uytube.modelos.ListaPredeterminada;
import org.uytube.modelos.Usuario;

class CUsuarioTest {
  private final CUsuario instance = new CUsuario();
  private final EntityManager entityManager = mock(EntityManager.class, RETURNS_DEEP_STUBS);

  @BeforeEach
  void setUp() {
    Persistencia.getInstance().setEntityManager(entityManager);
    when(entityManager.isOpen()).thenReturn(true);
  }

  @Test
  void altausuario() {
    DtUsuario dtUsuario =
        new DtUsuario("toto", "Mathias", "Zunino", "toto@mzunino.com", "toto", new Date());
    DtCanal dtCanal = new DtCanal("canalToto", "descripcion canal toto", false);
    List<ListaPredeterminada> listas = new ArrayList<>();
    listas.add(new ListaPredeterminada("lista"));
    when(entityManager
            .createNamedQuery("ListaPredeterminada.listar", ListaPredeterminada.class)
            .getResultList())
        .thenReturn(listas);
    when(entityManager.find(Canal.class, dtCanal.getNombre())).thenReturn(null);
    when(entityManager
            .createNamedQuery("Usuario.verificoNickYMail", Usuario.class)
            .setParameter("parmEmail", dtUsuario.getEmail())
            .setParameter("parmNickname", dtUsuario.getNickname())
            .getResultList())
        .thenReturn(new ArrayList<>());
    instance.altausuario(dtUsuario, dtCanal);
  }

  @Test
  void altausuarioNullCategoria() {
    DtUsuario dtUsuario =
        new DtUsuario("toto", "Mathias", "Zunino", "toto@mzunino.com", "toto", new Date());
    DtCanal dtCanal = new DtCanal("canalToto", "descripcion canal toto", false);
    when(entityManager.find(Canal.class, dtCanal.getNombre())).thenReturn(new Canal());
    assertThrows(YonaException.class, () -> instance.altausuario(dtUsuario, dtCanal));
  }

  @Test
  void altausuarioDuplicado() {
    DtUsuario dtUsuario =
        new DtUsuario("toto", "Mathias", "Zunino", "toto@mzunino.com", "toto", new Date());
    DtCanal dtCanal = new DtCanal("canalToto", "descripcion canal toto", false);
    when(entityManager.find(Canal.class, dtCanal.getNombre())).thenReturn(null);
    List<Usuario> usuarios = new ArrayList<>();
    usuarios.add(new Usuario());
    when(entityManager
            .createNamedQuery("Usuario.verificoNickYMail", Usuario.class)
            .setParameter("parmEmail", dtUsuario.getEmail())
            .setParameter("parmNickname", dtUsuario.getNickname())
            .getResultList())
        .thenReturn(usuarios);
    assertThrows(YonaException.class, () -> instance.altausuario(dtUsuario, dtCanal));
  }

  @Test
  void altausuarioFailCommit() {
    DtUsuario dtUsuario =
        new DtUsuario("toto", "Mathias", "Zunino", "toto@mzunino.com", "toto", new Date());
    DtCanal dtCanal = new DtCanal("canalToto", "descripcion canal toto", false);

    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    doThrow(RollbackException.class).when(entityTransaction).commit();
    when(entityManager.find(Canal.class, dtCanal.getNombre())).thenReturn(null);
    when(entityManager
            .createNamedQuery("Usuario.verificoNickYMail", Usuario.class)
            .setParameter("parmEmail", dtUsuario.getEmail())
            .setParameter("parmNickname", dtUsuario.getNickname())
            .getResultList())
        .thenReturn(new ArrayList<>());

    assertThrows(YonaException.class, () -> instance.altausuario(dtUsuario, dtCanal));
  }

  @Test
  void listarUsuarios() {
    when(entityManager.createNamedQuery("Usuario.getUsuarios", String.class).getResultList())
        .thenReturn(Arrays.asList("toto", "juan"));
    assertTrue(instance.listarUsuarios().contains("toto"));
  }

  @Test
  void consultaDeUsuario() {
    Usuario u =
        new Usuario(
            "toto",
            "admin",
            "nopic",
            "mathias",
            "zunino",
            "toto@toto.com",
            new Date(),
            new Canal("canal", "descripcion", false));
    when(entityManager.find(Usuario.class, "toto")).thenReturn(u);
    assertEquals(instance.consultaDeUsuario("toto").getUsuario().getNickname(), "toto");
  }

  @Test
  void consultaDeUsuarioInexistente() {
    assertThrows(YonaException.class, () -> instance.consultaDeUsuario("toto"));
  }

  @Test
  void modificarUsuario() {
    Usuario u =
        new Usuario(
            "toto",
            "admin",
            "nopic",
            "mathias",
            "zunino",
            "toto@toto.com",
            new Date(),
            new Canal("canal", "descripcion", false));
    DtModUsuario dtModUsuario =
        new DtModUsuario("Mathias", "Zunino", "some.png", new Date(), false);
    assertNotEquals(u.getNombre(), dtModUsuario.getNombre());
    when(entityManager.find(Usuario.class, "toto")).thenReturn(u);
    instance.modificarUsuario("toto", dtModUsuario);
    assertEquals(u.getNombre(), dtModUsuario.getNombre());
  }

  @Test
  void modificarUsuarioInexistente() {
    Usuario u =
        new Usuario(
            "toto",
            "admin",
            "nopic",
            "mathias",
            "zunino",
            "toto@toto.com",
            new Date(),
            new Canal("canal", "descripcion", false));
    DtModUsuario dtModUsuario =
        new DtModUsuario("Mathias", "Zunino", "some.png", new Date(), false);
    assertThrows(YonaException.class, () -> instance.modificarUsuario("toto", dtModUsuario));
  }

  @Test
  void modificarUsuarioFailPersist() {
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    doThrow(RollbackException.class).when(entityTransaction).begin();
    Usuario u =
        new Usuario(
            "toto",
            "admin",
            "nopic",
            "mathias",
            "zunino",
            "toto@toto.com",
            new Date(),
            new Canal("canal", "descripcion", false));
    DtModUsuario dtModUsuario =
        new DtModUsuario("Mathias", "Zunino", "some.png", new Date(), false);
    assertNotEquals(u.getNombre(), dtModUsuario.getNombre());
    when(entityManager.find(Usuario.class, "toto")).thenReturn(u);
    assertThrows(YonaException.class, () -> instance.modificarUsuario("toto", dtModUsuario));
    assertNotEquals(u.getNombre(), dtModUsuario.getNombre());
  }

  @Test
  void dejarSeguirUsuario() {
    Usuario user1 = new Usuario();
    Usuario user2 = new Usuario();
    user1.setNickname("user1");
    user2.setNickname("user2");
    user2.addSuscripctor(user1);
    when(entityManager.find(Usuario.class, "user1")).thenReturn(user1);
    when(entityManager.find(Usuario.class, "user2")).thenReturn(user2);
    instance.dejarSeguirUsuario(user1.getNickname(), user2.getNickname());
    assertTrue(user2.getSuscriptores().isEmpty());
  }

  @Test
  void dejarSeguirUsuarioFailTransaction() {
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    doThrow(RollbackException.class).when(entityTransaction).commit();
    Usuario user1 = new Usuario();
    Usuario user2 = new Usuario();
    user1.setNickname("user1");
    user2.setNickname("user2");
    user2.addSuscripctor(user1);
    when(entityManager.find(Usuario.class, "user1")).thenReturn(user1);
    when(entityManager.find(Usuario.class, "user2")).thenReturn(user2);
    assertThrows(
        YonaException.class,
        () -> instance.dejarSeguirUsuario(user1.getNickname(), user2.getNickname()));
  }

  @Test
  void dejarSeguirUsuarioNllSeguido() {
    Usuario user1 = new Usuario();
    Usuario user2 = new Usuario();
    user1.addSuscripctor(user2);
    user1.setNickname("user1");
    user2.setNickname("user2");
    assertThrows(
        YonaException.class,
        () -> instance.dejarSeguirUsuario(user1.getNickname(), user2.getNickname()));
  }

  @Test
  void dejarSeguirUsuarioNullSeguidor() {
    Usuario user1 = new Usuario();
    Usuario user2 = new Usuario();
    user1.addSuscripctor(user2);
    user1.setNickname("user1");
    user2.setNickname("user2");
    when(entityManager.find(Usuario.class, "user1")).thenReturn(user1);
    assertThrows(
        YonaException.class,
        () -> instance.dejarSeguirUsuario(user1.getNickname(), user2.getNickname()));
  }

  @Test
  void seguirUsuario() {
    Usuario user1 = new Usuario();
    Usuario user2 = new Usuario();
    user1.setNickname("user1");
    user2.setNickname("user2");
    when(entityManager.find(Usuario.class, "user1")).thenReturn(user1);
    when(entityManager.find(Usuario.class, "user2")).thenReturn(user2);
    instance.seguirUsuario(user1.getNickname(), user2.getNickname());
    assertFalse(user2.getSuscriptores().isEmpty());
  }

  @Test
  void seguirUsuarioFailTransaction() {
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    when(entityTransaction.isActive()).thenReturn(true);
    doThrow(RollbackException.class).when(entityTransaction).commit();

    Usuario user1 = new Usuario();
    Usuario user2 = new Usuario();
    user1.setNickname("user1");
    user2.setNickname("user2");
    when(entityManager.find(Usuario.class, "user1")).thenReturn(user1);
    when(entityManager.find(Usuario.class, "user2")).thenReturn(user2);
    assertThrows(
        YonaException.class,
        () -> instance.seguirUsuario(user1.getNickname(), user2.getNickname()));
  }

  @Test
  void seguirUsuarioNllSeguido() {
    Usuario user1 = new Usuario();
    Usuario user2 = new Usuario();
    user1.setNickname("user1");
    user2.setNickname("user2");
    assertThrows(
        YonaException.class,
        () -> instance.seguirUsuario(user1.getNickname(), user2.getNickname()));
  }

  @Test
  void seguirUsuarioNullSeguidor() {
    Usuario user1 = new Usuario();
    Usuario user2 = new Usuario();
    user1.setNickname("user1");
    user2.setNickname("user2");
    when(entityManager.find(Usuario.class, "user1")).thenReturn(user1);
    assertThrows(
        YonaException.class,
        () -> instance.seguirUsuario(user1.getNickname(), user2.getNickname()));
  }

  @Test
  void login() {
    Usuario user1 =
        new Usuario(
            "user1",
            "admin",
            "nopic",
            "mathias",
            "zunino",
            "toto@toto.com",
            new Date(),
            new Canal("canal", "descripcion", false));
    user1.prePersist();
    when(entityManager.find(Usuario.class, "user1")).thenReturn(user1);
    assertNotNull(instance.login("user1", "admin"));
  }

  @Test
  void loginInvalidNickname() {
    assertThrows(YonaException.class, () -> instance.login("user1", "admin"));
  }

  @Test
  void loginInvalidPassword() {
    Usuario user1 =
        new Usuario(
            "user1",
            "admin",
            "nopic",
            "mathias",
            "zunino",
            "toto@toto.com",
            new Date(),
            new Canal("canal", "descripcion", false));
    user1.prePersist();
    when(entityManager.find(Usuario.class, "user1")).thenReturn(user1);
    assertThrows(YonaException.class, () -> instance.login("user1", "otra"));
  }
}
