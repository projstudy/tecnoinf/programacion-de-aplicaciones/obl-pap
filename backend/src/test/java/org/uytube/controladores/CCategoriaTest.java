/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.controladores;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.uytube.exception.YonaException;
import org.uytube.manejador.Persistencia;
import org.uytube.modelos.Categoria;

/** @author forbi */
class CCategoriaTest {

  private final CCategoria instance = new CCategoria();
  private final EntityManager entityManager = mock(EntityManager.class, RETURNS_DEEP_STUBS);

  CCategoriaTest() {}

  @BeforeEach
  void setUp() {
    Persistencia.getInstance().setEntityManager(entityManager);
    when(entityManager.isOpen()).thenReturn(true);
  }

  /** Test of AltaCategoria method, of class CCategoria. */
  @Test
  void testAltaCategoria() {
    when(entityManager.find(Categoria.class, "Deportes")).thenReturn(null);
    Assertions.assertDoesNotThrow(() -> instance.altaCategoria("Deportes"));
  }

  /** Test of AltaCategoria method, of class CCategoria. */
  @Test
  void testAltaCategoriaExistente() {
    when(entityManager.find(Categoria.class, "Deportes")).thenReturn(null);
    when(entityManager.find(Categoria.class, "Deportes")).thenReturn(new Categoria());
    Assertions.assertThrows(YonaException.class, () -> instance.altaCategoria("Deportes"));
  }

  /** Test of AltaCategoria method, of class CCategoria. */
  @Test
  void testAltaCategoriaRollback() {
    EntityTransaction entityTransaction = mock(EntityTransaction.class);
    when(entityManager.getTransaction()).thenReturn(entityTransaction);
    doThrow(RollbackException.class).when(entityTransaction).commit();
    when(entityTransaction.isActive()).thenReturn(true);
    when(entityManager.find(Categoria.class, "Deportes")).thenReturn(null);
    Assertions.assertThrows(YonaException.class, () -> instance.altaCategoria("Deportes"));
  }

  /** Test of AltaCategoria method, of class CCategoria. */
  @Test
  void testListarCategorias() {
    Categoria c1 = new Categoria("deportes");
    Categoria c2 = new Categoria("musica");
    List<String> result = new ArrayList<>();
    result.add(c1.getNombre());
    result.add(c2.getNombre());
    when(entityManager.createNamedQuery("Categoria.listarTodas", String.class).getResultList())
        .thenReturn(result);
    when(entityManager.getTransaction()).thenThrow(RollbackException.class);
    assertEquals(result, instance.listarCategorias());
  }

  /** Test of AltaCategoria method, of class CCategoria. */
  @Test
  void testConsultaCategoriaNotFound() {
    when(entityManager.find(Categoria.class, "deportes")).thenReturn(null);
    assertThrows(YonaException.class, () -> instance.consultaCategoria("deportes"));
  }

  /** Test of AltaCategoria method, of class CCategoria. */
  @Test
  void testConsultaCategoria() {
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[] {"video1", "toto"});
    Categoria c1 = new Categoria("deportes");
    when(entityManager.find(Categoria.class, "deportes")).thenReturn(c1);
    when(entityManager
            .createNamedQuery("Categoria.listarVideos", Object[].class)
            .setParameter("nombreCategoria", "deportes")
            .getResultList())
        .thenReturn(result);
    assertTrue(instance.consultaCategoria("deportes").size() > 0);
    assertEquals(instance.consultaCategoria("deportes").get(0).getPropietario(), "toto");
    assertEquals(instance.consultaCategoria("deportes").get(0).getVideo(), "video1");
  }

  /** Test of AltaCategoria method, of class CCategoria. */
  @Test
  void testConsultaCategoriaNoOwner() {
    List<Object[]> result = new ArrayList<>();
    Categoria c1 = new Categoria("deportes");
    when(entityManager.find(Categoria.class, "deportes")).thenReturn(c1);
    when(entityManager
            .createNamedQuery("Categoria.listarVideos", Object[].class)
            .setParameter("nombreCategoria", "deportes")
            .getResultList())
        .thenReturn(result);
    assertTrue(instance.consultaCategoria("deportes").isEmpty());
  }

  /** Test of ListarListas method, of class CCategoria. */
  @Test
  void testListarListas() {
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[] {"lista1", "toto"});
    Categoria c1 = new Categoria("deportes");
    when(entityManager.find(Categoria.class, "deportes")).thenReturn(c1);
    when(entityManager
            .createNamedQuery("Categoria.listarListas", Object[].class)
            .setParameter("nombreCategoria", "deportes")
            .getResultList())
        .thenReturn(result);

    assertTrue(instance.listarListas("deportes").size() > 0);
    assertEquals(instance.listarListas("deportes").get(0).getPropietario(), "toto");
    assertEquals(instance.listarListas("deportes").get(0).getLista(), "lista1");
  }

  /** Test of ListarListas method, of class CCategoria. */
  @Test
  void testListarListasNull() {
    assertThrows(YonaException.class, () -> instance.listarListas("deportes"));
  }

  /** Test of ListarListas method, of class CCategoria. */
  @Test
  void testListarListasEmpty() {
    List<Object[]> result = new ArrayList<>();
    Categoria c1 = new Categoria("deportes");
    when(entityManager.find(Categoria.class, "deportes")).thenReturn(c1);
    when(entityManager
            .createNamedQuery("Categoria.listarVideos", Object[].class)
            .setParameter("nombreCategoria", "deportes")
            .getResultList())
        .thenReturn(result);
    assertTrue(instance.listarListas("deportes").isEmpty());
  }
}
