/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.modelos;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** @author toto */
class PredeterminadaTest {

  private static Predeterminada listaA;
  private static Predeterminada listaB;

  PredeterminadaTest() {}

  @BeforeAll
  static void setUpClass() {
    listaB = new Predeterminada("lista");
  }

  @AfterAll
  static void tearDownClass() {}

  @BeforeEach
  void setUp() {
    listaA = new Predeterminada();
  }

  @AfterEach
  void tearDown() {}

  @Test
  void testGetNombreA() {
    assertNull(listaA.getNombre());
  }

  @Test
  void testGetNombreB() {
    assertNotNull(listaB.getNombre());
    assertEquals("lista", listaB.getNombre());
  }

  @Test
  void testSetNombre() {
    listaA.setNombre("lista");
    assertEquals("lista", listaA.getNombre());
  }

  @Test
  void testIsPrivado() {
    assertTrue(listaA.isPrivada());
  }

  @Test
  void testSetPrivado() {
    assertTrue(listaA.isPrivada());
    listaA.setPrivada(false);
    assertTrue(listaA.isPrivada());
  }

  @Test
  void testGetCategoriaA() {
    assertNull(listaA.getCategoria());
  }

  @Test
  void testGetCategoriaB() {
    assertNull(listaB.getCategoria());
  }

  @Test
  void testSetCategoria() {
    Categoria categoria = new Categoria("categoria");
    listaA.setCategoria(categoria);
    assertEquals(categoria, listaA.getCategoria());
  }

  @Test
  void testGetVideos() {
    assertNotNull(listaA.getVideos());
    assertTrue(listaA.getVideos().isEmpty());
  }

  /** Test of setComentarios method, of class Comentario. */
  @Test
  void testSetVideos() {
    Set<Video> videos = new HashSet<>();
    listaA.setVideos(videos);
    assertEquals(videos, listaA.getVideos());
  }

  /** Test of addRespuesta method, of class Comentario. */
  @Test
  void testAddRespuesta() {
    assertTrue(listaA.getVideos().isEmpty());
    listaA.addVideo(new Video());
    assertFalse(listaA.getVideos().isEmpty());
  }
}
