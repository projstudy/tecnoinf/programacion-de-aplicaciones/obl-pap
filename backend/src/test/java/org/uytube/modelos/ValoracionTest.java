/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.modelos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** @author forbi */
class ValoracionTest {

  private static Valoracion valoracionA;
  private static Valoracion valoracionB;

  ValoracionTest() {}

  @BeforeAll
  static void setUpClass() {
    valoracionA = new Valoracion();
    valoracionB = new Valoracion(true, null);
  }

  @BeforeEach
  void setUp() {
    valoracionA = new Valoracion();
  }

  @AfterEach
  void tearDown() {}

  /** Test of leGusta method, of class Valoracion. */
  @Test
  void testLeGustaA() {
    assertFalse(valoracionA.leGusta());
  }

  @Test
  void testLeGustaB() {
    assertTrue(valoracionB.leGusta());
  }
  /** Test of setLeGusta method, of class Valoracion. */
  @Test
  void testSetLeGusta() {
    valoracionA.setLeGusta(true);
    assertTrue(valoracionA.leGusta());
  }
}
