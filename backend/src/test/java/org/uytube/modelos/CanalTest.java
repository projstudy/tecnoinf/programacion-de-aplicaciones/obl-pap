package org.uytube.modelos;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class CanalTest {

  private static Canal canalA;
  private static Canal canalB;

  CanalTest() {}

  @BeforeAll
  static void setUpClass() {
    canalA = new Canal();
    canalB = new Canal("canal", "canal", true);
  }

  @AfterEach
  void tearDown() {
    canalA = new Canal();
  }

  /** Test of getNombre method, of class Canal. */
  @Test
  void testGetNombreA() {
    String result = canalA.getNombre();
    assertNull(result);
  }

  @Test
  void testGetNombreB() {
    String result = canalB.getNombre();
    assertEquals(result, "canal");
  }

  /** Test of setNombre method, of class Canal. */
  @Test
  void testSetNombre() {
    canalA.setNombre("test");
    assertEquals(canalA.getNombre(), "test");
  }

  /** Test of getDescripcion method, of class Canal. */
  @Test
  void testGetDescripcionA() {
    String result = canalA.getDescripcion();
    assertNull(result);
  }

  @Test
  void testGetDescripcionB() {
    String result = canalB.getDescripcion();
    assertEquals(result, "canal");
  }

  /** Test of setDescripcion method, of class Canal. */
  @Test
  void testSetDescripcion() {
    canalA.setDescripcion("test");
    assertEquals(canalA.getDescripcion(), "test");
  }

  /** Test of isPrivado method, of class Canal. */
  @Test
  void testGetEsPrivadoA() {
    boolean result = canalA.isPrivado();
    assertFalse(result);
  }

  /** Test of isPrivado method, of class Canal. */
  @Test
  void testGetEsPrivadoB() {
    boolean result = canalA.isPrivado();
    assertFalse(result);
  }

  /** Test of setPrivado method, of class Canal. */
  @Test
  void testSetEsPrivado() {
    canalA.setPrivado(true);
    assertTrue(canalA.isPrivado());
  }

  /** Test of getVideos method, of class Canal. */
  @Test
  void testGetVideos() {
    Set<Video> result = canalA.getVideos();
    assertNotNull(result);
    assertTrue(result.isEmpty());
  }

  /** Test of setVideos method, of class Canal. */
  @Test
  void testSetVideos() {
    Set<Video> set = canalA.getVideos();
    canalA.addVideo(new Video());
    canalA.setVideos(set);
    Set<Video> result = canalA.getVideos();
    assertEquals(result, set);
    assertFalse(result.isEmpty());
  }

  /** Test of addVideo method, of class Canal. */
  @Test
  void testAddVideo() {
    canalA.addVideo(new Video());
    Set<Video> result = canalA.getVideos();
    assertFalse(result.isEmpty());
  }

  /** Test of getListas method, of class Canal. */
  @Test
  void testGetListas() {
    Set<Lista> result = canalA.getListas();
    assertNotNull(result);
    assertTrue(result.isEmpty());
  }

  /** Test of setListas method, of class Canal. */
  @Test
  void testSetListas() {
    Set<Lista> set = canalA.getListas();
    canalA.addLista(new Lista() {});
    canalA.setListas(set);
    Set<Lista> result = canalA.getListas();
    assertEquals(result, set);
    assertFalse(result.isEmpty());
  }

  /** Test of addLista method, of class Canal. */
  @Test
  void testAddLista() {
    canalA.addLista(new Lista() {});
    Set<Lista> result = canalA.getListas();
    assertFalse(result.isEmpty());
  }
}
