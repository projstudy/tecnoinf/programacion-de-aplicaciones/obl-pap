/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.modelos;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** @author toto */
class ComentarioTest {

  private static Comentario comentarioA;
  private static Comentario comentarioB;

  ComentarioTest() {}

  @BeforeAll
  static void setUpClass() {
    comentarioB = new Comentario(new Date(), "texto", null);
  }

  @AfterAll
  static void tearDownClass() {}

  @BeforeEach
  void setUp() {
    comentarioA = new Comentario();
  }

  @AfterEach
  void tearDown() {}

  /** Test of getFecha method, of class Comentario. */
  @Test
  void testGetFechaA() {
    assertNull(comentarioA.getFecha());
  }

  /** Test of getId method, of class Comentario. */
  @Test
  void getId() {
    assertEquals(comentarioA.getId(), 0);
  }

  void testGetFechaB() {
    assertNotNull(comentarioB.getFecha());
  }

  /** Test of setFecha method, of class Comentario. */
  @Test
  void testSetFecha() {
    Date date = new GregorianCalendar().getTime();
    comentarioA.setFecha(date);
    assertEquals(date, comentarioA.getFecha());
  }

  /** Test of getTexto method, of class Comentario. */
  @Test
  void testGetTextoA() {
    assertNull(comentarioA.getTexto());
  }

  void testGetTextoB() {
    assertNotNull(comentarioB.getTexto());
  }

  /** Test of setTexto method, of class Comentario. */
  @Test
  void testSetTexto() {
    comentarioA.setTexto("texto");
    assertEquals("texto", comentarioA.getTexto());
  }

  /** Test of getComentarios method, of class Comentario. */
  @Test
  void testGetComentarios() {
    assertNotNull(comentarioA.getComentarios());
    assertTrue(comentarioA.getComentarios().isEmpty());
  }

  /** Test of setComentarios method, of class Comentario. */
  @Test
  void testSetComentarios() {
    ArrayList<Comentario> comentarios = new ArrayList<>();
    comentarioA.setComentarios(comentarios);
    assertEquals(comentarios, comentarioA.getComentarios());
  }

  /** Test of addRespuesta method, of class Comentario. */
  @Test
  void testAddRespuesta() {
    assertTrue(comentarioA.getComentarios().isEmpty());
    comentarioA.addRespuesta(new Comentario());
    assertFalse(comentarioA.getComentarios().isEmpty());
  }
}
