/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.modelos;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** @author forbi */
class VideoTest {

  private static Video videoA;
  private static Video videoB;

  VideoTest() {}

  @BeforeAll
  static void setUpClass() {
    videoA = new Video();
    videoB =
        new Video(
            "nombre",
            "descripcion",
            69,
            new GregorianCalendar().getTime(),
            "url",
            true,
            new Categoria());
  }

  @BeforeEach
  void setUp() {
    videoA = new Video();
  }

  /** Test of getNombre method, of class Video. */
  @Test
  void testGetNombreA() {
    String result = videoA.getNombre();
    assertNull(result);
  }

  @Test
  void testGetNombreB() {
    String result = videoB.getNombre();
    assertEquals(result, "nombre");
  }

  /** Test of setNombre method, of class Video. */
  @Test
  void testSetNombre() {
    videoA.setNombre("nombre");
    assertEquals(videoA.getNombre(), "nombre");
  }

  /** Test of getDescripcion method, of class Video. */
  @Test
  void testGetDescripcionA() {
    String result = videoA.getDescripcion();
    assertNull(result);
  }

  @Test
  void testGetDescripcionB() {
    String result = videoB.getDescripcion();
    assertEquals(result, "descripcion");
  }

  /** Test of setDescripcion method, of class Video. */
  @Test
  void testSetDescripcion() {
    videoA.setDescripcion("descripcion");
    assertEquals(videoA.getDescripcion(), "descripcion");
  }

  /** Test of getDuracion method, of class Video. */
  @Test
  void testGetDuracionA() {
    int result = videoA.getDuracion();
    assertEquals(result, 0);
  }

  @Test
  void testGetDuracionB() {
    int result = videoB.getDuracion();
    assertEquals(result, 69);
  }

  /** Test of setDuracion method, of class Video. */
  @Test
  void testSetDuracion() {
    videoA.setDuracion(1);
    assertEquals(videoA.getDuracion(), 1);
  }

  /** Test of getFechaPublicacion method, of class Video. */
  @Test
  void testGetFechaPublicacionA() {
    Date result = videoA.getFechaPublicacion();
    assertNull(result);
  }

  @Test
  void testGetFechaPublicacionB() {
    Date result = videoB.getFechaPublicacion();
    assertNotNull(result);
  }

  /** Test of setFechaPublicacion method, of class Video. */
  @Test
  void testSetFechaPublicacion() {
    Date fechaPublicacion = new GregorianCalendar().getTime();
    videoA.setFechaPublicacion(fechaPublicacion);
    assertEquals(videoA.getFechaPublicacion(), fechaPublicacion);
  }

  /** Test of getURL method, of class Video. */
  @Test
  void testGetURLA() {
    String result = videoA.getURL();
    assertNull(result);
  }

  @Test
  void testGetURLB() {
    String result = videoB.getURL();
    assertEquals(result, "url");
  }

  /** Test of setURL method, of class Video. */
  @Test
  void testSetURL() {
    videoA.setURL("test");
    assertEquals(videoA.getURL(), "test");
  }

  /** Test of isPrivado method, of class Video. */
  @Test
  void testGetEsPrivadoA() {
    boolean result = videoA.isPrivado();
    assertFalse(result);
  }

  @Test
  void testGetEsPrivadoB() {
    boolean result = videoB.isPrivado();
    assertTrue(result);
  }

  /** Test of setPrivado method, of class Video. */
  @Test
  void testSetEsPrivado() {
    videoA.setPrivado(true);
    boolean result = videoA.isPrivado();
    assertTrue(result);
  }

  /** Test of getCategoria method, of class Video. */
  @Test
  void testGetCategoriaA() {
    Categoria result = videoA.getCategoria();
    assertNull(result);
  }

  @Test
  void testGetCategoriaB() {
    Categoria result = videoB.getCategoria();
    assertNotNull(result);
  }

  /** Test of getId method, of class Video. */
  @Test
  void getId() {
    // assertEquals(videoA.getId(), 0);
  }

  /** Test of setCategoria method, of class Video. */
  @Test
  void testSetCategoria() {
    videoA.setCategoria(new Categoria());
    Categoria result = videoA.getCategoria();
    assertNotNull(result);
  }

  /** Test of getComentarios method, of class Video. */
  @Test
  void testGetComentarios() {
    List<Comentario> result = videoA.getComentarios();
    assertNotNull(result);
    assertTrue(result.isEmpty());
  }

  /** Test of setComentarios method, of class Video. */
  @Test
  void testSetComentarios() {
    List<Comentario> result = videoA.getComentarios();
    result.add(new Comentario());
    videoA.setComentarios(result);
    assertFalse(videoA.getComentarios().isEmpty());
  }

  /** Test of getValoraciones method, of class Video. */
  @Test
  void testGetValoraciones() {
    Set<Valoracion> result = videoA.getValoraciones();
    assertNotNull(result);
    assertTrue(result.isEmpty());
  }

  /** Test of setValoraciones method, of class Video. */
  @Test
  void testSetValoraciones() {
    Set<Valoracion> result = videoA.getValoraciones();
    result.add(new Valoracion());
    videoA.setValoraciones(result);
    assertFalse(videoA.getValoraciones().isEmpty());
  }

  /** Test of addComentario method, of class Video. */
  @Test
  void testAddComentario() {
    assertTrue(videoA.getComentarios().isEmpty());
    videoA.addComentario(new Comentario());
    assertFalse(videoA.getComentarios().isEmpty());
  }

  /** Test of addValoracion method, of class Video. */
  @Test
  void testAddValoracion() {
    assertTrue(videoA.getValoraciones().isEmpty());
    videoA.addValoracion(new Valoracion());
    assertFalse(videoA.getValoraciones().isEmpty());
  }
}
