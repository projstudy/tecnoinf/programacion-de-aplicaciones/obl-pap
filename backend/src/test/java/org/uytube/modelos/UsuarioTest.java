/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.modelos;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.*;

/** @author e974616 */
class UsuarioTest {

  private static Usuario usuarioA;
  private static Usuario usuarioB;

  UsuarioTest() {}

  @BeforeAll
  static void setUpClass() {
    /*Constructor por defecto*/
    usuarioA = new Usuario();
    /*Constructor por parametros*/
    new GregorianCalendar().getTime();
    usuarioB =
        new Usuario(
            "admin",
            "admin",
            "/home/pic.png",
            "Admin",
            "Admin",
            "admin@admin.com",
            new GregorianCalendar().getTime(),
            new Canal());
  }

  @AfterEach
  void tearDown() {
    usuarioA = new Usuario();
  }

  /** Test of getNickname method, of class Usuario. */
  @Test
  void testGetNicknameA() {
    String result = usuarioA.getNickname();
    assertNull(result);
  }

  @Test
  void testGetNicknameB() {
    String result = usuarioB.getNickname();
    assertEquals("admin", result);
  }

  /** Test of setNickname method, of class Usuario. */
  @Test
  void testSetNickname() {
    usuarioA.setNickname("test");
    assertEquals(usuarioA.getNickname(), "test");
  }

  /** Test of getImg method, of class Usuario. */
  @Test
  void testGetImgUsuarioA() {
    String result = usuarioA.getImg();
    assertNull(result);
  }

  /** Test of getImg method, of class Usuario. */
  @Test
  void testGetImgUsuarioB() {
    String result = usuarioB.getImg();
    assertEquals(result, "/home/pic.png");
  }

  /** Test of setImg method, of class Usuario. */
  @Test
  void testSetImg() {
    usuarioA.setImg("test");
    assertEquals(usuarioA.getImg(), "test");
  }

  /** Test of getNombre method, of class Usuario. */
  @Test
  void testGetNombreA() {
    String result = usuarioA.getNombre();
    assertNull(result);
  }

  @Test
  void testGetNombreB() {
    String result = usuarioB.getNombre();
    assertEquals(result, "Admin");
  }

  /** Test of setNombre method, of class Usuario. */
  @Test
  void testSetNombre() {
    usuarioA.setNombre("test");
    assertEquals(usuarioA.getNombre(), "test");
  }

  /** Test of getApellido method, of class Usuario. */
  @Test
  void testGetApellidoA() {
    String result = usuarioA.getApellido();
    assertNull(result);
  }

  /** Test of getApellido method, of class Usuario. */
  @Test
  void testGetApellidoB() {
    String result = usuarioB.getApellido();
    assertEquals(result, "Admin");
  }

  /** Test of setApellido method, of class Usuario. */
  @Test
  void testSetApellido() {
    usuarioA.setApellido("test");
    assertEquals(usuarioA.getApellido(), "test");
  }

  /** Test of getEmail method, of class Usuario. */
  @Test
  void testGetEmailA() {
    String result = usuarioA.getEmail();
    assertNull(result);
  }

  /** Test of getEmail method, of class Usuario. */
  @Test
  void testGetEmailB() {
    String result = usuarioB.getEmail();
    assertEquals(result, "admin@admin.com");
  }

  /** Test of setEmail method, of class Usuario. */
  @Test
  void testSetEmail() {
    usuarioA.setEmail("test");
    assertEquals(usuarioA.getEmail(), "test");
  }

  /** Test of getDate method, of class Usuario. */
  @Test
  void testGetDateA() {
    Date result = usuarioA.getDate();
    assertNull(result);
  }

  /** Test of getDate method, of class Usuario. */
  @Test
  void testGetDateB() {
    Date result = usuarioB.getDate();
    assertNotNull(result);
    assertNotNull(result);
  }

  /** Test of setDate method, of class Usuario. */
  @Test
  void testSetDate() {
    Date date = new GregorianCalendar().getTime();
    usuarioA.setDate(date);
    assertEquals(usuarioA.getDate(), date);
  }

  /** Test of getSuscripciones method, of class Usuario. */
  @Test
  void testGetSuscripciones() {
    Set<Usuario> expResult = new HashSet<>();
    Set<Usuario> result = usuarioA.getSuscripciones();
    assertEquals(expResult, result);
  }

  /** Test of setSuscripciones method, of class Usuario. */
  @Test
  void testSetSuscripciones() {
    Set<Usuario> expResult = new HashSet<>();
    expResult.add(usuarioB);
    usuarioA.setSuscripciones(expResult);
    assertEquals(expResult, usuarioA.getSuscripciones());
  }

  /** Test of getCanal method, of class Usuario. */
  @Test
  void testGetCanalA() {
    Canal result = usuarioA.getCanal();
    assertNull(result);
  }

  @Test
  void testGetCanalB() {
    Canal result = usuarioB.getCanal();
    assertNotNull(result);
  }

  /** Test of setCanal method, of class Usuario. */
  @Test
  void testSetCanal() {
    Canal canal = new Canal();
    usuarioA.setCanal(canal);
    assertNotNull(usuarioA.getCanal());
  }
}
