package org.uytube.modelos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ListaTest {
  private static Lista listaA;

  ListaTest() {}

  @BeforeAll
  static void setUpClass() {}

  @AfterAll
  static void tearDownClass() {}

  @BeforeEach
  void setUp() {
    listaA = new Lista();
  }

  @AfterEach
  void tearDown() {}

  @Test
  void testIsPrivadaA() {
    assertTrue(listaA.isPrivada());
  }

  /** Test of getId method, of class Lista. */
  @Test
  void getId() {
    // assertEquals(listaA.getId(), 0);
  }

  @Test
  void testSetPrivada() {
    assertTrue(listaA.isPrivada());
    listaA.setPrivada(false);
    assertFalse(listaA.isPrivada());
  }
}
