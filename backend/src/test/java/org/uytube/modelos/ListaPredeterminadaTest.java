/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.modelos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** @author forbi */
class ListaPredeterminadaTest {

  ListaPredeterminadaTest() {}

  @BeforeAll
  static void setUpClass() {}

  @AfterAll
  static void tearDownClass() {}

  @BeforeEach
  void setUp() {}

  @AfterEach
  void tearDown() {}

  /** Test of getNombre method, of class ListaPredeterminada. */
  @Test
  void testGetNombre() {
    ListaPredeterminada lista = new ListaPredeterminada("falsa");
    assertEquals("falsa", lista.getNombre());
  }

  /** Test of setNombre method, of class ListaPredeterminada. */
  @Test
  void testSetNombre() {
    ListaPredeterminada lista = new ListaPredeterminada();
    assertNull(lista.getNombre());
    lista.setNombre("nombre");
    assertEquals("nombre", lista.getNombre());
  }
}
