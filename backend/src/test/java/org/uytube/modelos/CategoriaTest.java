/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.modelos;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** @author forbi */
class CategoriaTest {

  private static Categoria categoriaA;
  private static Categoria categoriaB;

  CategoriaTest() {}

  @BeforeAll
  static void setUpClass() {
    categoriaA = new Categoria();
    categoriaB = new Categoria("categoria");
  }

  @AfterAll
  static void tearDownClass() {}

  @BeforeEach
  void setUp() {
    categoriaA = new Categoria();
  }

  @AfterEach
  void tearDown() {}

  /** Test of getNombre method, of class Categoria. */
  @Test
  void testGetNombreA() {
    assertNull(categoriaA.getNombre());
  }

  @Test
  void testGetNombreB() {
    assertEquals(categoriaB.getNombre(), "categoria");
  }

  /** Test of setNombre method, of class Categoria. */
  @Test
  void testSetNombre() {
    categoriaA.setNombre("categoria");
    assertEquals(categoriaA.getNombre(), "categoria");
  }
}
