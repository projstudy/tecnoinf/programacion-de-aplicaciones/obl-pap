package org.uytube.datatypes;

import java.util.ArrayList;

public class DtUsuarioExt {
  private final DtUsuario usuario;
  private final DtCanal canal;
  private final ArrayList<DtSeguidor> suscriptores;
  private final ArrayList<DtSeguidor> suscripciones;

  public DtCanal getCanal() {
    return canal;
  }

  public DtUsuario getUsuario() {
    return usuario;
  }

  public ArrayList<DtSeguidor> getSuscriptores() {
    return suscriptores;
  }

  public ArrayList<DtSeguidor> getSuscripciones() {
    return suscripciones;
  }

  public DtUsuarioExt(
      DtUsuario usuario,
      DtCanal canal,
      ArrayList<DtSeguidor> suscriptores,
      ArrayList<DtSeguidor> suscripciones) {
    this.usuario = usuario;
    this.canal = canal;
    this.suscripciones = suscripciones;
    this.suscriptores = suscriptores;
  }
}
