package org.uytube.datatypes;

import java.util.ArrayList;

public class DtLista {

  private final String nombre;
  private final int id;
  private final boolean privado;
  private final String categoria;
  private final String tipo;
  private final String canal;
  private ArrayList<DtVideo> videos;

  public String getCategoria() {
    return categoria;
  }

  public String getCanal() {
    return canal;
  }

  public String getTipo() {
    return tipo;
  }

  public ArrayList<DtVideo> getVideos() {
    return videos;
  }

  public String getNombre() {
    return nombre;
  }

  public boolean isPrivado() {
    return privado;
  }

  public int getId() {
    return id;
  }

  public DtLista(
      String nombre,
      int id,
      boolean privado,
      String categoria,
      String tipo,
      String canal,
      ArrayList<DtVideo> videos) {
    this.nombre = nombre;
    this.privado = privado;
    this.categoria = categoria;
    this.id = id;
    this.tipo = tipo;
    this.canal = canal;
    this.videos = videos;
  }

  public DtLista(String nombre, int id, boolean privado, String categoria, String tipo) {
    this.nombre = nombre;
    this.privado = privado;
    this.categoria = categoria;
    this.id = id;
    this.tipo = tipo;
    this.canal = "";
  }

  public DtLista(String nombre, boolean privado, String categoria) {
    this.nombre = nombre;
    this.privado = privado;
    this.categoria = categoria;
    this.id = 0;
    this.tipo = "";
    this.canal = "";
  }

  public DtLista(String nombre, boolean privado) {
    this(nombre, privado, null);
  }
}
