package org.uytube.datatypes;

public class DtCanal {
  private final String nombre;
  private final String descripcion;
  private final boolean privado;
  private final String foto;
  private final String categoria;

  public DtCanal(String nombre, String descripcion, boolean privado) {
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.privado = privado;
    this.foto = null;
    this.categoria = null;
  }

  public DtCanal(
      String nombre, String descripcion, boolean privado, String foto, String categoria) {
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.privado = privado;
    this.foto = foto;
    this.categoria = categoria;
  }

  public DtCanal(String nombre, String descripcion, boolean privado, String foto) {
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.privado = privado;
    this.foto = foto;
    this.categoria = null;
  }

  public String getNombre() {
    return nombre;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public boolean isPrivado() {
    return privado;
  }

  public String getFoto() {
    return foto;
  }

  public String getCategoria() {
    return categoria;
  }
}
