package org.uytube.datatypes;

public class DtValoracion {
  private final int id;
  private final boolean leGusta;
  private final String nickname;

  public boolean isLeGusta() {
    return leGusta;
  }

  public String getNickname() {
    return nickname;
  }

  public int getId() {
    return id;
  }

  public DtValoracion(int id, boolean leGusta, String nickname) {
    this.id = id;
    this.leGusta = leGusta;
    this.nickname = nickname;
  }
}
