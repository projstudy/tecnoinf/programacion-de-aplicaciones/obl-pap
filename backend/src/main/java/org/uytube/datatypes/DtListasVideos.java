package org.uytube.datatypes;

import java.util.ArrayList;

public class DtListasVideos {
  private final ArrayList<DtLista> listas;
  private final ArrayList<DtVideo> videos;

  public DtListasVideos(ArrayList<DtLista> listas, ArrayList<DtVideo> videos) {
    this.listas = listas;
    this.videos = videos;
  }

  public ArrayList<DtLista> getListas() {
    return listas;
  }

  public ArrayList<DtVideo> getVideos() {
    return videos;
  }
}
