package org.uytube.datatypes;

import java.util.Date;

public class DtUsuario {
  private final String nickname;
  private final String password;
  private final String nombre;
  private final String apellido;
  private final String email;
  private final String foto;
  private final Date fechaNacimiento;

  public DtUsuario(
      String nickname,
      String password,
      String nombre,
      String apellido,
      String email,
      String foto,
      Date fechaNacimiento) {
    this.nickname = nickname;
    this.password = password;
    this.nombre = nombre;
    this.apellido = apellido;
    this.email = email;
    this.foto = foto;
    this.fechaNacimiento = fechaNacimiento;
  }

  public DtUsuario(
      String nickname,
      String password,
      String nombre,
      String apellido,
      String email,
      Date fechaNacimiento) {
    this(nickname, password, nombre, apellido, email, "", fechaNacimiento);
  }

  public String getNickname() {
    return nickname;
  }

  public String getPassword() {
    return password;
  }

  public String getNombre() {
    return nombre;
  }

  public String getApellido() {
    return apellido;
  }

  public String getEmail() {
    return email;
  }

  public String getFoto() {
    return foto;
  }

  public Date getFechaNacimiento() {
    return fechaNacimiento;
  }
}
