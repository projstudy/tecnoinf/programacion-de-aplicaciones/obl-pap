package org.uytube.datatypes;

import java.util.Date;

public class DtModUsuario {
  private final String nombre;
  private final String apellido;
  private final String foto;
  private final Date fechaNacimiento;
  private final boolean privado;

  public String getNombre() {
    return nombre;
  }

  public String getApellido() {
    return apellido;
  }

  public String getFoto() {
    return foto;
  }

  public boolean isPrivado() {
    return privado;
  }

  public Date getFechaNacimiento() {
    return fechaNacimiento;
  }

  public DtModUsuario(
      String nombre, String apellido, String foto, Date fechaNacimiento, boolean privado) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.foto = foto;
    this.fechaNacimiento = fechaNacimiento;
    this.privado = privado;
  }
}
