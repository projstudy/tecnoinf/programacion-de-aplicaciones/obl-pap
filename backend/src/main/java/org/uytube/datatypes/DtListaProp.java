/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.datatypes;

/** @author toto */
public class DtListaProp {
  private final String lista;
  private final String propietario;

  public DtListaProp(String lista, String propietario) {
    this.lista = lista;
    this.propietario = propietario;
  }

  public String getLista() {
    return lista;
  }

  public String getPropietario() {
    return propietario;
  }
}
