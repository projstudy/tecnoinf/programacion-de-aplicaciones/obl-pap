package org.uytube.datatypes;

import java.util.ArrayList;
import java.util.Date;

public class DtComentario {
  private final int id;
  private final String texto;
  private final String nickname;
  private final String foto;
  private final ArrayList<DtComentario> respuestas;
  private final Date fechaPublicacion;

  @Override
  public String toString() {
    return nickname + ": " + texto;
  }

  public DtComentario(
      int id,
      String texto,
      String nickname,
      ArrayList<DtComentario> respuestas,
      Date fechaPublicacion,
      String foto) {
    this.id = id;
    this.texto = texto;
    this.nickname = nickname;
    this.respuestas = respuestas;
    this.fechaPublicacion = fechaPublicacion;
    this.foto = foto;
  }

  public DtComentario(
      String texto, String nickname, ArrayList<DtComentario> respuestas, Date fechaPublicacion) {
    this(0, texto, nickname, respuestas, fechaPublicacion, "");
  }

  public int getId() {
    return id;
  }

  public String getTexto() {
    return texto;
  }

  public String getFoto() {
    return foto;
  }

  public String getNickname() {
    return nickname;
  }

  public ArrayList<DtComentario> getRespuestas() {
    return respuestas;
  }

  public Date getFechaPublicacion() {
    return fechaPublicacion;
  }
}
