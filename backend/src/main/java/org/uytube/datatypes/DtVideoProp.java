/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.datatypes;

/** @author juan */
public class DtVideoProp {
  private final String video;
  private final String propietario;

  public DtVideoProp(String video, String propietario) {
    this.video = video;
    this.propietario = propietario;
  }

  public String getVideo() {
    return video;
  }

  public String getPropietario() {
    return propietario;
  }
}
