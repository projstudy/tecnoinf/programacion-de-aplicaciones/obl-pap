package org.uytube.datatypes;

class DtCategoria {
  private final String nombre;

  public String getNombre() {
    return nombre;
  }

  public DtCategoria(String nombre) {
    this.nombre = nombre;
  }
}
