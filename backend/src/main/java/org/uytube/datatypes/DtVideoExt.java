package org.uytube.datatypes;

import java.util.ArrayList;

public class DtVideoExt {
  private final String canal;
  private final DtVideo video;
  private final ArrayList<DtValoracion> valoraciones;
  private final ArrayList<DtComentario> comentarios;

  public DtVideo getVideo() {
    return video;
  }

  public ArrayList<DtValoracion> getValoraciones() {
    return valoraciones;
  }

  public ArrayList<DtComentario> getComentarios() {
    return comentarios;
  }

  public String getCanal() {
    return canal;
  }

  public DtVideoExt(
      DtVideo video,
      ArrayList<DtValoracion> valoraciones,
      ArrayList<DtComentario> comentarios,
      String canal) {
    this.video = video;
    this.valoraciones = valoraciones;
    this.comentarios = comentarios;
    this.canal = canal;
  }

  public DtVideoExt(
      DtVideo video, ArrayList<DtValoracion> valoraciones, ArrayList<DtComentario> comentarios) {
    this.canal = "";
    this.video = video;
    this.valoraciones = valoraciones;
    this.comentarios = comentarios;
  }
}
