package org.uytube.datatypes;

public class DtSeguidor {
  private final String nickname;
  private final String foto;

  public DtSeguidor(String nickname, String foto) {
    this.nickname = nickname;
    this.foto = foto;
  }

  public String getNickname() {
    return nickname;
  }

  public String getFoto() {
    return foto;
  }
}
