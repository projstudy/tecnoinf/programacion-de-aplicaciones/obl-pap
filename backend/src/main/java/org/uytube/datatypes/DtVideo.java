package org.uytube.datatypes;

import java.util.Date;

public class DtVideo {
  private final String nombre;
  private final String url;
  private final String descripcion;
  private final Date fechaPublicacion;
  private final String categoria;
  private final int duracion;
  private final boolean privado;
  private final String canal;
  private final int id;

  public DtVideo(
      int id,
      String nombre,
      String url,
      String descripcion,
      Date fechaPublicacion,
      String categoria,
      int duracion,
      boolean privado) {
    this.nombre = nombre;
    this.url = url;
    this.descripcion = descripcion;
    this.fechaPublicacion = fechaPublicacion;
    this.categoria = categoria;
    this.duracion = duracion;
    this.privado = privado;
    this.canal = null;
    this.id = id;
  }

  public DtVideo(
      String nombre,
      String url,
      String descripcion,
      Date fechaPublicacion,
      String categoria,
      int duracion,
      boolean privado) {
    this.nombre = nombre;
    this.url = url;
    this.descripcion = descripcion;
    this.fechaPublicacion = fechaPublicacion;
    this.categoria = categoria;
    this.duracion = duracion;
    this.privado = privado;
    this.canal = null;
    this.id = 0;
  }

  public DtVideo(
      int id,
      String nombre,
      String url,
      String descripcion,
      Date fechaPublicacion,
      int duracion,
      boolean privado) {
    this(id, nombre, url, descripcion, fechaPublicacion, null, duracion, privado);
  }

  public DtVideo(
      String nombre,
      String url,
      String descripcion,
      Date fechaPublicacion,
      int duracion,
      boolean privado) {
    this(nombre, url, descripcion, fechaPublicacion, null, duracion, privado);
  }

  public DtVideo(
      String nombre,
      String url,
      String descripcion,
      Date fechaPublicacion,
      String categoria,
      int duracion,
      boolean privado,
      String canal,
      int id) {
    this.nombre = nombre;
    this.url = url;
    this.descripcion = descripcion;
    this.fechaPublicacion = fechaPublicacion;
    this.categoria = categoria;
    this.duracion = duracion;
    this.privado = privado;
    this.canal = canal;
    this.id = id;
  }

  public DtVideo(
      String nombre,
      String url,
      String descripcion,
      Date fechaPublicacion,
      int duracion,
      boolean privado,
      String canal,
      int id) {
    this.nombre = nombre;
    this.url = url;
    this.descripcion = descripcion;
    this.fechaPublicacion = fechaPublicacion;
    this.duracion = duracion;
    this.privado = privado;
    this.canal = canal;
    this.categoria = null;
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public String getUrl() {
    return url;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public Date getFechaPublicacion() {
    return fechaPublicacion;
  }

  public String getCategoria() {
    return categoria;
  }

  public int getDuracion() {
    return duracion;
  }

  public int getId() {
    return id;
  }

  public String getCanal() {
    return canal;
  }

  public boolean isPrivado() {
    return privado;
  }
}
