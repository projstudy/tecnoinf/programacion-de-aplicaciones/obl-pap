/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.controladores;

import java.util.ArrayList;
import java.util.List;
import org.uytube.datatypes.DtListaProp;
import org.uytube.datatypes.DtListasVideos;
import org.uytube.datatypes.DtVideoProp;
import org.uytube.exception.YonaException;

/** @author e974616 */
public interface ICategoria {

  /**
   * Se da de alta una categoria con el nombre especificado
   *
   * @param nombre nombre de la nueva categoria
   * @throws YonaException si ya existe el nombre de la categoria
   */
  void altaCategoria(String nombre) throws YonaException;

  /** @return una lista con los nombres de las categorias */
  List<String> listarCategorias();

  /**
   * Dado una categoria, se buscan todos los videos y sus propietarios que han usado dicha
   * categoria.
   *
   * @param nombre nombre de la categoria
   * @return Lista de DtVideoProp
   */
  ArrayList<DtVideoProp> consultaCategoria(String nombre);
  /**
   * Dado una categoria, se buscan todas las listas y sus propietarios que han usado dicha
   * categoria.
   *
   * @param nombre nombre de la categoria
   * @return Lista de DtListaProp
   */
  ArrayList<DtListaProp> listarListas(String nombre);

  DtListasVideos consultaCategoriaPublica(String nombre);
}
