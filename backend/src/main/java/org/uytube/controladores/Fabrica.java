package org.uytube.controladores;

public class Fabrica {

  /** Representa los tipo de controladores que se le puede solicitar a la factory. */
  public enum Controller {
    USUARIO,
    LISTA,
    CATEOGRIA,
    VIDEO
  }

  /**
   * @param controller tipo de controlador solicitado
   * @param <T> tipo de interface que sera retornada ICategoria, IUsuario, ILista, IVideo
   * @return retorna ICategoria o IUsuario o ILista o IVideo
   */
  @SuppressWarnings({"unchecked"})
  public static <T> T getControllerOf(Controller controller) {
    T resultado;
    switch (controller) {
      case CATEOGRIA:
        {
          resultado = (T) new CCategoria();
        }
        break;
      case LISTA:
        {
          resultado = (T) new CLista();
        }
        break;
      case USUARIO:
        {
          resultado = (T) new CUsuario();
        }
        break;
      case VIDEO:
        {
          resultado = (T) new CVideo();
        }
        break;
      default:
        throw new IllegalStateException("Unexpected value: " + controller);
    }
    return resultado;
  }
}
