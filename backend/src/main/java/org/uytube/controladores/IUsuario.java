/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.controladores;

import java.util.List;
import org.uytube.datatypes.DtCanal;
import org.uytube.datatypes.DtModUsuario;
import org.uytube.datatypes.DtUsuario;
import org.uytube.datatypes.DtUsuarioExt;
import org.uytube.exception.YonaException;

/** @author juan */
public interface IUsuario {

  List<DtCanal> listarCanalesPublicos();

  /**
   * Crea un usuario y le asocia un canal
   *
   * @param nickname nickname del usuario
   * @param password password del usuario
   * @return DtUsuarioExt
   * @throws YonaException en caso de que el usuario/password sean incorrectos
   */
  DtUsuarioExt login(String nickname, String password) throws YonaException;

  /**
   * Crea un usuario y le asocia un canal
   *
   * @param dtUsuario Dt con los datos del usuario
   * @param dtCanal Dt con los datos del canal
   * @throws YonaException en caso de que el usuario/canal ya existan
   */
  void altausuario(DtUsuario dtUsuario, DtCanal dtCanal) throws YonaException;

  /** @return List con los nombre de los usuarios */
  List<String> listarUsuarios();

  /**
   * Retorna informacion del usuario, del canal, sus listas, suscriptores, suscripciones, etc
   *
   * @param nick del usuario
   * @return DtusuarioExt con los datos del usuario
   * @throws YonaException en caso de que no exista el usuario
   */
  DtUsuarioExt consultaDeUsuario(String nick) throws YonaException;

  /**
   * modifica los datos de un usuario
   *
   * @param nick del usuario
   * @param datosUsuario DtModUsuario con los datos para modificar
   * @throws YonaException en caso de que el usuario no exista
   */
  void modificarUsuario(String nick, DtModUsuario datosUsuario) throws YonaException;

  /**
   * A un Usuario ya creado le agrega un Seguidor que es otro usuario ya creado
   *
   * @param seguidor es el Usuario que va a seguir a otro
   * @param seguido es el Usuario al que se va seguir
   * @throws YonaException en caso de que uno/ o los dos usuario/s no exista/n
   */
  void seguirUsuario(String seguidor, String seguido) throws YonaException;

  /**
   * Se desasocia el seguidor a un Usuario seguido
   *
   * @param seguidor es el Usuario que va a seguir a otro
   * @param seguido es el Usuario al que se va seguir
   * @throws YonaException en caso de que uno/ o los dos usuario/s no exista/n
   */
  void dejarSeguirUsuario(String seguidor, String seguido) throws YonaException;

  /**
   * Busca todos los canales publicos
   *
   * @param canalBusc es el nombre del canal a buscar (o la descripcion del canal)
   * @param ordenFecha en true para devolver los canales ordenados por fecha, false en caso
   *     contrario
   * @param ordenNombre en true para devolver los canales ordenados por nombre, false en caso
   *     contrario
   */
  List<DtCanal> buscarCanales(
      String canalBusc, boolean ordenFecha, boolean ordenNombre, int pagina, int offset)
      throws YonaException;

  void actualizarPasswd(String nickname, String password) throws YonaException;
}
