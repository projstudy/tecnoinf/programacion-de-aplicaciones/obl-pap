package org.uytube.controladores;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import org.uytube.datatypes.DtLista;
import org.uytube.datatypes.DtVideo;
import org.uytube.exception.YonaException;
import org.uytube.manejador.Persistencia;
import org.uytube.modelos.Canal;
import org.uytube.modelos.Categoria;
import org.uytube.modelos.Lista;
import org.uytube.modelos.ListaPredeterminada;
import org.uytube.modelos.Predeterminada;
import org.uytube.modelos.Usuario;
import org.uytube.modelos.Video;

class CLista implements ILista {

  @Override
  public void altaListaDefecto(String nombre) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    List<Lista> listas =
        em.createNamedQuery("Lista.buscarPorNombre", Lista.class)
            .setParameter("nombreLista", nombre)
            .getResultList();
    if (!listas.isEmpty()) {
      em.close();
      throw new YonaException("Oh por dios, la lista " + nombre + " ya existe");
    } else {
      ListaPredeterminada listaPredeterminada = new ListaPredeterminada(nombre);
      try {
        em.getTransaction().begin();
        em.persist(listaPredeterminada);
        List<Canal> canales =
            em.createNamedQuery("Canal.listarCanales", Canal.class).getResultList();
        for (Canal canal : canales) {
          Predeterminada predeterminada = new Predeterminada(nombre);
          predeterminada.setCanal(canal);
          em.persist(predeterminada);
        }
        em.getTransaction().commit();
      } catch (Exception e) {
        if (e instanceof RollbackException) {
          if (em.getTransaction().isActive()) {
            em.getTransaction().rollback();
          }
        }
        throw new YonaException("Oh por dios estimado, que ha hecho?!");
      } finally {
        em.close();
      }
    }
  }

  @Override
  public void altaListaParticular(String nickname, DtLista dtLista) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Categoria categoria = null;
    if (dtLista.getCategoria() != null) {
      categoria = em.find(Categoria.class, dtLista.getCategoria());
      if (categoria == null) {
        em.close();
        throw new YonaException("Oh por dios, no exsite la categoria " + dtLista.getCategoria());
      }
    }
    Usuario usuario = em.find(Usuario.class, nickname);
    if (usuario != null) {
      try {
        em.getTransaction().begin();
        Canal canal = usuario.getCanal();
        Lista lista = new Lista(dtLista.getNombre(), dtLista.isPrivado());
        lista.setCategoria(categoria);
        canal.addLista(lista);
        lista.setCanal(canal);
        em.persist(lista);
        em.getTransaction().commit();
      } catch (YonaException e) {
        throw e;
      } catch (Exception e) {
        if (e instanceof RollbackException && em.getTransaction().isActive()) {
          em.getTransaction().rollback();
        }
        throw new YonaException("Oh por dios estimado, que ha hecho?");
      } finally {
        em.close();
      }
    } else {
      em.close();
      throw new YonaException("El usuario ingresado no existe.");
    }
  }

  @Override
  public ArrayList<DtLista> listasParticularesUsuarioPublico(String nickname) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Usuario usuario = em.find(Usuario.class, nickname);
    if (usuario != null) {
      ArrayList<DtLista> dtListas = new ArrayList<>();
      List<Lista> listas =
          em.createNamedQuery("Canal.getParticularesPublicas", Lista.class)
              .setParameter("nombreCanal", usuario.getCanal().getNombre())
              .setParameter("clases", Collections.singletonList(Predeterminada.class))
              .getResultList();
      for (Lista lista : listas) {
        dtListas.add(Mapper.getDtLista(lista));
      }
      em.close();
      return dtListas;
    } else {
      em.close();
      throw new YonaException("Oh por dios, el usuario " + nickname + " no existe");
    }
  }

  @Override
  public ArrayList<DtLista> listasParticularesUsuario(String nickname) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Usuario usuario = em.find(Usuario.class, nickname);
    if (usuario != null) {
      ArrayList<DtLista> dtListas = new ArrayList<>();
      List<Lista> listas =
          em.createNamedQuery("Canal.getParticulares", Lista.class)
              .setParameter("nombreCanal", usuario.getCanal().getNombre())
              .setParameter("clases", Collections.singletonList(Predeterminada.class))
              .getResultList();
      for (Lista lista : listas) {
        dtListas.add(Mapper.getDtLista(lista));
      }
      em.close();
      return dtListas;
    } else {
      em.close();
      throw new YonaException("Oh por dios, el usuario " + nickname + " no existe");
    }
  }

  @Override
  public void modificarLista(boolean privacidad, String categoria, int idLista) {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Lista lista = em.find(Lista.class, idLista);
    if (lista != null) {
      Categoria cate = null;
      if (categoria != null) {
        cate = em.find(Categoria.class, categoria);
        if (cate == null) {
          throw new YonaException("Oh por dios, no existe esa Categoria");
        }
      }
      lista.setCategoria(cate);
      lista.setPrivada(privacidad);
      persistLista(em, lista);
    } else {
      em.close();
      throw new YonaException("Oh por dios, no existe esa Lista");
    }
  }

  @Override
  public ArrayList<DtLista> listasDeUsuario(String nickname) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    List<Lista> listas =
        em.createNamedQuery("Lista.listasUsuario", Lista.class)
            .setParameter("parmNickName", nickname)
            .getResultList();
    ArrayList<DtLista> dtListas = new ArrayList<>();
    for (Lista lista : listas) {
      dtListas.add(Mapper.getDtLista(lista));
    }
    em.close();
    return dtListas;
  }

  @Override
  public void agregarVideoLista(int idVideo, int idLista) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Lista lista = em.find(Lista.class, idLista);
    if (lista != null) {
      Video video = em.find(Video.class, idVideo);
      if (video != null) {
        lista.addVideo(video);
        persistLista(em, lista);
      } else {
        em.close();
        throw new YonaException("Oh por dios, el video no existe");
      }
    } else {
      em.close();
      throw new YonaException("Oh por dios, la lista no existe");
    }
  }

  @Override
  public void quitarVideoLista(int idVideo, int idLista) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Lista lista = em.find(Lista.class, idLista);
    if (lista != null) {
      Video video = em.find(Video.class, idVideo);
      if (video != null) {
        lista.removeVideo(video);
        persistLista(em, lista);
      } else {
        em.close();
        throw new YonaException("Oh por dios, el video no existe");
      }
    } else {
      em.close();
      throw new YonaException("Oh por dios, la lista no existe");
    }
  }

  private void persistLista(EntityManager em, Lista lista) {
    try {
      em.getTransaction().begin();
      em.persist(lista);
      em.getTransaction().commit();
    } catch (Exception e) {
      if (e instanceof RollbackException && em.getTransaction().isActive()) {
        em.getTransaction().rollback();
      }
      throw new YonaException("Oh por dios estimado, que ha hecho?");
    } finally {
      em.close();
    }
  }

  @Override
  public HashMap<Integer, String> listarVideos(int idLista) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Lista lista = em.find(Lista.class, idLista);
    if (lista != null) {
      HashMap<Integer, String> data = new HashMap<>();
      for (Video video : lista.getVideos()) {
        data.put(video.getId(), video.getNombre());
      }
      em.close();
      return data;
    } else {
      em.close();
      throw new YonaException("Oh por dios, la lista no existe");
    }
  }

  @Override
  public DtLista consultaLista(int idLista) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Lista lista = em.find(Lista.class, idLista);
    if (lista != null) {
      ArrayList<DtVideo> videos = new ArrayList<>();
      DtLista listas = Mapper.getDtLista(lista);
      em.close();
      return listas;
    } else {
      em.close();
      throw new YonaException("Oh por dios, la lista no existe");
    }
  }

  public List<DtLista> buscarListas(
      String listaBusc, boolean ordenFecha, boolean ordenNombre, int pagina, int offset) {
    List<DtLista> dtListas = new ArrayList<>();
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Query query;

    if (!ordenFecha && !ordenNombre) {
      query =
          em.createNamedQuery("Lista.buscar", Lista.class)
              .setParameter("nombreLista", listaBusc)
              .setParameter("clases", Collections.singletonList(Predeterminada.class))
              .setMaxResults(offset)
              .setFirstResult(pagina * offset);
    } else if (ordenFecha) {
      query =
          em.createNamedQuery("Lista.buscarOrdenarPorFecha", Lista.class)
              .setParameter(1, "Predeterminada")
              .setParameter(2, listaBusc)
              .setMaxResults(offset)
              .setFirstResult(pagina * offset);
    } else {
      query =
          em.createNamedQuery("Lista.buscarOrdenarPorNombre", Lista.class)
              .setParameter(1, "Predeterminada")
              .setParameter(2, listaBusc)
              .setMaxResults(offset)
              .setFirstResult(pagina * offset);
    }
    try {
      @SuppressWarnings("unchecked")
      List<Lista> resultado = query.getResultList();
      for (Lista lista : resultado) {
        dtListas.add(Mapper.getDtLista(lista));
      }
      return dtListas;
    } catch (Exception e) {
      e.printStackTrace();
      throw new YonaException("Oops");
    } finally {
      em.close();
    }
  }
}
