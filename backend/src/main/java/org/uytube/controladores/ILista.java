package org.uytube.controladores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.uytube.datatypes.DtLista;
import org.uytube.exception.YonaException;

public interface ILista {

  /**
   * Crea una lista predeterminada y se asigna a todos los usuarios existentes
   *
   * @param nombre noombre de la nueva lista
   * @throws YonaException si la lista ya existe, o ocurre un error en la transaction
   */
  void altaListaDefecto(String nombre) throws YonaException;

  /**
   * Crea una lista particular para un usuario
   *
   * @param nickname nombre del usuario
   * @param dtLista datos de la nueva lista
   * @throws YonaException si no existe la categoria pasada en el dtLista, o no existe el usuario ,
   *     o ocurre un error en la transaction
   */
  void altaListaParticular(String nickname, DtLista dtLista) throws YonaException;

  ArrayList<DtLista> listasParticularesUsuarioPublico(String nickname) throws YonaException;

  /**
   * @param nickname nombre del usuario a consultar
   * @return Una lista de sus Listas Particulares de reproduccion
   * @throws YonaException si no existe la categoria pasada en el dtLista, o no existe el usuario ,
   *     o ocurre un error en la transaction
   */
  ArrayList<DtLista> listasParticularesUsuario(String nickname) throws YonaException;

  /**
   * Modifica una lista dada
   *
   * @param privacidad cambia el estado de la privacidad de la lista
   * @param categoria cambia la categoria de la lista
   * @param idLista id de la lista a modificar
   */
  void modificarLista(boolean privacidad, String categoria, int idLista);

  /**
   * @param nickname nick del usuario a consultar
   * @return un set de listas de un usuario (particulares y predeterminadas)
   * @throws YonaException si no existe el usuario
   */
  ArrayList<DtLista> listasDeUsuario(String nickname) throws YonaException;

  /**
   * Agrega un video de una lista
   *
   * @param idVideo id del video que se desea agregar a una lista
   * @param idLista id de la lista destino
   * @throws YonaException si no existe el video, la lista o el video ya se encuentra en la lista
   */
  void agregarVideoLista(int idVideo, int idLista) throws YonaException;

  /**
   * Quita un video de una lista
   *
   * @param idVideo id del video que se desea agregar a una lista
   * @param idLista id de la lista destino
   * @throws YonaException si no existe el video, la lista
   */
  void quitarVideoLista(int idVideo, int idLista) throws YonaException;

  /**
   * @param idLista id de la lista a consultar
   * @return lista los videos de una lista
   * @throws YonaException si no existe la lista
   */
  HashMap<Integer, String> listarVideos(int idLista) throws YonaException;

  /**
   * @param idLista id de la lista a consultar
   * @return un DtLista que contiene todos los datos de la lista
   * @throws YonaException si no existe la lista
   */
  DtLista consultaLista(int idLista) throws YonaException;

  /**
   * @param listaBusc es por el nombre de lista a buscar
   * @param ordenFecha en true en caso de devolver las listas ordenadas por fecha
   * @param ordenNombre en true en caso de devolver las listas ordenadas por Nombre
   * @return un DtLista que contiene todos los datos de la lista
   */
  List<DtLista> buscarListas(
      String listaBusc, boolean ordenFecha, boolean ordenNombre, int pagina, int offset);
}
