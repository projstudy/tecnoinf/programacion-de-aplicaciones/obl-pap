/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.controladores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import org.uytube.datatypes.DtComentario;
import org.uytube.datatypes.DtValoracion;
import org.uytube.datatypes.DtVideo;
import org.uytube.datatypes.DtVideoExt;
import org.uytube.exception.YonaException;
import org.uytube.manejador.Persistencia;
import org.uytube.modelos.Categoria;
import org.uytube.modelos.Comentario;
import org.uytube.modelos.Usuario;
import org.uytube.modelos.Valoracion;
import org.uytube.modelos.Video;

/** @author mpiccardo */
class CVideo implements IVideo {

  @Override
  public void crearVideo(DtVideo dtVideo, String nick) throws YonaException {
    // Para buscar el usuario uso la controladora de usuarios, o busco directo en
    // persistencia...
    // CUsuario cUsuarios = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
    EntityManager em = Persistencia.getInstance().getEntityManager();
    // busco y y verifico que exista el usuario
    Usuario u = em.find(Usuario.class, nick);
    if (u == null) {
      em.close();
      throw new YonaException("El usuairo ingresado no existe.");
    } else {
      // verifico si tiene categoria y la cargo sino queda null (es opcional)
      Categoria cat = null;
      if (dtVideo.getCategoria() != null) {
        cat = em.find(Categoria.class, dtVideo.getCategoria());
      }
      // creo el obj video
      Video nuevoVideo =
          new Video(
              dtVideo.getNombre(),
              dtVideo.getDescripcion(),
              dtVideo.getDuracion(),
              dtVideo.getFechaPublicacion(),
              dtVideo.getUrl(),
              dtVideo.isPrivado(),
              cat);
      // obtengo el canal del usuario y agrego el video
      u.getCanal().addVideo(nuevoVideo);
      nuevoVideo.setCanal(u.getCanal());
      try {
        em.getTransaction().begin();
        em.persist(nuevoVideo);
        em.getTransaction().commit();
        em.close();
      } catch (Exception exc) {
        if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
          em.getTransaction().rollback();
        }
        exc.printStackTrace();
        throw new YonaException("Error al guardar datos. ");
      } finally {
        em.close();
      }
    }
  }

  @Override
  public HashMap<Integer, String> listarVideosUsuario(String nick) throws YonaException {
    HashMap<Integer, String> data = new HashMap<>();
    EntityManager em = Persistencia.getInstance().getEntityManager();
    // cuando se retornan campos, se obtiene una lista de array de objectos.
    List<Object[]> videos =
        em.createNamedQuery("Video.videosUsuario", Object[].class)
            .setParameter("nick", nick)
            .getResultList();
    for (Object[] obj : videos) {
      int id = (int) obj[0];
      String nombre = (String) obj[1];
      data.put(id, nombre);
    }
    em.close();
    return data;
  }

  @Override
  public ArrayList<DtVideo> listarVideosPublicosDeCanal(String nick) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    // cuando se retornan campos, se obtiene una lista de array de objectos.
    List<Video> videos =
        em.createNamedQuery("Video.videosPublicosUsuario", Video.class)
            .setParameter("nick", nick)
            .getResultList();
    ArrayList<DtVideo> data = new ArrayList<>();
    for (Video video : videos) {
      data.add(Mapper.getDtVideo(video));
    }
    em.close();
    return data;
  }

  @Override
  public ArrayList<DtVideo> listarVideosPublicosPrivados(String nick) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    // cuando se retornan campos, se obtiene una lista de array de objectos.
    List<Video> videos =
        em.createNamedQuery("Video.videosPublicosyPrivadoUsuario", Video.class)
            .setParameter("nick", nick)
            .getResultList();
    ArrayList<DtVideo> data = new ArrayList<>();
    for (Video video : videos) {
      data.add(Mapper.getDtVideo(video));
    }
    em.close();
    return data;
  }

  @Override
  public void modificarVideo(int id, DtVideo dtVideo) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Video vmod = em.find(Video.class, id);
    if (vmod != null) {
      Categoria cat = null;
      if (dtVideo.getCategoria() != null) {
        cat = em.find(Categoria.class, dtVideo.getCategoria());
      }
      List<Video> videos =
          em.createNamedQuery("Video.existeVideoEnCanal", Video.class)
              .setParameter("nombreVideo", dtVideo.getNombre())
              .setParameter("idVideo", id)
              .setParameter("canal", vmod.getCanal())
              .getResultList();
      if (!videos.isEmpty()) {
        em.close();
        throw new YonaException("Video con nombre duplicado");
      }
      try {
        em.getTransaction().begin();
        vmod.setCategoria(cat);
        vmod.setDescripcion(dtVideo.getDescripcion());
        vmod.setDuracion(dtVideo.getDuracion());
        vmod.setFechaPublicacion(dtVideo.getFechaPublicacion());
        vmod.setNombre(dtVideo.getNombre());
        vmod.setPrivado(dtVideo.isPrivado());
        vmod.setURL(dtVideo.getUrl());
        em.getTransaction().commit();
        em.close();
      } catch (Exception exc) {
        if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
          em.getTransaction().rollback();
        }
        throw new YonaException("Error al modificar datos. " + exc.getMessage());
      } finally {
        em.close();
      }
    } else {
      em.close();
      throw new YonaException("Esa idea no existe");
    }
  }

  @Override
  public DtVideoExt verDatosVideo(int id) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Video v = em.find(Video.class, id);
    if (v == null) {
      em.close();
      throw new YonaException("No existe el video");
    }
    DtVideoExt dt = Mapper.getDtVideoExt(v);
    em.close();
    return dt;
  }

  @Override
  public List<DtComentario> listarComentarios(int idVideo) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    try {
      List<DtComentario> lis = new ArrayList<>();
      Video v = em.find(Video.class, idVideo);
      for (Comentario c : v.getComentarios()) {
        lis.add(Mapper.getDtComentario(c));
      }
      return lis;
    } catch (YonaException ex) {
      throw new YonaException("Error en listarComentarios. " + ex.getMessage());
    } finally {
      em.close();
    }
  }

  @Override
  public int responderComentario(int idCom, DtComentario c) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    try {
      Comentario coment = em.find(Comentario.class, idCom);
      Usuario u = em.find(Usuario.class, c.getNickname());
      try {
        em.getTransaction().begin();
        Comentario resp = new Comentario(c.getFechaPublicacion(), c.getTexto(), u);
        em.persist(resp);
        coment.addRespuesta(resp);
        // no se si hay que persistir o ya lo guarda.
        // em.persist(coment);
        em.getTransaction().commit();
        em.close();
        return resp.getId();
      } catch (Exception exc) {
        if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
          em.getTransaction().rollback();
        }
        throw new YonaException("Error al al guardar respuesta. " + exc.getMessage());
      }
    } catch (YonaException ex) {
      throw new YonaException("Error en responderComentario. " + ex.getMessage());
    } finally {
      em.close();
    }
  }

  @Override
  public int comentarVideo(int idVideo, DtComentario c) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    try {
      Video v = em.find(Video.class, idVideo);
      Usuario u = em.find(Usuario.class, c.getNickname());
      try {
        em.getTransaction().begin();
        Comentario coment = new Comentario(c.getFechaPublicacion(), c.getTexto(), u);
        v.addComentario(coment);
        em.persist(coment);
        em.getTransaction().commit();
        em.close();
        return coment.getId();
      } catch (Exception exc) {
        if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
          em.getTransaction().rollback();
        }
        throw new YonaException("Error al al guardar comentario. " + exc.getMessage());
      }
    } catch (YonaException ex) {
      throw new YonaException("Error en comentarVideo. " + ex.getMessage());
    } finally {
      em.close();
    }
  }

  @Override
  public void valorarVideo(int idVideo, DtValoracion dtValoracion) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Video video = em.find(Video.class, idVideo);
    Usuario usuario = em.find(Usuario.class, dtValoracion.getNickname());
    try {
      List<Valoracion> valoraciones =
          em.createNamedQuery("Valoracion.buscarValoracionDeUsuario", Valoracion.class)
              .setParameter("idVideo", idVideo)
              .setParameter("nickname", usuario.getNickname())
              .getResultList();
      em.getTransaction().begin();
      Valoracion valoracion;
      if (valoraciones.isEmpty()) {
        /*Crear valoracion*/
        valoracion = new Valoracion(dtValoracion.isLeGusta(), usuario);
        video.addValoracion(valoracion);
        em.persist(valoracion);
        em.persist(video);
      } else {
        /*actualizar valoracion*/
        valoracion = valoraciones.get(0);
        valoracion.setLeGusta(dtValoracion.isLeGusta());
        em.persist(valoracion);
      }
      em.getTransaction().commit();
    } catch (Exception exc) {
      if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
        em.getTransaction().rollback();
      }
      throw new YonaException("Error al al guardar valoracion. " + exc.getMessage());
    } finally {
      em.close();
    }
  }

  @Override
  public List<DtVideo> listarVideosPublicos() throws YonaException {
    List<DtVideo> data = new ArrayList<>();
    EntityManager em = Persistencia.getInstance().getEntityManager();
    // cuando se retornan campos, se obtiene una lista de array de objectos.
    List<Video> videos = em.createNamedQuery("Video.videosPublicos", Video.class).getResultList();
    for (Video v : videos) {
      data.add(Mapper.getDtVideo(v));
    }
    em.close();
    return data;
  }

  @Override
  public List<DtVideo> buscarVideos(
      String texto, Boolean sortFecha, Boolean sortAlfa, int pagina, int offset)
      throws YonaException {
    List<DtVideo> data = new ArrayList<>();
    EntityManager em = Persistencia.getInstance().getEntityManager();
    // cuando se retornan campos, se obtiene una lista de array de objectos.
    TypedQuery<Video> query;
    if (sortAlfa) {
      query =
          em.createNamedQuery("Video.buscarVideosAlfa", Video.class)
              .setParameter("nomVideo", texto)
              .setMaxResults(offset)
              .setFirstResult(pagina * offset);
    } else {
      if (sortFecha) {
        query =
            em.createNamedQuery("Video.buscarVideosAnio", Video.class)
                .setParameter("nomVideo", texto)
                .setMaxResults(offset)
                .setFirstResult(pagina * offset);
      } else {
        query =
            em.createNamedQuery("Video.buscarVideos", Video.class)
                .setParameter("nomVideo", texto)
                .setMaxResults(offset)
                .setFirstResult(pagina * offset);
      }
    }
    try {
      List<Video> videos = query.getResultList();
      for (Video v : videos) {
        data.add(Mapper.getDtVideoConCanal(v, v.getCanal()));
      }
      return data;
    } catch (Exception e) {
      e.printStackTrace();
      throw new YonaException("Oops");
    } finally {
      em.close();
    }
  }
}
