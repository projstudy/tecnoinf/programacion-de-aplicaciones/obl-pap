/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.controladores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.uytube.datatypes.*;
import org.uytube.exception.YonaException;

/** @author mpiccardo */
public interface IVideo {
  /**
   * Crea una video en el canal de un usuario
   *
   * @param dtVideo dtVideo con todos los datos necesarios para el alta de un video.
   * @param nick nombre del usuario a quien se le asigna el video
   * @throws YonaException en caso de que el usuario no exista, o el video ya exista en el canal de
   *     usuario
   */
  void crearVideo(DtVideo dtVideo, String nick) throws YonaException;

  ArrayList<DtVideo> listarVideosPublicosPrivados(String nick) throws YonaException;

  /**
   * @param id id del video a modificar
   * @param dtVideo datos para modificar el video
   * @throws YonaException en caso de que el video no exista
   */
  void modificarVideo(int id, DtVideo dtVideo) throws YonaException;

  /**
   * @param nick del usuario que se quieren listar sus videos
   * @return retorna un set de DtVideoLista
   * @throws YonaException en caso de que el usuario no exista
   */
  HashMap<Integer, String> listarVideosUsuario(String nick) throws YonaException;

  /**
   * @param id id del video a consultar
   * @return retorna un DtVideoExt con todos los datos del video
   * @throws YonaException en caso de que el video no exista
   */
  DtVideoExt verDatosVideo(int id) throws YonaException;

  List<DtComentario> listarComentarios(int idVideo) throws YonaException;

  int responderComentario(int idCom, DtComentario c) throws YonaException;

  int comentarVideo(int idVideo, DtComentario c) throws YonaException;

  void valorarVideo(int idVideo, DtValoracion v) throws YonaException;

  List<DtVideo> listarVideosPublicos() throws YonaException;

  List<DtVideo> buscarVideos(
      String texto, Boolean sortFecha, Boolean sortAlfa, int pagina, int offset)
      throws YonaException;

  ArrayList<DtVideo> listarVideosPublicosDeCanal(String nick) throws YonaException;
}
