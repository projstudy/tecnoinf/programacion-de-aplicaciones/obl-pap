/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.controladores;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import org.uytube.datatypes.DtLista;
import org.uytube.datatypes.DtListaProp;
import org.uytube.datatypes.DtListasVideos;
import org.uytube.datatypes.DtVideo;
import org.uytube.datatypes.DtVideoProp;
import org.uytube.exception.YonaException;
import org.uytube.manejador.Persistencia;
import org.uytube.modelos.Categoria;
import org.uytube.modelos.Lista;
import org.uytube.modelos.Video;

/** @author juan */
class CCategoria implements ICategoria {

  public CCategoria() {}

  @Override
  public void altaCategoria(String nombre) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Categoria categoria = em.find(Categoria.class, nombre);

    if (categoria == null) {
      categoria = new Categoria(nombre);
      try {
        em.getTransaction().begin();
        em.persist(categoria);
        em.getTransaction().commit();
      } catch (Exception exc) {
        if (exc instanceof RollbackException && em.getTransaction().isActive()) {
          em.getTransaction().rollback();
        }
        throw new YonaException("Oh por dios estimado, que ha hecho?");
      } finally {
        em.close();
      }
    } else {
      em.close();
      throw new YonaException("Oh por dios, ya existe la categoria " + nombre);
    }
  }

  @Override
  public List<String> listarCategorias() {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    List<String> categorias =
        em.createNamedQuery("Categoria.listarTodas", String.class)
            .getResultList(); // getSingleResult try y catch
    em.close();
    return categorias;
  }

  @Override
  public ArrayList<DtVideoProp> consultaCategoria(String nombre) {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Categoria categoria = em.find(Categoria.class, nombre);

    if (categoria == null) {
      em.close();
      throw new YonaException("Oh por dios, no existe la Categoria " + nombre);

    } else {
      List<Object[]> videos =
          em.createNamedQuery("Categoria.listarVideos", Object[].class)
              .setParameter("nombreCategoria", nombre)
              .getResultList();
      ArrayList<DtVideoProp> resultado = new ArrayList<>();
      for (Object[] video : videos) {
        String nombreVideo = (String) video[0];
        String propietario = (String) video[1];
        resultado.add(Mapper.getDtVideoProp(nombreVideo, propietario));
      }
      em.close();
      return resultado;
    }
  }

  @Override
  public ArrayList<DtListaProp> listarListas(String nombre) {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Categoria categoria = em.find(Categoria.class, nombre);

    if (categoria == null) {
      em.close();
      throw new YonaException("Oh por dios, no existe la Categoria " + nombre);
    } else {
      List<Object[]> listas =
          em.createNamedQuery("Categoria.listarListas", Object[].class)
              .setParameter("nombreCategoria", nombre)
              .getResultList();
      ArrayList<DtListaProp> resultado = new ArrayList<>();
      for (Object[] lista : listas) {
        String nombreLista = (String) lista[0];
        String propietario = (String) lista[1];
        resultado.add(Mapper.getDtListaProp(nombreLista, propietario));
      }
      em.close();
      return resultado;
    }
  }

  @Override
  public DtListasVideos consultaCategoriaPublica(String nombre) {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Categoria categoria = em.find(Categoria.class, nombre);
    if (categoria == null) {
      em.close();
      throw new YonaException("Oh por dios, no existe la Categoria " + nombre);
    } else {
      List<Video> videos =
          em.createNamedQuery("Categoria.listarVideosPublicos", Video.class)
              .setParameter("nombreCategoria", nombre)
              .getResultList();
      ArrayList<DtVideo> dtVideos = new ArrayList<>();
      for (Video video : videos) {
        dtVideos.add(Mapper.getDtVideoConCanal(video, video.getCanal()));
      }

      List<Lista> listas =
          em.createNamedQuery("Categoria.listarListasPublicas", Lista.class)
              .setParameter("nombreCategoria", nombre)
              .getResultList();
      ArrayList<DtLista> dtListas = new ArrayList<>();
      for (Lista lista : listas) {
        dtListas.add(Mapper.getDtListaConCanal(lista, lista.getCanal()));
      }
      DtListasVideos dt = new DtListasVideos(dtListas, dtVideos);
      em.close();
      return dt;
    }
  }
}
