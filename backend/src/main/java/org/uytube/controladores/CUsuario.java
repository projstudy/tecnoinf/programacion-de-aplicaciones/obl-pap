/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.controladores;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import org.uytube.datatypes.DtCanal;
import org.uytube.datatypes.DtModUsuario;
import org.uytube.datatypes.DtUsuario;
import org.uytube.datatypes.DtUsuarioExt;
import org.uytube.exception.YonaException;
import org.uytube.manejador.Persistencia;
import org.uytube.modelos.Canal;
import org.uytube.modelos.Categoria;
import org.uytube.modelos.ListaPredeterminada;
import org.uytube.modelos.Predeterminada;
import org.uytube.modelos.Usuario;

/** @author juan */
class CUsuario implements IUsuario {

  @Override
  public List<DtCanal> listarCanalesPublicos() {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    List<DtCanal> dtCanales = new ArrayList<>();
    List<Usuario> usuarios =
        em.createNamedQuery("Canal.listarCanalesPublicos", Usuario.class)
            .getResultList(); // getSingleResult try y catch
    usuarios.forEach((Usuario u) -> dtCanales.add(Mapper.getDtCanalConFoto(u.getCanal(), u)));
    em.close();
    return dtCanales;
  }

  @Override
  public DtUsuarioExt login(String nickname, String password) {
    EntityManager em = Persistencia.getInstance().getEntityManager();

    Usuario usuario = em.find(Usuario.class, nickname);
    if (usuario == null) {
      em.close();
      throw new YonaException("Usuario o contraseña incorrecto.");
    } else {
      if (!usuario.checkPassword(password)) {
        throw new YonaException("Usuario o contraseña incorrecto.");
      }
      DtUsuarioExt dt = Mapper.getDtUsuarioExt(usuario);
      em.close();
      return dt;
    }
  }

  @Override
  public void altausuario(DtUsuario dtUsuario, DtCanal dtCanal) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Canal canal = em.find(Canal.class, dtCanal.getNombre());
    Categoria categoria = null;
    if (dtCanal.getCategoria() != null) {
      categoria = em.find(Categoria.class, dtCanal.getCategoria());
      if (categoria == null) {
        throw new YonaException(
            "Oh por dios, la categoria " + dtCanal.getCategoria() + " no existe.");
      }
    }
    if (canal != null) {
      em.close();
      throw new YonaException("Oh por dios, ya existe el canal " + dtCanal.getNombre());
    }
    List<Usuario> existe =
        em.createNamedQuery("Usuario.verificoNickYMail", Usuario.class)
            .setParameter("parmEmail", dtUsuario.getEmail())
            .setParameter("parmNickname", dtUsuario.getNickname())
            .getResultList();
    if (!existe.isEmpty()) {
      em.close();
      throw new YonaException("Oh por dios, ya existe un usuario con esos datos.");
    }
    canal = new Canal(dtCanal.getNombre(), dtCanal.getDescripcion(), dtCanal.isPrivado());
    canal.setCategoria(categoria);
    List<ListaPredeterminada> listas =
        em.createNamedQuery("ListaPredeterminada.listar", ListaPredeterminada.class)
            .getResultList();
    Usuario usuario =
        new Usuario(
            dtUsuario.getNickname(),
            dtUsuario.getPassword(),
            dtUsuario.getFoto(),
            dtUsuario.getNombre(),
            dtUsuario.getApellido(),
            dtUsuario.getEmail(),
            dtUsuario.getFechaNacimiento(),
            canal);

    try {
      em.getTransaction().begin();
      em.persist(usuario);
      for (ListaPredeterminada lista : listas) {
        Predeterminada predeterminada = new Predeterminada(lista.getNombre());
        canal.addLista(predeterminada);
        predeterminada.setCanal(canal);
        em.persist(predeterminada);
      }
      em.persist(canal);
      em.getTransaction().commit();
      em.close();
    } catch (Exception exc) {
      if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
        em.getTransaction().rollback();
      }
      em.close();
      throw new YonaException("Oh por dios estimado, que ha hecho?");
    }
  }

  @Override
  public List<String> listarUsuarios() {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    List<String> usuarios =
        em.createNamedQuery("Usuario.getUsuarios", String.class)
            .getResultList(); // getSingleResult try y catch
    em.close();
    return usuarios;
  }

  @Override
  public DtUsuarioExt consultaDeUsuario(String nick) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Usuario usuario = em.find(Usuario.class, nick);
    if (usuario == null) {
      em.close();
      throw new YonaException("Oh por dios, no existe ese Usuario " + nick);
    } else {
      DtUsuarioExt dt = Mapper.getDtUsuarioExt(usuario);
      em.close();
      return dt;
    }
  }

  @Override
  public void modificarUsuario(String nick, DtModUsuario datosUsuario) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Usuario usuario = em.find(Usuario.class, nick);
    if (usuario == null) {
      em.close();
      throw new YonaException("Oh por dios, no existe ese Usuario " + nick);
    } else {
      try {
        em.getTransaction().begin();
        usuario.setNombre(datosUsuario.getNombre());
        usuario.setApellido(datosUsuario.getApellido());
        usuario.setDate(datosUsuario.getFechaNacimiento());
        usuario.getCanal().setPrivado(datosUsuario.isPrivado());
        usuario.setImg(datosUsuario.getFoto());
        em.getTransaction().commit();
      } catch (Exception e) {
        if ((e instanceof RollbackException) && (em.getTransaction().isActive())) {
          em.getTransaction().rollback();
        }
        throw new YonaException("Error al modificar datos de Usuario. " + e.getMessage());
      } finally {
        em.close();
      }
    }
  }

  public void seguirUsuario(String seguidor, String seguido) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Usuario uSeguidor = em.find(Usuario.class, seguidor);
    if (uSeguidor == null) {
      em.close();
      throw new YonaException("Oh por dios, no existe el usuario " + seguidor);
    } else {
      Usuario uSeguido = em.find(Usuario.class, seguido);
      if (uSeguido == null) {
        throw new YonaException("Oh por dios, no existe el usuario " + seguido);
      } else {
        uSeguido.addSuscripctor(uSeguidor);
        try {
          em.getTransaction().begin();
          em.persist(uSeguido);
          em.persist(uSeguidor);
          em.getTransaction().commit();
        } catch (Exception exc) {
          if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
            em.getTransaction().rollback();
          }
          throw new YonaException("Oh por dios estimado, que ha hecho?");
        } finally {
          em.close();
        }
      }
    }
  }

  public void dejarSeguirUsuario(String seguidor, String seguido) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Usuario uSeguidor = em.find(Usuario.class, seguidor);
    if (uSeguidor == null) {
      em.close();
      throw new YonaException("Oh por dios, no existe el usuario " + seguidor);
    } else {
      Usuario uSeguido = em.find(Usuario.class, seguido);
      if (uSeguido == null) {
        em.close();
        throw new YonaException("Oh por dios, no existe el usuario " + seguido);
      } else {
        uSeguido.removeSuscripctor(uSeguidor);
        try {
          em.getTransaction().begin();
          em.persist(uSeguido);
          em.persist(uSeguidor);
          em.getTransaction().commit();
        } catch (Exception exc) {
          if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
            em.getTransaction().rollback();
          }
          throw new YonaException("Oh por dios estimado, que ha hecho?");
        } finally {
          em.close();
        }
      }
    }
  }

  public List<DtCanal> buscarCanales(
      String canalBusc, boolean ordenFecha, boolean ordenNombre, int pagina, int offset) {
    List<DtCanal> dtCanales = new ArrayList<>();
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Query query;
    if (!ordenFecha && !ordenNombre) {
      query =
          em.createNamedQuery("Canal.buscar", Usuario.class)
              .setParameter("nombreCanal", canalBusc)
              .setMaxResults(offset)
              .setFirstResult(pagina * offset);
    } else if (ordenFecha) {
      query =
          em.createNamedQuery("Canal.buscarOrdenadoPorFecha", Usuario.class)
              .setParameter(1, canalBusc)
              .setParameter(2, canalBusc)
              .setMaxResults(offset)
              .setFirstResult(pagina * offset);

    } else {
      query =
          em.createNamedQuery("Canal.buscarOrdenarPorNombre", Usuario.class)
              .setParameter(1, canalBusc)
              .setParameter(2, canalBusc)
              .setMaxResults(offset)
              .setFirstResult(pagina * offset);
    }
    try {
      @SuppressWarnings("unchecked")
      List<Usuario> canales = query.getResultList();
      canales.forEach(
          (Usuario usuario) ->
              dtCanales.add(Mapper.getDtCanalConFoto(usuario.getCanal(), usuario)));
      return dtCanales;
    } catch (Exception e) {
      throw new YonaException("Oops");
    } finally {
      em.close();
    }
  }

  public void actualizarPasswd(String nickname, String password) throws YonaException {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    Usuario user = em.find(Usuario.class, nickname);
    if (user == null) {
      em.close();
      throw new YonaException("Oh por dios, no existe el usuario " + user);
    } else {
      user.setPassword(password);
      try {
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
      } catch (Exception exc) {
        if ((exc instanceof RollbackException) && (em.getTransaction().isActive())) {
          em.getTransaction().rollback();
        }
        throw new YonaException("Oh por dios estimado, que ha hecho?");
      } finally {
        em.close();
      }
    }
  }
}
