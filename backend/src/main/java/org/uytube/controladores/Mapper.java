package org.uytube.controladores;

import java.util.ArrayList;
import org.uytube.datatypes.DtCanal;
import org.uytube.datatypes.DtComentario;
import org.uytube.datatypes.DtLista;
import org.uytube.datatypes.DtListaProp;
import org.uytube.datatypes.DtSeguidor;
import org.uytube.datatypes.DtUsuario;
import org.uytube.datatypes.DtUsuarioExt;
import org.uytube.datatypes.DtValoracion;
import org.uytube.datatypes.DtVideo;
import org.uytube.datatypes.DtVideoExt;
import org.uytube.datatypes.DtVideoProp;
import org.uytube.modelos.Canal;
import org.uytube.modelos.Comentario;
import org.uytube.modelos.Lista;
import org.uytube.modelos.Predeterminada;
import org.uytube.modelos.Usuario;
import org.uytube.modelos.Valoracion;
import org.uytube.modelos.Video;

class Mapper {

  /**
   * Construye un DtValoracion
   *
   * @param valoracion valoracion
   * @return DtValoracion
   */
  private static DtValoracion getDtValoracion(Valoracion valoracion) {
    return new DtValoracion(
        valoracion.getId(), valoracion.leGusta(), valoracion.getUsuario().getNickname());
  }

  /**
   * Construye un DtComentario
   *
   * @param comentario comentario
   * @return DtComentario
   */
  static DtComentario getDtComentario(Comentario comentario) {
    ArrayList<DtComentario> respuestas = new ArrayList<>();
    for (Comentario respuesta : comentario.getComentarios()) {
      respuestas.add(getDtComentario(respuesta));
    }
    return new DtComentario(
        comentario.getId(),
        comentario.getTexto(),
        comentario.getUsuairo().getNickname(),
        respuestas,
        comentario.getFecha(),
        comentario.getUsuairo().getImg());
  }

  /**
   * Construye un DtUsuario
   *
   * @param usuario usuario
   * @return DtUsuario
   */
  private static DtUsuario getDtUsuario(Usuario usuario) {
    if (usuario.getImg() == null || usuario.getImg().isEmpty()) {
      return new DtUsuario(
          usuario.getNickname(),
          "",
          usuario.getNombre(),
          usuario.getApellido(),
          usuario.getEmail(),
          usuario.getDate());
    }
    return new DtUsuario(
        usuario.getNickname(),
        "",
        usuario.getNombre(),
        usuario.getApellido(),
        usuario.getEmail(),
        usuario.getImg(),
        usuario.getDate());
  }

  /**
   * Construye un DtUsuarioExt
   *
   * @param usuario usuario
   * @return DtUsuarioExt
   */
  static DtUsuarioExt getDtUsuarioExt(Usuario usuario) {

    ArrayList<DtSeguidor> suscriptores = new ArrayList<>();
    for (Usuario suscriptor : usuario.getSuscriptores()) {
      suscriptores.add(new DtSeguidor(suscriptor.getNickname(), suscriptor.getImg()));
    }

    ArrayList<DtSeguidor> suscripciones = new ArrayList<>();
    for (Usuario suscripcion : usuario.getSuscripciones()) {
      suscripciones.add(new DtSeguidor(suscripcion.getNickname(), suscripcion.getImg()));
    }

    return new DtUsuarioExt(
        getDtUsuario(usuario), getDtCanal(usuario.getCanal()), suscriptores, suscripciones);
  }

  /**
   * Construye un DtLista
   *
   * @param lista lista
   * @return DtLista
   */
  static DtLista getDtLista(Lista lista) {
    String tipo;
    if (lista instanceof Predeterminada) {
      tipo = "Predeterminada";
    } else {
      tipo = "Personalizada";
    }

    ArrayList<DtVideo> videos = new ArrayList<>();
    lista
        .getVideos()
        .forEach(
            video -> {
              videos.add(getDtVideo(video));
            });

    if (lista.getCategoria() != null) {
      return new DtLista(
          lista.getNombre(),
          lista.getId(),
          lista.isPrivada(),
          lista.getCategoria().getNombre(),
          tipo,
          lista.getCanal().getNombre(),
          videos);
    }
    return new DtLista(
        lista.getNombre(),
        lista.getId(),
        lista.isPrivada(),
        null,
        tipo,
        lista.getCanal().getNombre(),
        videos);
  }

  /**
   * Construye un DtVideo
   *
   * @param video video
   * @return DtVideo
   */
  static DtVideo getDtVideo(Video video) {
    if (video.getCategoria() == null) {
      return new DtVideo(
          video.getId(),
          video.getNombre(),
          video.getURL(),
          video.getDescripcion(),
          video.getFechaPublicacion(),
          video.getDuracion(),
          video.isPrivado());
    }
    return new DtVideo(
        video.getId(),
        video.getNombre(),
        video.getURL(),
        video.getDescripcion(),
        video.getFechaPublicacion(),
        video.getCategoria().getNombre(),
        video.getDuracion(),
        video.isPrivado());
  }

  /**
   * Construye un DtVideoExt
   *
   * @param video video
   * @return DtVideoExt
   */
  static DtVideoExt getDtVideoExt(Video video) {
    ArrayList<DtValoracion> valoraciones = new ArrayList<>();
    for (Valoracion valoracion : video.getValoraciones()) {
      valoraciones.add(getDtValoracion(valoracion));
    }

    ArrayList<DtComentario> comentarios = new ArrayList<>();
    for (Comentario comentario : video.getComentarios()) {
      comentarios.add(getDtComentario(comentario));
    }
    return new DtVideoExt(
        getDtVideo(video), valoraciones, comentarios, video.getCanal().getNombre());
  }

  /**
   * Construye un DtCanal
   *
   * @param canal canal
   * @return DtCanal
   */
  static DtCanal getDtCanalConFoto(Canal canal, Usuario u) {
    if (canal.getCategoria() == null) {
      return new DtCanal(
          canal.getNombre(), canal.getDescripcion(), canal.isPrivado(), u.getImg(), "");
    }
    return new DtCanal(
        canal.getNombre(),
        canal.getDescripcion(),
        canal.isPrivado(),
        u.getImg(),
        canal.getCategoria().getNombre());
  }

  /**
   * Construye un DtCanal
   *
   * @param canal canal
   * @return DtCanal
   */
  private static DtCanal getDtCanal(Canal canal) {
    if (canal.getCategoria() == null) {
      return new DtCanal(canal.getNombre(), canal.getDescripcion(), canal.isPrivado(), "", "");
    }
    return new DtCanal(
        canal.getNombre(),
        canal.getDescripcion(),
        canal.isPrivado(),
        "",
        canal.getCategoria().getNombre());
  }

  /**
   * Construye un DtVideoProp
   *
   * @param video nombre del video
   * @param usuario nombre del usuario
   * @return DtVideoProp
   */
  static DtVideoProp getDtVideoProp(String video, String usuario) {
    return new DtVideoProp(video, usuario);
  }

  /**
   * Construye un DtListaProp
   *
   * @param lista nombre de la lista
   * @param usuario nombre del usuario
   * @return DtVideoProp
   */
  static DtListaProp getDtListaProp(String lista, String usuario) {
    return new DtListaProp(lista, usuario);
  }

  static DtVideo getDtVideoConCanal(Video video, Canal canal) {
    if (video.getCategoria() == null) {
      return new DtVideo(
          video.getNombre(),
          video.getURL(),
          video.getDescripcion(),
          video.getFechaPublicacion(),
          video.getDuracion(),
          video.isPrivado(),
          canal.getNombre(),
          video.getId());
    }
    return new DtVideo(
        video.getNombre(),
        video.getURL(),
        video.getDescripcion(),
        video.getFechaPublicacion(),
        video.getCategoria().getNombre(),
        video.getDuracion(),
        video.isPrivado(),
        canal.getNombre(),
        video.getId());
  }

  static DtLista getDtListaConCanal(Lista lista, Canal canal) {
    String tipo;
    if (lista instanceof Predeterminada) {
      tipo = "Predeterminada";
    } else {
      tipo = "Personalizada";
    }

    ArrayList<DtVideo> videos = new ArrayList<>();
    lista
        .getVideos()
        .forEach(
            video -> {
              videos.add(getDtVideo(video));
            });

    if (lista.getCategoria() != null) {
      return new DtLista(
          lista.getNombre(),
          lista.getId(),
          lista.isPrivada(),
          lista.getCategoria().getNombre(),
          tipo,
          canal.getNombre(),
          videos);
    }
    return new DtLista(lista.getNombre(), lista.getId(), lista.isPrivada(), null, tipo);
  }
}
