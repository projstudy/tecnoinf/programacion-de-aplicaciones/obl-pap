package org.uytube;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ILista;
import org.uytube.controladores.IUsuario;
import org.uytube.manejador.Persistencia;
import org.uytube.modelos.Canal;
import org.uytube.modelos.Categoria;
import org.uytube.modelos.Comentario;
import org.uytube.modelos.Lista;
import org.uytube.modelos.Usuario;
import org.uytube.modelos.Valoracion;
import org.uytube.modelos.Video;

public class DatosDePrueba {

  private static final Categoria deportes = new Categoria("deportes");
  private static final Categoria musica = new Categoria("musica");
  private static final Categoria juegos = new Categoria("juegos");
  private static final Categoria otros = new Categoria("otros");

  private void crearUsuarios() {

    String[][] usuarios =
        new String[][] {
          {"Elyn", "Crosskill", "Elyn", "ecrosskill0@ucoz.ru", "1/3/2019"},
          {"Sunny", "Fogden", "Sunny", "sfogden1@nymag.com", "10/31/2018"},
          {"Ivie", "Endle", "Ivie", "iendle2@nifty.com", "12/23/2018"},
          {"Berke", "Masselin", "Berke", "bmasselin3@chron.com", "4/24/2019"},
          {"Rocky", "Elden", "Rocky", "relden4@skyrock.com", "1/19/2019"},
          {"Isaak", "Pleat", "Isaak", "ipleat5@ehow.com", "10/1/2018"},
          {"Etheline", "Lapslie", "Etheline", "elapslie6@princeton.edu", "4/18/2019"},
          {"Kareem", "Jovic", "Kareem", "kjovic7@naver.com", "11/12/2018"},
          {"Tessa", "Tolossi", "Tessa", "ttolossi8@diigo.com", "11/9/2018"},
          {"Charlena", "Pride", "Charlena", "cpride9@cocolog-nifty.com", "3/28/2019"},
          {"Yehudit", "Calverley", "Yehudit", "ycalverleya@comsenz.com", "5/24/2019"},
          {"Katti", "Simioli", "Katti", "ksimiolib@theguardian.com", "7/17/2019"},
          {"Charmaine", "Akess", "Charmaine", "cakessc@webeden.co.uk", "6/30/2019"},
          {"Rorke", "D'eathe", "Rorke	,rdeathed@php.net", "1/30/2019"},
          {"Morgun", "Regus", "Morgun", "mreguse@ning.com", "11/9/2018"},
          {"Cody", "Stockwell", "Cody", "cstockwellf@ning.com", "9/26/2018"},
          {"Kakalina", "Petrescu", "Kakalina", "kpetrescug@plala.or.jp", "4/2/2019"},
          {"Weider", "Gianotti", "Weider", "wgianottih@sphinn.com", "3/31/2019"},
          {"Tracey", "Tomaskov", "Tracey", "ttomaskovi@google.com.hk", "11/9/2018"},
          {"Jannelle", "Warlowe", "Jannelle", "jwarlowej@accuweather.com", "1/25/2019"}
        };

    EntityManager em = Persistencia.getInstance().getEntityManager();
    try {
      em.getTransaction().begin();
      em.persist(musica);
      em.persist(deportes);
      em.persist(otros);
      em.persist(juegos);
      em.getTransaction().commit();
      Categoria[] categorias = new Categoria[] {deportes, otros, musica, juegos, null};
      String[] urls =
          new String[] {
            "https://www.youtube.com/watch?v=_L9h2w798yY",
            "https://www.youtube.com/watch?v=q9JClFXo6dM",
            "https://www.youtube.com/watch?v=_KpkoF5eMhw",
            "https://www.youtube.com/watch?v=IuKltB-wPV8",
            "https://www.youtube.com/watch?v=S1_Iv8bviiw",
            "https://www.youtube.com/watch?v=Jy9pxDYupkM",
            "https://www.youtube.com/watch?v=WdDxrfiLXok",
            "https://www.youtube.com/watch?v=vBKLp12Vp6o",
            "https://www.youtube.com/watch?v=xLYEmLyn5ps",
            "https://www.youtube.com/watch?v=kMBxx8b2T4s",
            "https://www.youtube.com/watch?v=TplZO6V4p9g",
            "https://www.youtube.com/watch?v=vSB9UCTP4_I",
            "https://www.youtube.com/watch?v=LG8xLXlUsaY",
            "https://www.youtube.com/watch?v=xV5q4L1yCZ0",
            "https://www.youtube.com/watch?v=f4wD8Au5MzA",
            "https://www.youtube.com/watch?v=VKqpGCUmfXM",
            "https://www.youtube.com/watch?v=uZygWU5-9iw",
            "https://www.youtube.com/watch?v=dEWjHzLjZOg",
            "https://www.youtube.com/watch?v=0OGJNnMRw8Y",
            "https://www.youtube.com/watch?v=45t_lhHWsX4",
            "https://www.youtube.com/watch?v=ATDMcsngHdg",
            "https://www.youtube.com/watch?v=M81E8cDxkpc",
            "https://www.youtube.com/watch?v=R26_F7pecqo",
            "https://www.youtube.com/watch?v=gcBxFClkBKg",
            "https://www.youtube.com/watch?v=3KaSLXl-Lns",
            "https://www.youtube.com/watch?v=KEI4qSrkPAs",
            "https://www.youtube.com/watch?v=7SVBUdPjDWk",
            "https://www.youtube.com/watch?v=4c2XEHhZpMA",
            "https://www.youtube.com/watch?v=NbA89YbWoL8",
            "https://www.youtube.com/watch?v=dpRpzbY9QU8",
            "https://www.youtube.com/watch?v=rszPXD8PSdY",
            "https://www.youtube.com/watch?v=T-RtG5Z-9jQ",
            "https://www.youtube.com/watch?v=bhnaqcUZ7GQ",
            "https://www.youtube.com/watch?v=EGuJCl2OyaQ",
            "https://www.youtube.com/watch?v=GdJR58uDtXk",
            "https://www.youtube.com/watch?v=OrSh2CHmEW4",
            "https://www.youtube.com/watch?v=kZL4_hYzBUM",
            "https://www.youtube.com/watch?v=vktvh_b1Q48",
            "https://www.youtube.com/watch?v=8IPq0v3UMKg",
            "https://www.youtube.com/watch?v=iDxovNO8RN0",
            "https://www.youtube.com/watch?v=enAcAXAXdfg",
            "https://www.youtube.com/watch?v=Y_mi9JnoFi4",
            "https://www.youtube.com/watch?v=841OAVjn8C0",
            "https://www.youtube.com/watch?v=SmU74_8bOcM",
            "https://www.youtube.com/watch?v=OUXtvkfAi7s",
            "https://www.youtube.com/watch?v=01Ue7Dbf424",
            "https://www.youtube.com/watch?v=lMulcnyxzxQ",
            "https://www.youtube.com/watch?v=5oa_kGfExuA",
            "https://www.youtube.com/watch?v=0tIhR6wo6DE",
            "https://www.youtube.com/watch?v=7ujarE3fCpw",
            "https://www.youtube.com/watch?v=F50yjSws9gQ",
            "https://www.youtube.com/watch?v=0f679wqZttE",
            "https://www.youtube.com/watch?v=x-PDZIQP5xs",
            "https://www.youtube.com/watch?v=HQZ3-OD0ml0",
            "https://www.youtube.com/watch?v=jTDvSqoWKhc",
            "https://www.youtube.com/watch?v=WbjEpPaq7e4",
            "https://www.youtube.com/watch?v=iDxovNO8RN0",
            "https://www.youtube.com/watch?v=w0J4cN-V_l8",
            "https://www.youtube.com/watch?v=S8CDdDLApdI",
            "https://www.youtube.com/watch?v=Wtx8yAhOHZ8",
            "https://www.youtube.com/watch?v=a_jvEE8JoAw",
            "https://www.youtube.com/watch?v=Zi9cK-lI190",
            "https://www.youtube.com/watch?v=9IEFD_JVYd0",
            "https://www.youtube.com/watch?v=7ZLULdlbapo",
            "https://www.youtube.com/watch?v=8_YwhDm-K_A",
            "https://www.youtube.com/watch?v=FisaHV2hQYc",
            "https://www.youtube.com/watch?v=wop_KfvY8GU",
            "https://www.youtube.com/watch?v=pIG2_N1drc4",
            "https://www.youtube.com/watch?v=W6HN9soeGa8",
            "https://www.youtube.com/watch?v=Y0ol5HPlPlA",
            "https://www.youtube.com/watch?v=QoRkRohKgGU"
          };
      for (int i = 0; i < usuarios.length; i++) {
        List<String> url = Arrays.asList(urls);
        Collections.shuffle(url);
        List<Categoria> list = Arrays.asList(categorias);
        Collections.shuffle(list);

        em.getTransaction().begin();
        Canal c = new Canal(usuarios[i][0], "Este es el canal de " + usuarios[i][0], (i % 2 == 0));
        Usuario u =
            new Usuario(
                usuarios[i][0],
                "admin",
                "",
                usuarios[i][0],
                usuarios[i][1],
                usuarios[i][3],
                new Date(),
                c);

        Lista lista1 = new Lista("Lista 1 de " + usuarios[i][0], true);
        lista1.setCategoria(musica);
        Lista lista2 = new Lista("Lista 2 de " + usuarios[i][0], false);
        lista2.setCategoria(otros);
        Lista lista3 = new Lista("Lista 3 de " + usuarios[i][0], true);

        lista1.setCanal(c);
        lista1.setCategoria(list.get(0));
        lista2.setCanal(c);
        lista2.setCategoria(list.get(1));
        lista3.setCanal(c);
        lista3.setCategoria(list.get(2));
        Video video1 =
            new Video(
                "Video 1 de " + usuarios[i][0],
                LoremIpsum.getInstance().getWords(5, 10),
                10 * i + 10,
                new Date(),
                url.get(1),
                true,
                deportes);
        Video video2 =
            new Video(
                "Video 2 de " + usuarios[i][0],
                LoremIpsum.getInstance().getWords(5, 10),
                10 * i + 30,
                new Date(),
                url.get(2),
                false,
                musica);
        Video video3 =
            new Video(
                "Video 3 de " + usuarios[i][0],
                LoremIpsum.getInstance().getWords(5, 10),
                10 * i + 13,
                new Date(),
                url.get(3),
                true);
        Video video4 =
            new Video(
                "Video 4 de " + usuarios[i][0],
                LoremIpsum.getInstance().getWords(5, 10),
                10 * i + 14,
                new Date(),
                url.get(4),
                true);
        Video video5 =
            new Video(
                "Video 5 de " + usuarios[i][0],
                LoremIpsum.getInstance().getWords(5, 10),
                10 * i + 15,
                new Date(),
                url.get(5),
                false);
        Video video6 =
            new Video(
                "Video 6 de " + usuarios[i][0],
                LoremIpsum.getInstance().getWords(5, 10),
                10 * i + 16,
                new Date(),
                url.get(6),
                true);
        Video video7 =
            new Video(
                "Video 7 de " + usuarios[i][0],
                LoremIpsum.getInstance().getWords(5, 10),
                10 * i + 17,
                new Date(),
                url.get(7),
                false);
        Video video8 =
            new Video(
                "Video 8 de " + usuarios[i][0],
                LoremIpsum.getInstance().getWords(5, 10),
                10 * i + 17,
                new Date(),
                url.get(8),
                true);

        video1.setCanal(c);
        video2.setCanal(c);
        video3.setCanal(c);
        video4.setCanal(c);
        video5.setCanal(c);
        video6.setCanal(c);
        video7.setCanal(c);
        video8.setCanal(c);

        lista2.addVideo(video1);
        lista2.addVideo(video2);
        lista2.addVideo(video5);
        lista2.addVideo(video6);

        lista1.addVideo(video1);
        lista1.addVideo(video2);
        lista1.addVideo(video3);
        lista1.addVideo(video4);

        em.persist(u);
        em.persist(c);

        em.persist(lista1);
        em.persist(lista2);
        em.persist(lista3);

        em.persist(video1);
        em.persist(video2);
        em.persist(video3);
        em.persist(video4);
        em.persist(video5);
        em.persist(video6);
        em.persist(video7);
        em.persist(video8);
        em.getTransaction().commit();
      }
    } catch (RollbackException e) {
      System.out.println("Error " + e.getMessage());
    } catch (PersistenceException e) {
      System.out.println("Eroror " + e.getMessage());
    } finally {
      em.close();
    }
  }

  private void cargarComentarios() {
    EntityManager em = Persistencia.getInstance().getEntityManager();
    List<Video> videos = em.createNamedQuery("Video.todosLosVideos", Video.class).getResultList();

    String[] array =
        new String[] {
          "Elyn",
          "Sunny",
          "Ivie",
          "Berke",
          "Rocky",
          "Charmaine",
          "Rorke",
          "Morgun",
          "Cody",
          "Kakalina",
          "Weider",
          "Jannelle"
        };

    Lorem lorem = LoremIpsum.getInstance();
    IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
    for (Video video : videos) {
      List<String> list = Arrays.asList(array);
      Collections.shuffle(list);
      Usuario u1 = em.find(Usuario.class, list.get(0));
      Usuario u2 = em.find(Usuario.class, list.get(1));
      Usuario u3 = em.find(Usuario.class, list.get(2));
      Usuario u4 = em.find(Usuario.class, list.get(3));
      Usuario u5 = em.find(Usuario.class, list.get(4));

      iUsuario.seguirUsuario(u1.getNickname(), u2.getNickname());
      iUsuario.seguirUsuario(u1.getNickname(), u3.getNickname());
      iUsuario.seguirUsuario(u1.getNickname(), u4.getNickname());
      iUsuario.seguirUsuario(u1.getNickname(), u5.getNickname());

      Random random = new Random();
      Valoracion v1 = new Valoracion(random.nextBoolean(), u1);
      random = new Random();
      Valoracion v2 = new Valoracion(random.nextBoolean(), u2);
      random = new Random();
      Valoracion v3 = new Valoracion(random.nextBoolean(), u3);
      random = new Random();
      Valoracion v4 = new Valoracion(random.nextBoolean(), u4);
      random = new Random();
      Valoracion v5 = new Valoracion(random.nextBoolean(), u5);

      Comentario m1 = new Comentario(new Date(), lorem.getWords(5, 10), u1);
      Comentario m2 = new Comentario(new Date(), lorem.getWords(5, 10), u2);
      Comentario m3 = new Comentario(new Date(), lorem.getWords(5, 10), u3);
      Comentario m4 = new Comentario(new Date(), lorem.getWords(5, 10), u4);
      Comentario m5 = new Comentario(new Date(), lorem.getWords(5, 10), u5);
      em.getTransaction().begin();
      m1.addRespuesta(m2);
      m1.addRespuesta(m3);
      m2.addRespuesta(m4);
      video.addComentario(m1);
      video.addComentario(m2);

      video.addValoracion(v1);
      video.addValoracion(v2);
      video.addValoracion(v3);
      video.addValoracion(v4);
      video.addValoracion(v5);

      em.persist(v1);
      em.persist(v2);
      em.persist(v3);
      em.persist(v4);
      em.persist(v5);
      em.persist(m1);
      em.persist(m2);
      em.persist(m3);
      em.persist(m4);
      em.persist(m5);
      em.getTransaction().commit();
    }
    em.close();
  }

  public DatosDePrueba() {
    crearUsuarios();
    cargarComentarios();
    ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);
    iLista.altaListaDefecto("Esta es por defecto :)");
  }
}
