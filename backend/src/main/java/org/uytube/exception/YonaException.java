package org.uytube.exception;

public class YonaException extends IllegalArgumentException {

  public YonaException() {}

  public YonaException(String s) {
    super(s);
  }
}
