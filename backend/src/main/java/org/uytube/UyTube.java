package org.uytube;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.uytube.ui.LoadingForm;
import org.uytube.ui.MainFrame;

public class UyTube {

  public static void main(String[] args) {
    try {
      // server de h2 para mas placer oh yeah
      UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
      UIManager.put("Table.focusCellHighlightBorder", BorderFactory.createEmptyBorder(0, 0, 0, 0));
      UIManager.put(
          "ComboBox:\"ComboBox.listRenderer\"[Selected].background", new Color(53, 47, 91));
      UIManager.put("nimbusOrange", new Color(53, 47, 91));
      // iniciar la ventana
      new LoadingForm().setVisible(true);
    } catch (ClassNotFoundException
        | InstantiationException
        | IllegalAccessException
        | UnsupportedLookAndFeelException ex) {
      Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
