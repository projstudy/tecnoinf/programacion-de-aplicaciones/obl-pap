package org.uytube.modelos;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
  @NamedQuery(name = "Categoria.listarTodas", query = "select c.nombre from Categoria c"),
  @NamedQuery(
      name = "Categoria.listarVideosPublicos",
      query =
          "select v from "
              + "Usuario u inner join "
              + "u.canal c inner join "
              + "c.videos v inner join "
              + "v.categoria cat "
              + "where cat.nombre = :nombreCategoria "
              + "and c.privado = false and v.privado = false"),
  @NamedQuery(
      name = "Categoria.listarListasPublicas",
      query =
          "select l from "
              + "Usuario u inner join "
              + "u.canal c inner join "
              + "c.listas l inner join "
              + "l.categoria cat "
              + "where cat.nombre = :nombreCategoria "
              + "and c.privado = false and l.privada = false"),
  @NamedQuery(
      name = "Categoria.listarVideos",
      query =
          "select v from "
              + "Usuario u inner join "
              + "u.canal c inner join "
              + "c.videos v inner join "
              + "v.categoria cat "
              + "where cat.nombre = :nombreCategoria"),
  @NamedQuery(
      name = "Categoria.listarListas",
      query =
          "select l.nombre, u.nickname from "
              + "Usuario u inner join "
              + "u.canal c inner join "
              + "c.listas l inner join "
              + "l.categoria cat "
              + "where cat.nombre = :nombreCategoria")
})
public class Categoria {

  @Id private String nombre;

  public Categoria() {}

  public Categoria(String nombre) {
    this.nombre = nombre;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
}
