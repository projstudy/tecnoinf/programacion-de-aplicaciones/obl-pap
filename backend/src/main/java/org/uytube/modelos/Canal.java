package org.uytube.modelos;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.uytube.exception.YonaException;

@NamedQueries({
  @NamedQuery(name = "Canal.listarCanales", query = "select c from Canal c"),
  @NamedQuery(
      name = "Canal.listarCanalesPublicos",
      query = "select u from Usuario u inner join u.canal c where c.privado=false"),
  @NamedQuery(
      name = "Canal.existeLista",
      query =
          "select l from Canal c inner join c.listas l where l.nombre = :nombreLista and c.nombre = :nombreCanal"),
  @NamedQuery(
      name = "Canal.getParticularesPublicas",
      query =
          "select l from Canal c join c.listas l where TYPE(l) not in :clases and c.nombre = :nombreCanal and c.privado=false"),
  @NamedQuery(
      name = "Canal.getParticulares",
      query =
          "select l from Canal c join c.listas l where TYPE(l) not in :clases and c.nombre = :nombreCanal"),
  @NamedQuery(
      name = "Canal.buscar",
      query =
          "select u from Usuario u  inner join u.canal c where c.privado=false and "
              + "(UPPER(c.nombre) like CONCAT('%', UPPER(:nombreCanal),'%') or "
              + "UPPER(c.descripcion) like CONCAT('%', UPPER(:nombreCanal),'%'))"),
})
@NamedNativeQueries({
  @NamedNativeQuery(
      name = "Canal.buscarOrdenarPorNombre",
      query =
          "SELECT u.* "
              + "FROM USUARIO u"
              + " JOIN CANAL ON u.CANAL_NOMBRE = CANAL.NOMBRE"
              + " WHERE CANAL.PRIVADO = FALSE AND "
              + "( UPPER(CANAL.NOMBRE) LIKE ('%'||UPPER(?)||'%')"
              + " OR UPPER(CANAL.DESCRIPCION) LIKE ('%'||UPPER(?)||'%') )"
              + "GROUP BY u.NOMBRE",
      resultClass = Usuario.class),
  @NamedNativeQuery(
      name = "Canal.buscarOrdenadoPorFecha",
      query =
          "SELECT u.*, MAX(VIDEO.FECHAPUBLICACION) "
              + "FROM USUARIO u"
              + " JOIN CANAL ON u.CANAL_NOMBRE = CANAL.NOMBRE"
              + " JOIN VIDEO ON VIDEO.CANAL = CANAL.NOMBRE"
              + " WHERE CANAL.PRIVADO = FALSE AND "
              + "( UPPER(CANAL.NOMBRE) LIKE ('%'||UPPER(?)||'%')"
              + " OR UPPER(CANAL.DESCRIPCION) LIKE ('%'||UPPER(?)||'%') )"
              + "GROUP BY u.NOMBRE",
      resultClass = Usuario.class),
})
@Entity
public class Canal {

  @Id private String nombre;
  private String descripcion;
  private boolean privado;

  @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "canal")
  private Set<Video> videos = new HashSet<>();

  @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "canal")
  private Set<Lista> listas = new HashSet<>();

  @OneToOne private Categoria categoria;

  public Canal() {}

  public Canal(String nombre, String descripcion, Boolean privado) {
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.privado = privado;
  }

  public Categoria getCategoria() {
    return categoria;
  }

  public void setCategoria(Categoria categoria) {
    this.categoria = categoria;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public boolean isPrivado() {
    return privado;
  }

  public void setPrivado(boolean privado) {
    this.privado = privado;
  }

  public Set<Video> getVideos() {
    return videos;
  }

  public void setVideos(Set<Video> videos) {
    this.videos = videos;
  }

  public void addVideo(Video video) {
    if (!this.videos.add(video)) {
      throw new YonaException("Oh por dios, el video ya existe.");
    }
  }

  public Set<Lista> getListas() {
    return listas;
  }

  public void setListas(Set<Lista> listas) {
    this.listas = listas;
  }

  public void addLista(Lista lista) throws YonaException {
    if (!this.listas.add(lista)) {
      throw new YonaException("Oh por dios, la lista ya existe");
    }
  }
}
