package org.uytube.modelos;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "ListaPredeterminada.listar", query = "Select l from ListaPredeterminada l")
public class ListaPredeterminada {

  @Id private String nombre;

  public ListaPredeterminada(String nombre) {
    this.nombre = nombre;
  }

  public ListaPredeterminada() {}

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
}
