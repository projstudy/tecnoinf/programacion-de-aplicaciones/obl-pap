package org.uytube.modelos;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.uytube.exception.YonaException;

@NamedQueries({
  @NamedQuery(
      name = "Lista.buscarListaEnCanal",
      query =
          "select l from Usuario u "
              + "inner join u.canal c "
              + "inner join c.listas l "
              + "where UPPER(l.nombre)= UPPER(:nombreLista) and c.nombre = :nombreCanal"),
  @NamedQuery(
      name = "Lista.listasUsuario",
      query =
          "select l from Usuario u "
              + "inner join u.canal c "
              + "inner join c.listas l "
              + "where UPPER(u.nickname)= UPPER(:parmNickName) "),
  @NamedQuery(
      name = "Lista.buscarPorNombre",
      query = "select l from Lista l " + "where l.nombre = :nombreLista"),
  @NamedQuery(
      name = "Lista.listasPublicas",
      query =
          "select l from Lista l INNER JOIN l.canal c where TYPE(l) not in "
              + ":clases and  c.privado= false and l.privada= false  "),
  @NamedQuery(
      name = "Lista.buscar",
      query =
          "select l from Lista l INNER JOIN l.canal c where TYPE(l) not in "
              + ":clases and  c.privado= false and l.privada= false "
              + "AND (UPPER(l.nombre) like CONCAT('%',UPPER(:nombreLista),'%')) " +
                  "order by l.id DESC")
})
@NamedNativeQueries({
  @NamedNativeQuery(
      name = "Lista.buscarOrdenarPorNombre",
      query =
          "SELECT l.* "
              + "FROM LISTA l "
              + "JOIN CANAL c ON l.CANAL = c.NOMBRE "
              + "JOIN LISTA_VIDEO lv ON lv.LISTAS_ID = l.ID "
              + "LEFT JOIN  VIDEO v ON lv.VIDEOS_ID = v.ID "
              + "WHERE l.tipo not in (?) "
              + "and c.PRIVADO = FALSE AND "
              + "( UPPER(l.NOMBRE) LIKE ('%'||UPPER(?)||'%') ) "
              + "GROUP BY l.NOMBRE "
              + "ORDER BY l.NOMBRE ASC",
      resultClass = Lista.class),
  @NamedNativeQuery(
      name = "Lista.buscarOrdenarPorFecha",
      query =
          "SELECT l.*, MAX(v.FECHAPUBLICACION) as X "
              + "FROM LISTA l "
              + "JOIN CANAL c ON l.CANAL = c.NOMBRE "
              + "JOIN LISTA_VIDEO lv ON lv.LISTAS_ID = l.ID "
              + "LEFT JOIN  VIDEO v ON lv.VIDEOS_ID = v.ID "
              + "WHERE l.tipo not in (?) "
              + "and c.PRIVADO = FALSE AND "
              + "( UPPER(l.NOMBRE) LIKE ('%'||UPPER(?)||'%') ) "
              + "GROUP BY l.NOMBRE "
              + "ORDER BY X desc",
      resultClass = Lista.class)
})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo")
@DiscriminatorValue("Particular")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"nombre", "canal"}))
@Entity
public class Lista implements Serializable {
  @Id @GeneratedValue private int id;

  @Column(name = "nombre")
  private String nombre;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "canal", nullable = false)
  private Canal canal;

  @ManyToMany private Set<Video> videos = new HashSet<>();

  @OneToOne private Categoria categoria;

  private boolean privada;

  public Lista() {
    this.privada = true;
  }

  public Lista(String nombre) {
    this.privada = true;
    this.nombre = nombre;
  }

  public Lista(String nombre, boolean privado) {
    this.privada = privado;
    this.nombre = nombre;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Set<Video> getVideos() {
    return videos;
  }

  public void setVideos(Set<Video> videos) {
    this.videos = videos;
  }

  public void addVideo(Video video) throws YonaException {
    if (!this.videos.add(video)) {
      throw new YonaException("Oh por dios, el video ya existe en la lista " + this.nombre);
    }
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void removeVideo(Video video) {
    if (!this.videos.remove(video)) {
      throw new YonaException("El video ya no existe en la lista!");
    }
  }

  public Categoria getCategoria() {
    return categoria;
  }

  public void setCategoria(Categoria categoria) {
    this.categoria = categoria;
  }

  public boolean isPrivada() {
    return privada;
  }

  public void setPrivada(boolean privada) {
    this.privada = privada;
  }

  public Canal getCanal() {
    return canal;
  }

  public void setCanal(Canal canal) {
    this.canal = canal;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Lista other = (Lista) obj;
    return Objects.equals(this.nombre, other.nombre);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.nombre);
  }
}
