package org.uytube.modelos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.uytube.exception.YonaException;

@Entity
@NamedQueries({
  @NamedQuery(
      name = "Video.buscarPropietario",
      query =
          "select u.nombre from Usuario u inner join u.canal c inner join c.videos v where v.id = :idVideo"),
  @NamedQuery(
      name = "Video.videosPublicosUsuario",
      query =
          "SELECT v FROM Usuario u "
              + "inner join u.canal c "
              + "inner join c.videos v "
              + "where u.nickname=:nick "
              + "and v.privado = false "
              + "and c.privado = false"),
  @NamedQuery(
      name = "Video.videosPublicosyPrivadoUsuario",
      query =
          "SELECT v FROM Usuario u "
              + "inner join u.canal c "
              + "inner join c.videos v "
              + "where u.nickname=:nick "),
  @NamedQuery(
      name = "Video.videosUsuario",
      query =
          "SELECT v.id, v.nombre FROM Usuario u "
              + "inner join u.canal c "
              + "inner join c.videos v "
              + "where u.nickname=:nick"),
  @NamedQuery(
      name = "Video.buscarVideos",
      query =
          "SELECT v FROM Video v inner join v.canal c where (UPPER(v.nombre) like CONCAT('%',UPPER(:nomVideo),'%') OR UPPER(v.descripcion) like CONCAT('%',UPPER(:nomVideo),'%')) and v.privado = false and c.privado=false ORDER BY v.id DESC"),
  @NamedQuery(
      name = "Video.buscarVideosAlfa",
      query =
          "SELECT v FROM Video v inner JOIN v.canal c where (UPPER(v.nombre) like CONCAT('%',UPPER(:nomVideo),'%') OR UPPER(v.descripcion) like CONCAT('%',UPPER(:nomVideo),'%')) and v.privado = false and c.privado=false ORDER BY v.nombre ASC"),
  @NamedQuery(
      name = "Video.buscarVideosAnio",
      query =
          "SELECT v FROM Video v Inner Join v.canal c where (UPPER(v.nombre) like CONCAT('%',UPPER(:nomVideo),'%') OR UPPER(v.descripcion) like CONCAT('%',UPPER(:nomVideo),'%')) and v.privado = false and c.privado=false  ORDER BY v.fechaPublicacion DESC"),
  @NamedQuery(name = "Video.todosLosVideos", query = "select v from Video v"),
  @NamedQuery(
      name = "Video.existeVideoEnCanal",
      query =
          "select v from Video v where v.id != :idVideo and v.nombre = :nombreVideo and v.canal = :canal"),
  @NamedQuery(
      name = "Video.videosPublicos",
      query =
          "select v from Video v INNER JOIN v.canal c where c.privado= false and v.privado=false")
})
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"nombre", "canal"}))
public class Video implements Serializable {

  @Id @GeneratedValue private int id;

  @Column(name = "nombre")
  private String nombre;

  @ManyToOne
  @JoinColumn(name = "canal", nullable = false)
  private Canal canal;

  @ManyToMany(mappedBy = "videos")
  private Set<Lista> listas;

  private String descripcion;
  private int duracion;

  @Temporal(TemporalType.DATE)
  private Date fechaPublicacion;

  private String URL;
  private boolean privado;
  @OneToOne private Categoria categoria;

  @OneToMany(mappedBy = "video")
  private List<Comentario> comentarios = new ArrayList<>();

  @OneToMany(mappedBy = "video")
  private Set<Valoracion> valoraciones = new HashSet<>();

  public Video() {}

  public Video(
      String nombre,
      String descripcion,
      int duracion,
      Date fechaPublicacion,
      String URL,
      boolean privado) {
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.duracion = duracion;
    this.fechaPublicacion = fechaPublicacion;
    this.URL = URL;
    this.privado = privado;
    this.categoria = null;
  }

  public Video(
      String nombre,
      String descripcion,
      int duracion,
      Date fechaPublicacion,
      String URL,
      boolean privado,
      Categoria categoria) {
    this.nombre = nombre;
    this.descripcion = descripcion;
    this.duracion = duracion;
    this.fechaPublicacion = fechaPublicacion;
    this.URL = URL;
    this.privado = privado;
    this.categoria = categoria;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public int getDuracion() {
    return duracion;
  }

  public void setDuracion(int duracion) {
    this.duracion = duracion;
  }

  public Date getFechaPublicacion() {
    return fechaPublicacion;
  }

  public void setFechaPublicacion(Date fechaPublicacion) {
    this.fechaPublicacion = fechaPublicacion;
  }

  public String getURL() {
    return URL;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setURL(String URL) {
    this.URL = URL;
  }

  public Categoria getCategoria() {
    return categoria;
  }

  public void setCategoria(Categoria categoria) {
    this.categoria = categoria;
  }

  public List<Comentario> getComentarios() {
    return comentarios;
  }

  public void setComentarios(List<Comentario> comentarios) {
    this.comentarios = comentarios;
  }

  public Set<Valoracion> getValoraciones() {
    return valoraciones;
  }

  public void setValoraciones(Set<Valoracion> valoraciones) {
    this.valoraciones = valoraciones;
  }

  public void addComentario(Comentario comentario) {
    this.comentarios.add(comentario);
    comentario.setVideo(this);
  }

  public void addValoracion(Valoracion valoracion) {
    valoracion.setVideo(this);
    if (!this.valoraciones.add(valoracion)) {
      throw new YonaException("El video ya se encuentra valorado");
    }
  }

  public boolean isPrivado() {
    return privado;
  }

  public void setPrivado(boolean privado) {
    this.privado = privado;
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 67 * hash + Objects.hashCode(this.nombre);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Video other = (Video) obj;
    return Objects.equals(this.nombre, other.nombre);
  }

  public Canal getCanal() {
    return canal;
  }

  public void setCanal(Canal canal) {
    this.canal = canal;
  }
}
