package org.uytube.modelos;

import java.io.Serializable;
import javax.persistence.Entity;

@Entity
public class Predeterminada extends Lista implements Serializable {

  public Predeterminada(String nombre) {
    super(nombre);
  }

  public Predeterminada() {
    super();
  }

  @Override
  public boolean isPrivada() {
    return true;
  }

  @Override
  public void setPrivada(boolean value) {}
}
