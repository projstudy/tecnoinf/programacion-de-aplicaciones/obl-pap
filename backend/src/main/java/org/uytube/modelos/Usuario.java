package org.uytube.modelos;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import org.mindrot.jbcrypt.BCrypt;
import org.uytube.exception.YonaException;

@NamedQueries(
    value = {
      @NamedQuery(name = "Usuario.getUsuarios", query = "select u.nickname from Usuario u"),
      @NamedQuery(
          name = "Usuario.verificoNickYMail",
          query =
              "select u from Usuario u where UPPER(u.email) = UPPER(:parmEmail) or UPPER(u.nickname) = UPPER(:parmNickname) "),
      @NamedQuery(
          name = "Usuario.DatosDeUsuario",
          query = "select u from Usuario u WHERE UPPER(u.nickname)= UPPER(:parmNickname) ")
    })
@Entity
public class Usuario {

  @Id private String nickname;
  private String img;
  private String nombre;
  private String apellido;

  private String password;

  @Column(unique = true)
  private String email;

  @Temporal(TemporalType.DATE)
  private Date date;

  @JoinColumn(nullable = false)
  @ManyToMany
  private final Set<Usuario> suscriptores = new HashSet<>();

  @ManyToMany(mappedBy = "suscriptores")
  private Set<Usuario> suscripciones = new HashSet<>();

  @JoinColumn(nullable = false)
  @OneToOne(cascade = CascadeType.ALL)
  private Canal canal;

  public Usuario() {}

  public Usuario(
      String nickname,
      String password,
      String img,
      String nombre,
      String apellido,
      String email,
      Date date,
      Canal canal) {
    this.nickname = nickname;
    this.password = password;
    this.img = img;
    this.nombre = nombre;
    this.apellido = apellido;
    this.email = email;
    this.date = date;
    this.canal = canal;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellido() {
    return apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Set<Usuario> getSuscripciones() {
    return suscripciones;
  }

  public void setSuscripciones(Set<Usuario> suscripciones) {
    this.suscripciones = suscripciones;
  }

  public Set<Usuario> getSuscriptores() {
    return suscriptores;
  }

  public Canal getCanal() {
    return canal;
  }

  public void setCanal(Canal canal) {
    this.canal = canal;
  }

  public void addSuscripctor(Usuario usuario) throws YonaException {
    if (!suscriptores.add(usuario)) {
      throw new YonaException(
          "Oh por dios, ya se encuentra registrado el suscriptor: " + usuario.getNickname());
    }
  }

  public void removeSuscripctor(Usuario usuario) throws YonaException {
    if (!suscriptores.remove(usuario)) {
      throw new YonaException(
          "Oh por dios, no se encuentra registrado el suscriptor: " + usuario.getNickname());
    }
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = BCrypt.hashpw(password, BCrypt.gensalt());
  }

  @PrePersist
  public void prePersist() {
    // Hash a password for the first time
    this.password = BCrypt.hashpw(password, BCrypt.gensalt());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.nickname);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Usuario other = (Usuario) obj;
    return Objects.equals(this.nickname, other.nickname);
  }

  public boolean checkPassword(String password) {
    return BCrypt.checkpw(password, this.password);
  }
}
