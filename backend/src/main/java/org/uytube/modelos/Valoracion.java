package org.uytube.modelos;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;

@Entity
@NamedQueries({
  @NamedQuery(
      name = "Valoracion.buscarValoracionDeUsuario",
      query =
          "select v from Valoracion v"
              + " inner join v.video video"
              + " inner join v.usuario usuario "
              + " where video.id=:idVideo and v.usuario.nickname=:nickname"),
})
public class Valoracion {

  @TableGenerator(name = "valoracionSeq", allocationSize = 1, initialValue = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "valoracionSeq")
  @Id
  private int id;

  private boolean leGusta;

  @JoinColumn(nullable = false)
  @OneToOne
  private Usuario usuario;

  @JoinColumn(nullable = false)
  @ManyToOne
  private Video video;

  public Valoracion() {}

  public Valoracion(boolean leGusta, Usuario usuario) {
    this.usuario = usuario;
    this.leGusta = leGusta;
  }

  public boolean leGusta() {
    return leGusta;
  }

  public void setLeGusta(boolean leGusta) {
    this.leGusta = leGusta;
  }

  public int getId() {
    return id;
  }

  public Usuario getUsuario() {
    return usuario;
  }

  public void setUsuario(Usuario usuario) {
    this.usuario = usuario;
  }

  public Video getVideo() {
    return video;
  }

  public void setVideo(Video video) {
    this.video = video;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Valoracion that = (Valoracion) o;
    return Objects.equals(usuario, that.usuario) && Objects.equals(video, that.video);
  }

  @Override
  public int hashCode() {
    return Objects.hash(usuario, video);
  }
}
