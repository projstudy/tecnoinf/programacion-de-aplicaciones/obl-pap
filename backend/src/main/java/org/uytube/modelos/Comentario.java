package org.uytube.modelos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;

@Entity
public class Comentario {

  @TableGenerator(name = "comentarioSeq", allocationSize = 1, initialValue = 1)
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "comentarioSeq")
  @Id
  private int id;

  private Date fecha;
  private String texto;

  @OneToMany private List<Comentario> comentarios = new ArrayList<>();

  @JoinColumn(nullable = false)
  @ManyToOne
  private Usuario usuario;

  @ManyToOne private Video video;

  public Comentario() {}

  public Comentario(Date fecha, String texto, Usuario usuario) {
    this.fecha = fecha;
    this.texto = texto;
    this.usuario = usuario;
  }

  public int getId() {
    return id;
  }

  public Date getFecha() {
    return fecha;
  }

  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  public String getTexto() {
    return texto;
  }

  public void setTexto(String texto) {
    this.texto = texto;
  }

  public List<Comentario> getComentarios() {
    return comentarios;
  }

  public void setComentarios(ArrayList<Comentario> comentarios) {
    this.comentarios = comentarios;
  }

  public void addRespuesta(Comentario respuesta) {
    this.comentarios.add(respuesta);
  }

  public Usuario getUsuairo() {
    return usuario;
  }

  public void setUsuairo(Usuario usuairo) {
    this.usuario = usuairo;
  }

  public Video getVideo() {
    return video;
  }

  public void setVideo(Video video) {
    this.video = video;
  }
}
