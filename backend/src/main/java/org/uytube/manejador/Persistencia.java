/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.manejador;

import java.security.InvalidParameterException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.mockito.Mockito;

/** @author forbi */
public class Persistencia {

  private static final Persistencia INSTANCE = new Persistencia();

  private EntityManagerFactory emf = null;
  private EntityManager mockedEm;

  public static Persistencia getInstance() {
    return INSTANCE;
  }

  /**
   * SOLO PARA TESTING
   *
   * @param em mocked EntityManager
   */
  public void setEntityManager(EntityManager em) {
    if (Mockito.mockingDetails(em).isMock()) {
      this.mockedEm = em;
    } else {
      throw new InvalidParameterException("Solo se pueden pasar mocks");
    }
  }

  public EntityManager getEntityManager() {
    if (mockedEm != null) {
      return mockedEm;
    }
    if (emf == null || !emf.isOpen()) {
      emf = Persistence.createEntityManagerFactory("test");
    }
    return emf.createEntityManager();
  }
}
