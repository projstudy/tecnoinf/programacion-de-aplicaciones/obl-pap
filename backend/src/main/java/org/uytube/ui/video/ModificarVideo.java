/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.video;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ICategoria;
import org.uytube.controladores.IUsuario;
import org.uytube.controladores.IVideo;
import org.uytube.datatypes.DtVideo;
import org.uytube.datatypes.DtVideoExt;
import org.uytube.exception.YonaException;
import org.uytube.ui.PopUp;

/** @author toto */
public class ModificarVideo extends javax.swing.JInternalFrame {

  private HashMap<Integer, DtVideoExt> videosListados = new HashMap<>();
  private final Date fechaValida = new GregorianCalendar(1900, Calendar.FEBRUARY, 1).getTime();
  private Date fechaPublicacion = null;
  private int idVideo;

  // Modelos para las tablas
  private final DefaultTableModel modeloUsuarios =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };
  private final TableRowSorter<DefaultTableModel> rowSorterUsuario =
      new TableRowSorter<>(modeloUsuarios);

  private final DefaultTableModel modeloVideos =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };
  private final TableRowSorter<DefaultTableModel> rowSorterVideo =
      new TableRowSorter<>(modeloVideos);

  public ModificarVideo(int idVideo) {
    this();
    this.idVideo = idVideo;
    cargarCategorias();
    mostrarVideo();
    spModificar.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
    spUsuario.setBorder(BorderFactory.createEmptyBorder());
    lblUsuario.setForeground(new Color(204, 204, 204));
    lblModificar.setForeground(new Color(32, 32, 32));
    panelVideo.setVisible(false);
    panelUsuario.setVisible(false);
    panelModificar.setVisible(true);
  }

  private void registerFilter(
      JTextField txtField, TableRowSorter<DefaultTableModel> rowSorterVideo) {
    txtField
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                find();
              }

              private void find() {
                String text = txtField.getText();
                if (text.trim().length() == 0) {
                  rowSorterVideo.setRowFilter(null);
                } else {
                  rowSorterVideo.setRowFilter(RowFilter.regexFilter("(?i)" + Pattern.quote(text)));
                }
              }
            });
  }

  private void enableButton() {
    try {
      SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
      df.setLenient(false);
      fechaPublicacion = df.parse(txtFecha.getText());
      if (fechaPublicacion.after(fechaValida)
          && !txtNombre.getText().isEmpty()
          && !txtURL.getText().isEmpty()
          && !txtDescripcion.getText().isEmpty()
          && txtDuracion.getText().matches("\\d\\d:\\d\\d:\\d\\d")) {
        btnModificar.setEnabled(true);
        btnModificar.setBackground(new Color(255, 132, 45));
      } else {
        btnModificar.setEnabled(false);
        btnModificar.setBackground(Color.GRAY);
      }
    } catch (ParseException ex) {
      btnModificar.setEnabled(false);
      btnModificar.setBackground(Color.GRAY);
    }
  }

  private void initTblUsuarios() {
    modeloUsuarios.addColumn("Nickname");
    tblUsuarios.setRowSorter(rowSorterUsuario);
    tblUsuarios.setModel(modeloUsuarios);
    tblUsuarios
        .getSelectionModel()
        .addListSelectionListener(
            (ListSelectionEvent event) -> {
              if (!event.getValueIsAdjusting() && tblUsuarios.getSelectedRow() >= 0) {
                btnSiguiente.setEnabled(true);
                btnSiguiente.setBackground(new Color(0, 123, 64));
              }
            });
    registerFilter(txtFindNick, rowSorterUsuario);
  }

  private void initTblVideos() {
    modeloVideos.addColumn("id");
    modeloVideos.addColumn("Nombre");
    modeloVideos.addColumn("Categoria");
    modeloVideos.addColumn("Privacidad");
    tblVideos.setModel(modeloVideos);
    TableColumnModel tcm = tblVideos.getColumnModel();
    tcm.removeColumn(tcm.getColumn(0));
    tblVideos.setRowSorter(rowSorterVideo);
    tblVideos
        .getSelectionModel()
        .addListSelectionListener(
            (ListSelectionEvent event) -> {
              if (!event.getValueIsAdjusting() && tblVideos.getSelectedRow() >= 0) {
                btnSiguienteVideo.setEnabled(true);
                btnSiguienteVideo.setBackground(new Color(0, 123, 64));

                btnModificar.setEnabled(true);
                btnModificar.setBackground(new Color(0, 123, 64));
              }
            });
    registerFilter(txtFindVideo, rowSorterVideo);
  }

  /** Creates new form ModificarVideo */
  public ModificarVideo() {
    initComponents();
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    btnSiguienteVideo.setEnabled(false);
    btnSiguienteVideo.setBackground(Color.GRAY);
    panelVideo.setVisible(false);
    panelModificar.setVisible(false);

    // Construir tablas
    initTblUsuarios();
    initTblVideos();

    // Cargar usuarios a la tabla
    IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
    List<String> usuarios = iUsuario.listarUsuarios();
    for (String usuario : usuarios) modeloUsuarios.addRow(new Object[] {usuario});

    // Listener para los textfields
    txtDuracion
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {

                if (txtDuracion.getText().matches("\\d\\d:\\d\\d:\\d\\d")) {
                  txtDuracion.setBackground(new Color(255, 255, 255));
                  enableButton();
                } else {
                  txtDuracion.setBackground(new Color(250, 151, 151));
                }
              }
            });
    txtFecha
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                try {
                  SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                  df.setLenient(false);
                  Date fechaNacimiento = df.parse(txtFecha.getText());
                  if (fechaNacimiento.after(fechaValida)) {
                    txtFecha.setBackground(new Color(255, 255, 255));
                    enableButton();
                  } else {
                    txtFecha.setBackground(new Color(250, 151, 151));
                    btnModificar.setEnabled(false);
                    btnModificar.setBackground(Color.GRAY);
                  }
                } catch (ParseException ex) {
                  txtFecha.setBackground(new Color(250, 151, 151));
                  btnModificar.setEnabled(false);
                  btnModificar.setBackground(Color.GRAY);
                }
              }
            });
    txtNombre
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                enableButton();
              }
            });

    txtURL
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                enableButton();
              }
            });

    txtDescripcion
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                enableButton();
              }
            });
  }

  private void cargarVideosUsuario() {
    String usuarioSeleccionado =
        modeloUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 0).toString();
    IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
    HashMap<Integer, String> videos = iVideo.listarVideosUsuario(usuarioSeleccionado);
    modeloVideos.setRowCount(0);
    videosListados = new HashMap<>();
    videos.forEach(
        (k, v) -> {
          DtVideoExt datosVideo = iVideo.verDatosVideo(k);
          videosListados.put(k, datosVideo);
          String privacidad = "Publica";
          if (datosVideo.getVideo().isPrivado()) {
            privacidad = "Privada";
          }
          modeloVideos.addRow(
              new Object[] {
                k,
                datosVideo.getVideo().getNombre(),
                datosVideo.getVideo().getCategoria(),
                privacidad
              });
        });
  }

  private void cargarCategorias() {
    // Cargar categorias
    ICategoria iCategoria = Fabrica.getControllerOf(Fabrica.Controller.CATEOGRIA);
    List<String> categorias = iCategoria.listarCategorias();
    cmbCategorias.removeAllItems();
    cmbCategorias.addItem("");
    for (String categoria : categorias) {
      cmbCategorias.addItem(categoria);
    }
  }

  private void mostrarVideo() {

    // DtVideoExt dtVideo = videosListados.get(idVideo);
    IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
    DtVideoExt dtVideo = iVideo.verDatosVideo(idVideo);

    txtNombre.setText(dtVideo.getVideo().getNombre());
    txtURL.setText(dtVideo.getVideo().getUrl());
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    int totalSecs = dtVideo.getVideo().getDuracion();
    int hours = totalSecs / 3600;
    int minutes = (totalSecs % 3600) / 60;
    int seconds = totalSecs % 60;
    String duracion = String.format("%02d:%02d:%02d", hours, minutes, seconds);
    txtFecha.setText(df.format(dtVideo.getVideo().getFechaPublicacion()));
    txtDuracion.setText(duracion);
    txtDescripcion.setText(dtVideo.getVideo().getDescripcion());
    rbtPrivada.setSelected(dtVideo.getVideo().isPrivado());
    if (dtVideo.getVideo().getCategoria() != null)
      cmbCategorias.setSelectedItem(dtVideo.getVideo().getCategoria());
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel3 = new javax.swing.JPanel();
    lblUsuario = new javax.swing.JLabel();
    lblModificar = new javax.swing.JLabel();
    spUsuario = new javax.swing.JSeparator();
    spVideo = new javax.swing.JSeparator();
    panelUsuario = new javax.swing.JPanel();
    jSeparator2 = new javax.swing.JSeparator();
    txtFindNick = new javax.swing.JTextField();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblUsuarios = new javax.swing.JTable();
    jLabel14 = new javax.swing.JLabel();
    jSeparator3 = new javax.swing.JSeparator();
    jLabel1 = new javax.swing.JLabel();
    btnSiguiente = new javax.swing.JButton();
    panelVideo = new javax.swing.JPanel();
    jLabel13 = new javax.swing.JLabel();
    jSeparator1 = new javax.swing.JSeparator();
    jScrollPane4 = new javax.swing.JScrollPane();
    tblVideos = new javax.swing.JTable();
    jSeparator4 = new javax.swing.JSeparator();
    txtFindVideo = new javax.swing.JTextField();
    jLabel2 = new javax.swing.JLabel();
    btnVolver = new javax.swing.JButton();
    btnSiguienteVideo = new javax.swing.JButton();
    panelModificar = new javax.swing.JPanel();
    jLabel15 = new javax.swing.JLabel();
    jSeparator5 = new javax.swing.JSeparator();
    cmbCategorias = new javax.swing.JComboBox<>();
    rbtPublica = new javax.swing.JRadioButton();
    rbtPrivada = new javax.swing.JRadioButton();
    btnModificar = new javax.swing.JButton();
    btnVolver1 = new javax.swing.JButton();
    jLabel4 = new javax.swing.JLabel();
    txtURL = new javax.swing.JTextField();
    jSeparator6 = new javax.swing.JSeparator();
    jScrollPane2 = new javax.swing.JScrollPane();
    txtDescripcion = new javax.swing.JTextArea();
    jSeparator7 = new javax.swing.JSeparator();
    txtFecha = new javax.swing.JFormattedTextField(new SimpleDateFormat("dd/MM/yyyy"));
    jSeparator8 = new javax.swing.JSeparator();
    jLabel5 = new javax.swing.JLabel();
    txtNombre = new javax.swing.JTextField();
    jSeparator9 = new javax.swing.JSeparator();
    txtDuracion = new javax.swing.JFormattedTextField(new SimpleDateFormat("HH:mm:ss"));
    jLabel6 = new javax.swing.JLabel();
    lblLista = new javax.swing.JLabel();
    spModificar = new javax.swing.JSeparator();
    jPanel1 = new javax.swing.JPanel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    lblTitle = new javax.swing.JLabel();

    setClosable(true);
    setIconifiable(true);
    setTitle("Modificar Video");
    getContentPane().setLayout(null);

    jPanel3.setBackground(new java.awt.Color(255, 255, 255));
    jPanel3.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    jPanel3.setLayout(null);

    lblUsuario.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblUsuario.setForeground(new java.awt.Color(32, 32, 32));
    lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblUsuario.setText("Usuario");
    lblUsuario.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel3.add(lblUsuario);
    lblUsuario.setBounds(30, 20, 120, 40);

    lblModificar.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblModificar.setForeground(new java.awt.Color(204, 204, 204));
    lblModificar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblModificar.setText("Modificar");
    lblModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel3.add(lblModificar);
    lblModificar.setBounds(310, 20, 120, 40);

    spUsuario.setBorder(
        javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 89, 245), 2));
    jPanel3.add(spUsuario);
    spUsuario.setBounds(30, 60, 120, 3);
    jPanel3.add(spVideo);
    spVideo.setBounds(170, 60, 120, 3);

    panelUsuario.setBackground(new java.awt.Color(255, 255, 255));
    panelUsuario.setLayout(null);
    panelUsuario.add(jSeparator2);
    jSeparator2.setBounds(50, 100, 130, 10);

    txtFindNick.setForeground(new java.awt.Color(60, 63, 65));
    txtFindNick.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar usuario:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelUsuario.add(txtFindNick);
    txtFindNick.setBounds(50, 64, 130, 40);

    tblUsuarios.setModel(
        new javax.swing.table.DefaultTableModel(new Object[][] {}, new String[] {"Nickname"}) {

          final boolean[] canEdit = new boolean[] {false};

          public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
          }
        });
    tblUsuarios.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(tblUsuarios);

    panelUsuario.add(jScrollPane1);
    jScrollPane1.setBounds(30, 110, 380, 160);

    jLabel14.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel14.setForeground(new java.awt.Color(33, 33, 33));
    jLabel14.setText("Seleccionar Usuario");
    panelUsuario.add(jLabel14);
    jLabel14.setBounds(20, 20, 160, 17);
    panelUsuario.add(jSeparator3);
    jSeparator3.setBounds(20, 40, 400, 10);

    jLabel1.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelUsuario.add(jLabel1);
    jLabel1.setBounds(26, 78, 24, 24);

    btnSiguiente.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguiente.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguiente.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguiente.setText("Siguiente");
    btnSiguiente.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguiente.setContentAreaFilled(false);
    btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguiente.setOpaque(true);
    btnSiguiente.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteMouseClicked(evt);
          }
        });
    panelUsuario.add(btnSiguiente);
    btnSiguiente.setBounds(300, 320, 110, 40);

    jPanel3.add(panelUsuario);
    panelUsuario.setBounds(10, 80, 440, 370);

    panelVideo.setBackground(new java.awt.Color(255, 255, 255));
    panelVideo.setLayout(null);

    jLabel13.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel13.setForeground(new java.awt.Color(33, 33, 33));
    jLabel13.setText("Seleccionar Video");
    panelVideo.add(jLabel13);
    jLabel13.setBounds(20, 20, 150, 17);
    panelVideo.add(jSeparator1);
    jSeparator1.setBounds(20, 40, 400, 10);

    tblVideos.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {}, new String[] {"Id", "Nombre", "Categoria", "Privacidad"}) {

          final boolean[] canEdit = new boolean[] {false, false, false, false};

          public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
          }
        });
    tblVideos.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblVideos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblVideos.getTableHeader().setResizingAllowed(false);
    tblVideos.getTableHeader().setReorderingAllowed(false);
    jScrollPane4.setViewportView(tblVideos);

    panelVideo.add(jScrollPane4);
    jScrollPane4.setBounds(30, 110, 380, 160);
    panelVideo.add(jSeparator4);
    jSeparator4.setBounds(50, 100, 130, 10);

    txtFindVideo.setForeground(new java.awt.Color(60, 63, 65));
    txtFindVideo.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar video:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelVideo.add(txtFindVideo);
    txtFindVideo.setBounds(50, 64, 130, 40);

    jLabel2.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelVideo.add(jLabel2);
    jLabel2.setBounds(26, 78, 24, 24);

    btnVolver.setBackground(new java.awt.Color(51, 89, 245));
    btnVolver.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVolver.setForeground(new java.awt.Color(255, 255, 255));
    btnVolver.setText("Volver");
    btnVolver.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVolver.setContentAreaFilled(false);
    btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVolver.setOpaque(true);
    btnVolver.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVolverMouseClicked(evt);
          }
        });
    panelVideo.add(btnVolver);
    btnVolver.setBounds(20, 320, 110, 40);

    btnSiguienteVideo.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguienteVideo.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguienteVideo.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguienteVideo.setText("Siguiente");
    btnSiguienteVideo.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguienteVideo.setContentAreaFilled(false);
    btnSiguienteVideo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguienteVideo.setOpaque(true);
    btnSiguienteVideo.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteVideoMouseClicked(evt);
          }
        });
    panelVideo.add(btnSiguienteVideo);
    btnSiguienteVideo.setBounds(300, 320, 110, 40);

    jPanel3.add(panelVideo);
    panelVideo.setBounds(10, 80, 440, 370);

    panelModificar.setBackground(new java.awt.Color(255, 255, 255));
    panelModificar.setLayout(null);

    jLabel15.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel15.setForeground(new java.awt.Color(33, 33, 33));
    jLabel15.setText("Datos de Video");
    panelModificar.add(jLabel15);
    jLabel15.setBounds(20, 17, 120, 20);
    panelModificar.add(jSeparator5);
    jSeparator5.setBounds(20, 40, 400, 10);

    cmbCategorias.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    cmbCategorias.setForeground(new java.awt.Color(60, 63, 65));
    panelModificar.add(cmbCategorias);
    cmbCategorias.setBounds(200, 175, 130, 30);

    rbtPublica.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    rbtPublica.setForeground(new java.awt.Color(60, 63, 65));
    rbtPublica.setSelected(true);
    rbtPublica.setText("Publica");
    rbtPublica.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    rbtPublica.addItemListener(this::rbtPublicaItemStateChanged);
    panelModificar.add(rbtPublica);
    rbtPublica.setBounds(200, 120, 80, 30);

    rbtPrivada.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    rbtPrivada.setForeground(new java.awt.Color(60, 63, 65));
    rbtPrivada.setText("Privada");
    rbtPrivada.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    rbtPrivada.addItemListener(this::rbtPrivadaItemStateChanged);
    panelModificar.add(rbtPrivada);
    rbtPrivada.setBounds(280, 120, 90, 30);

    btnModificar.setBackground(new java.awt.Color(255, 132, 45));
    btnModificar.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnModificar.setForeground(new java.awt.Color(255, 255, 255));
    btnModificar.setText("Modificar");
    btnModificar.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnModificar.setContentAreaFilled(false);
    btnModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnModificar.setOpaque(true);
    btnModificar.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnModificarMouseClicked(evt);
          }
        });
    panelModificar.add(btnModificar);
    btnModificar.setBounds(300, 320, 110, 40);

    btnVolver1.setBackground(new java.awt.Color(51, 89, 245));
    btnVolver1.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVolver1.setForeground(new java.awt.Color(255, 255, 255));
    btnVolver1.setText("Volver");
    btnVolver1.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVolver1.setContentAreaFilled(false);
    btnVolver1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVolver1.setOpaque(true);
    btnVolver1.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVolver1MouseClicked(evt);
          }
        });
    panelModificar.add(btnVolver1);
    btnVolver1.setBounds(20, 320, 110, 40);

    jLabel4.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel4.setText("Duracion");
    panelModificar.add(jLabel4);
    jLabel4.setBounds(45, 163, 60, 15);

    txtURL.setForeground(new java.awt.Color(60, 63, 65));
    txtURL.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "URL",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelModificar.add(txtURL);
    txtURL.setBounds(200, 60, 190, 40);
    panelModificar.add(jSeparator6);
    jSeparator6.setBounds(40, 150, 140, 10);

    jScrollPane2.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Descripcion",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N

    txtDescripcion.setColumns(20);
    txtDescripcion.setForeground(new java.awt.Color(60, 63, 65));
    txtDescripcion.setRows(5);
    txtDescripcion.setBorder(
        javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    txtDescripcion.setMargin(new java.awt.Insets(2, 2, 2, 2));
    jScrollPane2.setViewportView(txtDescripcion);

    panelModificar.add(jScrollPane2);
    jScrollPane2.setBounds(30, 209, 370, 100);
    panelModificar.add(jSeparator7);
    jSeparator7.setBounds(40, 198, 90, 10);

    txtFecha.setBorder(null);
    txtFecha.setForeground(new java.awt.Color(60, 63, 65));
    try {
      txtFecha.setFormatterFactory(
          new javax.swing.text.DefaultFormatterFactory(
              new javax.swing.text.MaskFormatter("##/##/####")));
    } catch (java.text.ParseException ex) {
      ex.printStackTrace();
    }
    txtFecha.setHorizontalAlignment(javax.swing.JTextField.CENTER);
    txtFecha.setText(null);
    panelModificar.add(txtFecha);
    txtFecha.setBounds(40, 130, 140, 20);
    panelModificar.add(jSeparator8);
    jSeparator8.setBounds(200, 97, 190, 10);

    jLabel5.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel5.setText("Categoria");
    panelModificar.add(jLabel5);
    jLabel5.setBounds(200, 160, 70, 15);

    txtNombre.setForeground(new java.awt.Color(60, 63, 65));
    txtNombre.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Nombre",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelModificar.add(txtNombre);
    txtNombre.setBounds(40, 60, 130, 40);
    panelModificar.add(jSeparator9);
    jSeparator9.setBounds(40, 97, 130, 10);

    txtDuracion.setBorder(null);
    txtDuracion.setForeground(new java.awt.Color(60, 63, 65));
    try {
      txtDuracion.setFormatterFactory(
          new javax.swing.text.DefaultFormatterFactory(
              new javax.swing.text.MaskFormatter("##:##:##")));
    } catch (java.text.ParseException ex) {
      ex.printStackTrace();
    }
    txtDuracion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
    txtDuracion.setText("00:00:00");
    panelModificar.add(txtDuracion);
    txtDuracion.setBounds(40, 180, 90, 20);

    jLabel6.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel6.setText("Fecha Publicacion");
    panelModificar.add(jLabel6);
    jLabel6.setBounds(45, 110, 120, 15);

    jPanel3.add(panelModificar);
    panelModificar.setBounds(10, 80, 440, 370);

    lblLista.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblLista.setForeground(new java.awt.Color(204, 204, 204));
    lblLista.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblLista.setText("Video");
    lblLista.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel3.add(lblLista);
    lblLista.setBounds(170, 20, 120, 40);
    jPanel3.add(spModificar);
    spModificar.setBounds(307, 60, 122, 3);

    getContentPane().add(jPanel3);
    jPanel3.setBounds(30, 70, 460, 460);

    jPanel1.setBackground(new java.awt.Color(246, 246, 246));
    jPanel1.setLayout(null);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel1.add(jLabel9);
    jLabel9.setBounds(440, 540, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel1.add(jLabel8);
    jLabel8.setBounds(470, 550, 50, 20);

    jLabel3.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel1.add(jLabel3);
    jLabel3.setBounds(240, 350, 310, 240);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Modificar Video");
    jPanel1.add(lblTitle);
    lblTitle.setBounds(160, 20, 210, 29);

    getContentPane().add(jPanel1);
    jPanel1.setBounds(0, 0, 540, 590);

    setBounds(241, 79, 542, 610);
  } // </editor-fold>//GEN-END:initComponents

  private void btnSiguienteMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteMouseClicked
    if (tblUsuarios.getSelectedRow() >= 0) {
      if (btnSiguiente.isEnabled()) {
        panelVideo.setVisible(true);
        panelUsuario.setVisible(false);
        spVideo.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
        spUsuario.setBorder(BorderFactory.createEmptyBorder());
        lblUsuario.setForeground(new Color(204, 204, 204));
        lblLista.setForeground(new Color(32, 32, 32));
        cargarVideosUsuario();
      }
    }
  } // GEN-LAST:event_btnSiguienteMouseClicked

  private void btnVolverMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVolverMouseClicked
    panelUsuario.setVisible(true);
    panelVideo.setVisible(false);
    btnSiguienteVideo.setBackground(Color.GRAY);
    btnSiguienteVideo.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    btnSiguiente.setEnabled(false);
    spUsuario.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
    spVideo.setBorder(BorderFactory.createEmptyBorder());
    lblLista.setForeground(new Color(204, 204, 204));
    lblUsuario.setForeground(new Color(32, 32, 32));
    tblVideos.getSelectionModel().removeIndexInterval(0, tblVideos.getRowCount());
    tblUsuarios.getSelectionModel().removeIndexInterval(0, tblUsuarios.getRowCount());
  } // GEN-LAST:event_btnVolverMouseClicked

  private void btnSiguienteVideoMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteVideoMouseClicked
    if (tblVideos.getSelectedRow() >= 0) {
      if (btnSiguienteVideo.isEnabled()) {
        panelVideo.setVisible(false);
        panelModificar.setVisible(true);
        spModificar.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
        spVideo.setBorder(BorderFactory.createEmptyBorder());
        lblLista.setForeground(new Color(204, 204, 204));
        lblModificar.setForeground(new Color(32, 32, 32));
        cargarCategorias();
        this.idVideo =
            Integer.parseInt(modeloVideos.getValueAt(tblVideos.getSelectedRow(), 0).toString());
        mostrarVideo();
      }
    }
  } // GEN-LAST:event_btnSiguienteVideoMouseClicked

  private void rbtPublicaItemStateChanged(
      java.awt.event.ItemEvent evt) { // GEN-FIRST:event_rbtPublicaItemStateChanged
    if (evt.getStateChange() == ItemEvent.SELECTED) {
      rbtPrivada.setSelected(false);
    } else if (evt.getStateChange() == ItemEvent.DESELECTED) {
      rbtPrivada.setSelected(true);
    }
  } // GEN-LAST:event_rbtPublicaItemStateChanged

  private void rbtPrivadaItemStateChanged(
      java.awt.event.ItemEvent evt) { // GEN-FIRST:event_rbtPrivadaItemStateChanged
    if (evt.getStateChange() == ItemEvent.SELECTED) {
      rbtPublica.setSelected(false);
    } else if (evt.getStateChange() == ItemEvent.DESELECTED) {
      rbtPublica.setSelected(true);
    }
  } // GEN-LAST:event_rbtPrivadaItemStateChanged

  private int toSeg(String s) {
    String[] hourMin = s.split(":");
    int hour = Integer.parseInt(hourMin[0]);
    int mins = Integer.parseInt(hourMin[1]);
    int seg = Integer.parseInt(hourMin[2]);
    int hoursInSecs = hour * 60 * 60;
    int minInSecs = mins * 60;
    return hoursInSecs + minInSecs + seg;
  }

  private void btnModificarMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnModificarMouseClicked
    JFrame frame = (JFrame) this.getDesktopPane().getTopLevelAncestor();
    try {
      if (tblVideos.getSelectedRow() >= 0) {
        Integer idvideo =
            Integer.parseInt(modeloVideos.getValueAt(tblVideos.getSelectedRow(), 0).toString());
      }
      SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
      Date fech = df.parse(txtFecha.getText());

      int duracion = toSeg(txtDuracion.getText());
      String cat = null;
      if (cmbCategorias.getSelectedIndex() > 0) {
        cat = cmbCategorias.getSelectedItem().toString();
      }
      DtVideo dt =
          new DtVideo(
              txtNombre.getText(),
              txtURL.getText(),
              txtDescripcion.getText(),
              fech,
              cat,
              duracion,
              rbtPrivada.isSelected());
      IVideo cvid = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
      cvid.modificarVideo(idVideo, dt);
      dispose();
      new PopUp(frame, "Modificar Video", "Video modificado", 5, PopUp.LEVEL.SUCCESS);
    } catch (YonaException e) {
      new PopUp(frame, "Modificar Video", e.getMessage(), 5, PopUp.LEVEL.ERROR);
    } catch (ParseException ex) {
      Logger.getLogger(ModificarVideo.class.getName()).log(Level.SEVERE, null, ex);
      new PopUp(frame, "Modificar Video", "Error al convertir fecha", 5, PopUp.LEVEL.ERROR);
    }
  } // GEN-LAST:event_btnModificarMouseClicked

  private void btnVolver1MouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVolver1MouseClicked
    panelVideo.setVisible(true);
    panelModificar.setVisible(false);
    spVideo.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
    spModificar.setBorder(BorderFactory.createEmptyBorder());
    lblModificar.setForeground(new Color(204, 204, 204));
    lblLista.setForeground(new Color(32, 32, 32));
    btnSiguienteVideo.setEnabled(false);
    btnSiguienteVideo.setBackground(Color.GRAY);
    tblVideos.getSelectionModel().removeIndexInterval(0, tblVideos.getRowCount());
  } // GEN-LAST:event_btnVolver1MouseClicked

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnModificar;
  private javax.swing.JButton btnSiguiente;
  private javax.swing.JButton btnSiguienteVideo;
  private javax.swing.JButton btnVolver;
  private javax.swing.JButton btnVolver1;
  private javax.swing.JComboBox<String> cmbCategorias;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel13;
  private javax.swing.JLabel jLabel14;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JScrollPane jScrollPane4;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JSeparator jSeparator3;
  private javax.swing.JSeparator jSeparator4;
  private javax.swing.JSeparator jSeparator5;
  private javax.swing.JSeparator jSeparator6;
  private javax.swing.JSeparator jSeparator7;
  private javax.swing.JSeparator jSeparator8;
  private javax.swing.JSeparator jSeparator9;
  private javax.swing.JLabel lblLista;
  private javax.swing.JLabel lblModificar;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JLabel lblUsuario;
  private javax.swing.JPanel panelModificar;
  private javax.swing.JPanel panelUsuario;
  private javax.swing.JPanel panelVideo;
  private javax.swing.JRadioButton rbtPrivada;
  private javax.swing.JRadioButton rbtPublica;
  private javax.swing.JSeparator spModificar;
  private javax.swing.JSeparator spUsuario;
  private javax.swing.JSeparator spVideo;
  private javax.swing.JTable tblUsuarios;
  private javax.swing.JTable tblVideos;
  private javax.swing.JTextArea txtDescripcion;
  private javax.swing.JFormattedTextField txtDuracion;
  private javax.swing.JFormattedTextField txtFecha;
  private javax.swing.JTextField txtFindNick;
  private javax.swing.JTextField txtFindVideo;
  private javax.swing.JTextField txtNombre;
  private javax.swing.JTextField txtURL;
  // End of variables declaration//GEN-END:variables
}
