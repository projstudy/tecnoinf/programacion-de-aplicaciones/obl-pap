/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.video;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ICategoria;
import org.uytube.controladores.IUsuario;
import org.uytube.controladores.IVideo;
import org.uytube.datatypes.DtVideo;
import org.uytube.exception.YonaException;
import org.uytube.ui.PopUp;

/** @author toto */
public class AltaVideo extends javax.swing.JInternalFrame {

  private Date fechaPublicacion = null;
  private final Date fechaValida = new GregorianCalendar(1900, Calendar.FEBRUARY, 1).getTime();
  private final IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
  private final IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
  private final ICategoria iCategoria = Fabrica.getControllerOf(Fabrica.Controller.CATEOGRIA);
  private final DefaultTableModel modeloUsuarios =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };
  private final TableRowSorter<DefaultTableModel> rowSorterUsuario =
      new TableRowSorter<>(modeloUsuarios);

  /** Creates new form AltaVideo */
  public AltaVideo() {

    // Create a time picker with some custom settings.
    initComponents();
    initTblUsuarios();
    panelVideo.setVisible(false);
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    btnAceptar.setEnabled(false);
    btnAceptar.setBackground(Color.GRAY);
    // Cargar usuarios
    List<String> usuarios = iUsuario.listarUsuarios();
    for (String usuario : usuarios) {
      modeloUsuarios.addRow(new Object[] {usuario});
    }
    // Cargar categorias
    List<String> categorias = iCategoria.listarCategorias();
    cmbCategorias.addItem("");

    for (String categoria : categorias) {
      cmbCategorias.addItem(categoria);
    }

    txtDuracion
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {

                if (txtDuracion.getText().matches("\\d\\d:\\d\\d:\\d\\d")) {
                  txtDuracion.setBackground(new Color(255, 255, 255));
                  enableButton();
                } else {
                  txtDuracion.setBackground(new Color(250, 151, 151));
                }
              }
            });

    txtFecha
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                try {
                  SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                  df.setLenient(false);
                  Date fechaNacimiento = df.parse(txtFecha.getText());
                  if (fechaNacimiento.after(fechaValida)) {
                    txtFecha.setBackground(new Color(255, 255, 255));
                    enableButton();
                  } else {
                    txtFecha.setBackground(new Color(250, 151, 151));
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackground(Color.GRAY);
                  }
                } catch (ParseException ex) {
                  txtFecha.setBackground(new Color(250, 151, 151));
                  btnAceptar.setEnabled(false);
                  btnAceptar.setBackground(Color.GRAY);
                }
              }
            });

    txtNombre
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                enableButton();
              }
            });

    txtURL
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                enableButton();
              }
            });

    txtDescripcion
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                enableButton();
              }
            });
  }

  private void initTblUsuarios() {
    modeloUsuarios.addColumn("Nickname");
    tblUsuarios.setModel(modeloUsuarios);
    tblUsuarios.setRowSorter(rowSorterUsuario);
    tblUsuarios
        .getSelectionModel()
        .addListSelectionListener(
            (ListSelectionEvent event) -> {
              if (!event.getValueIsAdjusting() && tblUsuarios.getSelectedRow() >= 0) {
                btnSiguiente.setEnabled(true);
                btnSiguiente.setBackground(new Color(0, 123, 64));
              }
            });
    registerFilter(txtFindNick, rowSorterUsuario);
  }

  private int toSeg(String s) {
    String[] hourMin = s.split(":");
    int hour = Integer.parseInt(hourMin[0]);
    int mins = Integer.parseInt(hourMin[1]);
    int seg = Integer.parseInt(hourMin[2]);
    int hoursInSecs = hour * 60 * 60;
    int minInSecs = mins * 60;
    return hoursInSecs + minInSecs + seg;
  }

  private void registerFilter(
      JTextField txtField, TableRowSorter<DefaultTableModel> rowSorterVideo) {
    txtField
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                find();
              }

              private void find() {
                String text = txtField.getText();
                if (text.trim().length() == 0) {
                  rowSorterVideo.setRowFilter(null);
                } else {
                  rowSorterVideo.setRowFilter(RowFilter.regexFilter("(?i)" + Pattern.quote(text)));
                }
              }
            });
  }

  /*Verifica que el boton se puede activar*/
  private void enableButton() {
    try {
      SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
      df.setLenient(false);
      fechaPublicacion = df.parse(txtFecha.getText());
      if (fechaPublicacion.after(fechaValida)
          && !txtNombre.getText().isEmpty()
          && !txtURL.getText().isEmpty()
          && !txtDescripcion.getText().isEmpty()
          && txtDuracion.getText().matches("\\d\\d:\\d\\d:\\d\\d")) {
        btnAceptar.setEnabled(true);
        btnAceptar.setBackground(new Color(0, 123, 64));
      } else {
        btnAceptar.setEnabled(false);
        btnAceptar.setBackground(Color.GRAY);
      }
    } catch (ParseException ex) {
      btnAceptar.setEnabled(false);
      btnAceptar.setBackground(Color.GRAY);
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel2 = new javax.swing.JPanel();
    panelVideo = new javax.swing.JPanel();
    jSeparator6 = new javax.swing.JSeparator();
    txtFecha = new javax.swing.JFormattedTextField(new SimpleDateFormat("dd/MM/yyyy"));
    jSeparator5 = new javax.swing.JSeparator();
    jSeparator9 = new javax.swing.JSeparator();
    jSeparator8 = new javax.swing.JSeparator();
    jLabel15 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();
    txtNombre = new javax.swing.JTextField();
    txtURL = new javax.swing.JTextField();
    jLabel5 = new javax.swing.JLabel();
    rbtPublica = new javax.swing.JRadioButton();
    cmbCategorias = new javax.swing.JComboBox<>();
    rbtPrivada = new javax.swing.JRadioButton();
    jScrollPane2 = new javax.swing.JScrollPane();
    txtDescripcion = new javax.swing.JTextArea();
    jSeparator1 = new javax.swing.JSeparator();
    btnAceptar = new javax.swing.JButton();
    btnVolver = new javax.swing.JButton();
    txtDuracion = new javax.swing.JFormattedTextField(new SimpleDateFormat("HH:mm:ss"));
    panelUsuario = new javax.swing.JPanel();
    jSeparator7 = new javax.swing.JSeparator();
    jLabel16 = new javax.swing.JLabel();
    txtFindNick = new javax.swing.JTextField();
    jLabel1 = new javax.swing.JLabel();
    jSeparator2 = new javax.swing.JSeparator();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblUsuarios = new javax.swing.JTable();
    btnSiguiente = new javax.swing.JButton();
    lblUsuario = new javax.swing.JLabel();
    spUsuario = new javax.swing.JSeparator();
    lblVideo = new javax.swing.JLabel();
    spVideo = new javax.swing.JSeparator();
    jPanel1 = new javax.swing.JPanel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    lblTitle = new javax.swing.JLabel();

    setClosable(true);
    setIconifiable(true);
    setTitle("Alta Video");
    getContentPane().setLayout(null);

    jPanel2.setBackground(new java.awt.Color(255, 255, 255));
    jPanel2.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    jPanel2.setLayout(null);

    panelVideo.setBackground(new java.awt.Color(255, 255, 255));
    panelVideo.setLayout(null);
    panelVideo.add(jSeparator6);
    jSeparator6.setBounds(30, 148, 140, 10);

    txtFecha.setBorder(null);
    txtFecha.setForeground(new java.awt.Color(60, 63, 65));
    try {
      txtFecha.setFormatterFactory(
          new javax.swing.text.DefaultFormatterFactory(
              new javax.swing.text.MaskFormatter("##/##/####")));
    } catch (java.text.ParseException ex) {
      ex.printStackTrace();
    }
    txtFecha.setHorizontalAlignment(javax.swing.JTextField.CENTER);
    txtFecha.setText(null);
    txtFecha.addFocusListener(
        new java.awt.event.FocusAdapter() {
          public void focusLost(java.awt.event.FocusEvent evt) {
            txtFechaFocusLost(evt);
          }
        });
    txtFecha.addInputMethodListener(
        new java.awt.event.InputMethodListener() {
          public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {}

          public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            txtFechaInputMethodTextChanged(evt);
          }
        });
    txtFecha.addActionListener(this::txtFechaActionPerformed);
    panelVideo.add(txtFecha);
    txtFecha.setBounds(30, 130, 140, 20);
    panelVideo.add(jSeparator5);
    jSeparator5.setBounds(10, 30, 410, 10);
    panelVideo.add(jSeparator9);
    jSeparator9.setBounds(30, 87, 130, 10);
    panelVideo.add(jSeparator8);
    jSeparator8.setBounds(190, 87, 190, 10);

    jLabel15.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel15.setForeground(new java.awt.Color(33, 33, 33));
    jLabel15.setText("Datos de Video");
    panelVideo.add(jLabel15);
    jLabel15.setBounds(10, 10, 120, 20);

    jLabel4.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel4.setText("Fecha Publicacion*");
    panelVideo.add(jLabel4);
    jLabel4.setBounds(30, 110, 130, 15);

    txtNombre.setForeground(new java.awt.Color(60, 63, 65));
    txtNombre.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Nombre*",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelVideo.add(txtNombre);
    txtNombre.setBounds(30, 50, 130, 40);

    txtURL.setForeground(new java.awt.Color(60, 63, 65));
    txtURL.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "URL*",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelVideo.add(txtURL);
    txtURL.setBounds(190, 50, 190, 40);

    jLabel5.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel5.setText("Categoria");
    panelVideo.add(jLabel5);
    jLabel5.setBounds(30, 170, 70, 15);

    rbtPublica.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    rbtPublica.setSelected(true);
    rbtPublica.setText("Publica");
    rbtPublica.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    rbtPublica.addItemListener(this::rbtPublicaItemStateChanged);
    rbtPublica.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            rbtPublicaMouseClicked(evt);
          }
        });
    panelVideo.add(rbtPublica);
    rbtPublica.setBounds(200, 125, 80, 30);

    cmbCategorias.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    panelVideo.add(cmbCategorias);
    cmbCategorias.setBounds(30, 190, 130, 30);

    rbtPrivada.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    rbtPrivada.setText("Privada");
    rbtPrivada.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    rbtPrivada.addItemListener(this::rbtPrivadaItemStateChanged);
    rbtPrivada.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            rbtPrivadaMouseClicked(evt);
          }
        });
    panelVideo.add(rbtPrivada);
    rbtPrivada.setBounds(280, 125, 90, 30);

    jScrollPane2.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Descripcion*",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N

    txtDescripcion.setColumns(20);
    txtDescripcion.setForeground(new java.awt.Color(60, 63, 65));
    txtDescripcion.setRows(5);
    txtDescripcion.setBorder(
        javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    jScrollPane2.setViewportView(txtDescripcion);

    panelVideo.add(jScrollPane2);
    jScrollPane2.setBounds(30, 230, 370, 100);
    panelVideo.add(jSeparator1);
    jSeparator1.setBounds(200, 214, 90, 10);

    btnAceptar.setBackground(new java.awt.Color(0, 123, 64));
    btnAceptar.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnAceptar.setForeground(new java.awt.Color(255, 255, 255));
    btnAceptar.setText("Aceptar");
    btnAceptar.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnAceptar.setContentAreaFilled(false);
    btnAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnAceptar.setOpaque(true);
    btnAceptar.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnAceptarMouseClicked(evt);
          }
        });
    panelVideo.add(btnAceptar);
    btnAceptar.setBounds(300, 340, 110, 40);

    btnVolver.setBackground(new java.awt.Color(51, 89, 245));
    btnVolver.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVolver.setForeground(new java.awt.Color(255, 255, 255));
    btnVolver.setText("Volver");
    btnVolver.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVolver.setContentAreaFilled(false);
    btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVolver.setOpaque(true);
    btnVolver.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVolverMouseClicked(evt);
          }
        });
    panelVideo.add(btnVolver);
    btnVolver.setBounds(20, 340, 110, 40);

    txtDuracion.setBorder(null);
    txtDuracion.setForeground(new java.awt.Color(60, 63, 65));
    try {
      txtDuracion.setFormatterFactory(
          new javax.swing.text.DefaultFormatterFactory(
              new javax.swing.text.MaskFormatter("##:##:##")));
    } catch (java.text.ParseException ex) {
      ex.printStackTrace();
    }
    txtDuracion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
    txtDuracion.setText("00:00:00");
    txtDuracion.addFocusListener(
        new java.awt.event.FocusAdapter() {
          public void focusLost(java.awt.event.FocusEvent evt) {
            txtDuracionFocusLost(evt);
          }
        });
    txtDuracion.addInputMethodListener(
        new java.awt.event.InputMethodListener() {
          public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {}

          public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            txtDuracionInputMethodTextChanged(evt);
          }
        });
    txtDuracion.addActionListener(this::txtDuracionActionPerformed);
    panelVideo.add(txtDuracion);
    txtDuracion.setBounds(200, 190, 90, 30);

    jPanel2.add(panelVideo);
    panelVideo.setBounds(10, 80, 430, 390);

    panelUsuario.setBackground(new java.awt.Color(255, 255, 255));
    panelUsuario.setLayout(null);
    panelUsuario.add(jSeparator7);
    jSeparator7.setBounds(10, 30, 410, 10);

    jLabel16.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel16.setForeground(new java.awt.Color(33, 33, 33));
    jLabel16.setText("Seleccionar Usuario");
    panelUsuario.add(jLabel16);
    jLabel16.setBounds(10, 10, 160, 20);

    txtFindNick.setForeground(new java.awt.Color(60, 63, 65));
    txtFindNick.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar usuario:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelUsuario.add(txtFindNick);
    txtFindNick.setBounds(50, 64, 130, 40);

    jLabel1.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelUsuario.add(jLabel1);
    jLabel1.setBounds(26, 78, 24, 24);
    panelUsuario.add(jSeparator2);
    jSeparator2.setBounds(50, 101, 130, 10);

    tblUsuarios.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {
              {null, null, null},
              {null, null, null},
              {null, null, null},
              {null, null, null}
            },
            new String[] {"Nickname", "Nombre", "Apellido"}));
    tblUsuarios.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(tblUsuarios);

    panelUsuario.add(jScrollPane1);
    jScrollPane1.setBounds(30, 120, 380, 190);

    btnSiguiente.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguiente.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguiente.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguiente.setText("Siguiente");
    btnSiguiente.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguiente.setContentAreaFilled(false);
    btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguiente.setOpaque(true);
    btnSiguiente.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteMouseClicked(evt);
          }
        });
    panelUsuario.add(btnSiguiente);
    btnSiguiente.setBounds(300, 340, 110, 40);

    jPanel2.add(panelUsuario);
    panelUsuario.setBounds(10, 80, 430, 390);

    lblUsuario.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblUsuario.setForeground(new java.awt.Color(32, 32, 32));
    lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblUsuario.setText("Usuario");
    lblUsuario.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel2.add(lblUsuario);
    lblUsuario.setBounds(90, 20, 120, 40);

    spUsuario.setBorder(
        javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 89, 245), 2));
    jPanel2.add(spUsuario);
    spUsuario.setBounds(88, 61, 125, 3);

    lblVideo.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblVideo.setForeground(new java.awt.Color(204, 204, 204));
    lblVideo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblVideo.setText("Video");
    lblVideo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel2.add(lblVideo);
    lblVideo.setBounds(220, 20, 120, 40);
    jPanel2.add(spVideo);
    spVideo.setBounds(220, 60, 120, 3);

    getContentPane().add(jPanel2);
    jPanel2.setBounds(50, 60, 450, 480);

    jPanel1.setBackground(new java.awt.Color(246, 246, 246));
    jPanel1.setLayout(null);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel1.add(jLabel9);
    jLabel9.setBounds(460, 550, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel1.add(jLabel8);
    jLabel8.setBounds(490, 560, 50, 20);

    jLabel3.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel1.add(jLabel3);
    jLabel3.setBounds(240, 360, 310, 270);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Alta Video");
    jPanel1.add(lblTitle);
    lblTitle.setBounds(200, 10, 140, 29);

    getContentPane().add(jPanel1);
    jPanel1.setBounds(0, 0, 550, 600);

    setBounds(237, 75, 550, 606);
  } // </editor-fold>//GEN-END:initComponents

  private void txtDuracionFocusLost(
      java.awt.event.FocusEvent evt) { // GEN-FIRST:event_txtDuracionFocusLost
  } // GEN-LAST:event_txtDuracionFocusLost

  private void txtDuracionInputMethodTextChanged(
      java.awt.event.InputMethodEvent evt) { // GEN-FIRST:event_txtDuracionInputMethodTextChanged
  } // GEN-LAST:event_txtDuracionInputMethodTextChanged

  private void txtDuracionActionPerformed(
      java.awt.event.ActionEvent evt) { // GEN-FIRST:event_txtDuracionActionPerformed
  } // GEN-LAST:event_txtDuracionActionPerformed

  private void btnSiguienteMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteMouseClicked
    if (btnSiguiente.isEnabled()) {
      panelUsuario.setVisible(false);
      panelVideo.setVisible(true);
      spVideo.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
      spUsuario.setBorder(BorderFactory.createEmptyBorder());
      lblUsuario.setForeground(new Color(204, 204, 204));
      lblVideo.setForeground(new Color(32, 32, 32));
    }
  } // GEN-LAST:event_btnSiguienteMouseClicked

  private void btnAceptarMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnAceptarMouseClicked
    if (btnAceptar.isEnabled()) {
      JFrame frame = (JFrame) this.getDesktopPane().getTopLevelAncestor();
      try {
        String nickname = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 0).toString();
        int duracion = toSeg(txtDuracion.getText());
        String cat = null;
        if (cmbCategorias.getSelectedIndex() > 0) {
          cat = cmbCategorias.getSelectedItem().toString();
        }
        DtVideo dt =
            new DtVideo(
                txtNombre.getText(),
                txtURL.getText(),
                txtDescripcion.getText(),
                fechaPublicacion,
                cat,
                duracion,
                rbtPrivada.isSelected());
        iVideo.crearVideo(dt, nickname);
        dispose();
        new PopUp(frame, "Alta Video", "Se creo el video", 5, PopUp.LEVEL.SUCCESS);
      } catch (YonaException e) {
        new PopUp(frame, "Alta Video", e.getMessage(), 5, PopUp.LEVEL.ERROR);
      }
    }
  } // GEN-LAST:event_btnAceptarMouseClicked

  private void btnVolverMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVolverMouseClicked
    panelVideo.setVisible(false);
    panelUsuario.setVisible(true);
    spUsuario.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
    spVideo.setBorder(BorderFactory.createEmptyBorder());
    lblVideo.setForeground(new Color(204, 204, 204));
    lblUsuario.setForeground(new Color(32, 32, 32));
  } // GEN-LAST:event_btnVolverMouseClicked

  private void rbtPublicaMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_rbtPublicaMouseClicked
    rbtPublica.setSelected(true);
    rbtPrivada.setSelected(false);
  } // GEN-LAST:event_rbtPublicaMouseClicked

  private void rbtPrivadaMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_rbtPrivadaMouseClicked
    rbtPublica.setSelected(false);
    rbtPrivada.setSelected(true);
  } // GEN-LAST:event_rbtPrivadaMouseClicked

  private void txtFechaFocusLost(
      java.awt.event.FocusEvent evt) { // GEN-FIRST:event_txtFechaFocusLost
  } // GEN-LAST:event_txtFechaFocusLost

  private void txtFechaInputMethodTextChanged(
      java.awt.event.InputMethodEvent evt) { // GEN-FIRST:event_txtFechaInputMethodTextChanged
  } // GEN-LAST:event_txtFechaInputMethodTextChanged

  private void txtFechaActionPerformed(
      java.awt.event.ActionEvent evt) { // GEN-FIRST:event_txtFechaActionPerformed
  } // GEN-LAST:event_txtFechaActionPerformed

  private void rbtPublicaItemStateChanged(
      java.awt.event.ItemEvent evt) { // GEN-FIRST:event_rbtPublicaItemStateChanged
  } // GEN-LAST:event_rbtPublicaItemStateChanged

  private void rbtPrivadaItemStateChanged(
      java.awt.event.ItemEvent evt) { // GEN-FIRST:event_rbtPrivadaItemStateChanged
  } // GEN-LAST:event_rbtPrivadaItemStateChanged

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnAceptar;
  private javax.swing.JButton btnSiguiente;
  private javax.swing.JButton btnVolver;
  private javax.swing.JComboBox<String> cmbCategorias;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel16;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JSeparator jSeparator5;
  private javax.swing.JSeparator jSeparator6;
  private javax.swing.JSeparator jSeparator7;
  private javax.swing.JSeparator jSeparator8;
  private javax.swing.JSeparator jSeparator9;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JLabel lblUsuario;
  private javax.swing.JLabel lblVideo;
  private javax.swing.JPanel panelUsuario;
  private javax.swing.JPanel panelVideo;
  private javax.swing.JRadioButton rbtPrivada;
  private javax.swing.JRadioButton rbtPublica;
  private javax.swing.JSeparator spUsuario;
  private javax.swing.JSeparator spVideo;
  private javax.swing.JTable tblUsuarios;
  private javax.swing.JTextArea txtDescripcion;
  private javax.swing.JFormattedTextField txtDuracion;
  private javax.swing.JFormattedTextField txtFecha;
  private javax.swing.JTextField txtFindNick;
  private javax.swing.JTextField txtNombre;
  private javax.swing.JTextField txtURL;
  // End of variables declaration//GEN-END:variables
}
