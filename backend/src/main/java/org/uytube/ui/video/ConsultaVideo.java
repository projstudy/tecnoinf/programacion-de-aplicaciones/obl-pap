/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.video;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ILista;
import org.uytube.controladores.IUsuario;
import org.uytube.controladores.IVideo;
import org.uytube.datatypes.*;
import org.uytube.exception.YonaException;

/** @author toto */
public class ConsultaVideo extends javax.swing.JInternalFrame {

  /** Creates new form ConsultaVideo */
  public ConsultaVideo() {
    initComponents();

    trComentarios.setRootVisible(false);
    DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) trComentarios.getCellRenderer();
    Icon closedIcon = new ImageIcon(getClass().getResource("/images/response.png"));
    Icon openIcon = new ImageIcon(getClass().getResource("/images/response.png"));
    Icon leafIcon = new ImageIcon(getClass().getResource("/images/response.png"));
    renderer.setClosedIcon(closedIcon);
    renderer.setOpenIcon(openIcon);
    renderer.setLeafIcon(leafIcon);
    trComentarios.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

    initTblUsuarios();
    initTblValoraciones();
    initTblVideos();
    List<String> usuarios = iUsuario.listarUsuarios();
    for (String usuario : usuarios) {
      modeloUsuarios.addRow(new Object[] {usuario});
    }

    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    panelDatos.setVisible(false);
    panelVideos.setVisible(true);
  }

  public ConsultaVideo(int idVideo) {
    this();
    cargarDatosVideo(idVideo);
    panelDatos.setVisible(true);
    panelVideos.setVisible(false);
  }

  private ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);
  private final IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
  private final IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);

  private final DefaultTableModel modeloVideos =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final DefaultTableModel modeloUsuarios =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final DefaultTableModel modeloValoraciones =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final TableRowSorter<DefaultTableModel> rowSorterVideo =
      new TableRowSorter<>(modeloVideos);
  private final TableRowSorter<DefaultTableModel> rowSorterUsuario =
      new TableRowSorter<>(modeloUsuarios);

  private void registerFilter(
      JTextField txtField, TableRowSorter<DefaultTableModel> rowSorterVideo) {
    txtField
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                find();
              }

              private void find() {
                String text = txtField.getText();
                if (text.trim().length() == 0) {
                  rowSorterVideo.setRowFilter(null);
                } else {
                  rowSorterVideo.setRowFilter(RowFilter.regexFilter("(?i)" + Pattern.quote(text)));
                }
              }
            });
  }

  private void initTblUsuarios() {
    modeloUsuarios.addColumn("Nickname");
    tblUsuarios.setModel(modeloUsuarios);
    tblUsuarios.setRowSorter(rowSorterUsuario);
    tblUsuarios
        .getSelectionModel()
        .addListSelectionListener(
            (ListSelectionEvent event) -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblUsuarios.getSelectedRow() >= 0) {
                String nickname =
                    tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 0).toString();
                HashMap<Integer, String> data = iVideo.listarVideosUsuario(nickname);
                modeloVideos.setRowCount(0);
                data.forEach((k, v) -> modeloVideos.addRow(new Object[] {v, k}));
                // quitar ultima columna
              }
            });
    registerFilter(txtFindNick, rowSorterUsuario);
  }

  private void initTblVideos() {
    modeloVideos.addColumn("Nombre");
    modeloVideos.addColumn("id");
    tblVideos.setModel(modeloVideos);
    TableColumnModel tcm = tblVideos.getColumnModel();
    tcm.removeColumn(tcm.getColumn(1));
    tblVideos.setRowSorter(rowSorterVideo);
    tblVideos
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              if (!event.getValueIsAdjusting() && tblVideos.getSelectedRow() >= 0) {
                btnSiguiente.setEnabled(true);
                btnSiguiente.setBackground(new Color(0, 123, 64));
              }
            });

    registerFilter(txtFindVideo, rowSorterVideo);
  }

  private void initTblValoraciones() {
    modeloValoraciones.addColumn("Nickname");
    modeloValoraciones.addColumn("Valoracion");
    tblValoraciones.setModel(modeloValoraciones);
  }

  private void cargarDatosVideo(int idVideo) {
    try {
      DtVideoExt data = iVideo.verDatosVideo(idVideo);
      lblNombre.setText(data.getVideo().getNombre());
      lblCategoria.setText(data.getVideo().getCategoria());
      lblURL.setText(data.getVideo().getUrl());
      txtDescripcion.setText(data.getVideo().getDescripcion());
      SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
      int totalSecs = data.getVideo().getDuracion();
      int hours = totalSecs / 3600;
      int minutes = (totalSecs % 3600) / 60;
      int seconds = totalSecs % 60;
      String duracion = String.format("%02d:%02d:%02d", hours, minutes, seconds);
      lblFechaPublicacion.setText(df.format(data.getVideo().getFechaPublicacion()));
      lblDuracion.setText(duracion);
      String privacidad = data.getVideo().isPrivado() ? "Privado" : "Publico";

      lblPrivacidad.setText(privacidad);

      for (DtValoracion valoracion : data.getValoraciones()) {
        String leGusta = valoracion.isLeGusta() ? "Positiva" : "Negativa";
        modeloValoraciones.addRow(new Object[] {valoracion.getNickname(), leGusta});
      }
      for (DtComentario comentario : data.getComentarios()) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) trComentarios.getModel().getRoot();
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(comentario);
        root.add(node);
        cargarRespuestas(comentario.getRespuestas(), node);
      }
      DefaultTreeModel model = (DefaultTreeModel) trComentarios.getModel();
      model.reload();
    } catch (YonaException e) {
      System.out.println("upps");
    }
  }

  private void cargarRespuestas(ArrayList<DtComentario> respuestas, DefaultMutableTreeNode root) {
    for (DtComentario respuesta : respuestas) {
      DefaultMutableTreeNode node = new DefaultMutableTreeNode(respuesta);
      root.add(node);
      cargarRespuestas(respuesta.getRespuestas(), node);
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    panelDatos = new javax.swing.JPanel();
    jLabel14 = new javax.swing.JLabel();
    jSeparator3 = new javax.swing.JSeparator();
    jScrollPane2 = new javax.swing.JScrollPane();
    txtDescripcion = new javax.swing.JTextArea();
    jScrollPane3 = new javax.swing.JScrollPane();
    trComentarios = new javax.swing.JTree();
    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();
    jLabel5 = new javax.swing.JLabel();
    jLabel6 = new javax.swing.JLabel();
    jLabel7 = new javax.swing.JLabel();
    jLabel15 = new javax.swing.JLabel();
    jSeparator4 = new javax.swing.JSeparator();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblValoraciones = new javax.swing.JTable();
    btnVolver = new javax.swing.JButton();
    lblTitle1 = new javax.swing.JLabel();
    lblCategoria = new javax.swing.JLabel();
    lblNombre = new javax.swing.JLabel();
    lblURL = new javax.swing.JLabel();
    lblFechaPublicacion = new javax.swing.JLabel();
    lblPrivacidad = new javax.swing.JLabel();
    lblDuracion = new javax.swing.JLabel();
    panelVideos = new javax.swing.JPanel();
    jLabel11 = new javax.swing.JLabel();
    jSeparator1 = new javax.swing.JSeparator();
    txtFindVideo = new javax.swing.JTextField();
    jLabel12 = new javax.swing.JLabel();
    jSeparator14 = new javax.swing.JSeparator();
    jScrollPane5 = new javax.swing.JScrollPane();
    tblVideos = new javax.swing.JTable();
    jLabel13 = new javax.swing.JLabel();
    jSeparator2 = new javax.swing.JSeparator();
    btnSiguiente = new javax.swing.JButton();
    jScrollPane6 = new javax.swing.JScrollPane();
    tblUsuarios = new javax.swing.JTable();
    txtFindNick = new javax.swing.JTextField();
    jLabel16 = new javax.swing.JLabel();
    jSeparator15 = new javax.swing.JSeparator();
    jPanel1 = new javax.swing.JPanel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    lblTitle = new javax.swing.JLabel();

    setClosable(true);
    setIconifiable(true);
    setTitle("Consulta Video");
    getContentPane().setLayout(null);

    panelDatos.setBackground(new java.awt.Color(255, 255, 255));
    panelDatos.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelDatos.setLayout(null);

    jLabel14.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel14.setForeground(new java.awt.Color(33, 33, 33));
    jLabel14.setText("Comentarios");
    panelDatos.add(jLabel14);
    jLabel14.setBounds(20, 295, 100, 17);
    panelDatos.add(jSeparator3);
    jSeparator3.setBounds(20, 315, 430, 10);

    jScrollPane2.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Descripcion:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N

    txtDescripcion.setEditable(false);
    txtDescripcion.setColumns(20);
    txtDescripcion.setForeground(new java.awt.Color(60, 63, 65));
    txtDescripcion.setLineWrap(true);
    txtDescripcion.setRows(5);
    txtDescripcion.setAutoscrolls(false);
    txtDescripcion.setBorder(
        javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    jScrollPane2.setViewportView(txtDescripcion);

    panelDatos.add(jScrollPane2);
    jScrollPane2.setBounds(33, 180, 190, 100);

    javax.swing.tree.DefaultMutableTreeNode treeNode1 =
        new javax.swing.tree.DefaultMutableTreeNode("Comentarios");
    trComentarios.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
    trComentarios.setCellRenderer(null);
    trComentarios.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    trComentarios.addTreeSelectionListener(this::trComentariosValueChanged);
    jScrollPane3.setViewportView(trComentarios);

    panelDatos.add(jScrollPane3);
    jScrollPane3.setBounds(50, 330, 360, 110);

    jLabel1.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel1.setText("Categoria:");
    panelDatos.add(jLabel1);
    jLabel1.setBounds(40, 160, 70, 15);

    jLabel2.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel2.setText("Duracion:");
    panelDatos.add(jLabel2);
    jLabel2.setBounds(40, 140, 70, 15);

    jLabel4.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel4.setText("URL:");
    panelDatos.add(jLabel4);
    jLabel4.setBounds(40, 80, 28, 15);

    jLabel5.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel5.setText("Privacidad:");
    panelDatos.add(jLabel5);
    jLabel5.setBounds(40, 120, 80, 15);

    jLabel6.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel6.setText("Nombre:");
    panelDatos.add(jLabel6);
    jLabel6.setBounds(40, 60, 49, 15);

    jLabel7.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel7.setText("Fecha Publicacion:");
    panelDatos.add(jLabel7);
    jLabel7.setBounds(40, 100, 130, 15);

    jLabel15.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel15.setForeground(new java.awt.Color(33, 33, 33));
    jLabel15.setText("Datos de Video");
    panelDatos.add(jLabel15);
    jLabel15.setBounds(20, 20, 120, 17);
    panelDatos.add(jSeparator4);
    jSeparator4.setBounds(20, 40, 430, 10);

    tblValoraciones.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {
              {null, null},
              {null, null},
              {null, null},
              {null, null}
            },
            new String[] {"Nickname", "Le Gusta"}));
    tblValoraciones.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblValoraciones.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(tblValoraciones);

    panelDatos.add(jScrollPane1);
    jScrollPane1.setBounds(250, 180, 200, 100);

    btnVolver.setBackground(new java.awt.Color(51, 89, 245));
    btnVolver.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVolver.setForeground(new java.awt.Color(255, 255, 255));
    btnVolver.setText("Volver");
    btnVolver.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVolver.setContentAreaFilled(false);
    btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVolver.setOpaque(true);
    btnVolver.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVolverMouseClicked(evt);
          }
        });
    panelDatos.add(btnVolver);
    btnVolver.setBounds(20, 455, 110, 40);

    lblTitle1.setFont(new java.awt.Font("Monospaced", 0, 18)); // NOI18N
    lblTitle1.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle1.setText("Valoraciones");
    panelDatos.add(lblTitle1);
    lblTitle1.setBounds(277, 150, 150, 20);

    lblCategoria.setForeground(new java.awt.Color(60, 63, 65));
    lblCategoria.setText("jLabel10");
    panelDatos.add(lblCategoria);
    lblCategoria.setBounds(116, 160, 140, 15);

    lblNombre.setForeground(new java.awt.Color(60, 63, 65));
    lblNombre.setText("jLabel10");
    panelDatos.add(lblNombre);
    lblNombre.setBounds(95, 60, 360, 15);

    lblURL.setForeground(new java.awt.Color(60, 63, 65));
    lblURL.setText("jLabel10");
    panelDatos.add(lblURL);
    lblURL.setBounds(74, 80, 380, 15);

    lblFechaPublicacion.setForeground(new java.awt.Color(60, 63, 65));
    lblFechaPublicacion.setText("jLabel10");
    panelDatos.add(lblFechaPublicacion);
    lblFechaPublicacion.setBounds(172, 100, 280, 15);

    lblPrivacidad.setForeground(new java.awt.Color(60, 63, 65));
    lblPrivacidad.setText("jLabel10");
    panelDatos.add(lblPrivacidad);
    lblPrivacidad.setBounds(122, 120, 280, 15);

    lblDuracion.setForeground(new java.awt.Color(60, 63, 65));
    lblDuracion.setText("jLabel10");
    panelDatos.add(lblDuracion);
    lblDuracion.setBounds(110, 141, 140, 15);

    getContentPane().add(panelDatos);
    panelDatos.setBounds(30, 70, 470, 510);

    panelVideos.setBackground(new java.awt.Color(255, 255, 255));
    panelVideos.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelVideos.setLayout(null);

    jLabel11.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel11.setForeground(new java.awt.Color(33, 33, 33));
    jLabel11.setText("Seleccionar Video");
    panelVideos.add(jLabel11);
    jLabel11.setBounds(20, 230, 170, 17);
    panelVideos.add(jSeparator1);
    jSeparator1.setBounds(20, 250, 430, 10);

    txtFindVideo.setForeground(new java.awt.Color(60, 63, 65));
    txtFindVideo.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar video:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelVideos.add(txtFindVideo);
    txtFindVideo.setBounds(53, 280, 130, 40);

    jLabel12.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelVideos.add(jLabel12);
    jLabel12.setBounds(30, 292, 24, 24);
    panelVideos.add(jSeparator14);
    jSeparator14.setBounds(55, 317, 130, 10);

    tblVideos.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nickname"}));
    tblVideos.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblVideos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane5.setViewportView(tblVideos);

    panelVideos.add(jScrollPane5);
    jScrollPane5.setBounds(40, 330, 390, 100);

    jLabel13.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel13.setForeground(new java.awt.Color(33, 33, 33));
    jLabel13.setText("Seleccionar Usuario");
    panelVideos.add(jLabel13);
    jLabel13.setBounds(20, 20, 170, 17);
    panelVideos.add(jSeparator2);
    jSeparator2.setBounds(20, 40, 430, 10);

    btnSiguiente.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguiente.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguiente.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguiente.setText("Siguiente");
    btnSiguiente.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguiente.setContentAreaFilled(false);
    btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguiente.setOpaque(true);
    btnSiguiente.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteMouseClicked(evt);
          }
        });
    panelVideos.add(btnSiguiente);
    btnSiguiente.setBounds(330, 455, 110, 40);

    tblUsuarios.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nickname"}));
    tblUsuarios.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane6.setViewportView(tblUsuarios);

    panelVideos.add(jScrollPane6);
    jScrollPane6.setBounds(40, 110, 390, 100);

    txtFindNick.setForeground(new java.awt.Color(60, 63, 65));
    txtFindNick.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar usuario:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelVideos.add(txtFindNick);
    txtFindNick.setBounds(53, 65, 130, 40);

    jLabel16.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelVideos.add(jLabel16);
    jLabel16.setBounds(30, 77, 24, 24);
    panelVideos.add(jSeparator15);
    jSeparator15.setBounds(55, 102, 130, 10);

    getContentPane().add(panelVideos);
    panelVideos.setBounds(30, 70, 470, 510);

    jPanel1.setBackground(new java.awt.Color(246, 246, 246));
    jPanel1.setLayout(null);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel1.add(jLabel9);
    jLabel9.setBounds(460, 585, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel1.add(jLabel8);
    jLabel8.setBounds(490, 595, 50, 20);

    jLabel3.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel1.add(jLabel3);
    jLabel3.setBounds(240, 400, 310, 230);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Consulta Video");
    jPanel1.add(lblTitle);
    lblTitle.setBounds(170, 30, 210, 29);

    getContentPane().add(jPanel1);
    jPanel1.setBounds(0, 0, 550, 630);

    setBounds(237, 66, 552, 650);
  } // </editor-fold>//GEN-END:initComponents

  private void btnSiguienteMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteMouseClicked
    if (btnSiguiente.isEnabled()) {
      int idVideo = (int) modeloVideos.getValueAt(tblVideos.getSelectedRow(), 1);
      cargarDatosVideo(idVideo);
      panelVideos.setVisible(false);
      panelDatos.setVisible(true);
    }
  } // GEN-LAST:event_btnSiguienteMouseClicked

  private void trComentariosValueChanged(
      javax.swing.event.TreeSelectionEvent evt) { // GEN-FIRST:event_trComentariosValueChanged
  } // GEN-LAST:event_trComentariosValueChanged

  private void btnVolverMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVolverMouseClicked
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    tblVideos.getSelectionModel().removeIndexInterval(0, tblVideos.getRowCount());
    modeloValoraciones.setRowCount(0);
    DefaultTreeModel model = (DefaultTreeModel) trComentarios.getModel();
    DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
    root.removeAllChildren();
    model.reload();
    //    model.setRoot(null);
    panelVideos.setVisible(true);
    panelDatos.setVisible(false);
  } // GEN-LAST:event_btnVolverMouseClicked

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnSiguiente;
  private javax.swing.JButton btnVolver;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel11;
  private javax.swing.JLabel jLabel12;
  private javax.swing.JLabel jLabel13;
  private javax.swing.JLabel jLabel14;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel16;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JScrollPane jScrollPane3;
  private javax.swing.JScrollPane jScrollPane5;
  private javax.swing.JScrollPane jScrollPane6;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator14;
  private javax.swing.JSeparator jSeparator15;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JSeparator jSeparator3;
  private javax.swing.JSeparator jSeparator4;
  private javax.swing.JLabel lblCategoria;
  private javax.swing.JLabel lblDuracion;
  private javax.swing.JLabel lblFechaPublicacion;
  private javax.swing.JLabel lblNombre;
  private javax.swing.JLabel lblPrivacidad;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JLabel lblTitle1;
  private javax.swing.JLabel lblURL;
  private javax.swing.JPanel panelDatos;
  private javax.swing.JPanel panelVideos;
  private javax.swing.JTable tblUsuarios;
  private javax.swing.JTable tblValoraciones;
  private javax.swing.JTable tblVideos;
  private javax.swing.JTree trComentarios;
  private javax.swing.JTextArea txtDescripcion;
  private javax.swing.JTextField txtFindNick;
  private javax.swing.JTextField txtFindVideo;
  // End of variables declaration//GEN-END:variables
}
