/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.usuario;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.IUsuario;
import org.uytube.datatypes.DtCanal;
import org.uytube.datatypes.DtUsuario;
import org.uytube.ui.PopUp;

/** @author toto */
public class AltaUsuario extends JInternalFrame {

  private final IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
  private Date fechaNacimiento = null;
  private final Date fechaValida = new GregorianCalendar(1900, 1, 1).getTime();
  private final String EMAIL_PATTERN =
      "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
  private BufferedImage img;

  /*Verifica que el boton se puede activar*/
  private void enableButton() {
    try {
      SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
      df.setLenient(false);
      fechaNacimiento = df.parse(txtFecha.getText());
      if (fechaNacimiento.after(fechaValida)
          && txtCorreo.getText().matches(EMAIL_PATTERN)
          && !txtNombre.getText().isEmpty()
          && !txtContrasena.getText().isEmpty()
          && !txtNombreCanal.getText().isEmpty()
          && !txtNickname.getText().isEmpty()) {
        btnAceptar.setEnabled(true);
        btnAceptar.setBackground(new Color(0, 123, 64));
      } else {
        btnAceptar.setEnabled(false);
        btnAceptar.setBackground(Color.GRAY);
      }
    } catch (ParseException ex) {
      btnAceptar.setEnabled(false);
      btnAceptar.setBackground(Color.GRAY);
    }
  }

  /** Creates new form AltaUsuario */
  public AltaUsuario() {
    initComponents();
    enableButton();
    txtCorreo
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                if (txtCorreo.getText().matches(EMAIL_PATTERN)) {
                  enableButton();
                  txtCorreo.setBackground(new Color(255, 255, 255));
                } else {
                  txtCorreo.setBackground(new Color(250, 151, 151));
                  btnAceptar.setEnabled(false);
                  btnAceptar.setBackground(Color.GRAY);
                }
              }
            });

    txtFecha
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                try {
                  SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                  df.setLenient(false);
                  Date fechaNacimiento = df.parse(txtFecha.getText());
                  if (fechaNacimiento.after(fechaValida)) {
                    txtFecha.setBackground(new Color(255, 255, 255));
                    enableButton();
                  } else {
                    txtFecha.setBackground(new Color(250, 151, 151));
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackground(Color.GRAY);
                  }
                } catch (ParseException ex) {
                  txtFecha.setBackground(new Color(250, 151, 151));
                  btnAceptar.setEnabled(false);
                  btnAceptar.setBackground(Color.GRAY);
                }
              }
            });

    txtNombre
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                txtNombreCanal.setText(txtNombre.getText());
                enableButton();
              }
            });

    txtNombreCanal
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                enableButton();
              }
            });

    txtContrasena
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                enableButton();
              }
            });
    txtNickname
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                enableButton();
              }
            });
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jButton3 = new javax.swing.JButton();
    jPanel2 = new javax.swing.JPanel();
    txtFecha = new javax.swing.JFormattedTextField(new SimpleDateFormat("dd/MM/yyyy"));
    txtCorreo = new javax.swing.JTextField();
    jSeparator13 = new javax.swing.JSeparator();
    jLabel11 = new javax.swing.JLabel();
    jSeparator7 = new javax.swing.JSeparator();
    txtContrasena = new javax.swing.JTextField();
    jSeparator6 = new javax.swing.JSeparator();
    jSeparator3 = new javax.swing.JSeparator();
    jSeparator5 = new javax.swing.JSeparator();
    txtNombre = new javax.swing.JTextField();
    txtNombreCanal = new javax.swing.JTextField();
    jLabel1 = new javax.swing.JLabel();
    lblPic = new javax.swing.JLabel();
    jSeparator4 = new javax.swing.JSeparator();
    jLabel6 = new javax.swing.JLabel();
    jSeparator2 = new javax.swing.JSeparator();
    jSeparator8 = new javax.swing.JSeparator();
    txtNickname = new javax.swing.JTextField();
    rbPrivado = new javax.swing.JRadioButton();
    rbPublico = new javax.swing.JRadioButton();
    jScrollPane1 = new javax.swing.JScrollPane();
    txtDescripcionCanal = new javax.swing.JTextArea();
    btnAceptar = new javax.swing.JButton();
    btnCancelar = new javax.swing.JButton();
    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    txtApellido = new javax.swing.JTextField();
    jSeparator9 = new javax.swing.JSeparator();
    jPanel1 = new javax.swing.JPanel();
    lblTitle = new javax.swing.JLabel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();

    jButton3.setText("jButton3");

    setBackground(new java.awt.Color(237, 237, 237));
    setBorder(null);
    setClosable(true);
    setIconifiable(true);
    setTitle("Alta Usuario");
    setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
    setMinimumSize(new java.awt.Dimension(418, 583));
    setPreferredSize(new java.awt.Dimension(418, 583));
    getContentPane().setLayout(null);

    jPanel2.setBackground(new java.awt.Color(255, 255, 255));
    jPanel2.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    jPanel2.setLayout(null);

    txtFecha.setBorder(null);
    try {
      txtFecha.setFormatterFactory(
          new javax.swing.text.DefaultFormatterFactory(
              new javax.swing.text.MaskFormatter("##/##/####")));
    } catch (java.text.ParseException ex) {
      ex.printStackTrace();
    }
    txtFecha.setHorizontalAlignment(javax.swing.JTextField.CENTER);
    txtFecha.setText(null);
    txtFecha.addFocusListener(
        new java.awt.event.FocusAdapter() {
          public void focusLost(java.awt.event.FocusEvent evt) {
            txtFechaFocusLost(evt);
          }
        });
    txtFecha.addInputMethodListener(
        new java.awt.event.InputMethodListener() {
          public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {}

          public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            txtFechaInputMethodTextChanged(evt);
          }
        });
    txtFecha.addActionListener(this::txtFechaActionPerformed);
    jPanel2.add(txtFecha);
    txtFecha.setBounds(150, 140, 110, 20);

    txtCorreo.setBorder(null);
    jPanel2.add(txtCorreo);
    txtCorreo.setBounds(150, 200, 170, 20);
    jPanel2.add(jSeparator13);
    jSeparator13.setBounds(10, 40, 370, 10);

    jLabel11.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel11.setForeground(new java.awt.Color(33, 33, 33));
    jLabel11.setText("Datos de Usuario");
    jPanel2.add(jLabel11);
    jLabel11.setBounds(10, 20, 150, 17);
    jPanel2.add(jSeparator7);
    jSeparator7.setBounds(30, 100, 100, 10);

    txtContrasena.setForeground(new java.awt.Color(60, 63, 65));
    txtContrasena.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Contraseña *",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    jPanel2.add(txtContrasena);
    txtContrasena.setBounds(30, 183, 100, 40);
    jPanel2.add(jSeparator6);
    jSeparator6.setBounds(30, 160, 100, 10);
    jPanel2.add(jSeparator3);
    jSeparator3.setBounds(30, 220, 100, 10);
    jPanel2.add(jSeparator5);
    jSeparator5.setBounds(150, 160, 110, 10);

    txtNombre.setForeground(new java.awt.Color(60, 63, 65));
    txtNombre.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Nombre *",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    jPanel2.add(txtNombre);
    txtNombre.setBounds(30, 64, 100, 40);

    txtNombreCanal.setForeground(new java.awt.Color(60, 63, 65));
    txtNombreCanal.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Nombre *",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    jPanel2.add(txtNombreCanal);
    txtNombreCanal.setBounds(30, 273, 100, 40);

    jLabel1.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/removePic.png"))); // NOI18N
    jLabel1.setToolTipText("Cargar imagen por defecto");
    jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    jLabel1.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            jLabel1MouseClicked(evt);
          }
        });
    jPanel2.add(jLabel1);
    jLabel1.setBounds(340, 73, 30, 20);

    lblPic.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblPic.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/addImage.png"))); // NOI18N
    lblPic.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true));
    lblPic.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    lblPic.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            lblPicMouseClicked(evt);
          }
        });
    jPanel2.add(lblPic);
    lblPic.setBounds(280, 70, 90, 80);
    jPanel2.add(jSeparator4);
    jSeparator4.setBounds(150, 220, 170, 10);

    jLabel6.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel6.setForeground(new java.awt.Color(33, 33, 33));
    jLabel6.setText("Datos de Canal");
    jPanel2.add(jLabel6);
    jLabel6.setBounds(10, 240, 130, 17);
    jPanel2.add(jSeparator2);
    jSeparator2.setBounds(30, 310, 100, 10);
    jPanel2.add(jSeparator8);
    jSeparator8.setBounds(10, 260, 370, 10);

    txtNickname.setForeground(new java.awt.Color(60, 63, 65));
    txtNickname.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Nickname *",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    jPanel2.add(txtNickname);
    txtNickname.setBounds(30, 123, 100, 40);

    rbPrivado.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    rbPrivado.setSelected(true);
    rbPrivado.setText("Privado");
    rbPrivado.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    rbPrivado.addItemListener(this::rbPrivadoItemStateChanged);
    jPanel2.add(rbPrivado);
    rbPrivado.setBounds(260, 286, 90, 28);

    rbPublico.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    rbPublico.setText("Publico");
    rbPublico.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    rbPublico.addItemListener(this::rbPublicoItemStateChanged);
    jPanel2.add(rbPublico);
    rbPublico.setBounds(170, 286, 90, 28);

    jScrollPane1.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Descripcion",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N

    txtDescripcionCanal.setColumns(20);
    txtDescripcionCanal.setRows(5);
    txtDescripcionCanal.setBorder(
        javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    jScrollPane1.setViewportView(txtDescripcionCanal);

    jPanel2.add(jScrollPane1);
    jScrollPane1.setBounds(30, 320, 320, 100);

    btnAceptar.setBackground(new java.awt.Color(0, 123, 64));
    btnAceptar.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnAceptar.setForeground(new java.awt.Color(255, 255, 255));
    btnAceptar.setText("Aceptar");
    btnAceptar.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnAceptar.setContentAreaFilled(false);
    btnAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnAceptar.setEnabled(false);
    btnAceptar.setOpaque(true);
    btnAceptar.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnAceptarMouseClicked(evt);
          }
        });
    jPanel2.add(btnAceptar);
    btnAceptar.setBounds(60, 430, 110, 40);

    btnCancelar.setBackground(new java.awt.Color(255, 50, 40));
    btnCancelar.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
    btnCancelar.setText("Cancelar");
    btnCancelar.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnCancelar.setContentAreaFilled(false);
    btnCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnCancelar.setOpaque(true);
    btnCancelar.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnCancelarMouseClicked(evt);
          }
        });
    jPanel2.add(btnCancelar);
    btnCancelar.setBounds(220, 430, 110, 40);

    jLabel2.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel2.setText("Fecha Nac. *");
    jPanel2.add(jLabel2);
    jLabel2.setBounds(150, 120, 90, 15);

    jLabel3.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel3.setText("Email *");
    jPanel2.add(jLabel3);
    jLabel3.setBounds(150, 180, 49, 15);

    txtApellido.setForeground(new java.awt.Color(60, 63, 65));
    txtApellido.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Apellido *",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    jPanel2.add(txtApellido);
    txtApellido.setBounds(150, 63, 110, 40);
    jPanel2.add(jSeparator9);
    jSeparator9.setBounds(150, 100, 110, 10);

    getContentPane().add(jPanel2);
    jPanel2.setBounds(30, 70, 400, 490);

    jPanel1.setBackground(new java.awt.Color(246, 246, 246));
    jPanel1.setLayout(null);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Alta Usuario");
    jPanel1.add(lblTitle);
    lblTitle.setBounds(150, 20, 170, 29);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel1.add(jLabel9);
    jLabel9.setBounds(380, 563, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel1.add(jLabel8);
    jLabel8.setBounds(410, 576, 50, 15);

    jLabel4.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel1.add(jLabel4);
    jLabel4.setBounds(170, 380, 310, 230);

    getContentPane().add(jPanel1);
    jPanel1.setBounds(0, 0, 480, 610);

    setBounds(278, 77, 477, 629);
  } // </editor-fold>//GEN-END:initComponents

  private void jLabel1MouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_jLabel1MouseClicked
    lblPic.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/addImage.png"))); // NOI18N
    img = null;
  } // GEN-LAST:event_jLabel1MouseClicked

  private void btnAceptarMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnAceptarMouseClicked
    if (btnAceptar.isEnabled()) {
      JFrame frame = (JFrame) this.getDesktopPane().getTopLevelAncestor();
      try {
        String foto = UUID.randomUUID().toString() + ".png";
        if (img != null) {
          Path directory = Paths.get(System.getProperty("user.home") + "/uytube/imagenes");
          if (Files.notExists(directory)) {
            Files.createDirectories(directory);
          }
          File outputfile = new File(System.getProperty("user.home") + "/uytube/imagenes/" + foto);
          ImageIO.write(img, "png", outputfile);
        }
        DtUsuario dtUsuario =
            new DtUsuario(
                txtNickname.getText(),
                txtContrasena.getText(),
                txtNombre.getText(),
                txtApellido.getText(),
                txtCorreo.getText(),
                foto,
                fechaNacimiento);
        DtCanal dtCanal =
            new DtCanal(
                txtNombreCanal.getText(), txtDescripcionCanal.getText(), rbPrivado.isSelected());
        iUsuario.altausuario(dtUsuario, dtCanal);
        new PopUp(frame, "Alta Usuario", "Usuario creado correctamente", 5, PopUp.LEVEL.SUCCESS);
        dispose();
      } catch (IllegalArgumentException e) {
        new PopUp(frame, "Alta Usuario", e.getMessage(), 5, PopUp.LEVEL.ERROR);
      } catch (IOException e) {
        e.printStackTrace();
        new PopUp(
            frame, "Alta Usuario", "Ocurrio un error al guardar la imagen.", 5, PopUp.LEVEL.ERROR);
      }
    }
  } // GEN-LAST:event_btnAceptarMouseClicked

  private void btnCancelarMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnCancelarMouseClicked
    dispose();
  } // GEN-LAST:event_btnCancelarMouseClicked

  private void rbPublicoItemStateChanged(
      java.awt.event.ItemEvent evt) { // GEN-FIRST:event_rbPublicoItemStateChanged
    if (evt.getStateChange() == ItemEvent.SELECTED) {
      rbPrivado.setSelected(false);

    } else if (evt.getStateChange() == ItemEvent.DESELECTED) {
      rbPrivado.setSelected(true);
    }
  } // GEN-LAST:event_rbPublicoItemStateChanged

  private void rbPrivadoItemStateChanged(
      java.awt.event.ItemEvent evt) { // GEN-FIRST:event_rbPrivadoItemStateChanged
    if (evt.getStateChange() == ItemEvent.SELECTED) {
      rbPublico.setSelected(false);
    } else if (evt.getStateChange() == ItemEvent.DESELECTED) {
      rbPublico.setSelected(true);
    }
  } // GEN-LAST:event_rbPrivadoItemStateChanged

  private void lblPicMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_lblPicMouseClicked
    JFileChooser chooser = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG, JPG", "jpg", "png");
    chooser.setFileFilter(filter);
    int returnVal = chooser.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      try {
        img = ImageIO.read(new File(chooser.getSelectedFile().getAbsolutePath()));
        Image dimg =
            img.getScaledInstance(lblPic.getWidth(), lblPic.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon imageIcon = new ImageIcon(dimg);
        lblPic.setIcon(imageIcon);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  } // GEN-LAST:event_lblPicMouseClicked

  private void txtFechaInputMethodTextChanged(
      java.awt.event.InputMethodEvent evt) { // GEN-FIRST:event_txtFechaInputMethodTextChanged
  } // GEN-LAST:event_txtFechaInputMethodTextChanged

  private void txtFechaFocusLost(
      java.awt.event.FocusEvent evt) { // GEN-FIRST:event_txtFechaFocusLost
  } // GEN-LAST:event_txtFechaFocusLost

  private void txtFechaActionPerformed(
      java.awt.event.ActionEvent evt) { // GEN-FIRST:event_txtFechaActionPerformed
  } // GEN-LAST:event_txtFechaActionPerformed

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnAceptar;
  private javax.swing.JButton btnCancelar;
  private javax.swing.JButton jButton3;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel11;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JSeparator jSeparator13;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JSeparator jSeparator3;
  private javax.swing.JSeparator jSeparator4;
  private javax.swing.JSeparator jSeparator5;
  private javax.swing.JSeparator jSeparator6;
  private javax.swing.JSeparator jSeparator7;
  private javax.swing.JSeparator jSeparator8;
  private javax.swing.JSeparator jSeparator9;
  private javax.swing.JLabel lblPic;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JRadioButton rbPrivado;
  private javax.swing.JRadioButton rbPublico;
  private javax.swing.JTextField txtApellido;
  private javax.swing.JTextField txtContrasena;
  private javax.swing.JTextField txtCorreo;
  private javax.swing.JTextArea txtDescripcionCanal;
  private javax.swing.JFormattedTextField txtFecha;
  private javax.swing.JTextField txtNickname;
  private javax.swing.JTextField txtNombre;
  private javax.swing.JTextField txtNombreCanal;
  // End of variables declaration//GEN-END:variables
}
