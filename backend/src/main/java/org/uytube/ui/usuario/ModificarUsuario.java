/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.usuario;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ILista;
import org.uytube.controladores.IUsuario;
import org.uytube.controladores.IVideo;
import org.uytube.datatypes.DtLista;
import org.uytube.datatypes.DtModUsuario;
import org.uytube.datatypes.DtUsuarioExt;
import org.uytube.exception.YonaException;
import org.uytube.ui.PopUp;
import org.uytube.ui.lista.ModificarLista;
import org.uytube.ui.video.ModificarVideo;

/** @author toto */
public class ModificarUsuario extends javax.swing.JInternalFrame {

  private final ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);
  private final IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
  private final IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
  private final Date fechaValida = new GregorianCalendar(1900, 1, 1).getTime();
  private int idLista;
  private int idVideo;
  private String path = null;

  private final DefaultTableModel modeloUsuarios =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final DefaultTableModel modeloVideos =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final DefaultTableModel modeloListas =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private void initTblUsuarios() {
    tblUsuarios.setModel(modeloUsuarios);
    tblUsuarios.setRowSorter(rowSorterUsuario);
    tblUsuarios
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblUsuarios.getSelectedRow() >= 0) {
                btnSiguiente.setEnabled(true);
                btnSiguiente.setBackground(new Color(0, 123, 64));
              }
            });
    registerFilter(txtFindNick, rowSorterUsuario);
  }

  private void initTablaVideo() {
    tblVideos.setModel(modeloVideos);
    tblVideos.setRowSorter(rowSorterVideos);
    registerFilter(txtFindVideo, rowSorterVideos);
    modeloVideos.addColumn("id");
    modeloVideos.addColumn("Nombre");
    TableColumnModel tcm = tblVideos.getColumnModel();
    tcm.removeColumn(tcm.getColumn(0));
    tblVideos
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblVideos.getSelectedRow() >= 0) {
                idVideo = (int) modeloVideos.getValueAt(tblVideos.getSelectedRow(), 0);
                btnVerVideo.setEnabled(true);
                btnVerVideo.setBackground(new Color(255, 132, 45));
              }
            });
  }

  private void initTablaLista() {
    tblListas.setModel(modeloListas);
    tblListas.setRowSorter(rowSorterListas);
    registerFilter(txtFindLista, rowSorterListas);
    modeloListas.addColumn("id");
    modeloListas.addColumn("Nombre");
    TableColumnModel tcm = tblListas.getColumnModel();
    tcm.removeColumn(tcm.getColumn(0));
    tblListas
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblListas.getSelectedRow() >= 0) {
                idLista = (int) modeloListas.getValueAt(tblListas.getSelectedRow(), 0);
                btnVerLista.setEnabled(true);
                btnVerLista.setBackground(new Color(255, 132, 45));
              }
            });
  }

  private final TableRowSorter<DefaultTableModel> rowSorterUsuario =
      new TableRowSorter<>(modeloUsuarios);

  private final TableRowSorter<DefaultTableModel> rowSorterVideos =
      new TableRowSorter<>(modeloVideos);

  private final TableRowSorter<DefaultTableModel> rowSorterListas =
      new TableRowSorter<>(modeloListas);
  private BufferedImage img;

  private void registerFilter(
      JTextField txtField, TableRowSorter<DefaultTableModel> rowSorterVideo) {
    txtField
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                find();
              }

              private void find() {
                String text = txtField.getText();
                if (text.trim().length() == 0) {
                  rowSorterVideo.setRowFilter(null);
                } else {
                  rowSorterVideo.setRowFilter(RowFilter.regexFilter("(?i)" + Pattern.quote(text)));
                }
              }
            });
  }

  private void enableButton() {
    try {
      SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
      df.setLenient(false);
      Date fechaNacimiento = df.parse(txtFecha5.getText());
      if (fechaNacimiento.after(fechaValida)
          && !txtNombre.getText().isEmpty()
          && !txtApellido.getText().isEmpty()) {
        btnAceptar.setEnabled(true);
        btnAceptar.setBackground(new Color(0, 123, 64));
      } else {
        btnAceptar.setEnabled(false);
        btnAceptar.setBackground(Color.GRAY);
      }
    } catch (ParseException ex) {
      btnAceptar.setEnabled(false);
      btnAceptar.setBackground(Color.GRAY);
    }
  }

  /** Creates new form ModificarUsuario */
  public ModificarUsuario() {
    initComponents();
    initTblUsuarios();
    initTablaVideo();
    initTablaLista();

    panelDatosCanal.setVisible(false);
    panelDatosUser.setVisible(false);
    btnVerLista.setEnabled(false);
    btnVerLista.setBackground(Color.GRAY);
    btnVerVideo.setEnabled(false);
    btnVerVideo.setBackground(Color.GRAY);

    List<String> usuarios = iUsuario.listarUsuarios();
    modeloUsuarios.addColumn("Nickname");
    for (String usuario : usuarios) {
      modeloUsuarios.addRow(new Object[] {usuario});
    }
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    panelDatosUser.setVisible(false);

    lblVolver.setVisible(false);

    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);

    lblVolver.setVisible(false);

    txtFecha5
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void changedUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                changed();
              }

              @Override
              public void insertUpdate(DocumentEvent e) {
                changed();
              }

              void changed() {
                try {
                  SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                  df.setLenient(false);
                  Date fechaNacimiento = df.parse(txtFecha5.getText());
                  if (fechaNacimiento.after(fechaValida)) {
                    txtFecha5.setBackground(new Color(255, 255, 255));
                    enableButton();
                  } else {
                    txtFecha5.setBackground(new Color(250, 151, 151));
                    btnAceptar.setEnabled(false);
                    btnAceptar.setBackground(Color.GRAY);
                  }
                } catch (ParseException ex) {
                  txtFecha5.setBackground(new Color(250, 151, 151));
                  btnAceptar.setEnabled(false);
                  btnAceptar.setBackground(Color.GRAY);
                }
              }
            });
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    panelUsuario = new javax.swing.JPanel();
    btnSiguiente = new javax.swing.JButton();
    jLabel15 = new javax.swing.JLabel();
    jSeparator13 = new javax.swing.JSeparator();
    txtFindNick = new javax.swing.JTextField();
    jLabel12 = new javax.swing.JLabel();
    jSeparator14 = new javax.swing.JSeparator();
    jScrollPane5 = new javax.swing.JScrollPane();
    tblUsuarios = new javax.swing.JTable();
    panelDatosCanal = new javax.swing.JPanel();
    lblTitle = new javax.swing.JLabel();
    lblTitle1 = new javax.swing.JLabel();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblVideos = new javax.swing.JTable();
    jSeparator1 = new javax.swing.JSeparator();
    btnVerVideo = new javax.swing.JButton();
    txtFindVideo = new javax.swing.JTextField();
    jLabel10 = new javax.swing.JLabel();
    jSeparator4 = new javax.swing.JSeparator();
    jSeparator2 = new javax.swing.JSeparator();
    jScrollPane2 = new javax.swing.JScrollPane();
    tblListas = new javax.swing.JTable();
    btnVerLista = new javax.swing.JButton();
    txtFindLista = new javax.swing.JTextField();
    jLabel11 = new javax.swing.JLabel();
    jSeparator5 = new javax.swing.JSeparator();
    btnAceptar = new javax.swing.JButton();
    rbPublico = new javax.swing.JRadioButton();
    rbPrivado = new javax.swing.JRadioButton();
    lblTitle2 = new javax.swing.JLabel();
    jSeparator3 = new javax.swing.JSeparator();
    panelDatosUser = new javax.swing.JPanel();
    lblImg = new javax.swing.JLabel();
    Nickname = new javax.swing.JLabel();
    Nombre = new javax.swing.JLabel();
    Apellido = new javax.swing.JLabel();
    FechNac = new javax.swing.JLabel();
    Email = new javax.swing.JLabel();
    jSeparator6 = new javax.swing.JSeparator();
    jSeparator7 = new javax.swing.JSeparator();
    jSeparator8 = new javax.swing.JSeparator();
    jSeparator9 = new javax.swing.JSeparator();
    jSeparator10 = new javax.swing.JSeparator();
    lblNick = new javax.swing.JLabel();
    lblMail = new javax.swing.JLabel();
    txtApellido = new javax.swing.JTextField();
    txtNombre = new javax.swing.JTextField();
    txtFecha5 = new javax.swing.JFormattedTextField(new SimpleDateFormat("dd/MM/yyyy"));
    jPanel3 = new javax.swing.JPanel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel7 = new javax.swing.JLabel();
    jPanel1 = new javax.swing.JPanel();
    lblVolver = new javax.swing.JLabel();
    lblTitle4 = new javax.swing.JLabel();

    setClosable(true);
    setIconifiable(true);
    setTitle("Modificar Usuario");
    getContentPane().setLayout(null);

    panelUsuario.setBackground(new java.awt.Color(255, 255, 255));
    panelUsuario.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelUsuario.setLayout(null);

    btnSiguiente.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguiente.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguiente.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguiente.setText("Siguiente");
    btnSiguiente.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguiente.setContentAreaFilled(false);
    btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguiente.setOpaque(true);
    btnSiguiente.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteMouseClicked(evt);
          }
        });
    panelUsuario.add(btnSiguiente);
    btnSiguiente.setBounds(380, 290, 110, 40);

    jLabel15.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel15.setForeground(new java.awt.Color(33, 33, 33));
    jLabel15.setText("Seleccionar Usuario");
    panelUsuario.add(jLabel15);
    jLabel15.setBounds(20, 30, 160, 17);
    panelUsuario.add(jSeparator13);
    jSeparator13.setBounds(20, 50, 480, 10);

    txtFindNick.setForeground(new java.awt.Color(60, 63, 65));
    txtFindNick.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar usuario:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelUsuario.add(txtFindNick);
    txtFindNick.setBounds(53, 65, 130, 40);

    jLabel12.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelUsuario.add(jLabel12);
    jLabel12.setBounds(30, 77, 24, 24);
    panelUsuario.add(jSeparator14);
    jSeparator14.setBounds(55, 102, 130, 10);

    tblUsuarios.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nickname"}));
    tblUsuarios.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane5.setViewportView(tblUsuarios);

    panelUsuario.add(jScrollPane5);
    jScrollPane5.setBounds(40, 110, 430, 160);

    getContentPane().add(panelUsuario);
    panelUsuario.setBounds(40, 140, 520, 350);

    panelDatosCanal.setBackground(new java.awt.Color(255, 255, 255));
    panelDatosCanal.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelDatosCanal.setLayout(null);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 20)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Videos");
    panelDatosCanal.add(lblTitle);
    lblTitle.setBounds(90, 30, 80, 20);

    lblTitle1.setFont(new java.awt.Font("Monospaced", 0, 20)); // NOI18N
    lblTitle1.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle1.setText("Listas");
    panelDatosCanal.add(lblTitle1);
    lblTitle1.setBounds(340, 30, 80, 20);

    tblVideos.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nombre"}));
    tblVideos.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblVideos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(tblVideos);

    panelDatosCanal.add(jScrollPane1);
    jScrollPane1.setBounds(10, 130, 150, 100);
    panelDatosCanal.add(jSeparator1);
    jSeparator1.setBounds(10, 60, 230, 20);

    btnVerVideo.setBackground(new java.awt.Color(255, 132, 45));
    btnVerVideo.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVerVideo.setForeground(new java.awt.Color(255, 255, 255));
    btnVerVideo.setText("Modificar");
    btnVerVideo.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVerVideo.setContentAreaFilled(false);
    btnVerVideo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVerVideo.setOpaque(true);
    btnVerVideo.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVerVideoMouseClicked(evt);
          }
        });
    panelDatosCanal.add(btnVerVideo);
    btnVerVideo.setBounds(170, 132, 80, 30);

    txtFindVideo.setForeground(new java.awt.Color(60, 63, 65));
    txtFindVideo.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar video:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelDatosCanal.add(txtFindVideo);
    txtFindVideo.setBounds(35, 83, 120, 40);

    jLabel10.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelDatosCanal.add(jLabel10);
    jLabel10.setBounds(10, 97, 24, 24);
    panelDatosCanal.add(jSeparator4);
    jSeparator4.setBounds(35, 120, 120, 10);
    panelDatosCanal.add(jSeparator2);
    jSeparator2.setBounds(260, 60, 250, 20);

    tblListas.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nombre"}));
    tblListas.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblListas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane2.setViewportView(tblListas);

    panelDatosCanal.add(jScrollPane2);
    jScrollPane2.setBounds(270, 130, 150, 100);

    btnVerLista.setBackground(new java.awt.Color(255, 132, 45));
    btnVerLista.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVerLista.setForeground(new java.awt.Color(255, 255, 255));
    btnVerLista.setText("Modificar");
    btnVerLista.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVerLista.setContentAreaFilled(false);
    btnVerLista.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVerLista.setOpaque(true);
    btnVerLista.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVerListaMouseClicked(evt);
          }
        });
    panelDatosCanal.add(btnVerLista);
    btnVerLista.setBounds(430, 132, 80, 30);

    txtFindLista.setForeground(new java.awt.Color(60, 63, 65));
    txtFindLista.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar lista:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelDatosCanal.add(txtFindLista);
    txtFindLista.setBounds(295, 83, 120, 40);

    jLabel11.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelDatosCanal.add(jLabel11);
    jLabel11.setBounds(270, 97, 24, 24);
    panelDatosCanal.add(jSeparator5);
    jSeparator5.setBounds(300, 120, 115, 10);

    btnAceptar.setBackground(new java.awt.Color(0, 123, 64));
    btnAceptar.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnAceptar.setForeground(new java.awt.Color(255, 255, 255));
    btnAceptar.setText("Aceptar");
    btnAceptar.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnAceptar.setContentAreaFilled(false);
    btnAceptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnAceptar.setOpaque(true);
    btnAceptar.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnAceptarMouseClicked(evt);
          }
        });
    panelDatosCanal.add(btnAceptar);
    btnAceptar.setBounds(390, 260, 110, 40);

    rbPublico.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    rbPublico.setText("Publico");
    rbPublico.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    rbPublico.addItemListener(
        new java.awt.event.ItemListener() {
          public void itemStateChanged(java.awt.event.ItemEvent evt) {
            rbPublicoItemStateChanged(evt);
          }
        });
    panelDatosCanal.add(rbPublico);
    rbPublico.setBounds(130, 280, 90, 23);

    rbPrivado.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    rbPrivado.setSelected(true);
    rbPrivado.setText("Privado");
    rbPrivado.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    rbPrivado.addItemListener(
        new java.awt.event.ItemListener() {
          public void itemStateChanged(java.awt.event.ItemEvent evt) {
            rbPrivadoItemStateChanged(evt);
          }
        });
    panelDatosCanal.add(rbPrivado);
    rbPrivado.setBounds(220, 280, 90, 23);

    lblTitle2.setFont(new java.awt.Font("Monospaced", 0, 20)); // NOI18N
    lblTitle2.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle2.setText("Canal");
    panelDatosCanal.add(lblTitle2);
    lblTitle2.setBounds(190, 240, 70, 20);
    panelDatosCanal.add(jSeparator3);
    jSeparator3.setBounds(130, 270, 180, 20);

    getContentPane().add(panelDatosCanal);
    panelDatosCanal.setBounds(40, 180, 520, 310);

    panelDatosUser.setBackground(new java.awt.Color(255, 255, 255));
    panelDatosUser.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelDatosUser.setLayout(null);

    lblImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblImg.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/addImage.png"))); // NOI18N
    lblImg.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
    lblImg.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            lblImgMouseClicked(evt);
          }
        });
    panelDatosUser.add(lblImg);
    lblImg.setBounds(17, 18, 87, 100);

    Nickname.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    Nickname.setText("Nickname:");
    panelDatosUser.add(Nickname);
    Nickname.setBounds(110, 18, 63, 15);

    Nombre.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    Nombre.setText("Nombre:");
    panelDatosUser.add(Nombre);
    Nombre.setBounds(110, 39, 49, 15);

    Apellido.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    Apellido.setText("Apellido:");
    panelDatosUser.add(Apellido);
    Apellido.setBounds(110, 59, 63, 15);

    FechNac.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    FechNac.setText("Fecha de Nacimiento:");
    FechNac.setToolTipText("");
    panelDatosUser.add(FechNac);
    FechNac.setBounds(110, 80, 140, 15);

    Email.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    Email.setText("Email:");
    panelDatosUser.add(Email);
    Email.setBounds(110, 100, 42, 15);
    panelDatosUser.add(jSeparator6);
    jSeparator6.setBounds(110, 53, 270, 10);
    panelDatosUser.add(jSeparator7);
    jSeparator7.setBounds(110, 73, 270, 10);
    panelDatosUser.add(jSeparator8);
    jSeparator8.setBounds(110, 94, 270, 10);
    panelDatosUser.add(jSeparator9);
    jSeparator9.setBounds(110, 114, 270, 10);
    panelDatosUser.add(jSeparator10);
    jSeparator10.setBounds(111, 32, 270, 10);

    lblNick.setForeground(new java.awt.Color(60, 63, 65));
    lblNick.setText("Forbi");
    panelDatosUser.add(lblNick);
    lblNick.setBounds(180, 15, 200, 20);

    lblMail.setForeground(new java.awt.Color(60, 63, 65));
    lblMail.setText("Marianoz@vera.com.uy");
    panelDatosUser.add(lblMail);
    lblMail.setBounds(159, 92, 220, 30);

    txtApellido.setForeground(new java.awt.Color(60, 63, 65));
    txtApellido.setText("Apellido");
    txtApellido.setBorder(null);
    panelDatosUser.add(txtApellido);
    txtApellido.setBounds(179, 52, 210, 30);

    txtNombre.setForeground(new java.awt.Color(60, 63, 65));
    txtNombre.setText("Nombre");
    txtNombre.setBorder(null);
    panelDatosUser.add(txtNombre);
    txtNombre.setBounds(166, 37, 210, 20);

    txtFecha5.setBorder(null);
    txtFecha5.setForeground(new java.awt.Color(60, 63, 65));
    try {
      txtFecha5.setFormatterFactory(
          new javax.swing.text.DefaultFormatterFactory(
              new javax.swing.text.MaskFormatter("##/##/####")));
    } catch (java.text.ParseException ex) {
      ex.printStackTrace();
    }
    txtFecha5.setHorizontalAlignment(javax.swing.JTextField.LEFT);
    txtFecha5.setText(null);
    txtFecha5.addFocusListener(
        new java.awt.event.FocusAdapter() {
          public void focusLost(java.awt.event.FocusEvent evt) {
            txtFecha5FocusLost(evt);
          }
        });
    txtFecha5.addInputMethodListener(
        new java.awt.event.InputMethodListener() {
          public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            txtFecha5InputMethodTextChanged(evt);
          }

          public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {}
        });
    txtFecha5.addActionListener(
        new java.awt.event.ActionListener() {
          public void actionPerformed(java.awt.event.ActionEvent evt) {
            txtFecha5ActionPerformed(evt);
          }
        });
    panelDatosUser.add(txtFecha5);
    txtFecha5.setBounds(256, 78, 110, 20);

    getContentPane().add(panelDatosUser);
    panelDatosUser.setBounds(100, 20, 400, 130);

    jPanel3.setBackground(new java.awt.Color(246, 246, 246));
    jPanel3.setLayout(null);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel3.add(jLabel9);
    jLabel9.setBounds(510, 510, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel3.add(jLabel8);
    jLabel8.setBounds(540, 520, 49, 15);

    jLabel7.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel3.add(jLabel7);
    jLabel7.setBounds(290, 320, 310, 230);

    jPanel1.setBackground(new java.awt.Color(53, 47, 91));
    jPanel1.setLayout(null);

    lblVolver.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/backArrow.png"))); // NOI18N
    lblVolver.setToolTipText("Volver");
    lblVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    lblVolver.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            lblVolverMouseClicked(evt);
          }
        });
    jPanel1.add(lblVolver);
    lblVolver.setBounds(4, 10, 40, 20);

    lblTitle4.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle4.setForeground(new java.awt.Color(255, 255, 255));
    lblTitle4.setText("Modificar Usuario");
    jPanel1.add(lblTitle4);
    lblTitle4.setBounds(175, 20, 260, 70);

    jPanel3.add(jPanel1);
    jPanel1.setBounds(0, 0, 600, 110);

    getContentPane().add(jPanel3);
    jPanel3.setBounds(0, 0, 600, 550);

    setBounds(212, 118, 599, 572);
  } // </editor-fold>//GEN-END:initComponents

  private void rbPublicoItemStateChanged(
      java.awt.event.ItemEvent evt) { // GEN-FIRST:event_rbPublicoItemStateChanged
    if (evt.getStateChange() == ItemEvent.SELECTED) {
      rbPrivado.setSelected(false);

    } else if (evt.getStateChange() == ItemEvent.DESELECTED) {
      rbPrivado.setSelected(true);
    }
  } // GEN-LAST:event_rbPublicoItemStateChanged

  private void rbPrivadoItemStateChanged(
      java.awt.event.ItemEvent evt) { // GEN-FIRST:event_rbPrivadoItemStateChanged
    if (evt.getStateChange() == ItemEvent.SELECTED) {
      rbPublico.setSelected(false);
    } else if (evt.getStateChange() == ItemEvent.DESELECTED) {
      rbPublico.setSelected(true);
    }
  } // GEN-LAST:event_rbPrivadoItemStateChanged

  private void btnVerVideoMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVerVideoMouseClicked
    if (btnVerVideo.isEnabled()) {
      ModificarVideo modificarVideo = new ModificarVideo(idVideo);
      this.getDesktopPane().add(modificarVideo);
      modificarVideo.show();
      try {
        this.setIcon(true);
      } catch (PropertyVetoException ex) {
        Logger.getLogger(ModificarUsuario.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  } // GEN-LAST:event_btnVerVideoMouseClicked

  private void btnVerListaMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVerListaMouseClicked
    if (btnVerLista.isEnabled()) {
      ModificarLista modificarLista = new ModificarLista(idLista);
      this.getDesktopPane().add(modificarLista);
      modificarLista.show();
      try {
        this.setIcon(true);
      } catch (PropertyVetoException ex) {
        Logger.getLogger(ModificarUsuario.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  } // GEN-LAST:event_btnVerListaMouseClicked

  private void txtFecha5FocusLost(
      java.awt.event.FocusEvent evt) { // GEN-FIRST:event_txtFecha5FocusLost
  } // GEN-LAST:event_txtFecha5FocusLost

  private void txtFecha5InputMethodTextChanged(
      java.awt.event.InputMethodEvent evt) { // GEN-FIRST:event_txtFecha5InputMethodTextChanged
  } // GEN-LAST:event_txtFecha5InputMethodTextChanged

  private void txtFecha5ActionPerformed(
      java.awt.event.ActionEvent evt) { // GEN-FIRST:event_txtFecha5ActionPerformed
  } // GEN-LAST:event_txtFecha5ActionPerformed

  private void lblImgMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_lblImgMouseClicked
    JFileChooser chooser = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG, JPG", "jpg", "png");
    chooser.setFileFilter(filter);
    int returnVal = chooser.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      try {
        img = ImageIO.read(new File(chooser.getSelectedFile().getAbsolutePath()));
        Image dimg =
            img.getScaledInstance(lblImg.getWidth(), lblImg.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon imageIcon = new ImageIcon(dimg);
        lblImg.setIcon(imageIcon);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  } // GEN-LAST:event_lblImgMouseClicked

  private Date fecha;

  private void btnAceptarMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnAceptarMouseClicked
    if (btnAceptar.isEnabled()) {
      JFrame frame = (JFrame) this.getDesktopPane().getTopLevelAncestor();
      try {
        String foto = "";
        if (img != null) {
          foto = UUID.randomUUID().toString() + ".png";
          Path directory = Paths.get(System.getProperty("user.home") + "/uytube/imagenes");
          if (Files.notExists(directory)) {
            Files.createDirectories(directory);
          }
          File outputfile = new File(System.getProperty("user.home") + "/uytube/imagenes/" + foto);
          ImageIO.write(img, "png", outputfile);
          // Eliminar la foto vieja
          if (path != null) {
            Files.deleteIfExists(Paths.get(path));
          }
          path = outputfile.getAbsolutePath();
        }

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        df.setLenient(false);
        Date fecha = df.parse(txtFecha5.getText());

        DtModUsuario dtUsuario =
            new DtModUsuario(
                txtNombre.getText(), txtApellido.getText(), foto, fecha, rbPrivado.isSelected());

        iUsuario.modificarUsuario(lblNick.getText(), dtUsuario);
        new PopUp(
            frame, "Modificar Usuario", "Usuario modificado correctamente", 5, PopUp.LEVEL.SUCCESS);
        dispose();
      } catch (IOException e) {
        e.printStackTrace();
        new PopUp(
            frame,
            "Modificar Usuario",
            "Ocurrio un error al guardar la imagen.",
            5,
            PopUp.LEVEL.ERROR);
      } catch (ParseException ex) {
        txtFecha5.setBackground(new Color(250, 151, 151));
        btnAceptar.setEnabled(false);
        btnAceptar.setBackground(Color.GRAY);
      } catch (YonaException e) {
        new PopUp(frame, "Modificar Usuario", e.getMessage(), 5, PopUp.LEVEL.ERROR);
      }
    }
  } // GEN-LAST:event_btnAceptarMouseClicked

  private void btnSiguienteMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteMouseClicked
    if (btnSiguiente.isEnabled()) {
      panelDatosCanal.setVisible(true);
      panelDatosUser.setVisible(true);
      panelUsuario.setVisible(false);
      lblVolver.setVisible(true);
      btnAceptar.setEnabled(true);

      JFrame frame = (JFrame) this.getDesktopPane().getTopLevelAncestor();
      try {
        String nickname = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 0).toString();
        DtUsuarioExt data = iUsuario.consultaDeUsuario(nickname);
        ArrayList<DtLista> dtListas = iLista.listasParticularesUsuario(nickname);
        HashMap<Integer, String> dtVideos = iVideo.listarVideosUsuario(nickname);

        /*cargar imagen*/
        if (!data.getUsuario().getFoto().isEmpty()) {
          BufferedImage img = ImageIO.read(new File(data.getUsuario().getFoto()));
          Image dimg =
              img.getScaledInstance(lblImg.getWidth(), lblImg.getHeight(), Image.SCALE_SMOOTH);
          path = data.getUsuario().getFoto();
          ImageIcon imageIcon = new ImageIcon(dimg);
          lblImg.setIcon(imageIcon);
        }

        txtApellido.setText(data.getUsuario().getApellido());
        txtNombre.setText(data.getUsuario().getNombre());
        lblNick.setText(data.getUsuario().getNickname());
        lblMail.setText(data.getUsuario().getEmail());

        Date date = data.getUsuario().getFechaNacimiento();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = df.format(date);
        txtFecha5.setText(strDate);

        dtListas.forEach(
            (dt) -> {
              Object[] fila = new Object[2]; // creo el array de 1
              fila[1] = dt.getNombre(); // en la pos 1 cargo el nombre
              fila[0] = dt.getId(); // en la pos 0 cargo el id
              modeloListas.addRow(fila);
            });
        dtVideos.forEach(
            (k, v) -> {
              Object[] fila = new Object[2]; // creo el array de 1
              fila[1] = v; // en la pos 1 cargo el nombre
              fila[0] = k; // en la pos 0 cargo el id
              modeloVideos.addRow(fila);
            });
        panelDatosCanal.setVisible(true);
        rbPrivado.setSelected(data.getCanal().isPrivado());
        rbPublico.setSelected(!data.getCanal().isPrivado());
      } catch (YonaException e) {
        new PopUp(frame, "Modificar Usuario", e.getMessage(), 5, PopUp.LEVEL.ERROR);
      } catch (IOException ex) {
        new PopUp(
            frame,
            "Modificar Usuario",
            "No se pudo leer la imagen del usuario.",
            5,
            PopUp.LEVEL.ERROR);
        this.dispose();
      }
    }
  } // GEN-LAST:event_btnSiguienteMouseClicked

  private void lblVolverMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_lblVolverMouseClicked
    panelDatosCanal.setVisible(false);
    panelDatosUser.setVisible(false);
    panelUsuario.setVisible(true);
    modeloListas.setRowCount(0);
    modeloVideos.setRowCount(0);
    lblImg.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/addImage.png"))); // NOI18N

    lblVolver.setVisible(false);
  } // GEN-LAST:event_lblVolverMouseClicked

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JLabel Apellido;
  private javax.swing.JLabel Email;
  private javax.swing.JLabel FechNac;
  private javax.swing.JLabel Nickname;
  private javax.swing.JLabel Nombre;
  private javax.swing.JButton btnAceptar;
  private javax.swing.JButton btnSiguiente;
  private javax.swing.JButton btnVerLista;
  private javax.swing.JButton btnVerVideo;
  private javax.swing.JLabel jLabel10;
  private javax.swing.JLabel jLabel11;
  private javax.swing.JLabel jLabel12;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JScrollPane jScrollPane5;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator10;
  private javax.swing.JSeparator jSeparator13;
  private javax.swing.JSeparator jSeparator14;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JSeparator jSeparator3;
  private javax.swing.JSeparator jSeparator4;
  private javax.swing.JSeparator jSeparator5;
  private javax.swing.JSeparator jSeparator6;
  private javax.swing.JSeparator jSeparator7;
  private javax.swing.JSeparator jSeparator8;
  private javax.swing.JSeparator jSeparator9;
  private javax.swing.JLabel lblImg;
  private javax.swing.JLabel lblMail;
  private javax.swing.JLabel lblNick;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JLabel lblTitle1;
  private javax.swing.JLabel lblTitle2;
  private javax.swing.JLabel lblTitle4;
  private javax.swing.JLabel lblVolver;
  private javax.swing.JPanel panelDatosCanal;
  private javax.swing.JPanel panelDatosUser;
  private javax.swing.JPanel panelUsuario;
  private javax.swing.JRadioButton rbPrivado;
  private javax.swing.JRadioButton rbPublico;
  private javax.swing.JTable tblListas;
  private javax.swing.JTable tblUsuarios;
  private javax.swing.JTable tblVideos;
  private javax.swing.JTextField txtApellido;
  private javax.swing.JFormattedTextField txtFecha5;
  private javax.swing.JTextField txtFindLista;
  private javax.swing.JTextField txtFindNick;
  private javax.swing.JTextField txtFindVideo;
  private javax.swing.JTextField txtNombre;
  // End of variables declaration//GEN-END:variables
}
