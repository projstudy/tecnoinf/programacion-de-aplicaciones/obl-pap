/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.usuario;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ILista;
import org.uytube.controladores.IUsuario;
import org.uytube.controladores.IVideo;
import org.uytube.datatypes.DtLista;
import org.uytube.datatypes.DtSeguidor;
import org.uytube.datatypes.DtUsuarioExt;
import org.uytube.exception.YonaException;
import org.uytube.ui.PopUp;
import org.uytube.ui.lista.ConsultaLista;
import org.uytube.ui.video.ConsultaVideo;

/** @author toto */
public class ConsultaUsuario extends javax.swing.JInternalFrame {

  private final IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
  private final IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
  private final ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);

  private int idLista;
  private int idVideo;

  private final DefaultTableModel modeloUsuarios =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  /////////
  private final DefaultTableModel modeloVideos =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final DefaultTableModel modeloListas =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final DefaultTableModel modeloSeguidores =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final DefaultTableModel modeloSeguidos =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  /////////////////
  private void initTablaVideo() {
    tblVideos.setModel(modeloVideos);
    tblVideos.setRowSorter(rowSorterVideos);
    registerFilter(txtFindVideo, rowSorterVideos);
    modeloVideos.addColumn("id");
    modeloVideos.addColumn("Nombre");
    TableColumnModel tcm = tblVideos.getColumnModel();
    tcm.removeColumn(tcm.getColumn(0));
    tblVideos
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblVideos.getSelectedRow() >= 0) {
                idVideo = (int) modeloVideos.getValueAt(tblVideos.getSelectedRow(), 0);
                btnVerVideo.setEnabled(true);
                btnVerVideo.setBackground(new Color(0, 123, 64));
              }
            });
  }

  private void initTablaLista() {
    tblListas.setModel(modeloListas);
    tblListas.setRowSorter(rowSorterListas);
    registerFilter(txtFindLista, rowSorterListas);
    modeloListas.addColumn("id");
    modeloListas.addColumn("Nombre");
    TableColumnModel tcm = tblListas.getColumnModel();
    tcm.removeColumn(tcm.getColumn(0));
    tblListas
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblListas.getSelectedRow() >= 0) {
                idLista = (int) modeloListas.getValueAt(tblListas.getSelectedRow(), 0);
                btnVerLista.setEnabled(true);
                btnVerLista.setBackground(new Color(0, 123, 64));
              }
            });
  }

  private void initTablaSeguidores() {
    tblSeguidores.setModel(modeloSeguidores);
    modeloSeguidores.addColumn("Nombre");
  }

  private void initTablaSeguidos() {
    tblSeguidos.setModel(modeloSeguidos);
    modeloSeguidos.addColumn("Nombre");
  }

  private final TableRowSorter<DefaultTableModel> rowSorterVideos =
      new TableRowSorter<>(modeloVideos);

  private final TableRowSorter<DefaultTableModel> rowSorterListas =
      new TableRowSorter<>(modeloListas);

  private final TableRowSorter<DefaultTableModel> rowSorterUsuario =
      new TableRowSorter<>(modeloUsuarios);

  private void initTblUsuarios() {
    tblUsuarios.setModel(modeloUsuarios);
    tblUsuarios.setRowSorter(rowSorterUsuario);
    tblUsuarios
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblUsuarios.getSelectedRow() >= 0) {

                btnSiguiente.setEnabled(true);
                btnSiguiente.setBackground(new Color(0, 123, 64));
              }
            });
    registerFilter(txtFindNick, rowSorterUsuario);
  }

  private void registerFilter(
      JTextField txtField, TableRowSorter<DefaultTableModel> rowSorterVideo) {
    txtField
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                find();
              }

              private void find() {
                String text = txtField.getText();
                if (text.trim().length() == 0) {
                  rowSorterVideo.setRowFilter(null);
                } else {
                  rowSorterVideo.setRowFilter(RowFilter.regexFilter("(?i)" + Pattern.quote(text)));
                }
              }
            });
  }

  /** Creates new form ConsultaUsuario */
  public ConsultaUsuario() {
    initComponents();
    initTblUsuarios();
    initTablaVideo();
    initTablaLista();
    initTablaSeguidores();
    initTablaSeguidos();

    List<String> usuarios = iUsuario.listarUsuarios();
    modeloUsuarios.addColumn("Nickname");
    for (String usuario : usuarios) {
      modeloUsuarios.addRow(new Object[] {usuario});
    }
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    panelDatosUser.setVisible(false);
    panelInfo.setVisible(false);
    lblVolver.setVisible(false);

    btnVerLista.setEnabled(false);
    btnVerVideo.setEnabled(false);
    btnVerLista.setBackground(Color.GRAY);
    btnVerVideo.setBackground(Color.GRAY);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    panelUsuario = new javax.swing.JPanel();
    btnSiguiente = new javax.swing.JButton();
    jLabel15 = new javax.swing.JLabel();
    jSeparator13 = new javax.swing.JSeparator();
    txtFindNick = new javax.swing.JTextField();
    jLabel12 = new javax.swing.JLabel();
    jSeparator14 = new javax.swing.JSeparator();
    jScrollPane5 = new javax.swing.JScrollPane();
    tblUsuarios = new javax.swing.JTable();
    panelDatosUser = new javax.swing.JPanel();
    lblPic = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();
    jLabel5 = new javax.swing.JLabel();
    jLabel6 = new javax.swing.JLabel();
    jSeparator6 = new javax.swing.JSeparator();
    jSeparator7 = new javax.swing.JSeparator();
    jSeparator8 = new javax.swing.JSeparator();
    jSeparator9 = new javax.swing.JSeparator();
    jSeparator10 = new javax.swing.JSeparator();
    lblMail = new javax.swing.JLabel();
    lblNick = new javax.swing.JLabel();
    lblNombre = new javax.swing.JLabel();
    lblApellido = new javax.swing.JLabel();
    lblFechaNacimiento = new javax.swing.JLabel();
    jPanel1 = new javax.swing.JPanel();
    lblTitle4 = new javax.swing.JLabel();
    lblVolver = new javax.swing.JLabel();
    panelInfo = new javax.swing.JPanel();
    lblTitle = new javax.swing.JLabel();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblVideos = new javax.swing.JTable();
    jSeparator1 = new javax.swing.JSeparator();
    btnVerVideo = new javax.swing.JButton();
    txtFindVideo = new javax.swing.JTextField();
    jLabel10 = new javax.swing.JLabel();
    jSeparator4 = new javax.swing.JSeparator();
    jSeparator3 = new javax.swing.JSeparator();
    jScrollPane3 = new javax.swing.JScrollPane();
    tblSeguidos = new javax.swing.JTable();
    lblTitle2 = new javax.swing.JLabel();
    lblTitle3 = new javax.swing.JLabel();
    jScrollPane4 = new javax.swing.JScrollPane();
    tblSeguidores = new javax.swing.JTable();
    jSeparator11 = new javax.swing.JSeparator();
    jSeparator15 = new javax.swing.JSeparator();
    jSeparator12 = new javax.swing.JSeparator();
    lblTitle1 = new javax.swing.JLabel();
    jSeparator2 = new javax.swing.JSeparator();
    jScrollPane2 = new javax.swing.JScrollPane();
    tblListas = new javax.swing.JTable();
    btnVerLista = new javax.swing.JButton();
    txtFindLista = new javax.swing.JTextField();
    jLabel11 = new javax.swing.JLabel();
    jSeparator5 = new javax.swing.JSeparator();
    jPanel3 = new javax.swing.JPanel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel7 = new javax.swing.JLabel();

    setClosable(true);
    setIconifiable(true);
    setTitle("Consulta Usuario");
    getContentPane().setLayout(null);

    panelUsuario.setBackground(new java.awt.Color(255, 255, 255));
    panelUsuario.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelUsuario.setLayout(null);

    btnSiguiente.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguiente.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguiente.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguiente.setText("Siguiente");
    btnSiguiente.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguiente.setContentAreaFilled(false);
    btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguiente.setOpaque(true);
    btnSiguiente.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteMouseClicked(evt);
          }
        });
    panelUsuario.add(btnSiguiente);
    btnSiguiente.setBounds(380, 300, 110, 40);

    jLabel15.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel15.setForeground(new java.awt.Color(33, 33, 33));
    jLabel15.setText("Seleccionar Usuario");
    panelUsuario.add(jLabel15);
    jLabel15.setBounds(20, 30, 160, 17);
    panelUsuario.add(jSeparator13);
    jSeparator13.setBounds(20, 50, 480, 10);

    txtFindNick.setForeground(new java.awt.Color(60, 63, 65));
    txtFindNick.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar usuario:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelUsuario.add(txtFindNick);
    txtFindNick.setBounds(53, 65, 130, 40);

    jLabel12.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelUsuario.add(jLabel12);
    jLabel12.setBounds(30, 77, 24, 24);
    panelUsuario.add(jSeparator14);
    jSeparator14.setBounds(55, 102, 130, 10);

    tblUsuarios.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nickname"}));
    tblUsuarios.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane5.setViewportView(tblUsuarios);

    panelUsuario.add(jScrollPane5);
    jScrollPane5.setBounds(40, 110, 430, 170);

    getContentPane().add(panelUsuario);
    panelUsuario.setBounds(40, 150, 520, 370);

    panelDatosUser.setBackground(new java.awt.Color(255, 255, 255));
    panelDatosUser.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelDatosUser.setLayout(null);

    lblPic.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblPic.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/addImage.png"))); // NOI18N
    lblPic.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
    panelDatosUser.add(lblPic);
    lblPic.setBounds(17, 18, 87, 100);

    jLabel2.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel2.setText("Nickname:");
    panelDatosUser.add(jLabel2);
    jLabel2.setBounds(110, 18, 63, 15);

    jLabel3.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel3.setText("Nombre:");
    panelDatosUser.add(jLabel3);
    jLabel3.setBounds(110, 39, 49, 15);

    jLabel4.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel4.setText("Apellido:");
    panelDatosUser.add(jLabel4);
    jLabel4.setBounds(110, 59, 63, 15);

    jLabel5.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel5.setText("Fecha de Nacimiento:");
    jLabel5.setToolTipText("");
    panelDatosUser.add(jLabel5);
    jLabel5.setBounds(110, 80, 140, 15);

    jLabel6.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel6.setText("Email:");
    panelDatosUser.add(jLabel6);
    jLabel6.setBounds(110, 100, 42, 15);
    panelDatosUser.add(jSeparator6);
    jSeparator6.setBounds(110, 53, 270, 10);
    panelDatosUser.add(jSeparator7);
    jSeparator7.setBounds(110, 73, 270, 10);
    panelDatosUser.add(jSeparator8);
    jSeparator8.setBounds(110, 94, 270, 10);
    panelDatosUser.add(jSeparator9);
    jSeparator9.setBounds(110, 114, 270, 10);
    panelDatosUser.add(jSeparator10);
    jSeparator10.setBounds(111, 32, 270, 10);

    lblMail.setForeground(new java.awt.Color(60, 63, 65));
    lblMail.setText("Marianoz@vera.com.uy");
    panelDatosUser.add(lblMail);
    lblMail.setBounds(160, 92, 220, 30);

    lblNick.setForeground(new java.awt.Color(60, 63, 65));
    lblNick.setText("Forbi");
    panelDatosUser.add(lblNick);
    lblNick.setBounds(180, 15, 200, 20);

    lblNombre.setForeground(new java.awt.Color(60, 63, 65));
    lblNombre.setText("Mariano");
    panelDatosUser.add(lblNombre);
    lblNombre.setBounds(164, 31, 220, 30);

    lblApellido.setForeground(new java.awt.Color(60, 63, 65));
    lblApellido.setText("Zunino");
    panelDatosUser.add(lblApellido);
    lblApellido.setBounds(180, 51, 200, 30);

    lblFechaNacimiento.setForeground(new java.awt.Color(60, 63, 65));
    lblFechaNacimiento.setText("02/02/2018");
    panelDatosUser.add(lblFechaNacimiento);
    lblFechaNacimiento.setBounds(260, 72, 120, 30);

    getContentPane().add(panelDatosUser);
    panelDatosUser.setBounds(100, 20, 400, 130);

    jPanel1.setBackground(new java.awt.Color(53, 47, 91));
    jPanel1.setLayout(null);

    lblTitle4.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle4.setForeground(new java.awt.Color(255, 255, 255));
    lblTitle4.setText("Consulta Usuario");
    jPanel1.add(lblTitle4);
    lblTitle4.setBounds(185, 20, 250, 70);

    lblVolver.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/backArrow.png"))); // NOI18N
    lblVolver.setToolTipText("Volver");
    lblVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    lblVolver.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            lblVolverMouseClicked(evt);
          }
        });
    jPanel1.add(lblVolver);
    lblVolver.setBounds(4, 10, 40, 20);

    getContentPane().add(jPanel1);
    jPanel1.setBounds(0, 0, 600, 100);

    panelInfo.setBackground(new java.awt.Color(255, 255, 255));
    panelInfo.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelInfo.setLayout(null);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 20)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Videos");
    panelInfo.add(lblTitle);
    lblTitle.setBounds(92, 10, 80, 20);

    tblVideos.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nombre"}));
    tblVideos.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblVideos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(tblVideos);

    panelInfo.add(jScrollPane1);
    jScrollPane1.setBounds(10, 100, 150, 94);
    panelInfo.add(jSeparator1);
    jSeparator1.setBounds(10, 40, 240, 20);

    btnVerVideo.setBackground(new java.awt.Color(0, 123, 64));
    btnVerVideo.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVerVideo.setForeground(new java.awt.Color(255, 255, 255));
    btnVerVideo.setText("Ver");
    btnVerVideo.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVerVideo.setContentAreaFilled(false);
    btnVerVideo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVerVideo.setOpaque(true);
    btnVerVideo.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVerVideoMouseClicked(evt);
          }
        });
    panelInfo.add(btnVerVideo);
    btnVerVideo.setBounds(170, 100, 80, 30);

    txtFindVideo.setForeground(new java.awt.Color(60, 63, 65));
    txtFindVideo.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar video:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelInfo.add(txtFindVideo);
    txtFindVideo.setBounds(30, 54, 130, 40);

    jLabel10.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelInfo.add(jLabel10);
    jLabel10.setBounds(4, 68, 24, 24);
    panelInfo.add(jSeparator4);
    jSeparator4.setBounds(30, 91, 120, 10);

    jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
    panelInfo.add(jSeparator3);
    jSeparator3.setBounds(270, 0, 10, 370);

    tblSeguidos.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nombre"}));
    tblSeguidos.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblSeguidos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane3.setViewportView(tblSeguidos);

    panelInfo.add(jScrollPane3);
    jScrollPane3.setBounds(290, 260, 240, 92);

    lblTitle2.setFont(new java.awt.Font("Monospaced", 0, 20)); // NOI18N
    lblTitle2.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle2.setText("Seguidores");
    panelInfo.add(lblTitle2);
    lblTitle2.setBounds(70, 220, 130, 20);

    lblTitle3.setFont(new java.awt.Font("Monospaced", 0, 20)); // NOI18N
    lblTitle3.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle3.setText("Seguidos");
    panelInfo.add(lblTitle3);
    lblTitle3.setBounds(370, 220, 110, 20);

    tblSeguidores.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nombre"}));
    tblSeguidores.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblSeguidores.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane4.setViewportView(tblSeguidores);

    panelInfo.add(jScrollPane4);
    jScrollPane4.setBounds(10, 260, 240, 92);
    panelInfo.add(jSeparator11);
    jSeparator11.setBounds(290, 250, 240, 10);
    panelInfo.add(jSeparator15);
    jSeparator15.setBounds(10, 250, 240, 10);
    panelInfo.add(jSeparator12);
    jSeparator12.setBounds(0, 200, 550, 10);

    lblTitle1.setFont(new java.awt.Font("Monospaced", 0, 20)); // NOI18N
    lblTitle1.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle1.setText("Listas");
    panelInfo.add(lblTitle1);
    lblTitle1.setBounds(370, 10, 80, 20);
    panelInfo.add(jSeparator2);
    jSeparator2.setBounds(290, 40, 240, 20);

    tblListas.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nombre"}));
    tblListas.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblListas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane2.setViewportView(tblListas);

    panelInfo.add(jScrollPane2);
    jScrollPane2.setBounds(290, 100, 150, 94);

    btnVerLista.setBackground(new java.awt.Color(0, 123, 64));
    btnVerLista.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVerLista.setForeground(new java.awt.Color(255, 255, 255));
    btnVerLista.setText("Ver");
    btnVerLista.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVerLista.setContentAreaFilled(false);
    btnVerLista.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVerLista.setOpaque(true);
    btnVerLista.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVerListaMouseClicked(evt);
          }
        });
    panelInfo.add(btnVerLista);
    btnVerLista.setBounds(450, 100, 80, 30);

    txtFindLista.setForeground(new java.awt.Color(60, 63, 65));
    txtFindLista.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar lista:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelInfo.add(txtFindLista);
    txtFindLista.setBounds(310, 54, 130, 40);

    jLabel11.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelInfo.add(jLabel11);
    jLabel11.setBounds(285, 68, 24, 24);
    panelInfo.add(jSeparator5);
    jSeparator5.setBounds(310, 91, 130, 10);

    getContentPane().add(panelInfo);
    panelInfo.setBounds(20, 170, 550, 370);

    jPanel3.setBackground(new java.awt.Color(246, 246, 246));
    jPanel3.setLayout(null);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel3.add(jLabel9);
    jLabel9.setBounds(513, 464, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel3.add(jLabel8);
    jLabel8.setBounds(540, 476, 49, 15);

    jLabel7.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel3.add(jLabel7);
    jLabel7.setBounds(290, 290, 310, 230);

    getContentPane().add(jPanel3);
    jPanel3.setBounds(0, 80, 600, 510);

    setBounds(221, 93, 598, 610);
  } // </editor-fold>//GEN-END:initComponents

  private void btnVerListaMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVerListaMouseClicked
    if (btnVerLista.isEnabled()) {
      ConsultaLista consultaLista = new ConsultaLista(idLista);
      this.getDesktopPane().add(consultaLista);
      consultaLista.show();
      try {
        this.setIcon(true);
      } catch (PropertyVetoException ex) {
        Logger.getLogger(ConsultaUsuario.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  } // GEN-LAST:event_btnVerListaMouseClicked

  private void btnVerVideoMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVerVideoMouseClicked
    if (btnVerVideo.isEnabled()) {
      ConsultaVideo consultaVideo = new ConsultaVideo(idVideo);
      this.getDesktopPane().add(consultaVideo);
      consultaVideo.show();
      try {
        this.setIcon(true);
      } catch (PropertyVetoException ex) {
        Logger.getLogger(ConsultaUsuario.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  } // GEN-LAST:event_btnVerVideoMouseClicked

  private void btnSiguienteMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteMouseClicked
    if (btnSiguiente.isEnabled()) {
      JFrame frame = (JFrame) this.getDesktopPane().getTopLevelAncestor();
      try {
        String nickname = tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 0).toString();
        DtUsuarioExt data = iUsuario.consultaDeUsuario(nickname);
        ArrayList<DtLista> dtListas = iLista.listasDeUsuario(nickname);
        HashMap<Integer, String> dtVideos = iVideo.listarVideosUsuario(nickname);

        lblApellido.setText(data.getUsuario().getApellido());
        lblNombre.setText(data.getUsuario().getNombre());
        lblNick.setText(data.getUsuario().getNickname());
        lblMail.setText(data.getUsuario().getEmail());

        Date date = data.getUsuario().getFechaNacimiento();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = df.format(date);
        lblFechaNacimiento.setText(strDate);

        /*cargar imagen*/
        if (!data.getUsuario().getFoto().isEmpty()) {
          BufferedImage img =
              ImageIO.read(
                  new File(
                      System.getProperty("user.home")
                          + "/uytube/imagenes/"
                          + data.getUsuario().getFoto()));
          Image dimg =
              img.getScaledInstance(lblPic.getWidth(), lblPic.getHeight(), Image.SCALE_SMOOTH);
          ImageIcon imageIcon = new ImageIcon(dimg);
          lblPic.setIcon(imageIcon);
        }

        dtListas.forEach(
            (dt) -> {
              Object[] fila = new Object[2]; // creo el array de 1
              fila[1] = dt.getNombre(); // en la pos 1 cargo el nombre
              fila[0] = dt.getId(); // en la pos 0 cargo el id
              modeloListas.addRow(fila);
            });
        dtVideos.forEach(
            (k, v) -> {
              Object[] fila = new Object[2]; // creo el array de 1
              fila[1] = v; // en la pos 1 cargo el nombre
              fila[0] = k; // en la pos 0 cargo el id
              modeloVideos.addRow(fila);
            });

        ArrayList<DtSeguidor> seguidos = data.getSuscripciones();
        ArrayList<DtSeguidor> seguidores = data.getSuscriptores();

        for (DtSeguidor seguido : seguidos) {
          Object[] fila = new Object[1];
          fila[0] = seguido.getNickname();
          modeloSeguidos.addRow(fila);
        }

        for (DtSeguidor seguidor : seguidores) {
          Object[] fila = new Object[1];
          fila[0] = seguidor.getNickname();
          modeloSeguidores.addRow(fila);
        }

        panelUsuario.setVisible(false);
        panelDatosUser.setVisible(true);
        panelInfo.setVisible(true);
        lblVolver.setVisible(true);
      } catch (YonaException e) {
        new PopUp(frame, "Consulta Usuario", e.getMessage(), 5, PopUp.LEVEL.ERROR);
      } catch (IOException e) {
        new PopUp(
            frame,
            "Consulta Usuario",
            "Error al intentar cargar la foto de perfil.",
            5,
            PopUp.LEVEL.ERROR);
      }
    }
  } // GEN-LAST:event_btnSiguienteMouseClicked

  private void lblVolverMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_lblVolverMouseClicked
    panelUsuario.setVisible(true);
    panelDatosUser.setVisible(false);
    panelInfo.setVisible(false);
    lblVolver.setVisible(false);

    /*Limpiar form*/
    lblPic.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/addImage.png"))); // NOI18N
    lblApellido.setText("");
    lblNombre.setText("");
    lblNick.setText("");
    lblFechaNacimiento.setText("");
    lblMail.setText("");
    lblPic.setText("");
    modeloListas.setRowCount(0);
    modeloVideos.setRowCount(0);
    modeloSeguidores.setRowCount(0);
    modeloSeguidos.setRowCount(0);
    btnVerLista.setEnabled(false);
    btnVerVideo.setEnabled(false);
    btnVerLista.setBackground(Color.GRAY);
    btnVerVideo.setBackground(Color.GRAY);
  } // GEN-LAST:event_lblVolverMouseClicked

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnSiguiente;
  private javax.swing.JButton btnVerLista;
  private javax.swing.JButton btnVerVideo;
  private javax.swing.JLabel jLabel10;
  private javax.swing.JLabel jLabel11;
  private javax.swing.JLabel jLabel12;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JScrollPane jScrollPane3;
  private javax.swing.JScrollPane jScrollPane4;
  private javax.swing.JScrollPane jScrollPane5;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator10;
  private javax.swing.JSeparator jSeparator11;
  private javax.swing.JSeparator jSeparator12;
  private javax.swing.JSeparator jSeparator13;
  private javax.swing.JSeparator jSeparator14;
  private javax.swing.JSeparator jSeparator15;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JSeparator jSeparator3;
  private javax.swing.JSeparator jSeparator4;
  private javax.swing.JSeparator jSeparator5;
  private javax.swing.JSeparator jSeparator6;
  private javax.swing.JSeparator jSeparator7;
  private javax.swing.JSeparator jSeparator8;
  private javax.swing.JSeparator jSeparator9;
  private javax.swing.JLabel lblApellido;
  private javax.swing.JLabel lblFechaNacimiento;
  private javax.swing.JLabel lblMail;
  private javax.swing.JLabel lblNick;
  private javax.swing.JLabel lblNombre;
  private javax.swing.JLabel lblPic;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JLabel lblTitle1;
  private javax.swing.JLabel lblTitle2;
  private javax.swing.JLabel lblTitle3;
  private javax.swing.JLabel lblTitle4;
  private javax.swing.JLabel lblVolver;
  private javax.swing.JPanel panelDatosUser;
  private javax.swing.JPanel panelInfo;
  private javax.swing.JPanel panelUsuario;
  private javax.swing.JTable tblListas;
  private javax.swing.JTable tblSeguidores;
  private javax.swing.JTable tblSeguidos;
  private javax.swing.JTable tblUsuarios;
  private javax.swing.JTable tblVideos;
  private javax.swing.JTextField txtFindLista;
  private javax.swing.JTextField txtFindNick;
  private javax.swing.JTextField txtFindVideo;
  // End of variables declaration//GEN-END:variables
}
