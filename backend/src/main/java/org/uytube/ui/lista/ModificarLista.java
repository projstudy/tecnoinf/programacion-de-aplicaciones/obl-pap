/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.lista;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ICategoria;
import org.uytube.controladores.ILista;
import org.uytube.controladores.IUsuario;
import org.uytube.datatypes.DtLista;
import org.uytube.ui.PopUp;

/** @author toto */
public class ModificarLista extends javax.swing.JInternalFrame {
  /** Creates new form ModificarLista */
  private final DefaultTableModel modeloListas =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final DefaultTableModel modeloUsuarios =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final TableRowSorter<DefaultTableModel> rowSorterUsuario =
      new TableRowSorter<>(modeloUsuarios);

  private final ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);
  private final IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
  private final ICategoria iCategoria = Fabrica.getControllerOf(Fabrica.Controller.CATEOGRIA);
  private final TableRowSorter<DefaultTableModel> rowSorterLista =
      new TableRowSorter<>(modeloListas);
  private int idLista;

  public ModificarLista(int idLista) {
    this();
    this.idLista = idLista;
    DtLista dtLista = iLista.consultaLista(idLista);
    panelLista.setVisible(false);
    panelUsuario.setVisible(false);
    panelModificar.setVisible(true);

    rbtPrivada.setSelected(dtLista.isPrivado());
    rbtPublica.setSelected(!dtLista.isPrivado());

    String cat = dtLista.getCategoria();
    cmbCategorias.setSelectedItem(cat);

    spModificar.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
    spLista.setBorder(BorderFactory.createEmptyBorder());
    lblLista.setForeground(new Color(204, 204, 204));
    lblModificar.setForeground(new Color(32, 32, 32));
    spUsuario.setBorder(BorderFactory.createEmptyBorder());
    lblUsuario.setForeground(new Color(204, 204, 204));

    // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated
    // methods, choose Tools | Templates.
  }

  private void initTblUsuarios() {
    modeloUsuarios.addColumn("Nickname");
    tblUsuarios.setModel(modeloUsuarios);
    tblUsuarios.setRowSorter(rowSorterUsuario);
    tblUsuarios
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblUsuarios.getSelectedRow() >= 0) {
                String nickname =
                    tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 0).toString();
                ArrayList<DtLista> data = iLista.listasParticularesUsuario(nickname);
                modeloListas.setRowCount(0);
                for (DtLista lista : data) {
                  String privada = lista.isPrivado() ? "Privada" : "Publica";
                  modeloListas.addRow(
                      new Object[] {
                        lista.getNombre(), lista.getCategoria(), privada, lista.getId()
                      });
                }
                tblListas.setModel(modeloListas);
                TableColumnModel tcm = tblListas.getColumnModel();
                if (tcm.getColumnCount() == 4) {
                  tcm.removeColumn(tcm.getColumn(3));
                }
                btnSiguiente.setEnabled(true);
                btnSiguiente.setBackground(new Color(0, 123, 64));
              }
            });
    registerFilter(txtFindNick, rowSorterUsuario);
  }

  private void initTblListas() {
    modeloListas.addColumn("Nombre");
    modeloListas.addColumn("Categoria");
    modeloListas.addColumn("Privacidad");
    modeloListas.addColumn("id");
    tblListas.setModel(modeloListas);
    tblListas.setRowSorter(rowSorterLista);
    tblListas
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblListas.getSelectedRow() >= 0) {
                btnSiguienteLista.setEnabled(true);
                btnSiguienteLista.setBackground(new Color(0, 123, 64));
                if (tblListas.getSelectedRow() >= 0) {
                  btnSiguienteLista.setEnabled(true);
                  btnSiguienteLista.setBackground(new Color(0, 123, 64));
                } else {
                  btnSiguienteLista.setEnabled(false);
                  btnSiguienteLista.setBackground(Color.GRAY);
                }
              }
            });
    registerFilter(txtFindLista, rowSorterLista);
  }

  private void registerFilter(
      JTextField txtField, TableRowSorter<DefaultTableModel> rowSorterVideo) {
    txtField
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                find();
              }

              private void find() {
                String text = txtField.getText();
                if (text.trim().length() == 0) {
                  rowSorterVideo.setRowFilter(null);
                } else {
                  rowSorterVideo.setRowFilter(RowFilter.regexFilter("(?i)" + Pattern.quote(text)));
                }
              }
            });
  }

  public ModificarLista() {
    initComponents();
    initTblUsuarios();
    initTblListas();
    /*botones desactivados por defecto*/
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    btnSiguienteLista.setEnabled(false);
    btnSiguienteLista.setBackground(Color.GRAY);
    panelLista.setVisible(false);
    panelModificar.setVisible(false);

    /*load categorias*/
    List<String> categorias = iCategoria.listarCategorias();
    cmbCategorias.addItem("");
    for (String categoria : categorias) {
      cmbCategorias.addItem(categoria);
    }

    /*load usuarios*/
    List<String> usuarios = iUsuario.listarUsuarios();
    for (String usuario : usuarios) {
      modeloUsuarios.addRow(new Object[] {usuario});
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel3 = new javax.swing.JPanel();
    lblUsuario = new javax.swing.JLabel();
    lblModificar = new javax.swing.JLabel();
    spUsuario = new javax.swing.JSeparator();
    spLista = new javax.swing.JSeparator();
    panelUsuario = new javax.swing.JPanel();
    jSeparator2 = new javax.swing.JSeparator();
    txtFindNick = new javax.swing.JTextField();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblUsuarios = new javax.swing.JTable();
    jLabel14 = new javax.swing.JLabel();
    jSeparator3 = new javax.swing.JSeparator();
    jLabel1 = new javax.swing.JLabel();
    btnSiguiente = new javax.swing.JButton();
    panelLista = new javax.swing.JPanel();
    jLabel13 = new javax.swing.JLabel();
    jSeparator1 = new javax.swing.JSeparator();
    jScrollPane4 = new javax.swing.JScrollPane();
    tblListas = new javax.swing.JTable();
    jSeparator4 = new javax.swing.JSeparator();
    txtFindLista = new javax.swing.JTextField();
    jLabel2 = new javax.swing.JLabel();
    btnVolver = new javax.swing.JButton();
    btnSiguienteLista = new javax.swing.JButton();
    panelModificar = new javax.swing.JPanel();
    jLabel15 = new javax.swing.JLabel();
    jSeparator5 = new javax.swing.JSeparator();
    cmbCategorias = new javax.swing.JComboBox<>();
    rbtPublica = new javax.swing.JRadioButton();
    rbtPrivada = new javax.swing.JRadioButton();
    btnModificar = new javax.swing.JButton();
    btnVolver1 = new javax.swing.JButton();
    jLabel4 = new javax.swing.JLabel();
    lblLista = new javax.swing.JLabel();
    spModificar = new javax.swing.JSeparator();
    lblTitle = new javax.swing.JLabel();
    jPanel1 = new javax.swing.JPanel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();

    setBorder(null);
    setClosable(true);
    setIconifiable(true);
    setTitle("Modificar Lista");
    getContentPane().setLayout(null);

    jPanel3.setBackground(new java.awt.Color(255, 255, 255));
    jPanel3.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    jPanel3.setLayout(null);

    lblUsuario.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblUsuario.setForeground(new java.awt.Color(32, 32, 32));
    lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblUsuario.setText("Usuario");
    lblUsuario.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel3.add(lblUsuario);
    lblUsuario.setBounds(30, 20, 120, 40);

    lblModificar.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblModificar.setForeground(new java.awt.Color(204, 204, 204));
    lblModificar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblModificar.setText("Modificar");
    lblModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel3.add(lblModificar);
    lblModificar.setBounds(310, 20, 120, 40);

    spUsuario.setBorder(
        javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 89, 245), 2));
    jPanel3.add(spUsuario);
    spUsuario.setBounds(30, 60, 120, 3);
    jPanel3.add(spLista);
    spLista.setBounds(170, 60, 120, 3);

    panelUsuario.setBackground(new java.awt.Color(255, 255, 255));
    panelUsuario.setLayout(null);
    panelUsuario.add(jSeparator2);
    jSeparator2.setBounds(50, 100, 130, 10);

    txtFindNick.setForeground(new java.awt.Color(60, 63, 65));
    txtFindNick.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar usuario:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12),
            new java.awt.Color(0, 0, 0))); // NOI18N
    panelUsuario.add(txtFindNick);
    txtFindNick.setBounds(50, 64, 130, 40);

    tblUsuarios.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {
              {null, null, null},
              {null, null, null},
              {null, null, null},
              {null, null, null}
            },
            new String[] {"Nickname", "Nombre", "Apellido"}));
    tblUsuarios.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(tblUsuarios);

    panelUsuario.add(jScrollPane1);
    jScrollPane1.setBounds(30, 110, 380, 120);

    jLabel14.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel14.setForeground(new java.awt.Color(33, 33, 33));
    jLabel14.setText("Seleccionar Usuario");
    panelUsuario.add(jLabel14);
    jLabel14.setBounds(20, 20, 160, 17);
    panelUsuario.add(jSeparator3);
    jSeparator3.setBounds(20, 40, 400, 10);

    jLabel1.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelUsuario.add(jLabel1);
    jLabel1.setBounds(26, 78, 24, 24);

    btnSiguiente.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguiente.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguiente.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguiente.setText("Siguiente");
    btnSiguiente.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguiente.setContentAreaFilled(false);
    btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguiente.setOpaque(true);
    btnSiguiente.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteMouseClicked(evt);
          }
        });
    panelUsuario.add(btnSiguiente);
    btnSiguiente.setBounds(300, 247, 110, 40);

    jPanel3.add(panelUsuario);
    panelUsuario.setBounds(10, 80, 440, 300);

    panelLista.setBackground(new java.awt.Color(255, 255, 255));
    panelLista.setLayout(null);

    jLabel13.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel13.setForeground(new java.awt.Color(33, 33, 33));
    jLabel13.setText("Seleccionar Lista");
    panelLista.add(jLabel13);
    jLabel13.setBounds(20, 20, 150, 17);
    panelLista.add(jSeparator1);
    jSeparator1.setBounds(20, 40, 400, 10);

    tblListas.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {
              {null, null, null},
              {null, null, null},
              {null, null, null},
              {null, null, null}
            },
            new String[] {"Nombre", "Categoria", "Privacidad"}));
    tblListas.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblListas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblListas.getTableHeader().setResizingAllowed(false);
    tblListas.getTableHeader().setReorderingAllowed(false);
    jScrollPane4.setViewportView(tblListas);

    panelLista.add(jScrollPane4);
    jScrollPane4.setBounds(30, 110, 380, 120);
    panelLista.add(jSeparator4);
    jSeparator4.setBounds(50, 100, 130, 10);

    txtFindLista.setForeground(new java.awt.Color(60, 63, 65));
    txtFindLista.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar Lista:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12),
            new java.awt.Color(0, 0, 0))); // NOI18N
    panelLista.add(txtFindLista);
    txtFindLista.setBounds(50, 64, 130, 40);

    jLabel2.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelLista.add(jLabel2);
    jLabel2.setBounds(26, 78, 24, 24);

    btnVolver.setBackground(new java.awt.Color(51, 89, 245));
    btnVolver.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVolver.setForeground(new java.awt.Color(255, 255, 255));
    btnVolver.setText("Volver");
    btnVolver.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVolver.setContentAreaFilled(false);
    btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVolver.setOpaque(true);
    btnVolver.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVolverMouseClicked(evt);
          }
        });
    panelLista.add(btnVolver);
    btnVolver.setBounds(20, 247, 110, 40);

    btnSiguienteLista.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguienteLista.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguienteLista.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguienteLista.setText("Siguiente");
    btnSiguienteLista.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguienteLista.setContentAreaFilled(false);
    btnSiguienteLista.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguienteLista.setOpaque(true);
    btnSiguienteLista.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteListaMouseClicked(evt);
          }
        });
    panelLista.add(btnSiguienteLista);
    btnSiguienteLista.setBounds(300, 247, 110, 40);

    jPanel3.add(panelLista);
    panelLista.setBounds(10, 80, 440, 300);

    panelModificar.setBackground(new java.awt.Color(255, 255, 255));
    panelModificar.setLayout(null);

    jLabel15.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel15.setForeground(new java.awt.Color(33, 33, 33));
    jLabel15.setText("Datos de Lista");
    panelModificar.add(jLabel15);
    jLabel15.setBounds(20, 17, 120, 20);
    panelModificar.add(jSeparator5);
    jSeparator5.setBounds(20, 40, 400, 10);

    cmbCategorias.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    panelModificar.add(cmbCategorias);
    cmbCategorias.setBounds(40, 110, 130, 30);

    rbtPublica.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    rbtPublica.setSelected(true);
    rbtPublica.setText("Publica");
    rbtPublica.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    rbtPublica.addItemListener(this::rbtPublicaItemStateChanged);
    panelModificar.add(rbtPublica);
    rbtPublica.setBounds(200, 110, 80, 30);

    rbtPrivada.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    rbtPrivada.setText("Privada");
    rbtPrivada.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    rbtPrivada.addItemListener(this::rbtPrivadaItemStateChanged);
    panelModificar.add(rbtPrivada);
    rbtPrivada.setBounds(280, 110, 90, 30);

    btnModificar.setBackground(new java.awt.Color(255, 132, 45));
    btnModificar.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnModificar.setForeground(new java.awt.Color(255, 255, 255));
    btnModificar.setText("Modificar");
    btnModificar.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnModificar.setContentAreaFilled(false);
    btnModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnModificar.setOpaque(true);
    btnModificar.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnModificarMouseClicked(evt);
          }
        });
    panelModificar.add(btnModificar);
    btnModificar.setBounds(300, 247, 110, 40);

    btnVolver1.setBackground(new java.awt.Color(51, 89, 245));
    btnVolver1.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVolver1.setForeground(new java.awt.Color(255, 255, 255));
    btnVolver1.setText("Volver");
    btnVolver1.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVolver1.setContentAreaFilled(false);
    btnVolver1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVolver1.setOpaque(true);
    btnVolver1.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVolver1MouseClicked(evt);
          }
        });
    panelModificar.add(btnVolver1);
    btnVolver1.setBounds(20, 247, 110, 40);

    jLabel4.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel4.setForeground(new java.awt.Color(0, 0, 0));
    jLabel4.setText("Categoria");
    panelModificar.add(jLabel4);
    jLabel4.setBounds(40, 90, 70, 15);

    jPanel3.add(panelModificar);
    panelModificar.setBounds(10, 80, 440, 300);

    lblLista.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblLista.setForeground(new java.awt.Color(204, 204, 204));
    lblLista.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblLista.setText("Lista");
    lblLista.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel3.add(lblLista);
    lblLista.setBounds(170, 20, 120, 40);
    jPanel3.add(spModificar);
    spModificar.setBounds(307, 60, 122, 3);

    getContentPane().add(jPanel3);
    jPanel3.setBounds(30, 70, 460, 382);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Modificar Lista");
    getContentPane().add(lblTitle);
    lblTitle.setBounds(150, 20, 210, 29);

    jPanel1.setBackground(new java.awt.Color(246, 246, 246));
    jPanel1.setLayout(null);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel1.add(jLabel9);
    jLabel9.setBounds(437, 458, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel1.add(jLabel8);
    jLabel8.setBounds(466, 468, 50, 20);

    jLabel3.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel1.add(jLabel3);
    jLabel3.setBounds(230, 270, 310, 240);

    getContentPane().add(jPanel1);
    jPanel1.setBounds(0, 0, 540, 510);

    setBounds(244, 123, 536, 522);
  } // </editor-fold>//GEN-END:initComponents

  private void btnVolver1MouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVolver1MouseClicked
    panelModificar.setVisible(false);
    panelLista.setVisible(true);
    spLista.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
    spModificar.setBorder(BorderFactory.createEmptyBorder());
    lblModificar.setForeground(new Color(204, 204, 204));
    lblLista.setForeground(new Color(32, 32, 32));

    /*Vuelvo a las listas,  deselecciono la lista y bloqueo todos los botones*/
    btnSiguienteLista.setEnabled(false);
    btnSiguienteLista.setBackground(Color.GRAY);
    tblListas.getSelectionModel().removeIndexInterval(0, tblListas.getRowCount());
  } // GEN-LAST:event_btnVolver1MouseClicked

  private void rbtPublicaItemStateChanged(
      java.awt.event.ItemEvent evt) { // GEN-FIRST:event_rbtPublicaItemStateChanged
    if (evt.getStateChange() == ItemEvent.SELECTED) {
      rbtPrivada.setSelected(false);
    } else if (evt.getStateChange() == ItemEvent.DESELECTED) {
      rbtPrivada.setSelected(true);
    }
  } // GEN-LAST:event_rbtPublicaItemStateChanged

  private void rbtPrivadaItemStateChanged(
      java.awt.event.ItemEvent evt) { // GEN-FIRST:event_rbtPrivadaItemStateChanged
    if (evt.getStateChange() == ItemEvent.SELECTED) {
      rbtPublica.setSelected(false);
    } else if (evt.getStateChange() == ItemEvent.DESELECTED) {
      rbtPublica.setSelected(true);
    }
  } // GEN-LAST:event_rbtPrivadaItemStateChanged

  private void btnSiguienteListaMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteListaMouseClicked
    if (btnSiguienteLista.isEnabled()) {
      panelModificar.setVisible(true);
      panelLista.setVisible(false);
      spModificar.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
      spLista.setBorder(BorderFactory.createEmptyBorder());
      lblLista.setForeground(new Color(204, 204, 204));
      lblModificar.setForeground(new Color(32, 32, 32));
      String categoria = (String) modeloListas.getValueAt(tblListas.getSelectedRow(), 1);
      if (categoria == null) {
        categoria = "";
      }
      cmbCategorias.setSelectedItem(categoria);
      String privacidad = (String) modeloListas.getValueAt(tblListas.getSelectedRow(), 2);
      rbtPrivada.setSelected(privacidad.equalsIgnoreCase("privada"));
      rbtPublica.setSelected(!privacidad.equalsIgnoreCase("privada"));
      this.idLista = (int) modeloListas.getValueAt(tblListas.getSelectedRow(), 3);
    }
  } // GEN-LAST:event_btnSiguienteListaMouseClicked

  private void btnSiguienteMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteMouseClicked
    if (btnSiguiente.isEnabled()) {
      panelUsuario.setVisible(false);
      panelLista.setVisible(true);
      spLista.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
      spUsuario.setBorder(BorderFactory.createEmptyBorder());
      lblUsuario.setForeground(new Color(204, 204, 204));
      lblLista.setForeground(new Color(32, 32, 32));
    }
  } // GEN-LAST:event_btnSiguienteMouseClicked

  private void btnVolverMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVolverMouseClicked
    panelUsuario.setVisible(true);
    panelLista.setVisible(false);
    spUsuario.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
    spLista.setBorder(BorderFactory.createEmptyBorder());
    lblLista.setForeground(new Color(204, 204, 204));
    lblUsuario.setForeground(new Color(32, 32, 32));

    /*Vuelvo a los usuarios,  deselecciono el video y la lista y bloqueo todos los botones*/
    btnSiguienteLista.setEnabled(false);
    btnSiguienteLista.setBackground(Color.GRAY);
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    tblUsuarios.getSelectionModel().removeIndexInterval(0, tblUsuarios.getRowCount());
    tblListas.getSelectionModel().removeIndexInterval(0, tblListas.getRowCount());
  } // GEN-LAST:event_btnVolverMouseClicked

  private void btnModificarMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnResponderMouseClicked
    if (cmbCategorias.getSelectedIndex() >= 0 && btnModificar.isEnabled()) {
      JFrame frame = (JFrame) getDesktopPane().getTopLevelAncestor();
      try {
        String cat = null;
        if (cmbCategorias.getSelectedIndex() > 0) {
          cat = cmbCategorias.getSelectedItem().toString();
        }
        iLista.modificarLista(rbtPrivada.isSelected(), cat, idLista);
        new PopUp(frame, "Modificar Lista", "Se modifico la lista", 5, PopUp.LEVEL.SUCCESS);
        dispose();
      } catch (IllegalArgumentException e) {
        new PopUp(frame, "Modificar Lista", e.getMessage(), 5, PopUp.LEVEL.ERROR);
      }
    }
  } // GEN-LAST:event_btnResponderMouseClicked

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnModificar;
  private javax.swing.JButton btnSiguiente;
  private javax.swing.JButton btnSiguienteLista;
  private javax.swing.JButton btnVolver;
  private javax.swing.JButton btnVolver1;
  private javax.swing.JComboBox<String> cmbCategorias;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel13;
  private javax.swing.JLabel jLabel14;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane4;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JSeparator jSeparator3;
  private javax.swing.JSeparator jSeparator4;
  private javax.swing.JSeparator jSeparator5;
  private javax.swing.JLabel lblLista;
  private javax.swing.JLabel lblModificar;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JLabel lblUsuario;
  private javax.swing.JPanel panelLista;
  private javax.swing.JPanel panelModificar;
  private javax.swing.JPanel panelUsuario;
  private javax.swing.JRadioButton rbtPrivada;
  private javax.swing.JRadioButton rbtPublica;
  private javax.swing.JSeparator spLista;
  private javax.swing.JSeparator spModificar;
  private javax.swing.JSeparator spUsuario;
  private javax.swing.JTable tblListas;
  private javax.swing.JTable tblUsuarios;
  private javax.swing.JTextField txtFindLista;
  private javax.swing.JTextField txtFindNick;
  // End of variables declaration//GEN-END:variables
}
