/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.lista;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ILista;
import org.uytube.controladores.IUsuario;
import org.uytube.controladores.IVideo;
import org.uytube.datatypes.DtLista;
import org.uytube.exception.YonaException;
import org.uytube.ui.PopUp;

/** @author toto */
public class AgregarVideoLista extends javax.swing.JInternalFrame {

  private int idVideo;
  private int idLista;

  private final ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);
  private final IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
  private final IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);

  private final DefaultTableModel modeloVideos =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };
  private final DefaultTableModel modeloListas =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };
  private final DefaultTableModel modeloUsuarios =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final TableRowSorter<DefaultTableModel> rowSorterLista =
      new TableRowSorter<>(modeloListas);
  private final TableRowSorter<DefaultTableModel> rowSorterVideo =
      new TableRowSorter<>(modeloVideos);
  private final TableRowSorter<DefaultTableModel> rowSorterUsuarioOrigen =
      new TableRowSorter<>(modeloUsuarios);
  private TableRowSorter<DefaultTableModel> rowSorterUsuarioDestino =
      new TableRowSorter<>(modeloUsuarios);

  private void registerFilter(
      JTextField txtField, TableRowSorter<DefaultTableModel> rowSorterVideo) {
    txtField
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                find();
              }

              private void find() {
                String text = txtField.getText();
                if (text.trim().length() == 0) {
                  rowSorterVideo.setRowFilter(null);
                } else {
                  rowSorterVideo.setRowFilter(RowFilter.regexFilter("(?i)" + Pattern.quote(text)));
                }
              }
            });
  }

  private void initTblUsuarios2() {
    tblUsuarios2.setModel(modeloUsuarios);
    tblUsuarios2.setRowSorter(rowSorterUsuarioOrigen);
    tblUsuarios2
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblUsuarios2.getSelectedRow() >= 0) {
                String nickname =
                    tblUsuarios2.getValueAt(tblUsuarios2.getSelectedRow(), 0).toString();
                ArrayList<DtLista> data = iLista.listasDeUsuario(nickname);
                modeloListas.setRowCount(0);
                for (DtLista lista : data) {
                  String privada = lista.isPrivado() ? "Privada" : "Publica";
                  modeloListas.addRow(
                      new Object[] {
                        lista.getNombre(), lista.getCategoria(), privada, lista.getId()
                      });
                }
                btnAgregar.setEnabled(false);
                btnAgregar.setBackground(Color.GRAY);
              }
            });
    registerFilter(txtFindNick1, rowSorterUsuarioOrigen);
  }

  private void initTblUsuarios() {
    modeloUsuarios.addColumn("Nickname");
    tblUsuarios.setModel(modeloUsuarios);
    tblUsuarios.setRowSorter(rowSorterUsuarioOrigen);
    tblUsuarios
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblUsuarios.getSelectedRow() >= 0) {
                String nickname =
                    tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 0).toString();
                HashMap<Integer, String> data = iVideo.listarVideosUsuario(nickname);
                modeloVideos.setRowCount(0);
                data.forEach((k, v) -> modeloVideos.addRow(new Object[] {v, k}));
                tblVideo.setModel(modeloVideos);
                // quitar ultima columna
              }
            });
    registerFilter(txtFindNick, rowSorterUsuarioOrigen);
  }

  private void initTblVideos() {
    modeloVideos.addColumn("Nombre");
    modeloVideos.addColumn("id");
    tblVideo.setModel(modeloVideos);
    TableColumnModel tcm = tblVideo.getColumnModel();
    tcm.removeColumn(tcm.getColumn(1));
    tblVideo.setRowSorter(rowSorterVideo);
    tblVideo
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblVideo.getSelectedRow() >= 0) {
                btnSiguiente.setEnabled(true);
                btnSiguiente.setBackground(new Color(0, 123, 64));
              }
            });

    //    registerFilter(txtFindVideo, rowSorterVideo);
  }

  private void initTblListas() {
    modeloListas.addColumn("Nombre");
    modeloListas.addColumn("Categoria");
    modeloListas.addColumn("Privacidad");
    modeloListas.addColumn("id");
    tblListas.setModel(modeloListas);
    TableColumnModel tcm = tblListas.getColumnModel();
    tcm.removeColumn(tcm.getColumn(3));
    tblListas.setRowSorter(rowSorterLista);
    tblListas
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              if (!event.getValueIsAdjusting() && tblListas.getSelectedRow() >= 0) {
                btnAgregar.setEnabled(true);
                btnAgregar.setBackground(new Color(0, 123, 64));
              }
            });
    //    registerFilter(txtFindLista, rowSorterLista);
  }

  /** Creates new form AgregarVideoLista */
  public AgregarVideoLista() {
    initComponents();
    initTblUsuarios();
    initTblUsuarios2();
    initTblVideos();
    initTblListas();
    /*desactivar los botones*/
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    btnVolver.setVisible(false);
    btnVolver.setEnabled(true);
    btnAgregar.setEnabled(false);
    btnAgregar.setBackground(Color.GRAY);
    /*ocultar el siguiente panel*/
    panelParte2.setVisible(false);

    /*Cargar Datos a las tablas usuarios*/
    List<String> usuarios = iUsuario.listarUsuarios();
    for (String usuario : usuarios) {
      modeloUsuarios.addRow(new Object[] {usuario});
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    lblTitle = new javax.swing.JLabel();
    panelParte1 = new javax.swing.JPanel();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblUsuarios = new javax.swing.JTable();
    jScrollPane2 = new javax.swing.JScrollPane();
    tblVideo = new javax.swing.JTable();
    jLabel14 = new javax.swing.JLabel();
    jSeparator3 = new javax.swing.JSeparator();
    jLabel15 = new javax.swing.JLabel();
    jSeparator4 = new javax.swing.JSeparator();
    jSeparator2 = new javax.swing.JSeparator();
    txtFindNick = new javax.swing.JTextField();
    jLabel1 = new javax.swing.JLabel();
    btnSiguiente = new javax.swing.JButton();
    panelParte2 = new javax.swing.JPanel();
    jScrollPane3 = new javax.swing.JScrollPane();
    tblUsuarios2 = new javax.swing.JTable();
    jScrollPane4 = new javax.swing.JScrollPane();
    tblListas = new javax.swing.JTable();
    jLabel16 = new javax.swing.JLabel();
    jSeparator5 = new javax.swing.JSeparator();
    jLabel17 = new javax.swing.JLabel();
    jSeparator6 = new javax.swing.JSeparator();
    jSeparator7 = new javax.swing.JSeparator();
    txtFindNick1 = new javax.swing.JTextField();
    jLabel2 = new javax.swing.JLabel();
    btnAgregar = new javax.swing.JButton();
    btnVolver = new javax.swing.JButton();
    jPanel1 = new javax.swing.JPanel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();

    setClosable(true);
    setIconifiable(true);
    setTitle("Agregar Video A Lista");
    setMinimumSize(new java.awt.Dimension(501, 585));
    setName(""); // NOI18N
    setPreferredSize(new java.awt.Dimension(501, 585));
    getContentPane().setLayout(null);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Agregar Video");
    getContentPane().add(lblTitle);
    lblTitle.setBounds(160, 20, 182, 33);

    panelParte1.setBackground(new java.awt.Color(255, 255, 255));
    panelParte1.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelParte1.setLayout(null);

    tblUsuarios.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nickname"}));
    tblUsuarios.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(tblUsuarios);

    panelParte1.add(jScrollPane1);
    jScrollPane1.setBounds(50, 120, 340, 110);

    tblVideo.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nombre"}));
    tblVideo.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblVideo.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane2.setViewportView(tblVideo);

    panelParte1.add(jScrollPane2);
    jScrollPane2.setBounds(50, 290, 340, 100);

    jLabel14.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel14.setForeground(new java.awt.Color(33, 33, 33));
    jLabel14.setText("Seleccionar Video");
    panelParte1.add(jLabel14);
    jLabel14.setBounds(30, 250, 150, 20);
    panelParte1.add(jSeparator3);
    jSeparator3.setBounds(30, 270, 380, 10);

    jLabel15.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel15.setForeground(new java.awt.Color(33, 33, 33));
    jLabel15.setText("Seleccionar Usuario");
    panelParte1.add(jLabel15);
    jLabel15.setBounds(30, 40, 160, 20);
    panelParte1.add(jSeparator4);
    jSeparator4.setBounds(30, 60, 380, 10);
    panelParte1.add(jSeparator2);
    jSeparator2.setBounds(70, 110, 130, 10);

    txtFindNick.setForeground(new java.awt.Color(60, 63, 65));
    txtFindNick.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar usuario:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelParte1.add(txtFindNick);
    txtFindNick.setBounds(70, 75, 130, 40);

    jLabel1.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelParte1.add(jLabel1);
    jLabel1.setBounds(46, 88, 24, 24);

    btnSiguiente.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguiente.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguiente.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguiente.setText("Siguiente");
    btnSiguiente.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguiente.setContentAreaFilled(false);
    btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguiente.setEnabled(false);
    btnSiguiente.setOpaque(true);
    btnSiguiente.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteMouseClicked(evt);
          }
        });
    panelParte1.add(btnSiguiente);
    btnSiguiente.setBounds(280, 410, 120, 40);

    getContentPane().add(panelParte1);
    panelParte1.setBounds(30, 60, 430, 470);

    panelParte2.setBackground(new java.awt.Color(255, 255, 255));
    panelParte2.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelParte2.setLayout(null);

    tblUsuarios2.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nickname"}));
    tblUsuarios2.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblUsuarios2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane3.setViewportView(tblUsuarios2);

    panelParte2.add(jScrollPane3);
    jScrollPane3.setBounds(50, 120, 340, 110);

    tblListas.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nombre"}));
    tblListas.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblListas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane4.setViewportView(tblListas);

    panelParte2.add(jScrollPane4);
    jScrollPane4.setBounds(50, 290, 220, 100);

    jLabel16.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel16.setForeground(new java.awt.Color(33, 33, 33));
    jLabel16.setText("Seleccionar Lista");
    panelParte2.add(jLabel16);
    jLabel16.setBounds(30, 250, 150, 20);
    panelParte2.add(jSeparator5);
    jSeparator5.setBounds(30, 270, 380, 10);

    jLabel17.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel17.setForeground(new java.awt.Color(33, 33, 33));
    jLabel17.setText("Seleccionar Usuario");
    panelParte2.add(jLabel17);
    jLabel17.setBounds(30, 40, 160, 20);
    panelParte2.add(jSeparator6);
    jSeparator6.setBounds(30, 60, 380, 10);
    panelParte2.add(jSeparator7);
    jSeparator7.setBounds(70, 110, 130, 10);

    txtFindNick1.setForeground(new java.awt.Color(60, 63, 65));
    txtFindNick1.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar usuario:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelParte2.add(txtFindNick1);
    txtFindNick1.setBounds(70, 75, 130, 40);

    jLabel2.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelParte2.add(jLabel2);
    jLabel2.setBounds(46, 88, 24, 24);

    btnAgregar.setBackground(new java.awt.Color(0, 123, 64));
    btnAgregar.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnAgregar.setForeground(new java.awt.Color(255, 255, 255));
    btnAgregar.setText("Agregar");
    btnAgregar.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnAgregar.setContentAreaFilled(false);
    btnAgregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnAgregar.setEnabled(false);
    btnAgregar.setOpaque(true);
    btnAgregar.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnAgregarMouseClicked(evt);
          }
        });
    panelParte2.add(btnAgregar);
    btnAgregar.setBounds(290, 300, 110, 40);

    btnVolver.setBackground(new java.awt.Color(51, 89, 245));
    btnVolver.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVolver.setForeground(new java.awt.Color(255, 255, 255));
    btnVolver.setText("Volver");
    btnVolver.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVolver.setContentAreaFilled(false);
    btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVolver.setEnabled(false);
    btnVolver.setOpaque(true);
    btnVolver.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVolverMouseClicked(evt);
          }
        });
    panelParte2.add(btnVolver);
    btnVolver.setBounds(25, 410, 110, 40);

    getContentPane().add(panelParte2);
    panelParte2.setBounds(30, 60, 430, 470);

    jPanel1.setBackground(new java.awt.Color(246, 246, 246));
    jPanel1.setLayout(null);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel1.add(jLabel9);
    jLabel9.setBounds(405, 533, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel1.add(jLabel8);
    jLabel8.setBounds(435, 543, 50, 20);

    jLabel3.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel1.add(jLabel3);
    jLabel3.setBounds(190, 340, 310, 240);

    getContentPane().add(jPanel1);
    jPanel1.setBounds(0, 0, 500, 580);

    setBounds(261, 95, 501, 599);
  } // </editor-fold>//GEN-END:initComponents

  private void btnVolverMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVolverMouseClicked
    panelParte2.setVisible(false);
    panelParte1.setVisible(true);
    btnSiguiente.setVisible(true);
    btnVolver.setVisible(false);

    /*vuelvo a la seleccion de video*/
    btnAgregar.setEnabled(false);
    btnAgregar.setBackground(Color.GRAY);
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);

    tblListas.getSelectionModel().removeIndexInterval(0, tblListas.getRowCount());
    tblUsuarios.getSelectionModel().removeIndexInterval(0, tblUsuarios.getRowCount());
    tblVideo.getSelectionModel().removeIndexInterval(0, tblVideo.getRowCount());
    tblUsuarios2.getSelectionModel().removeIndexInterval(0, tblUsuarios2.getRowCount());
    /*limpiar las tablas*/
    modeloVideos.setRowCount(0);
    modeloListas.setRowCount(0);
  } // GEN-LAST:event_btnVolverMouseClicked

  private void btnSiguienteMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteMouseClicked
    if (btnSiguiente.isEnabled()) {
      panelParte1.setVisible(false);
      panelParte2.setVisible(true);
      btnSiguiente.setVisible(false);
      btnVolver.setVisible(true);
    }
  } // GEN-LAST:event_btnSiguienteMouseClicked

  private void btnAgregarMouseClicked(java.awt.event.MouseEvent evt) {
    if (btnAgregar.isEnabled()) {
      panelParte1.setVisible(false);
      panelParte2.setVisible(true);
      btnSiguiente.setVisible(false);
      btnVolver.setVisible(true);
      JFrame frame = (JFrame) getDesktopPane().getTopLevelAncestor();
      try {
        int idLista = (int) modeloListas.getValueAt(tblListas.getSelectedRow(), 3);
        int idVideo = (int) modeloVideos.getValueAt(tblVideo.getSelectedRow(), 1);
        iLista.agregarVideoLista(idVideo, idLista);
        new PopUp(frame, "Agregar Video", "Se agrego el video a la lista.", 5, PopUp.LEVEL.SUCCESS);
      } catch (YonaException e) {
        new PopUp(frame, "Agregar Video", e.getMessage(), 5, PopUp.LEVEL.ERROR);
      }
    }
  } // GEN-LAST:event_btnAgregarMouseClicked

  private void btnCancelarMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnCancelarMouseClicked
    dispose();
  } // GEN-LAST:event_btnCancelarMouseClicked

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnAgregar;
  private javax.swing.JButton btnSiguiente;
  private javax.swing.JButton btnVolver;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel14;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel16;
  private javax.swing.JLabel jLabel17;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JScrollPane jScrollPane3;
  private javax.swing.JScrollPane jScrollPane4;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JSeparator jSeparator3;
  private javax.swing.JSeparator jSeparator4;
  private javax.swing.JSeparator jSeparator5;
  private javax.swing.JSeparator jSeparator6;
  private javax.swing.JSeparator jSeparator7;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JPanel panelParte1;
  private javax.swing.JPanel panelParte2;
  private javax.swing.JTable tblListas;
  private javax.swing.JTable tblUsuarios;
  private javax.swing.JTable tblUsuarios2;
  private javax.swing.JTable tblVideo;
  private javax.swing.JTextField txtFindNick;
  private javax.swing.JTextField txtFindNick1;
  // End of variables declaration//GEN-END:variables
}
