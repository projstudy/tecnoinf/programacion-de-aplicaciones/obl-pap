/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.lista;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ILista;
import org.uytube.controladores.IUsuario;
import org.uytube.controladores.IVideo;
import org.uytube.datatypes.DtLista;
import org.uytube.exception.YonaException;
import org.uytube.ui.PopUp;

/** @author toto */
public class QuitarVideoLista extends javax.swing.JInternalFrame {

  /** Creates new form ModificarLista */
  private final ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);

  private IVideo iVideo = Fabrica.getControllerOf(Fabrica.Controller.VIDEO);
  private final IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
  private final DefaultTableModel modeloListas =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final DefaultTableModel modeloUsuarios =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final DefaultTableModel modeloVideos =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final TableRowSorter<DefaultTableModel> rowSorterUsuario =
      new TableRowSorter<>(modeloUsuarios);

  private final TableRowSorter<DefaultTableModel> rowSorterLista =
      new TableRowSorter<>(modeloListas);

  private final TableRowSorter<DefaultTableModel> rowSorterVideo =
      new TableRowSorter<>(modeloVideos);

  /** Creates new form ConsultaLista */
  private void initTblUsuarios() {
    tblUsuarios.setModel(modeloUsuarios);
    tblUsuarios.setRowSorter(rowSorterUsuario);
    tblUsuarios
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblUsuarios.getSelectedRow() >= 0) {
                String nickname =
                    tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 0).toString();
                ArrayList<DtLista> data = iLista.listasDeUsuario(nickname);
                modeloListas.setRowCount(0);
                for (DtLista lista : data) {
                  String privada = lista.isPrivado() ? "Privada" : "Publica";
                  modeloListas.addRow(
                      new Object[] {
                        lista.getNombre(), lista.getCategoria(), privada, lista.getId()
                      });
                }
                tblListas.setModel(modeloListas);
                TableColumnModel tcm = tblListas.getColumnModel();
                if (tcm.getColumnCount() == 4) {
                  tcm.removeColumn(tcm.getColumn(3));
                }
                btnSiguiente.setEnabled(true);
                btnSiguiente.setBackground(new Color(0, 123, 64));
              }
            });

    registerFilter(txtFindNick, rowSorterUsuario);
  }

  private void initTblListas() {
    modeloListas.addColumn("Nombre");
    modeloListas.addColumn("Categoria");
    modeloListas.addColumn("Privacidad");
    modeloListas.addColumn("id");
    tblListas.setModel(modeloListas);
    tblListas.setRowSorter(rowSorterLista);
    tblListas
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblListas.getSelectedRow() >= 0) {
                int idLista = (int) modeloListas.getValueAt(tblListas.getSelectedRow(), 3);
                HashMap<Integer, String> data = iLista.listarVideos(idLista);
                modeloVideos.setRowCount(0);
                data.forEach((k, v) -> modeloVideos.addRow(new Object[] {v, k}));
                tblVideos.setModel(modeloVideos);
                // quitar ultima columna
                TableColumnModel tcm = tblVideos.getColumnModel();
                if (tcm.getColumnCount() == 2) {
                  tcm.removeColumn(tcm.getColumn(1));
                }
                btnSiguienteLista.setEnabled(true);
                btnSiguienteLista.setBackground(new Color(0, 123, 64));
              }
            });

    registerFilter(txtFindLista, rowSorterLista);
  }

  private void initTblVideos() {
    modeloVideos.addColumn("Nombre");
    modeloVideos.addColumn("id");
    tblVideos.setModel(modeloVideos);
    tblVideos.setRowSorter(rowSorterVideo);
    tblVideos
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblVideos.getSelectedRow() >= 0) {
                btnQuitar.setEnabled(true);
                btnQuitar.setBackground(new Color(255, 69, 64));
              }
            });

    registerFilter(txtFindVideo, rowSorterVideo);
  }

  private void registerFilter(
      JTextField txtField, TableRowSorter<DefaultTableModel> rowSorterVideo) {
    txtField
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                find();
              }

              private void find() {
                String text = txtField.getText();
                if (text.trim().length() == 0) {
                  rowSorterVideo.setRowFilter(null);
                } else {
                  rowSorterVideo.setRowFilter(RowFilter.regexFilter("(?i)" + Pattern.quote(text)));
                }
              }
            });
  }

  public QuitarVideoLista() {
    initComponents();
    initTblUsuarios();
    initTblListas();
    initTblVideos();

    List<String> usuarios = iUsuario.listarUsuarios();
    modeloUsuarios.addColumn("Nickname");
    for (String usuario : usuarios) {
      modeloUsuarios.addRow(new Object[] {usuario});
    }

    /*Desactivar boton siguiete*/
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);

    btnQuitar.setEnabled(false);
    btnQuitar.setBackground(Color.GRAY);

    panelLista.setVisible(false);
    panelVideo.setVisible(false);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel1 = new javax.swing.JPanel();
    lblLista = new javax.swing.JLabel();
    lblVideo = new javax.swing.JLabel();
    lblUsuario = new javax.swing.JLabel();
    spLista = new javax.swing.JSeparator();
    spVideo = new javax.swing.JSeparator();
    spUsuario = new javax.swing.JSeparator();
    panelUsuario = new javax.swing.JPanel();
    btnSiguiente = new javax.swing.JButton();
    jLabel15 = new javax.swing.JLabel();
    jSeparator4 = new javax.swing.JSeparator();
    txtFindNick = new javax.swing.JTextField();
    jLabel1 = new javax.swing.JLabel();
    jSeparator1 = new javax.swing.JSeparator();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblUsuarios = new javax.swing.JTable();
    panelLista = new javax.swing.JPanel();
    btnSiguienteLista = new javax.swing.JButton();
    jLabel16 = new javax.swing.JLabel();
    jSeparator5 = new javax.swing.JSeparator();
    txtFindLista = new javax.swing.JTextField();
    jLabel2 = new javax.swing.JLabel();
    jSeparator2 = new javax.swing.JSeparator();
    jScrollPane4 = new javax.swing.JScrollPane();
    tblListas = new javax.swing.JTable();
    btnVolver = new javax.swing.JButton();
    panelVideo = new javax.swing.JPanel();
    btnQuitar = new javax.swing.JButton();
    jLabel17 = new javax.swing.JLabel();
    jSeparator6 = new javax.swing.JSeparator();
    txtFindVideo = new javax.swing.JTextField();
    jLabel3 = new javax.swing.JLabel();
    jSeparator3 = new javax.swing.JSeparator();
    jScrollPane3 = new javax.swing.JScrollPane();
    tblVideos = new javax.swing.JTable();
    btnVolver1 = new javax.swing.JButton();
    lblTitle = new javax.swing.JLabel();
    jPanel2 = new javax.swing.JPanel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel5 = new javax.swing.JLabel();

    setBackground(new java.awt.Color(246, 246, 246));
    setClosable(true);
    setIconifiable(true);
    setTitle("Quitar Video Lista");
    getContentPane().setLayout(null);

    jPanel1.setBackground(new java.awt.Color(255, 255, 255));
    jPanel1.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    jPanel1.setLayout(null);

    lblLista.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblLista.setForeground(new java.awt.Color(204, 204, 204));
    lblLista.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblLista.setText("Lista");
    lblLista.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel1.add(lblLista);
    lblLista.setBounds(170, 20, 130, 40);

    lblVideo.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblVideo.setForeground(new java.awt.Color(204, 204, 204));
    lblVideo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblVideo.setText("Video");
    lblVideo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel1.add(lblVideo);
    lblVideo.setBounds(310, 20, 130, 40);

    lblUsuario.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
    lblUsuario.setForeground(new java.awt.Color(32, 32, 32));
    lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblUsuario.setText("Usuario");
    lblUsuario.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    jPanel1.add(lblUsuario);
    lblUsuario.setBounds(30, 20, 130, 40);
    jPanel1.add(spLista);
    spLista.setBounds(170, 60, 130, 3);
    jPanel1.add(spVideo);
    spVideo.setBounds(310, 60, 130, 3);

    spUsuario.setBorder(
        javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 89, 245), 2));
    jPanel1.add(spUsuario);
    spUsuario.setBounds(30, 60, 130, 3);

    panelUsuario.setBackground(new java.awt.Color(255, 255, 255));
    panelUsuario.setLayout(null);

    btnSiguiente.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguiente.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguiente.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguiente.setText("Siguiente");
    btnSiguiente.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguiente.setContentAreaFilled(false);
    btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguiente.setOpaque(true);
    btnSiguiente.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteMouseClicked(evt);
          }
        });
    panelUsuario.add(btnSiguiente);
    btnSiguiente.setBounds(310, 230, 110, 40);

    jLabel15.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel15.setForeground(new java.awt.Color(33, 33, 33));
    jLabel15.setText("Seleccionar Usuario");
    panelUsuario.add(jLabel15);
    jLabel15.setBounds(10, 20, 160, 20);
    panelUsuario.add(jSeparator4);
    jSeparator4.setBounds(10, 40, 410, 10);

    txtFindNick.setForeground(new java.awt.Color(60, 63, 65));
    txtFindNick.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar usuario:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelUsuario.add(txtFindNick);
    txtFindNick.setBounds(53, 60, 130, 40);

    jLabel1.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelUsuario.add(jLabel1);
    jLabel1.setBounds(30, 72, 24, 24);
    panelUsuario.add(jSeparator1);
    jSeparator1.setBounds(55, 97, 130, 10);

    tblUsuarios.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nickname"}));
    tblUsuarios.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(tblUsuarios);

    panelUsuario.add(jScrollPane1);
    jScrollPane1.setBounds(40, 110, 360, 105);

    jPanel1.add(panelUsuario);
    panelUsuario.setBounds(20, 80, 430, 280);

    panelLista.setBackground(new java.awt.Color(255, 255, 255));
    panelLista.setLayout(null);

    btnSiguienteLista.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguienteLista.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguienteLista.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguienteLista.setText("Siguiente");
    btnSiguienteLista.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguienteLista.setContentAreaFilled(false);
    btnSiguienteLista.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguienteLista.setOpaque(true);
    btnSiguienteLista.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteListaMouseClicked(evt);
          }
        });
    panelLista.add(btnSiguienteLista);
    btnSiguienteLista.setBounds(310, 230, 110, 40);

    jLabel16.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel16.setForeground(new java.awt.Color(33, 33, 33));
    jLabel16.setText("Seleccionar Lista");
    panelLista.add(jLabel16);
    jLabel16.setBounds(10, 20, 160, 20);
    panelLista.add(jSeparator5);
    jSeparator5.setBounds(10, 40, 410, 10);

    txtFindLista.setForeground(new java.awt.Color(60, 63, 65));
    txtFindLista.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar lista:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelLista.add(txtFindLista);
    txtFindLista.setBounds(53, 60, 130, 40);

    jLabel2.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelLista.add(jLabel2);
    jLabel2.setBounds(30, 72, 24, 24);
    panelLista.add(jSeparator2);
    jSeparator2.setBounds(55, 97, 130, 10);

    tblListas.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {
              {null, null, null},
              {null, null, null},
              {null, null, null},
              {null, null, null}
            },
            new String[] {"Nombre", "Categoria", "Privacidad"}));
    tblListas.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblListas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblListas.getTableHeader().setResizingAllowed(false);
    tblListas.getTableHeader().setReorderingAllowed(false);
    jScrollPane4.setViewportView(tblListas);

    panelLista.add(jScrollPane4);
    jScrollPane4.setBounds(40, 110, 360, 105);

    btnVolver.setBackground(new java.awt.Color(51, 89, 245));
    btnVolver.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVolver.setForeground(new java.awt.Color(255, 255, 255));
    btnVolver.setText("Volver");
    btnVolver.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVolver.setContentAreaFilled(false);
    btnVolver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVolver.setOpaque(true);
    btnVolver.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVolverMouseClicked(evt);
          }
        });
    panelLista.add(btnVolver);
    btnVolver.setBounds(10, 230, 110, 40);

    jPanel1.add(panelLista);
    panelLista.setBounds(20, 80, 430, 280);

    panelVideo.setBackground(new java.awt.Color(255, 255, 255));
    panelVideo.setLayout(null);

    btnQuitar.setBackground(new java.awt.Color(255, 69, 64));
    btnQuitar.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnQuitar.setForeground(new java.awt.Color(255, 255, 255));
    btnQuitar.setText("Quitar");
    btnQuitar.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnQuitar.setContentAreaFilled(false);
    btnQuitar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnQuitar.setOpaque(true);
    btnQuitar.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnQuitarMouseClicked(evt);
          }
        });
    panelVideo.add(btnQuitar);
    btnQuitar.setBounds(300, 120, 110, 40);

    jLabel17.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel17.setForeground(new java.awt.Color(33, 33, 33));
    jLabel17.setText("Seleccionar Video");
    panelVideo.add(jLabel17);
    jLabel17.setBounds(10, 20, 160, 20);
    panelVideo.add(jSeparator6);
    jSeparator6.setBounds(10, 40, 410, 10);

    txtFindVideo.setForeground(new java.awt.Color(60, 63, 65));
    txtFindVideo.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar video:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelVideo.add(txtFindVideo);
    txtFindVideo.setBounds(53, 60, 130, 40);

    jLabel3.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelVideo.add(jLabel3);
    jLabel3.setBounds(30, 72, 24, 24);
    panelVideo.add(jSeparator3);
    jSeparator3.setBounds(55, 97, 130, 10);

    tblVideos.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nombre"}));
    tblVideos.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblVideos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane3.setViewportView(tblVideos);

    panelVideo.add(jScrollPane3);
    jScrollPane3.setBounds(40, 110, 250, 105);

    btnVolver1.setBackground(new java.awt.Color(51, 89, 245));
    btnVolver1.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVolver1.setForeground(new java.awt.Color(255, 255, 255));
    btnVolver1.setText("Volver");
    btnVolver1.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVolver1.setContentAreaFilled(false);
    btnVolver1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVolver1.setOpaque(true);
    btnVolver1.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVolver1MouseClicked(evt);
          }
        });
    panelVideo.add(btnVolver1);
    btnVolver1.setBounds(10, 230, 110, 40);

    jPanel1.add(panelVideo);
    panelVideo.setBounds(20, 80, 430, 280);

    getContentPane().add(jPanel1);
    jPanel1.setBounds(38, 75, 465, 369);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Quitar Video");
    getContentPane().add(lblTitle);
    lblTitle.setBounds(195, 28, 168, 33);

    jPanel2.setBackground(new java.awt.Color(246, 246, 246));
    jPanel2.setLayout(null);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel2.add(jLabel9);
    jLabel9.setBounds(473, 448, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel2.add(jLabel8);
    jLabel8.setBounds(500, 460, 49, 17);

    jLabel5.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel2.add(jLabel5);
    jLabel5.setBounds(250, 260, 310, 250);

    getContentPane().add(jPanel2);
    jPanel2.setBounds(0, 0, 560, 500);

    setBounds(240, 134, 559, 518);
  } // </editor-fold>//GEN-END:initComponents

  private void btnSiguienteListaMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteListaMouseClicked
    if (btnSiguienteLista.isEnabled()) {
      panelVideo.setVisible(true);
      panelLista.setVisible(false);
      spVideo.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
      spLista.setBorder(BorderFactory.createEmptyBorder());
      lblLista.setForeground(new Color(204, 204, 204));
      lblVideo.setForeground(new Color(32, 32, 32));
    }
  } // GEN-LAST:event_btnSiguienteListaMouseClicked

  private void btnQuitarMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnQuitarMouseClicked
    if (btnQuitar.isEnabled()) {
      JFrame frame = (JFrame) getDesktopPane().getTopLevelAncestor();
      try {
        int idLista = (int) modeloListas.getValueAt(tblListas.getSelectedRow(), 3);
        int idVideo = (int) modeloVideos.getValueAt(tblVideos.getSelectedRow(), 1);
        iLista.quitarVideoLista(idVideo, idLista);
        modeloVideos.removeRow(tblVideos.getSelectedRow());
        btnQuitar.setEnabled(false);
        btnQuitar.setBackground(Color.GRAY);
        new PopUp(frame, "Quitar Video", "Se quito el video", 5, PopUp.LEVEL.SUCCESS);
      } catch (YonaException e) {
        new PopUp(frame, "Quitar Video", e.getMessage(), 5, PopUp.LEVEL.ERROR);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  } // GEN-LAST:event_btnQuitarMouseClicked

  private void btnVolverMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVolverMouseClicked
    panelUsuario.setVisible(true);
    panelLista.setVisible(false);
    spUsuario.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
    spLista.setBorder(BorderFactory.createEmptyBorder());
    lblLista.setForeground(new Color(204, 204, 204));
    lblUsuario.setForeground(new Color(32, 32, 32));

    /*Vuelvo a usuarios deselecciono todos y bloqueo todos los botones*/
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    btnSiguienteLista.setEnabled(false);
    btnSiguienteLista.setBackground(Color.GRAY);

    tblListas.getSelectionModel().removeIndexInterval(0, tblListas.getRowCount());
    tblVideos.getSelectionModel().removeIndexInterval(0, tblVideos.getRowCount());
    tblUsuarios.getSelectionModel().removeIndexInterval(0, tblUsuarios.getRowCount());
  } // GEN-LAST:event_btnVolverMouseClicked

  private void btnVolver1MouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVolver1MouseClicked
    panelVideo.setVisible(false);
    panelLista.setVisible(true);
    spLista.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
    spVideo.setBorder(BorderFactory.createEmptyBorder());
    lblVideo.setForeground(new Color(204, 204, 204));
    lblLista.setForeground(new Color(32, 32, 32));

    /*Vuelvo a listas deselecciono el video y la lista y bloqueo todos los botones*/
    btnSiguienteLista.setEnabled(false);
    btnSiguienteLista.setBackground(Color.GRAY);
    btnQuitar.setEnabled(false);
    btnQuitar.setBackground(Color.GRAY);

    tblVideos.getSelectionModel().removeIndexInterval(0, tblVideos.getRowCount());
    tblListas.getSelectionModel().removeIndexInterval(0, tblListas.getRowCount());
  } // GEN-LAST:event_btnVolver1MouseClicked

  private void btnSiguienteMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteMouseClicked
    if (btnSiguiente.isEnabled()) {
      panelUsuario.setVisible(false);
      panelLista.setVisible(true);
      spLista.setBorder(BorderFactory.createLineBorder(new Color(51, 89, 245), 2));
      spUsuario.setBorder(BorderFactory.createEmptyBorder());
      lblUsuario.setForeground(new Color(204, 204, 204));
      lblLista.setForeground(new Color(32, 32, 32));
      btnSiguienteLista.setEnabled(false);
      btnSiguienteLista.setBackground(Color.GRAY);
    }
  } // GEN-LAST:event_btnSiguienteMouseClicked

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnQuitar;
  private javax.swing.JButton btnSiguiente;
  private javax.swing.JButton btnSiguienteLista;
  private javax.swing.JButton btnVolver;
  private javax.swing.JButton btnVolver1;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel16;
  private javax.swing.JLabel jLabel17;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane3;
  private javax.swing.JScrollPane jScrollPane4;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JSeparator jSeparator3;
  private javax.swing.JSeparator jSeparator4;
  private javax.swing.JSeparator jSeparator5;
  private javax.swing.JSeparator jSeparator6;
  private javax.swing.JLabel lblLista;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JLabel lblUsuario;
  private javax.swing.JLabel lblVideo;
  private javax.swing.JPanel panelLista;
  private javax.swing.JPanel panelUsuario;
  private javax.swing.JPanel panelVideo;
  private javax.swing.JSeparator spLista;
  private javax.swing.JSeparator spUsuario;
  private javax.swing.JSeparator spVideo;
  private javax.swing.JTable tblListas;
  private javax.swing.JTable tblUsuarios;
  private javax.swing.JTable tblVideos;
  private javax.swing.JTextField txtFindLista;
  private javax.swing.JTextField txtFindNick;
  private javax.swing.JTextField txtFindVideo;
  // End of variables declaration//GEN-END:variables

}
