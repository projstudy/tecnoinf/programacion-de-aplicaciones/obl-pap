/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.lista;

import java.awt.Color;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ILista;
import org.uytube.controladores.IUsuario;
import org.uytube.datatypes.DtLista;
import org.uytube.exception.YonaException;
import org.uytube.ui.video.ConsultaVideo;

/** @author toto */
public class ConsultaLista extends javax.swing.JInternalFrame {

  private final IUsuario iUsuario = Fabrica.getControllerOf(Fabrica.Controller.USUARIO);
  private final ILista iLista = Fabrica.getControllerOf(Fabrica.Controller.LISTA);

  private final DefaultTableModel modeloVideos =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };
  private final DefaultTableModel modeloListas =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };
  private final DefaultTableModel modeloUsuarios =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final TableRowSorter<DefaultTableModel> rowSorterUsuario =
      new TableRowSorter<>(modeloUsuarios);
  private final TableRowSorter<DefaultTableModel> rowSorterLista =
      new TableRowSorter<>(modeloListas);
  private final TableRowSorter<DefaultTableModel> rowSorterVideo =
      new TableRowSorter<>(modeloVideos);

  private void cargarDatosLista(int idLista) {
    try {
      DtLista data = iLista.consultaLista(idLista);
      HashMap<Integer, String> videos = iLista.listarVideos(idLista);
      String privada = data.isPrivado() ? "Privada" : "Publica";
      lblNombre.setText(data.getNombre());
      lblTipo.setText(data.getTipo());
      lblPrivacidad.setText(privada);
      videos.forEach(
          (k, v) -> {
            Object[] fila = new Object[2]; // creo el array de 1
            fila[0] = v; // en la pos 1 cargo el nombre
            fila[1] = k; // en la pos 0 cargo el id
            modeloVideos.addRow(fila);
          });
    } catch (YonaException e) {
      System.out.println("Upps");
    }
  }

  private void initTblUsuarios() {
    tblUsuarios.setModel(modeloUsuarios);
    tblUsuarios.setRowSorter(rowSorterUsuario);
    tblUsuarios
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblUsuarios.getSelectedRow() >= 0) {
                String nickname =
                    tblUsuarios.getValueAt(tblUsuarios.getSelectedRow(), 0).toString();
                ArrayList<DtLista> data = iLista.listasDeUsuario(nickname);
                modeloListas.setRowCount(0);
                for (DtLista lista : data) {
                  String privada = lista.isPrivado() ? "Privada" : "Publica";
                  modeloListas.addRow(
                      new Object[] {
                        lista.getNombre(), lista.getCategoria(), privada, lista.getId()
                      });
                }
              }
            });

    registerFilter(txtFindNick, rowSorterUsuario);
  }

  private void initTblVideos() {
    tblVideos.setModel(modeloVideos);
    tblVideos.setRowSorter(rowSorterVideo);
    modeloVideos.addColumn("Nombre");
    modeloVideos.addColumn("id");
    TableColumnModel tcm = tblVideos.getColumnModel();
    tcm.removeColumn(tcm.getColumn(1));
    tblVideos
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              if (!event.getValueIsAdjusting() && tblVideos.getSelectedRow() >= 0) {
                btnVerVideo.setEnabled(true);
                btnVerVideo.setBackground(new Color(0, 123, 64));
              }
            });

    registerFilter(txtFindLista1, rowSorterVideo);
  }

  private void initTblLista() {
    tblListas.setModel(modeloListas);
    tblListas.setRowSorter(rowSorterLista);
    modeloListas.addColumn("Nombre");
    modeloListas.addColumn("Categoria");
    modeloListas.addColumn("Privacidad");
    modeloListas.addColumn("id");
    TableColumnModel tcm = tblListas.getColumnModel();
    tcm.removeColumn(tcm.getColumn(3));
    tblListas
        .getSelectionModel()
        .addListSelectionListener(
            event -> {
              /*cargar datos en tabla de listas*/
              if (!event.getValueIsAdjusting() && tblUsuarios.getSelectedRow() >= 0) {
                btnSiguiente.setEnabled(true);
                btnSiguiente.setBackground(new Color(0, 123, 64));
              }
            });

    registerFilter(txtFindLista, rowSorterLista);
  }

  private void registerFilter(
      JTextField txtField, TableRowSorter<DefaultTableModel> rowSorterVideo) {
    txtField
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                find();
              }

              private void find() {
                String text = txtField.getText();
                if (text.trim().length() == 0) {
                  rowSorterVideo.setRowFilter(null);
                } else {
                  rowSorterVideo.setRowFilter(RowFilter.regexFilter("(?i)" + Pattern.quote(text)));
                }
              }
            });
  }

  /** Creates new form ConsultaLista */
  public ConsultaLista() {
    initComponents();
    initTblLista();
    initTblVideos();
    initTblUsuarios();
    btnVerVideo.setEnabled(false);
    btnVerVideo.setBackground(Color.GRAY);
    List<String> usuarios = iUsuario.listarUsuarios();
    modeloUsuarios.addColumn("Nickname");
    for (String usuario : usuarios) {
      modeloUsuarios.addRow(new Object[] {usuario});
    }
    btnSiguiente.setEnabled(false);
    btnSiguiente.setBackground(Color.GRAY);
    panel2.setVisible(false);
  }

  public ConsultaLista(int idLista) {
    this();
    panel1.setVisible(false);
    panel2.setVisible(true);
    cargarDatosLista(idLista);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    panel1 = new javax.swing.JPanel();
    jLabel15 = new javax.swing.JLabel();
    jSeparator13 = new javax.swing.JSeparator();
    txtFindNick = new javax.swing.JTextField();
    jLabel12 = new javax.swing.JLabel();
    jSeparator14 = new javax.swing.JSeparator();
    jScrollPane5 = new javax.swing.JScrollPane();
    tblListas = new javax.swing.JTable();
    jLabel16 = new javax.swing.JLabel();
    jSeparator15 = new javax.swing.JSeparator();
    txtFindLista = new javax.swing.JTextField();
    jLabel13 = new javax.swing.JLabel();
    jSeparator16 = new javax.swing.JSeparator();
    jScrollPane6 = new javax.swing.JScrollPane();
    tblUsuarios = new javax.swing.JTable();
    btnSiguiente = new javax.swing.JButton();
    panel2 = new javax.swing.JPanel();
    jSeparator17 = new javax.swing.JSeparator();
    jLabel18 = new javax.swing.JLabel();
    jSeparator18 = new javax.swing.JSeparator();
    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();
    jSeparator1 = new javax.swing.JSeparator();
    jSeparator2 = new javax.swing.JSeparator();
    jSeparator3 = new javax.swing.JSeparator();
    lblTitle1 = new javax.swing.JLabel();
    jScrollPane1 = new javax.swing.JScrollPane();
    tblVideos = new javax.swing.JTable();
    btnVerVideo = new javax.swing.JButton();
    btnVolver2 = new javax.swing.JButton();
    jLabel14 = new javax.swing.JLabel();
    jSeparator19 = new javax.swing.JSeparator();
    txtFindLista1 = new javax.swing.JTextField();
    lblTipo = new javax.swing.JLabel();
    lblNombre = new javax.swing.JLabel();
    lblPrivacidad = new javax.swing.JLabel();
    jPanel1 = new javax.swing.JPanel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel7 = new javax.swing.JLabel();
    lblTitle = new javax.swing.JLabel();

    setClosable(true);
    setIconifiable(true);
    setTitle("Consulta Lista");
    getContentPane().setLayout(null);

    panel1.setBackground(new java.awt.Color(255, 255, 255));
    panel1.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panel1.setLayout(null);

    jLabel15.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel15.setForeground(new java.awt.Color(33, 33, 33));
    jLabel15.setText("Seleccionar Lista");
    panel1.add(jLabel15);
    jLabel15.setBounds(20, 220, 160, 17);
    panel1.add(jSeparator13);
    jSeparator13.setBounds(20, 240, 430, 10);

    txtFindNick.setForeground(new java.awt.Color(60, 63, 65));
    txtFindNick.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar usuario:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panel1.add(txtFindNick);
    txtFindNick.setBounds(53, 65, 130, 40);

    jLabel12.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panel1.add(jLabel12);
    jLabel12.setBounds(30, 272, 24, 24);
    panel1.add(jSeparator14);
    jSeparator14.setBounds(55, 295, 130, 10);

    tblListas.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nickname"}));
    tblListas.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblListas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane5.setViewportView(tblListas);

    panel1.add(jScrollPane5);
    jScrollPane5.setBounds(40, 305, 390, 100);

    jLabel16.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel16.setForeground(new java.awt.Color(33, 33, 33));
    jLabel16.setText("Seleccionar Usuario");
    panel1.add(jLabel16);
    jLabel16.setBounds(20, 30, 160, 17);
    panel1.add(jSeparator15);
    jSeparator15.setBounds(20, 50, 430, 10);

    txtFindLista.setForeground(new java.awt.Color(60, 63, 65));
    txtFindLista.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar lista:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panel1.add(txtFindLista);
    txtFindLista.setBounds(53, 258, 130, 40);

    jLabel13.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panel1.add(jLabel13);
    jLabel13.setBounds(30, 77, 24, 24);
    panel1.add(jSeparator16);
    jSeparator16.setBounds(55, 102, 130, 10);

    tblUsuarios.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nickname"}));
    tblUsuarios.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblUsuarios.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane6.setViewportView(tblUsuarios);

    panel1.add(jScrollPane6);
    jScrollPane6.setBounds(40, 110, 390, 100);

    btnSiguiente.setBackground(new java.awt.Color(0, 123, 64));
    btnSiguiente.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnSiguiente.setForeground(new java.awt.Color(255, 255, 255));
    btnSiguiente.setText("Siguiente");
    btnSiguiente.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnSiguiente.setContentAreaFilled(false);
    btnSiguiente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnSiguiente.setOpaque(true);
    btnSiguiente.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnSiguienteMouseClicked(evt);
          }
        });
    panel1.add(btnSiguiente);
    btnSiguiente.setBounds(330, 425, 110, 40);

    getContentPane().add(panel1);
    panel1.setBounds(38, 80, 470, 480);

    panel2.setBackground(new java.awt.Color(255, 255, 255));
    panel2.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panel2.setLayout(null);
    panel2.add(jSeparator17);
    jSeparator17.setBounds(20, 50, 430, 10);

    jLabel18.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel18.setForeground(new java.awt.Color(33, 33, 33));
    jLabel18.setText("Datos de Lista");
    panel2.add(jLabel18);
    jLabel18.setBounds(20, 30, 120, 17);
    panel2.add(jSeparator18);
    jSeparator18.setBounds(40, 230, 390, 10);

    jLabel1.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel1.setText("Tipo");
    panel2.add(jLabel1);
    jLabel1.setBounds(60, 125, 50, 15);

    jLabel2.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel2.setText("Privacidad");
    panel2.add(jLabel2);
    jLabel2.setBounds(260, 75, 70, 15);

    jLabel4.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jLabel4.setText("Nombre");
    panel2.add(jLabel4);
    jLabel4.setBounds(60, 75, 50, 15);
    panel2.add(jSeparator1);
    jSeparator1.setBounds(260, 110, 160, 10);
    panel2.add(jSeparator2);
    jSeparator2.setBounds(60, 110, 170, 10);
    panel2.add(jSeparator3);
    jSeparator3.setBounds(60, 160, 170, 10);

    lblTitle1.setFont(new java.awt.Font("Monospaced", 0, 20)); // NOI18N
    lblTitle1.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle1.setText("Videos");
    panel2.add(lblTitle1);
    lblTitle1.setBounds(190, 200, 80, 20);

    tblVideos.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nombre"}));
    tblVideos.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblVideos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane1.setViewportView(tblVideos);

    panel2.add(jScrollPane1);
    jScrollPane1.setBounds(50, 300, 260, 94);

    btnVerVideo.setBackground(new java.awt.Color(0, 123, 64));
    btnVerVideo.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVerVideo.setForeground(new java.awt.Color(255, 255, 255));
    btnVerVideo.setText("Ver");
    btnVerVideo.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVerVideo.setContentAreaFilled(false);
    btnVerVideo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVerVideo.setOpaque(true);
    btnVerVideo.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVerVideoMouseClicked(evt);
          }
        });
    panel2.add(btnVerVideo);
    btnVerVideo.setBounds(320, 300, 90, 30);

    btnVolver2.setBackground(new java.awt.Color(51, 89, 245));
    btnVolver2.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    btnVolver2.setForeground(new java.awt.Color(255, 255, 255));
    btnVolver2.setText("Volver");
    btnVolver2.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    btnVolver2.setContentAreaFilled(false);
    btnVolver2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
    btnVolver2.setOpaque(true);
    btnVolver2.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            btnVolver2MouseClicked(evt);
          }
        });
    panel2.add(btnVolver2);
    btnVolver2.setBounds(30, 423, 110, 40);

    jLabel14.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panel2.add(jLabel14);
    jLabel14.setBounds(45, 265, 24, 24);
    panel2.add(jSeparator19);
    jSeparator19.setBounds(70, 290, 130, 10);

    txtFindLista1.setForeground(new java.awt.Color(60, 63, 65));
    txtFindLista1.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar video:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panel2.add(txtFindLista1);
    txtFindLista1.setBounds(70, 253, 130, 40);

    lblTipo.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    lblTipo.setForeground(new java.awt.Color(60, 63, 65));
    panel2.add(lblTipo);
    lblTipo.setBounds(60, 140, 170, 20);

    lblNombre.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    lblNombre.setForeground(new java.awt.Color(60, 63, 65));
    panel2.add(lblNombre);
    lblNombre.setBounds(60, 90, 170, 20);

    lblPrivacidad.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    lblPrivacidad.setForeground(new java.awt.Color(60, 63, 65));
    panel2.add(lblPrivacidad);
    lblPrivacidad.setBounds(260, 90, 160, 20);

    getContentPane().add(panel2);
    panel2.setBounds(38, 80, 470, 480);

    jPanel1.setBackground(new java.awt.Color(246, 246, 246));
    jPanel1.setLayout(null);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel1.add(jLabel9);
    jLabel9.setBounds(460, 565, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel1.add(jLabel8);
    jLabel8.setBounds(490, 578, 49, 15);

    jLabel7.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel1.add(jLabel7);
    jLabel7.setBounds(240, 380, 310, 230);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Consulta Lista");
    jPanel1.add(lblTitle);
    lblTitle.setBounds(170, 30, 200, 29);

    getContentPane().add(jPanel1);
    jPanel1.setBounds(0, 0, 550, 610);

    setBounds(236, 73, 551, 632);
  } // </editor-fold>//GEN-END:initComponents

  private void btnSiguienteMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnSiguienteMouseClicked
    if (btnSiguiente.isEnabled()) {
      int idLista = (int) modeloListas.getValueAt(tblListas.getSelectedRow(), 3);
      cargarDatosLista(idLista);
      panel1.setVisible(false);
      panel2.setVisible(true);
    }
  } // GEN-LAST:event_btnSiguienteMouseClicked

  private void btnVerVideoMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVerVideoMouseClicked
    int idVideo = (int) modeloVideos.getValueAt(tblVideos.getSelectedRow(), 1);
    ConsultaVideo consultaVideo = new ConsultaVideo(idVideo);
    this.getDesktopPane().add(consultaVideo);
    consultaVideo.show();
    try {
      this.setIcon(true);
    } catch (PropertyVetoException ex) {
      Logger.getLogger(ConsultaLista.class.getName()).log(Level.SEVERE, null, ex);
    }
  } // GEN-LAST:event_btnVerVideoMouseClicked

  private void btnVolver2MouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_btnVolver2MouseClicked
    modeloVideos.setRowCount(0);
    btnVerVideo.setEnabled(false);
    btnVerVideo.setBackground(Color.GRAY);
    panel1.setVisible(true);
    panel2.setVisible(false);
  } // GEN-LAST:event_btnVolver2MouseClicked

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnSiguiente;
  private javax.swing.JButton btnVerVideo;
  private javax.swing.JButton btnVolver2;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel12;
  private javax.swing.JLabel jLabel13;
  private javax.swing.JLabel jLabel14;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel16;
  private javax.swing.JLabel jLabel18;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane5;
  private javax.swing.JScrollPane jScrollPane6;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JSeparator jSeparator13;
  private javax.swing.JSeparator jSeparator14;
  private javax.swing.JSeparator jSeparator15;
  private javax.swing.JSeparator jSeparator16;
  private javax.swing.JSeparator jSeparator17;
  private javax.swing.JSeparator jSeparator18;
  private javax.swing.JSeparator jSeparator19;
  private javax.swing.JSeparator jSeparator2;
  private javax.swing.JSeparator jSeparator3;
  private javax.swing.JLabel lblNombre;
  private javax.swing.JLabel lblPrivacidad;
  private javax.swing.JLabel lblTipo;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JLabel lblTitle1;
  private javax.swing.JPanel panel1;
  private javax.swing.JPanel panel2;
  private javax.swing.JTable tblListas;
  private javax.swing.JTable tblUsuarios;
  private javax.swing.JTable tblVideos;
  private javax.swing.JTextField txtFindLista;
  private javax.swing.JTextField txtFindLista1;
  private javax.swing.JTextField txtFindNick;
  // End of variables declaration//GEN-END:variables
}
