/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui.categoria;

import java.util.List;
import java.util.regex.Pattern;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import org.uytube.controladores.Fabrica;
import org.uytube.controladores.ICategoria;

/** @author toto */
public class ListarCategorias extends javax.swing.JInternalFrame {

  private final ICategoria iCategoria = Fabrica.getControllerOf(Fabrica.Controller.CATEOGRIA);

  private final DefaultTableModel modeloCategorias =
      new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
          return false;
        }
      };

  private final TableRowSorter<DefaultTableModel> rowSorterCategorias =
      new TableRowSorter<>(modeloCategorias);
  /** Creates new form ListarCategorias */
  private void registerFilter(
      JTextField txtField, TableRowSorter<DefaultTableModel> rowSorterVideo) {
    txtField
        .getDocument()
        .addDocumentListener(
            new DocumentListener() {
              @Override
              public void insertUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void removeUpdate(DocumentEvent e) {
                find();
              }

              @Override
              public void changedUpdate(DocumentEvent e) {
                find();
              }

              private void find() {
                String text = txtField.getText();
                if (text.trim().length() == 0) {
                  rowSorterVideo.setRowFilter(null);
                } else {
                  rowSorterVideo.setRowFilter(RowFilter.regexFilter("(?i)" + Pattern.quote(text)));
                }
              }
            });
  }

  private void initTblCategorias() {
    modeloCategorias.addColumn("Nombre");
    tblCategorias.setModel(modeloCategorias);
    tblCategorias.setRowSorter(rowSorterCategorias);
    registerFilter(txtFindCate, rowSorterCategorias);
  }

  public ListarCategorias() {
    initComponents();
    initTblCategorias();
    List<String> categorias = iCategoria.listarCategorias();
    for (String categoria : categorias) {
      modeloCategorias.addRow(new Object[] {categoria});
    }
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    panelUsuario = new javax.swing.JPanel();
    jLabel15 = new javax.swing.JLabel();
    jSeparator13 = new javax.swing.JSeparator();
    txtFindCate = new javax.swing.JTextField();
    jLabel12 = new javax.swing.JLabel();
    jSeparator14 = new javax.swing.JSeparator();
    jScrollPane5 = new javax.swing.JScrollPane();
    tblCategorias = new javax.swing.JTable();
    jPanel3 = new javax.swing.JPanel();
    jLabel9 = new javax.swing.JLabel();
    jLabel8 = new javax.swing.JLabel();
    jLabel7 = new javax.swing.JLabel();
    lblTitle = new javax.swing.JLabel();

    setClosable(true);
    setIconifiable(true);
    setTitle("Listar Categorias");
    getContentPane().setLayout(null);

    panelUsuario.setBackground(new java.awt.Color(255, 255, 255));
    panelUsuario.setBorder(
        javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    panelUsuario.setLayout(null);

    jLabel15.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
    jLabel15.setForeground(new java.awt.Color(33, 33, 33));
    jLabel15.setText("Lista de Categorias");
    panelUsuario.add(jLabel15);
    jLabel15.setBounds(20, 30, 160, 17);
    panelUsuario.add(jSeparator13);
    jSeparator13.setBounds(20, 50, 390, 10);

    txtFindCate.setForeground(new java.awt.Color(60, 63, 65));
    txtFindCate.setBorder(
        javax.swing.BorderFactory.createTitledBorder(
            javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1),
            "Buscar categoria:",
            javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
            javax.swing.border.TitledBorder.DEFAULT_POSITION,
            new java.awt.Font("Monospaced", 0, 12))); // NOI18N
    panelUsuario.add(txtFindCate);
    txtFindCate.setBounds(53, 65, 135, 40);

    jLabel12.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
    panelUsuario.add(jLabel12);
    jLabel12.setBounds(30, 77, 24, 24);
    panelUsuario.add(jSeparator14);
    jSeparator14.setBounds(55, 102, 135, 10);

    tblCategorias.setModel(
        new javax.swing.table.DefaultTableModel(
            new Object[][] {{null}, {null}, {null}, {null}}, new String[] {"Nickname"}));
    tblCategorias.setSelectionBackground(new java.awt.Color(66, 58, 111));
    tblCategorias.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane5.setViewportView(tblCategorias);

    panelUsuario.add(jScrollPane5);
    jScrollPane5.setBounds(40, 110, 360, 190);

    getContentPane().add(panelUsuario);
    panelUsuario.setBounds(30, 90, 430, 340);

    jPanel3.setBackground(new java.awt.Color(246, 246, 246));
    jPanel3.setLayout(null);

    jLabel9.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/logo.png"))); // NOI18N
    jPanel3.add(jLabel9);
    jLabel9.setBounds(410, 433, 30, 30);

    jLabel8.setFont(new java.awt.Font("Monospaced", 1, 12)); // NOI18N
    jLabel8.setForeground(new java.awt.Color(255, 255, 255));
    jLabel8.setText("UyTube");
    jPanel3.add(jLabel8);
    jLabel8.setBounds(440, 446, 50, 15);

    jLabel7.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/formBackground.jpg"))); // NOI18N
    jPanel3.add(jLabel7);
    jLabel7.setBounds(190, 250, 310, 230);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 24)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 12, 87));
    lblTitle.setText("Listar Categorias");
    jPanel3.add(lblTitle);
    lblTitle.setBounds(120, 30, 240, 29);

    getContentPane().add(jPanel3);
    jPanel3.setBounds(0, 0, 510, 480);

    setBounds(262, 138, 500, 502);
  } // </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JLabel jLabel12;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JScrollPane jScrollPane5;
  private javax.swing.JSeparator jSeparator13;
  private javax.swing.JSeparator jSeparator14;
  private javax.swing.JLabel lblTitle;
  private javax.swing.JPanel panelUsuario;
  private javax.swing.JTable tblCategorias;
  private javax.swing.JTextField txtFindCate;
  // End of variables declaration//GEN-END:variables
}
