/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.uytube.ui;

import java.awt.Color;
import javax.swing.JFrame;

/** @author forbi */
public class PopUp extends javax.swing.JDialog {

  public enum LEVEL {
    ERROR,
    SUCCESS,
    INFO,
    WARNING
  }

  /**
   * Creates new form NewJDialog
   *
   * @param frame mainframe
   * @param title Titulo del YpopUp
   * @param msg Mensage del popup
   * @param time Duracion del popup (segundos)
   * @param level nivel del mesg {ERROR, SUCCESS, INFO, WARNING}
   */
  public PopUp(JFrame frame, String title, String msg, int time, LEVEL level) {
    super(new JFrame(), false);
    initComponents();
    this.setAlwaysOnTop(true);

    switch (level) {
      case ERROR:
        {
          lblIcon.setIcon(
              new javax.swing.ImageIcon(getClass().getResource("/images/error.png"))); // NOI18N
        }
        break;
      case WARNING:
        {
          lblIcon.setIcon(
              new javax.swing.ImageIcon(getClass().getResource("/images/warning.png"))); // NOI18N
        }
        break;
      case SUCCESS:
        {
          lblIcon.setIcon(
              new javax.swing.ImageIcon(getClass().getResource("/images/success.png"))); // NOI18N
        }
        break;
      case INFO:
        {
          lblIcon.setIcon(
              new javax.swing.ImageIcon(getClass().getResource("/images/info.png"))); // NOI18N
        }
        break;
    }
    lblMsg.setText("<HTML>" + msg);
    lblTitle.setText(title);
    // Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize(); // size of the screen
    // Insets toolHeight =
    //    Toolkit.getDefaultToolkit()
    //        .getScreenInsets(this.getGraphicsConfiguration()); // height of the task bar
    // this.setLocation(
    //    scrSize.width - this.getWidth(), scrSize.height - toolHeight.bottom - this.getHeight());
    this.setLocation(
        frame.getX() + frame.getWidth() - (this.getWidth() + 20),
        frame.getY() + frame.getHeight() - (this.getHeight() + 10));
    setVisible(true);
    new Thread(
            () -> {
              try {
                Thread.sleep(time * 1000); // time after which pop up will be disappeared.
                dispose();
              } catch (InterruptedException e) {
                System.out.println("uupss");
              }
            })
        .start();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT
   * modify this code. The content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel1 = new javax.swing.JPanel();
    lblIcon = new javax.swing.JLabel();
    lblTitle = new javax.swing.JLabel();
    lblExit = new javax.swing.JLabel();
    lblMsg = new javax.swing.JLabel();

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    setBackground(new java.awt.Color(0, 204, 0));
    setUndecorated(true);

    jPanel1.setBackground(new java.awt.Color(245, 245, 245));
    jPanel1.setForeground(new java.awt.Color(0, 204, 0));
    jPanel1.setLayout(null);

    lblIcon.setIcon(
        new javax.swing.ImageIcon(getClass().getResource("/images/success.png"))); // NOI18N
    lblIcon.setText("jLabel1");
    jPanel1.add(lblIcon);
    lblIcon.setBounds(0, 6, 32, 32);

    lblTitle.setFont(new java.awt.Font("Monospaced", 0, 20)); // NOI18N
    lblTitle.setForeground(new java.awt.Color(53, 47, 91));
    lblTitle.setText("DEFECTO");
    lblTitle.setToolTipText("");
    jPanel1.add(lblTitle);
    lblTitle.setBounds(38, 8, 210, 30);

    lblExit.setBackground(new java.awt.Color(245, 245, 245));
    lblExit.setForeground(new java.awt.Color(0, 0, 0));
    lblExit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    lblExit.setText("X");
    lblExit.setOpaque(true);
    lblExit.addMouseListener(
        new java.awt.event.MouseAdapter() {
          public void mouseClicked(java.awt.event.MouseEvent evt) {
            lblExitMouseClicked(evt);
          }

          public void mouseEntered(java.awt.event.MouseEvent evt) {
            lblExitMouseEntered(evt);
          }

          public void mouseExited(java.awt.event.MouseEvent evt) {
            lblExitMouseExited(evt);
          }
        });
    jPanel1.add(lblExit);
    lblExit.setBounds(264, 0, 30, 24);

    lblMsg.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
    jPanel1.add(lblMsg);
    lblMsg.setBounds(10, 40, 276, 40);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout
            .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(
                jPanel1,
                javax.swing.GroupLayout.PREFERRED_SIZE,
                294,
                javax.swing.GroupLayout.PREFERRED_SIZE));
    layout.setVerticalGroup(
        layout
            .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE));

    pack();
  } // </editor-fold>//GEN-END:initComponents

  private void lblExitMouseClicked(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_lblExitMouseClicked
    this.dispose();
  } // GEN-LAST:event_lblExitMouseClicked

  private void lblExitMouseEntered(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_lblExitMouseEntered
    lblExit.setBackground(new Color(255, 51, 51));
  } // GEN-LAST:event_lblExitMouseEntered

  private void lblExitMouseExited(
      java.awt.event.MouseEvent evt) { // GEN-FIRST:event_lblExitMouseExited
    lblExit.setBackground(new Color(246, 245, 245));
  } // GEN-LAST:event_lblExitMouseExited

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPanel jPanel1;
  private javax.swing.JLabel lblExit;
  private javax.swing.JLabel lblIcon;
  private javax.swing.JLabel lblMsg;
  private javax.swing.JLabel lblTitle;
  // End of variables declaration//GEN-END:variables
}
